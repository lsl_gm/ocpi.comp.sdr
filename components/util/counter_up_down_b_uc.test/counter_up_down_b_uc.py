#!/usr/bin/env python3

# Python implementation of counter_up_down_b_uc block for verification
#
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
# more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

"""Python implementation of counter_up_down_b_uc for verification."""

import opencpi.ocpi_testing as ocpi_testing


class counter_up_down_b_uc(ocpi_testing.Implementation):
    """Python implementation of counter_up_down_b_uc for verification."""

    def __init__(self, enable, direction, counter_value, size):
        """Initialise the class.

        Args:
            enable (bool): Enables / disables the input.
            direction (bool): Counter direction, where ``true`` is up and ``false`` is down.
            counter_value (uchar): Allows the current counter value to be read by the ACI and allows for the ACI to set the counter value.
            size (uchar): When counting up this is the number of times the counter will count before wrapping back to zero. When counting down, this is the value the counter is reset to after reaching zero.
        """
        super().__init__(enable=enable, direction=direction,
                         counter_value=counter_value, size=size)

    def reset(self):
        """Pass without reset."""
        # Removed due to order of operations causing failure
        # __init__() -> reset() - stream()
        # Wipes out value loaded into counter
        # self.counter_value = 0
        pass

    def sample(self, values):
        """Handles the sample opcode.

        Args:
            values (List of bool): Sample input values.

        Returns:
            Formatted messages.
        """
        # Handle rollover of 0 size value
        max_val = self.size
        if (max_val > 0):
            max_val = max_val - 1
        else:
            max_val = 0xFF
        # Empty output values array
        output_values = []
        for value in values:
            # Only modify counter value when enable and input data
            # are both true
            if self.enable and value:
                # Increment or decrement counter
                if self.direction:
                    # If equal to size or
                    # if greater than size
                    # (ie. user has loaded in a value greater that size)
                    # set to 0
                    if self.counter_value >= max_val:
                        self.counter_value = 0
                    else:
                        self.counter_value += 1
                else:
                    # If zero rollover to size
                    if not self.counter_value:
                        self.counter_value = max_val
                    # If value loaded greater than size, set to size
                    elif self.counter_value > max_val:
                        self.counter_value = max_val
                    else:
                        self.counter_value -= 1

            output_values.append(self.counter_value)

        return self.output_formatter(
            [{"opcode": "sample", "data": output_values}])

    def discontinuity(self, values):
        """Handles the discontinuity opcode.

        Args:
            values (list of bool): values on the input port

        Returns:
            Formatted messages.
        """
        self.counter_value = 0
        return self.output_formatter(
            [{"opcode": "discontinuity", "data": None}])

    def flush(self, values):
        """Handles the flush opcode.

        Args:
            values (list of bool): values on the input port

        Returns:
            Formatted messages.
        """
        self.counter_value = 0
        return self.output_formatter(
            [{"opcode": "flush", "data": None}])
