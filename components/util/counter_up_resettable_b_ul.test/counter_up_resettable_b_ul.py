#!/usr/bin/env python3

# Python implementation of counter_up_resettable_b_ul block for verification
#
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
# more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

"""Python implementation of counter_up_resettable_b_ul for verification."""

import opencpi.ocpi_testing as ocpi_testing
import opencpi.ocpi_protocols as ocpi_protocols


class CounterUpResettable(ocpi_testing.Implementation):
    """Python implementation of counter_up_resettable_b_ul for verification."""

    def __init__(self, enable, auto_reset, period, reset_count,
                 nonsample_output_select):
        """Initialise the class.

        Args:
            enable (bool): Enables / disables the input.
            auto_reset (bool): Enables / disables counter wrap around.
            period (ulong): This sets the number of counts after which the count stops incrementing or wraps around to zero
            reset_count (ulong): This is the value the counter is set to when a 1 is received on the input
            nonsample_output_select (bool): When set to 0 opcodes other than sample are sent through the output port if connected. When set to 1 opcodes other than sample are sent through the ``outtrigger`` port if connected.
        """
        super().__init__(enable=enable, auto_reset=auto_reset,
                         period=period, reset_count=reset_count,
                         nonsample_output_select=nonsample_output_select)
        self.output_ports = ["output", "outtrigger"]
        self.counter_value = 0
        self.count = self.counter_value
        self.outtrigger = False
        # Handle int rollover of period
        if self.period > 0:
            self.max_val = self.period - 1
        else:
            self.max_val = 0xFFFFFFFF

    def reset(self):
        """Pass without reset."""
        pass

    def forward(self, opcode, data):
        """Forward the values straight to the output.

        Args:
            opcode (string): opcode for the data
            data (bool): output data

        Returns:
            Formatted messages.
        """
        message = [{"opcode": opcode, "data": data}]
        if self.nonsample_output_select is True:
            return self.output_formatter([], message)
        else:
            return self.output_formatter(message, [])

    def sample(self, values):
        """Handle the sample opcodes.

        Args:
            values (list of bool): data on the input port

        Returns:
            Formatted messages
        """
        output_values = [0] * len(values)
        outtrigger_values = [False] * len(values)

        # If counter enabled:
        if self.enable is True:
            # For all values in message:
            for index, value in enumerate(values):
                # If input is true, set counter to reset_count
                if value is True:
                    self.counter_value = self.reset_count
                # If input is false:
                else:
                    if self.max_val <= self.counter_value:
                        # If auto reset enabled, wrap around to 0
                        if self.outtrigger is True:
                            if self.auto_reset is True:
                                self.counter_value = 0
                    else:
                        self.counter_value = self.counter_value + 1

                # If counter has reached period:
                # or (self.period == 0):
                if self.counter_value >= self.max_val:
                    # Set outtrigger as true
                    self.outtrigger = True
                else:
                    self.outtrigger = False

                # Output counter value
                output_values[index] = self.counter_value
                outtrigger_values[index] = self.outtrigger

        # Set representation of count property
        self.count = self.counter_value

        max_message_length = ocpi_protocols.PROTOCOLS[
            "ulong_timed_sample"].max_sample_length

        # Sample messages cannot have more than max_message_length samples
        # in each message. As this component increases the data size,
        # break long messages up into messages with no more than
        # max_message_length samples.
        messages = [{
            "opcode": "sample",
            "data": output_values[index: index + max_message_length]}
            for index in range(0, len(output_values),
                               max_message_length)]
        return self.output_formatter(
            messages, [{"opcode": "sample", "data": outtrigger_values}])

    def time(self, value):
        """Handle time opcodes.

        Args:
            value (bool): data on the input port

        Returns:
            Formatted messages.
        """
        return self.forward("time", value)

    def sample_interval(self, value):
        """Handle sample interval opcodes.

        Args:
            value (bool): data on the input port

        Returns:
            Formatted messages.
        """
        return self.forward("sample_interval", value)

    def discontinuity(self, value):
        """Handle discontinuity opcodes.

        Args:
            value (bool): data on the input port

        Returns:
            Formatted messages.
        """
        self.counter_value = 0
        self.count = self.counter_value
        return self.forward("discontinuity", value)

    def flush(self, value):
        """Handle flush opcodes.

        Args:
            value (bool): data on the input port

        Returns:
            Formatted messages.
        """
        self.counter_value = 0
        self.count = self.counter_value
        return self.forward("flush", value)

    def metadata(self, value):
        """Handle metadata opcodes.

        Args:
            value (bool): data on the input port

        Returns:
            Formatted messages.
        """
        return self.forward("metadata", value)
