#!/usr/bin/env python3

# Runs checks for counter_up_resettable_b_ul testing
#
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
# more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

"""OpenCPI verification script for counter_up_resettable_b_ul tests."""

import os
import pathlib
import sys

import opencpi.ocpi_testing as ocpi_testing

from counter_up_resettable_b_ul import CounterUpResettable

enable = (os.environ.get("OCPI_TEST_enable").lower() == "true")
auto_reset = (os.environ.get("OCPI_TEST_auto_reset").lower() == "true")
period = int(os.environ.get("OCPI_TEST_period"))
reset_count = int(os.environ.get("OCPI_TEST_reset_count"))
nonsample_output_select = \
    (os.environ.get("OCPI_TEST_nonsample_output_select").lower() == "true")

input_file_path = str(sys.argv[-1])
output_file_path = str(sys.argv[-2])

counter_implementation = CounterUpResettable(enable, auto_reset, period,
                                             reset_count,
                                             nonsample_output_select)

# Use the port name in the file path to identify the port number
if "outtrigger" in output_file_path:
    port_index = 2
else:
    port_index = 1

test_id = ocpi_testing.get_test_case()
test_case, test_subcase = ocpi_testing.id_to_case(test_id)

verifier = ocpi_testing.Verifier(counter_implementation)

verifier.set_port_types(["bool_timed_sample"],
                        ["ulong_timed_sample", "bool_timed_sample"],
                        ["equal", "equal"])

if verifier.verify(test_id, [input_file_path], output_file_path,
                   port_select=port_index) is True:
    # Read the volatile property after port 1 has been verified since the
    # implementation is not run again
    if port_index == 1:
        # Verify volatile property final value
        count_property = int(os.environ.get("OCPI_TEST_count"))
        if count_property != counter_implementation.count:
            fail_message = (
                "count_property does not match expected value\n" +
                "  Python count                   : " +
                f"{counter_implementation.count}\n" +
                f"  Implementation-under-test count: {count_property}")

            # No runtime variables has the worker under test so use the output
            # data file name
            case_worker_port = pathlib.Path(output_file_path).stem.split(".")
            worker = f"{case_worker_port[-3]}.{case_worker_port[-2]}"

            verifier.test_failed(worker, "count", test_case,
                                 test_subcase, fail_message)
            sys.exit(1)
    sys.exit(0)
else:
    sys.exit(1)
