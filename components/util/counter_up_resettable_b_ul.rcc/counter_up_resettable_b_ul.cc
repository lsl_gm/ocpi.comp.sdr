// counter_up_resettable_b_ul RCC implementation.
//
// This file is protected by Copyright. Please refer to the COPYRIGHT file
// distributed with this source distribution.
//
// This file is part of OpenCPI <http://www.opencpi.org>
//
// OpenCPI is free software: you can redistribute it and/or modify it under the
// terms of the GNU Lesser General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option) any
// later version.
//
// OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
// more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#include "counter_up_resettable_b_ul-worker.hh"
#include <algorithm>

using namespace OCPI::RCC;  // for easy access to RCC data types and constants
using namespace Counter_up_resettable_b_ulWorkerTypes;

class Counter_up_resettable_b_ulWorker
    : public Counter_up_resettable_b_ulWorkerBase {
  bool enable = false;
  bool auto_reset = false;
  bool nonsample_output_select =
      COUNTER_UP_RESETTABLE_B_UL_NONSAMPLE_OUTPUT_SELECT;
  uint32_t period = 0;
  uint32_t reset_count = 0;
  uint32_t counter_value = 0;
  bool trigger = false;
  uint16_t sent_samples = 0;

  RCCResult start() {
    this->sent_samples = 0;
    return RCC_OK;
  }

  RCCResult enable_written() {
    this->enable = properties().enable;
    return RCC_OK;
  }

  RCCResult auto_reset_written() {
    this->auto_reset = properties().auto_reset;
    return RCC_OK;
  }

  RCCResult period_written() {
    this->period = properties().period;
    return RCC_OK;
  }

  RCCResult reset_count_written() {
    this->reset_count = properties().reset_count;
    return RCC_OK;
  }

  RCCResult count_written() {
    this->counter_value = properties().count;
    return RCC_OK;
  }

  RCCResult run(bool) {
    if (input.opCode() == Bool_timed_sampleSample_OPERATION) {
      // Pointer to input data
      uint16_t inLength = input.sample().data().size();
      const RCCBoolean *inData = input.sample().data().data();
      inData = &inData[this->sent_samples];

      // Set maximum output sample length
      const uint16_t max_length =
          COUNTER_UP_RESETTABLE_B_UL_OCPI_MAX_BYTES_OUTPUT / sizeof(uint32_t);

      // Set output sample length
      uint16_t loop_max = 0;
      bool last_run = false;
      if (inLength - this->sent_samples > max_length) {
        loop_max = max_length;
      } else {
        loop_max = inLength - this->sent_samples;
        last_run = true;
      }

      // Configure output
      uint32_t *outData;
      if (output.isConnected()) {
        outData = output.sample().data().data();
        output.setOpCode(Ulong_timed_sampleSample_OPERATION);
        output.sample().data().resize(loop_max);
      }

      // Configure outtrigger
      bool *outtriggerData;
      if (outtrigger.isConnected()) {
        outtriggerData =
            reinterpret_cast<bool *>(outtrigger.sample().data().data());
        outtriggerData = &outtriggerData[this->sent_samples];
        outtrigger.setOpCode(Bool_timed_sampleSample_OPERATION);
        outtrigger.sample().data().resize(inLength);
      }

      // For all values in stream
      if (this->enable) {
        for (size_t i = 0; i < loop_max; i++) {
          // Reset to ``reset_count`` when a '1' is received on the input
          // Otherwise increment counter by 1
          if (*inData++) {
            this->counter_value = this->reset_count;
          } else {
            // Handle counter reaching ``period`` - 1:
            if (this->counter_value >= (this->period - 1)) {
              if (this->trigger) {
                if (this->auto_reset) {
                  // If auto_reset = '1', reset to 0
                  this->counter_value = 0;
                }
              }
            } else {
              this->counter_value = this->counter_value + 1;
            }
          }
          // Set trigger
          if (this->counter_value >= (this->period - 1)) {
            this->trigger = true;
          } else {
            this->trigger = false;
          }
          // If outputs connected, output values:
          if (output.isConnected()) {
            *outData++ = this->counter_value;
          }
          if (outtrigger.isConnected()) {
            *outtriggerData++ = this->trigger;
          }
        }
      } else {
        // Not enabled - fill outputs with this->counter_value and
        // this->trigger
        // If outputs connected, output values:
        if (output.isConnected()) {
          std::fill_n(outData, loop_max, this->counter_value);
        }
        if (outtrigger.isConnected()) {
          std::fill_n(outtriggerData, loop_max, this->trigger);
        }
      }

      // Output counter value to volatile
      properties().count = this->counter_value;

      this->sent_samples += loop_max;

      // Return counter values and trigger if they are connected
      if (last_run) {
        this->sent_samples = 0;
        input.advance();
        if (output.isConnected()) {
          output.advance();
        }
        if (outtrigger.isConnected()) {
          outtrigger.advance();
        }
      } else {
        output.advance();
      }
      return RCC_OK;
    } else if (input.opCode() == Bool_timed_sampleTime_OPERATION) {
      // If output connected and selected for nonstream outputs:
      if (output.isConnected() && !this->nonsample_output_select) {
        // Pass through time opcode and time data
        output.setOpCode(Ulong_timed_sampleTime_OPERATION);
        output.time().fraction() = input.time().fraction();
        output.time().seconds() = input.time().seconds();
        input.advance();
        output.advance();
        return RCC_OK;
      }
      // If outtrigger connected and selected for nonstream outputs:
      else if (outtrigger.isConnected() && this->nonsample_output_select) {
        // Pass through time opcode and time data
        outtrigger.setOpCode(Bool_timed_sampleTime_OPERATION);
        outtrigger.time().fraction() = input.time().fraction();
        outtrigger.time().seconds() = input.time().seconds();
        input.advance();
        outtrigger.advance();
        return RCC_OK;
      }
      // If no outputs are connected:
      else {
        input.advance();
        return RCC_OK;
      }
    } else if (input.opCode() == Bool_timed_sampleSample_interval_OPERATION) {
      // If output connected and selected for nonstream outputs:
      if (output.isConnected() && !this->nonsample_output_select) {
        // Pass through sample interval opcode and sample interval data
        output.setOpCode(Ulong_timed_sampleSample_interval_OPERATION);
        output.sample_interval().fraction() =
            input.sample_interval().fraction();
        output.sample_interval().seconds() = input.sample_interval().seconds();
        input.advance();
        output.advance();
        return RCC_OK;
      }
      // If outtrigger connected and selected for nonstream outputs:
      else if (outtrigger.isConnected() && this->nonsample_output_select) {
        // Pass through sample interval opcode and sample interval data
        outtrigger.setOpCode(Bool_timed_sampleSample_interval_OPERATION);
        outtrigger.sample_interval().fraction() =
            input.sample_interval().fraction();
        outtrigger.sample_interval().seconds() =
            input.sample_interval().seconds();
        input.advance();
        outtrigger.advance();
        return RCC_OK;
      } else {
        input.advance();
        return RCC_OK;
      }
    } else if (input.opCode() == Bool_timed_sampleFlush_OPERATION) {
      this->counter_value = 0;
      properties().count = this->counter_value;
      // Pass through flush opcode
      if (output.isConnected() && !this->nonsample_output_select) {
        output.setOpCode(Ulong_timed_sampleFlush_OPERATION);
        input.advance();
        output.advance();
        return RCC_OK;
      }
      // If outtrigger connected and selected for nonstream outputs:
      else if (outtrigger.isConnected() && this->nonsample_output_select) {
        outtrigger.setOpCode(Bool_timed_sampleFlush_OPERATION);
        input.advance();
        outtrigger.advance();
        return RCC_OK;
      } else {
        input.advance();
        return RCC_OK;
      }
    } else if (input.opCode() == Bool_timed_sampleDiscontinuity_OPERATION) {
      this->counter_value = 0;
      properties().count = this->counter_value;
      // Pass through flush opcode
      if (output.isConnected() && !this->nonsample_output_select) {
        output.setOpCode(Ulong_timed_sampleDiscontinuity_OPERATION);
        input.advance();
        output.advance();
        return RCC_OK;
      }
      // If outtrigger connected and selected for nonstream outputs:
      else if (outtrigger.isConnected() && this->nonsample_output_select) {
        outtrigger.setOpCode(Bool_timed_sampleDiscontinuity_OPERATION);
        input.advance();
        outtrigger.advance();
        return RCC_OK;
      } else {
        input.advance();
        return RCC_OK;
      }
    } else if (input.opCode() == Bool_timed_sampleMetadata_OPERATION) {
      // Pass through stream_metadata opcode, id, and data
      if (output.isConnected() && !this->nonsample_output_select) {
        output.setOpCode(Ulong_timed_sampleMetadata_OPERATION);
        output.metadata().id() = input.metadata().id();
        output.metadata().value() = input.metadata().value();
        input.advance();
        output.advance();
        return RCC_OK;
      } else if (outtrigger.isConnected() && this->nonsample_output_select) {
        outtrigger.setOpCode(Bool_timed_sampleMetadata_OPERATION);
        outtrigger.metadata().id() = input.metadata().id();
        outtrigger.metadata().value() = input.metadata().value();
        input.advance();
        outtrigger.advance();
        return RCC_OK;
      } else {
        input.advance();
        return RCC_OK;
      }
    } else {
      setError("Unknown OpCode received");
      return RCC_FATAL;
    }
  }
};

COUNTER_UP_RESETTABLE_B_UL_START_INFO
COUNTER_UP_RESETTABLE_B_UL_END_INFO
