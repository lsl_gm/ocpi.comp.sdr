#!/usr/bin/env python3

# Python implementation of triggered_sampler_xc for verification
#
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
# more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

"""Python implementation of triggered_sampler for verification."""

import opencpi.ocpi_testing as ocpi_testing
import decimal
import math
from sdr_interface.sample_time import SampleTime


class TriggeredSampler(ocpi_testing.Implementation):
    """Python implementation of triggered_sampler for verification."""

    def __init__(self, bypass, capture_length, send_flush,
                 capture_on_meta, capture_single, capture_continuous):
        """Initialise the class.

        Args:
            bypass (bool): When ``true``, trigger functionality is disabled: all messages received on input are routed straight to output.
            capture_length (ulong): Controls the number of samples in each capture.
            send_flush (bool): When ``true``, a flush is sent after the samples and before the discontinuity.
            capture_on_meta (bool): When ``true``, a capture is triggered on receipt of a metadata message on ``input``.
            capture_single (bool): Writing ``true`` triggers a single capture.
            capture_continuous (bool): When ``true``, back-to-back captures will be performed.
        """
        super().__init__()
        self.input_ports = ["input", "trigger"]
        # Save properties locally
        self._bypass = bypass
        self._capture_length = capture_length
        self._send_flush = send_flush
        self._capture_on_meta = capture_on_meta
        self._capture_single = capture_single
        self._capture_continuous = capture_continuous
        # Initialise internal state
        self.samples_since_last_time = 0
        self.samples_left_to_send = 0
        self.capture_queued = False
        self.capture_pending = False
        self._sample_time = SampleTime()

        self.opcodes = []

    def reset(self):
        """Reset the state held by the TriggeredSampler implementation."""
        self.samples_since_last_time = 0
        self.samples_left_to_send = 0
        self.capture_queued = False
        self.capture_pending = False
        self._sample_time.reset()
        self.opcodes = []

    def select_input(self, input, trigger):
        """Select either trigger or input as the input.

        Args:
            input (bool): data on the input port
            trigger (bool): data on the trigger port

        Returns:
            int: 1 for trigger port, 0 for input port
        """
        if trigger is not None:
            return 1
        if input is not None:
            return 0

    def start_capture(self):
        """Start a capture, calculate time, and send time and interval opcodes if they're not none."""
        self.samples_left_to_send = self._capture_length
        # Calculate current sample time and send Time/Interval if we have them
        self.calc_time()
        if self._sample_time.time is not None:
            self.opcodes += [{"opcode": "time",
                              "data": self._sample_time.opcode_time()}]
        if self._sample_time.interval is not None:
            self.opcodes += [{"opcode": "sample_interval",
                              "data": self._sample_time.opcode_interval()}]
        self.capture_pending = False

    def finish_capture(self):
        """If there is a capture queued move it to pending."""
        # Finished capture, should we start next?
        if self.capture_queued:
            # Prepare for next capture, pending first samples
            self.capture_pending = True
            self.capture_queued = False
        # Special case for capture single/continuous
        # After capture is complete, if single/continuous then we should
        # immediately go to pending again
        self.check_trigger_properties()

    def check_trigger_port(self, trigger):
        """If there is a trigger, start the handling of a trigger.

        Args:
            trigger (bool): data on the trigger port
        """
        # N.B. Check for False too as opcodes with no data (i.e. Flush and
        # Discontinuity) are passed as True if received on this port and as
        # False if received on the other port, and not data/None like other
        # opCodes
        if not self._bypass and trigger is not None and trigger is not False:
            self.trigger_received()

    def check_trigger_properties(self):
        """Are we not in a capture, and properties are asking for a capture? (continuous or single)."""
        if not self._bypass and self.samples_left_to_send <= 0 and \
                (not self.capture_pending) and \
                (self._capture_continuous or self._capture_single):
            self.capture_pending = True
            self._capture_single = False
        if self._bypass:
            # in bypass, cancel all current, pending and queued captures
            self.capture_pending = False
            self.capture_queued = False
            self._capture_single = False
            self.samples_left_to_send = 0

    def trigger_received(self):
        """Handle the reception of a trigger.

        Either moving to a capture pending state,
        or marking a capture as queued.
        """
        if not self._bypass:
            if self.samples_left_to_send > 0:
                # Capture in progress (capturing), queue next capture
                self.capture_queued = True
            elif self.capture_pending:
                # Capture in progress (pending first samples), queue
                # next capture
                self.capture_queued = True
            else:
                # No capture in progress.
                # Mark capture in progress (pending first samples)
                self.capture_pending = True

    def calc_time(self):
        """Update the time counter with the current sample time if it is not 0 or invalid."""
        if self.samples_since_last_time == 0:
            # No need to update time
            pass
        elif self._sample_time.time is None or self._sample_time.interval is None:
            # Time is invalid
            self._sample_time.time = None
        else:
            # Must update current sample time
            self._sample_time.advance_time_by(self.samples_since_last_time)
            self.samples_since_last_time = 0

    def format_opcodes(self):
        """Return the current list of opcodes, formatted for output, and then clear the list.

        Returns:
            Formatted messages.
        """
        # Default result
        result = []
        if len(self.opcodes) > 0:
            # Add current list of opcodes to result and then clear list
            result = self.output_formatter(self.opcodes)
            self.opcodes = []
        return result

    # We don't know what opcode we will receive on the trigger port so
    # all of them will accommodate data from one or both ports
    def discontinuity(self, input, trigger):
        """Handles discontinuity opcodes.

        Args:
            input (bool): data on the input port
            trigger (bool): data on the trigger port

        Returns:
            Formatted messages.
        """
        # Check trigger port
        self.check_trigger_port(trigger)
        self.check_trigger_properties()
        if input is not None:
            # Invalidate internal time counter
            self._sample_time.time = None
            # Forward discontinuity in bypass mode or when capture is active
            if self.samples_left_to_send > 0 or self._bypass:
                self.opcodes += [{"opcode": "discontinuity", "data": input}]
        return self.format_opcodes()

    def flush(self, input, trigger):
        """Handle flush opcodes.

        Args:
            input (bool): data on the input port
            trigger (bool): data on the trigger port

        Returns:
            Formatted messages.
        """
        # Check trigger port
        self.check_trigger_port(trigger)
        self.check_trigger_properties()
        if input is not None:
            # Always forward flush
            self.opcodes += [{"opcode": "flush", "data": input}]
        return self.format_opcodes()

    def metadata(self, input, trigger):
        """Handles the metadata opcode.

        Args:
            input (bool): data on the input port
            trigger (bool): data on the trigger port

        Returns:
            Formatted messages.
        """
        # Check trigger port
        self.check_trigger_port(trigger)
        self.check_trigger_properties()
        if input is not None:
            # Metadata is always sent forward
            self.opcodes += [{"opcode": "metadata", "data": input}]
            # But should we trigger
            if not self._bypass and self._capture_on_meta:
                self.trigger_received()
        return self.format_opcodes()

    def sample(self, input, trigger):
        """Handles the sample opcode.

        Args:
            input (list): data on the input port
            trigger (list): data on the trigger port

        Returns:
            Formatted messages.
        """
        # check trigger port
        self.check_trigger_port(trigger)
        self.check_trigger_properties()
        if input is not None:
            input = list(input)
            # Are we starting capture, pending first samples?
            if not self._bypass and self.capture_pending:
                self.start_capture()

            # add to counter
            self.samples_since_last_time += len(input)
            if self._bypass:
                # Bypass mode, send everything through
                self.opcodes += [{"opcode": "sample", "data": input}]
            elif self.samples_left_to_send > 0:
                # In a capture, send x amount through
                # More samples than needed?
                if len(input) > self.samples_left_to_send:
                    input = input[:self.samples_left_to_send]
                self.samples_left_to_send -= len(input)
                self.opcodes += [{"opcode": "sample", "data": input}]
                if self.samples_left_to_send == 0:
                    if self._send_flush:
                        self.opcodes += [{"opcode": "flush", "data": True}]
                    self.opcodes += [{"opcode": "discontinuity", "data": True}]
                    self.finish_capture()
        return self.format_opcodes()

    def sample_interval(self, input, trigger):
        """Handles the sample interval opcode.

        Args:
            input (bool): data on the input port
            trigger (bool): data on the trigger port

        Returns:
            Formatted messages.
        """
        # Check trigger port
        self.check_trigger_port(trigger)
        self.check_trigger_properties()
        if input is not None:
            # Update time first...as new interval invalidates time calculation
            self.calc_time()
            # Store the received sample interval
            self._sample_time.interval = input
            # Forward sample_interval in bypass mode or when capture is active
            if self._bypass or self.samples_left_to_send > 0:
                self.opcodes += [{"opcode": "sample_interval", "data": input}]
        return self.format_opcodes()

    def time(self, input, trigger):
        """Handles the time opcode.

        Args:
            input (bool): data on the input port
            trigger (bool): data on the trigger port

        Returns:
            Formatted messages.
        """
        # Check trigger port
        self.check_trigger_port(trigger)
        self.check_trigger_properties()
        if input is not None:
            # Store the received time
            self._sample_time.time = input
            self.samples_since_last_time = 0
            # Forward time in bypass mode or when capture is active
            if self._bypass or self.samples_left_to_send > 0:
                self.opcodes += [{"opcode": "time", "data": input}]
        return self.format_opcodes()
