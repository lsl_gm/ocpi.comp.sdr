#!/usr/bin/env python3

# Generates the input binary file for triggered_sampler_xc testing
#
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
# more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

"""OpenCPI generator for triggered_sampler_xc tests."""

import os
import random
import opencpi.ocpi_protocols as ocpi_protocols
import opencpi.ocpi_testing as ocpi_testing


class CustomGenerator(ocpi_testing.generator.ComplexCharacterGenerator):
    """Python implementation of a custom generator for triggered_sampler_xc tests."""

    def __init__(self, send_flush, capture_length):
        """Initialise the class.

        Args:
            send_flush (bool): When ``true``, a flush is sent after the samples and before the discontinuity.
            capture_length (ulong): Controls the number of samples in each capture.
        """
        super().__init__()
        self.CASES["empty"] = self.empty
        self.CASES["trigger"] = self.trigger
        self.CASES["interval_stress"] = self.interval_stress
        self.CASES["throughput_stress"] = self.throughput_stress
        self.CASES["meta_trigger"] = self.meta_trigger
        self._send_flush = send_flush
        self._capture_length = capture_length

    def empty(self, seed, subcase):
        """Generates an empty list of messages.

        Args:
            seed: ignored.
            subcase (string): ignored.

        Returns:
            Formatted empty message.
        """
        messages = []
        return messages

    def samples_message(self, minlen, maxlen, subcase):
        """Generates a random length samples message.

        Args:
            minlen (ulong): lowest length message
            maxlen (ulong): highest length message
            subcase (string): current subcase for test

        Returns:
            Formatted messages.
        """
        # Back up original message length
        original_message_length = self.SAMPLE_DATA_LENGTH

        self.SAMPLE_DATA_LENGTH = random.randint(minlen, maxlen)
        child_seed = random.randint(1, 9999)
        messages = super().typical(child_seed, subcase)

        # Reset SAMPLE_DATA_LENGTH back to its original value
        self.SAMPLE_DATA_LENGTH = original_message_length
        return messages

    def typical(self, seed, subcase):
        """Generates a simple input message stream with time, interval, samples + meta to trigger capture(s).

        Args:
            seed: value to generate the message content
            subcase (string): current subcase for test

        Returns:
            Formatted messages.
        """
        random.seed(seed)

        # Generate TISMSS
        messages = []
        messages += [{"opcode": "time", "data": 1.000000}]
        messages += [{"opcode": "sample_interval", "data": 0.000001}]
        messages += self.samples_message(100, 1000, subcase)
        messages += [{"opcode": "metadata", "data": {"id": 100, "value": 123}}]
        messages += self.samples_message(500, 1000, subcase)
        messages += self.samples_message(500, 1000, subcase)

        # For completeness, ensure the input has more sample messages than
        # needed and that they aren't output
        repeats = random.randint(4, 20)
        for i in range(repeats):
            messages += self.samples_message(500, 1000, subcase)
        return messages

    def trigger(self, seed, subcase):
        """Trigger message stream with some opcodes to trigger capture(s).

        Args:
            seed: value used to randomise
            subcase (string): current subcase for test

        Raises:
            ValueError: unexpected subcase

        Returns:
            Formatted messages.
        """
        random.seed(seed)

        if subcase == "one" or subcase == "":
            # Send a single message
            num_triggers = 1
        elif subcase == "two":
            # Send a pair of sample messages
            num_triggers = 2
        elif subcase == "three":
            # Send some messages
            num_triggers = 3
        elif subcase == "several":
            # Send several messages
            num_triggers = random.randint(4, 10)
        else:
            raise ValueError(f"Unexpected subcase of {subcase} for trigger()")

        # NB. Constraining length due to issue with trigger port being
        #     limited to 2048 bytes in RCC test harness
        num_samples = random.randint(1, 512)

        messages = []
        for frames in range(num_triggers):
            messages += self.samples_message(1, num_samples, subcase)
        return messages

    def meta_trigger(self, seed, subcase):
        """Metadata trigger message stream, with either single or multiple metadata opcodes to trigger captures.

        Args:
            seed: value used to randomise
            subcase (string): current subcase for test

        Raises:
            ValueError: unexpected subcase

        Returns:
            Formatted messages.
        """
        random.seed(seed)

        if subcase == "one" or subcase == "":
            # Send a single message
            num_triggers = 1
        elif subcase == "two":
            # Send a pair of sample messages
            num_triggers = 2
        elif subcase == "three":
            # Send some messages
            num_triggers = 3
        elif subcase == "several":
            # Send several messages
            num_triggers = random.randint(4, 10)
        else:
            raise ValueError(f"Unexpected subcase of {subcase} for trigger()")

        # Generate TIS
        messages = []
        messages += [{"opcode": "time", "data": 1.000000}]
        messages += [{"opcode": "sample_interval", "data": 0.000001}]
        messages += self.samples_message(100, 1000, subcase)

        # Add consecutive triggers
        while num_triggers > 0:
            messages += [{"opcode": "metadata",
                          "data": {"id": 100, "value": 123}}]
            num_triggers = num_triggers - 1

        # Add more samples, enough for all captures to complete
        for i in range((num_triggers * 2) + 1):
            messages += self.samples_message(500, 1000, subcase)

        return messages

    def property(self, seed, subcase):
        """Test properties used by triggered sampler.

        Args:
            seed: value used to randomise the content of the test
            subcase (string): current test being run

        Returns:
            Formatted messages.
        """
        return self.typical(seed, subcase)

    def sample(self, seed, subcase):
        """Tests trigger port.

        Args:
            seed: value used to randomise the content of the test
            subcase (string): current test being run

        Returns:
            Formatted messages.
        """
        return self.typical(seed, subcase)

    def sample_other_port(self, seed, subcase):
        """For testing bypass mode, ensuring capture triggers are ignored.

        Args:
            seed: value used to randomise the content of the test
            subcase (string): current test being run

        Returns:
            Formatted messages.
        """
        random.seed(seed)
        num_samples = random.randint(1, self.MESSAGE_SIZE_LONGEST)
        return [{
            "opcode": "sample",
            "data": self._get_sample_values(num_samples)
        }]

    def input_stressing(self, seed, subcase):
        """Exercises metadata triggered captures and forwarding of all op-code types.

        Args:
            seed: value used to randomise the content of the test
            subcase (string): current test being run

        Returns:
            Formatted messages.
        """
        random.seed(seed)

        # Generate STF M STS   MSIDS   DSM SFTS   M  S
        # Expect     F MTSTSFD MSIDSFD   MISFTSFD MTISFD
        messages = []
        messages += self.samples_message(100, 100, subcase)
        messages += [{"opcode": "time", "data": 1.000000}]
        messages += [{"opcode": "flush", "data": 0}]
        messages += [{"opcode": "metadata", "data": {"id": 100, "value": 123}}]
        messages += self.samples_message(100, 100, subcase)
        messages += [{"opcode": "time", "data": 2.000000}]
        messages += self.samples_message(100, 100, subcase)
        messages += [{"opcode": "metadata", "data": {"id": 200, "value": 456}}]
        messages += self.samples_message(100, 100, subcase)
        messages += [{"opcode": "sample_interval", "data": 0.000001}]
        messages += [{"opcode": "discontinuity", "data": 0}]
        messages += self.samples_message(100, 100, subcase)
        messages += [{"opcode": "discontinuity", "data": 0}]
        messages += self.samples_message(100, 100, subcase)
        messages += [{"opcode": "metadata", "data": {"id": 300, "value": 789}}]
        messages += self.samples_message(100, 100, subcase)
        messages += [{"opcode": "flush", "data": 0}]
        messages += [{"opcode": "time", "data": 3.000000}]
        messages += self.samples_message(100, 100, subcase)
        messages += [{"opcode": "metadata", "data": {"id": 400, "value": 147}}]
        messages += self.samples_message(100, 100, subcase)

        return messages

    def message_size(self, seed, subcase):
        """Tests minimum message sizes on input and output.

        Args:
            seed: value used to randomise the content of the test
            subcase (string): current test being run

        Returns:
            Formatted messages.
        """
        random.seed(seed)
        # prefix sequence with metadata to trigger a capture
        messages = []
        messages += [{"opcode": "metadata", "data": {"id": 400, "value": 147}}]
        messages += super().message_size(seed, subcase)
        return messages

    def time(self, seed, subcase):
        """Test a range of time values.

        Args:
            seed: value used to randomise the content of the test
            subcase (string): current test being run

        Returns:
            Formatted messages.
        """
        random.seed(seed)
        messages = []
        if subcase == "increment":
            # generate a series of messages to test if time stamp is incremented correctly when no trigger has been received
            # update worker with timestamp and interval information
            messages += [{"opcode": "time", "data": 1.000000}]
            messages += [{"opcode": "sample_interval", "data": 0.000001}]
            # input samples without a trigger to cause time stamp to be incremented
            messages += self.samples_message(self._capture_length,
                                             self._capture_length, subcase)
            # input metadata to trigger a capture so timestamp can be compared
            messages += [{"opcode": "metadata",
                          "data": {"id": 400, "value": 147}}]
            messages += self.samples_message(self._capture_length,
                                             self._capture_length, subcase)
        else:
            # proceed with generic timestamp tests
            messages = super().time(seed, subcase)
            # prefix final sample message with metadata to trigger a capture
            messages.insert(
                len(messages) - 1,
                {"opcode": "metadata", "data": {"id": 400, "value": 147}})
        return messages

    def sample_interval(self, seed, subcase):
        """Test a range of sample_interval values.

        Args:
            seed: value used to randomise the content of the test
            subcase (string): current test being run

        Returns:
            Formatted messages.
        """
        random.seed(seed)
        # prefix final sample message with metadata to trigger a capture
        messages = super().sample_interval(seed, subcase)
        messages.insert(
            len(messages) - 1,
            {"opcode": "metadata", "data": {"id": 400, "value": 147}})
        return messages

    def flush(self, seed, subcase):
        """Test flush during capture.

        Args:
            seed: value used to randomise the content of the test
            subcase (string): current test being run

        Returns:
            Formatted messages.
        """
        random.seed(seed)
        # prefix sequence with metadata to trigger a capture
        messages = []
        messages += [{"opcode": "metadata", "data": {"id": 400, "value": 147}}]
        messages += super().flush(seed, subcase)
        return messages

    def discontinuity(self, seed, subcase):
        """Test discontinuity during capture.

        Args:
            seed: value used to randomise the content of the test
            subcase (string): current test being run

        Returns:
            Formatted messages.
        """
        random.seed(seed)
        # prefix sequence with metadata to trigger a capture
        messages = []
        messages += [{"opcode": "metadata", "data": {"id": 400, "value": 147}}]
        messages += super().discontinuity(seed, subcase)
        return messages

    def soak(self, seed, subcase):
        """Constrained random soak tests and testing consecutive triggers.

        Args:
            seed: value used to randomise the content of the test
            subcase (string): current test being run

        Returns:
            Formatted messages.
        """
        random.seed(seed)
        # generate a longer sequence to increase functional coverage on HDL.
        messages = []
        num_messages = 8
        if subcase in ["one", "two", "three", "several"]:
            # soak used for "other port" when testing trigger subcases
            # but super().soak() doesn't know about these trigger subcases
            # so pass a suitable soak subcase
            num_messages = 1000
            subcase = "all_opcodes"
        elif subcase not in ["sample_only", "all_opcodes"]:
            # expect it to be numeric string of num_messages
            num_messages = int(subcase)
            subcase = "all_opcodes"

        for i in range(num_messages):
            messages += super().soak(random.randint(1, 9999), subcase)

        return messages

    def interval_stress(self, seed, subcase):
        """Test sample interval changes.

        Args:
            seed: value used to randomise the content of the test
            subcase (string): current test being run

        Raises:
            ValueError: unexpected subcase

        Returns:
            Formatted messages.
        """
        random.seed(seed)
        messages = []
        if subcase == "fixed":
            # Send a simple fixed test set
            messages += [{"opcode": "time", "data": 1.000000}]
            messages += [{"opcode": "sample_interval", "data": 0.25}]
            messages += self.samples_message(40, 40, subcase)  # Time=11
            messages += self.samples_message(40, 40, subcase)  # Time=21
            messages += self.samples_message(4, 4, subcase)    # Time=22
            messages += [{"opcode": "sample_interval", "data": 100.0}]
            messages += self.samples_message(40, 40, subcase)  # Time=4022
            messages += self.samples_message(40, 40, subcase)  # Time=8022
        elif subcase == "random":
            # Send a random test set
            messages += [
                {
                    "opcode": "time",
                    "data": random.uniform(self.TIME_MIN, float(self.TIME_MAX))
                },
                {
                    "opcode": "sample_interval",
                    "data": random.uniform(self.SAMPLE_INTERVAL_MIN,
                                           float(self.SAMPLE_INTERVAL_MAX))
                }]

            # Send in mix of samples that aren't necessarily a full capture
            # interspersed with changes in interval
            messages += self.samples_message(1, 5, subcase)
            for i in range(random.randint(10, 20)):
                messages += [{
                    "opcode": "sample_interval",
                    "data": random.uniform(self.SAMPLE_INTERVAL_MIN,
                                           float(self.SAMPLE_INTERVAL_MAX))
                }]
                messages += self.samples_message(1, 12, subcase)
        else:
            raise ValueError(
                f"Unexpected subcase of {subcase} for interval_stress()")
        return messages

    def throughput_stress(self, seed, subcase):
        """Test HDL throughput and back pressure.

        Args:
            seed: value used to randomise the content of the test
            subcase (string): current test being run

        Raises:
            ValueError: unexpected subcase

        Returns:
            Formatted messages.
        """
        random.seed(seed)
        messages = []
        # Generate data to exercise max data rate out
        # Set input sample message sizes to capture length
        N = self._capture_length
        # Generate fixed pattern with samples + time/interval where required
        # Phase 1 - just samples
        for i in range(10):
            messages += self.samples_message(N, N, subcase)
        # Phase 2 - time+samples
        # time before each sample to get time out because interval unknown yet
        for i in range(10):
            messages += [{"opcode": "time", "data": 1.000000}]
            messages += self.samples_message(N, N, subcase)
        # Phase 3 - int+samples
        messages += [{"opcode": "discontinuity", "data": 0}]
        messages += [{"opcode": "sample_interval", "data": 0.001000}]
        for i in range(10):
            messages += self.samples_message(N, N, subcase)
        # Phase 4 - time+int+samples
        messages += [{"opcode": "time", "data": 2.000000}]
        for i in range(10):
            messages += self.samples_message(N, N, subcase)

        if subcase == "all_opcodes":
            # inject random meta/flush
            for i in range(15):
                t = random.randint(1, 2)
                if t == 1:
                    m = {"opcode": "metadata",
                         "data": {"id": 135, "value": 246}}
                else:
                    m = {"opcode": "flush", "data": 0}
                idx = random.randint(0, len(messages)-1)
                messages.insert(idx, m)

        elif subcase != "sample_only":
            raise ValueError(
                f"Unexpected subcase of {subcase} for throughput_stress()")
        return messages

    def custom(self, seed, subcase):
        """Custom stream starting with time + sample_interval followed by samples.

        Args:
            seed: value used to randomise the content of the test
            subcase (string): current test being run

        Returns:
            Formatted messages.
        """
        random.seed(seed)
        messages = []
        messages += [{"opcode": "time", "data": 1.000000}]
        messages += [{"opcode": "sample_interval", "data": 0.25}]
        messages += self.samples_message(400, 400, subcase)
        messages += self.samples_message(400, 400, subcase)
        messages += self.samples_message(400, 400, subcase)
        messages += self.samples_message(400, 400, subcase)
        messages += self.samples_message(400, 400, subcase)
        messages += self.samples_message(400, 400, subcase)
        messages += self.samples_message(400, 400, subcase)
        messages += self.samples_message(400, 400, subcase)
        messages += self.samples_message(400, 400, subcase)
        messages += self.samples_message(400, 400, subcase)
        messages += self.samples_message(400, 400, subcase)
        messages += self.samples_message(400, 400, subcase)

        return messages


arguments = ocpi_testing.get_generate_arguments(["empty",
                                                 "trigger",
                                                 "interval_stress",
                                                 "throughput_stress",
                                                 "meta_trigger"])

subcase = os.environ["OCPI_TEST_subcase"]
bypass = (os.environ.get("OCPI_TEST_bypass").lower() == "true")
capture_length = int(os.environ.get("OCPI_TEST_capture_length"))
send_flush = (os.environ.get("OCPI_TEST_send_flush").lower() == "true")
capture_on_meta = \
    (os.environ.get("OCPI_TEST_capture_on_meta").lower() == "true")
capture_continuous = \
    (os.environ.get("OCPI_TEST_capture_continuous").lower() == "true")

seed = ocpi_testing.get_test_seed(arguments.case, subcase, bypass,
                                  capture_length, send_flush,
                                  capture_on_meta, capture_continuous)

generator = CustomGenerator(send_flush, capture_length)

# Generate the test data messages
messages = generator.generate(seed, arguments.case, subcase,
                              arguments.case_number, arguments.subcase_number)

with ocpi_protocols.WriteMessagesFile(
        arguments.save_path, "complex_char_timed_sample") as file_id:
    file_id.write_dict_messages(messages)
