#!/usr/bin/env python3

# Generates patterns for large_pattern_detector_b testing
#
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
# more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

"""Generates patterns for large_pattern_detector_b testing."""

import os
import argparse
import random


def csv_split(string):
    """Split a csv string.

    Args:
        string (str): The comma separated variable string to split.

    Returns:
        list of str: The separate parts of the csv string.
    """
    return [str(value).strip() for value in str(string).split(",")]


def parse_args():
    """Parse the command line arguments.

    Returns:
        The parsed arguments object
    """
    parser = argparse.ArgumentParser(description="Writes bool pattern to file")
    parser.add_argument("-p", "--pattern", type=csv_split, default=None,
                        help="sets the pattern explicitly, e.g., " +
                        "\"--pattern=0,1,0,1,...\"")
    parser.add_argument("-s", "--seed", type=int, default=0,
                        help="Sets the seed for the random bool generator")
    parser.add_argument("--pattern-type", type=str.lower, default="random",
                        choices=["random", "all-true",
                                 "all-false", "alternating"],
                        help="sets the type of pattern to generate")
    parser.add_argument("file_path", type=str,
                        help="File path to save pattern")
    arguments = parser.parse_args()
    return arguments


def generate_pattern():
    """Generate a pattern and write it to file.

    Parses the command line arguments, generates the required pattern,
    and saves it into the file name given on the command line.

    Raises:
        RuntimeError: If OCPI_TEST_pattern_length is not set.
        RuntimeError: If a fixed pattern passed in is shorter than pattern length.
        RuntimeError: If pattern_type is not a recognised type.
    """
    # Parse arguments
    args = parse_args()
    try:
        pattern_length = int(os.environ.get("OCPI_TEST_pattern_length"))
    except:
        raise RuntimeError(
            "OCPI_TEST_pattern_length must be set (and be an integer)")

    # Generate a fixed pattern if args.pattern is set
    if args.pattern is not None:
        # Convert args.pattern from "0" and "1" to "False" and "True"
        pattern = ["True" if value == "1" else "False"
                   for value in args.pattern]
        # Trim pattern to length (allowed to be too long, not too short)
        if len(pattern) < pattern_length:
            raise RuntimeError(
                "Pattern supplied was shorter than pattern_length!")
        pattern = pattern[:pattern_length]
    elif args.pattern_type == "random":
        random.seed(args.seed)
        pattern = [random.choice(["True", "False"])
                   for _ in range(pattern_length)]
    elif args.pattern_type == "all-true":
        pattern = ["True"] * pattern_length
    elif args.pattern_type == "all-false":
        pattern = ["False"] * pattern_length
    elif args.pattern_type == "alternating":
        pattern = ["True", "False"] * ((pattern_length + 1) // 2)
        pattern = pattern[:pattern_length]
    else:
        raise RuntimeError(f"Unexpected pattern_type ({args.pattern_type})")

    # Write pattern to file
    with open(args.file_path, "w") as pattern_file:
        for value in pattern:
            pattern_file.write(str(value) + "\n")


if __name__ == "__main__":
    generate_pattern()
