#!/usr/bin/env python3

# Generates the input binary file for large_pattern_detector_b testing
#
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
# more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

"""Generates the input binary file for large_pattern_detector_b testing."""

import os
import random

import numpy as np

import opencpi.ocpi_protocols as ocpi_protocols
import opencpi.ocpi_testing as ocpi_testing


class LargePatternDetectorGenerator(ocpi_testing.generator.BooleanGenerator):
    """Custom generator for large_pattern_detector_b testing."""

    def __init__(self, pattern, bitmask):
        """Initialise the generator.

        Args:
            pattern (List of bool): The pattern to add into the stream to test.
            bitmask (List of bool): The bitmask being used to test.
        """
        super().__init__()

        self._pattern = pattern
        self._bitmask = bitmask

        self.CASES["pattern_cross_messages"] = self.pattern_cross_messages
        self.CASES["ignored_pattern_bits"] = self.ignored_pattern_bits

    def typical(self, seed, subcase):
        """Generate a sample message, including pattern, with typical data inputs.

        Uses the standard BooleanGenerator but ensures the generated message
        contains the pattern to be detected.

        Args:
            seed (int): The seed value to use for random number generation.
            subcase (str): Name of the subcase messages are to be generated
                for.

        Returns:
            Messages for the typical case and the stated subcase.
        """
        messages = super().typical(seed, subcase)
        return self._add_pattern(seed, messages)

    def property(self, seed, subcase):
        """Generate a sample message for property testing.

        Uses the standard BooleanGenerator but ensures the generated message
        contains the pattern to be detected.

        Args:
            seed (int): The seed value to use for random number generation.
            subcase (str): Name of the subcase messages are to be generated
                for.

        Returns:
            Messages for the stated subcase.
        """
        messages = super().property(seed, subcase)
        if subcase == "pattern_length" and len(self._pattern) > self.SAMPLE_DATA_LENGTH:
            # Won't be able to fit pattern into a single opcode
            return self._add_large_pattern_into_messages(messages)
        return self._add_pattern(seed, messages)

    def sample(self, seed, subcase):
        """Generate a sample message for testing.

        Uses the standard BooleanGenerator but ensures the generated message
        contains the pattern to be detected.

        Args:
            seed (int): The seed value to use for random number generation.
            subcase (str): Name of the subcase messages are to be generated
                for.

        Returns:
            Messages for the stated subcase.
        """
        messages = super().sample(seed, subcase)
        return self._add_pattern(seed, messages)

    def message_size(self, seed, subcase):
        """Generate different size messages for testing.

        Uses the standard BooleanGenerator but ensures the generated messages
        contains the pattern to be detected.

        Args:
            seed (int): The seed value to use for random number generation.
            subcase (str): Name of the subcase messages are to be generated for.

        Returns:
            Messages for the stated subcase.
        """
        messages = super().message_size(seed, subcase)
        return self._add_pattern(seed, messages, must_insert=False)

    def input_stressing(self, seed, subcase):
        """Generate messages when testing a port's handling of input stressing.

        Uses the standard BooleanGenerator but ensures the generated message
        contains the pattern to be detected.

        Args:
        Args:
            seed (int): The seed value to use for random number generation.
            subcase (str): Name of the subcase messages are to be generated for.

        Returns:
            Messages for the stated subcase.
        """
        messages = super().input_stressing(seed, subcase)
        return self._add_pattern(seed, messages)

    def soak(self, seed, subcase):
        """Generate messages for soak test cases.

        Uses the standard BooleanGenerator but ensures the generated message
        contains the pattern to be detected.

        Args:
            seed (int): The seed value to use for random number generation.
            subcase (str): Name of the subcase messages are to be generated for.

        Returns:
            Messages for the stated subcase.
        """
        messages = super().soak(seed, subcase)
        return self._add_pattern(seed, messages, must_insert=False)

    def pattern_cross_messages(self, seed, subcase):
        """Generate messages where the pattern is split over two sample opcodes.

        Args:
            seed (int): The seed value to use for random number generation.
            subcase (str): Name of the subcase messages are to be generated for.

        Returns:
            Messages for the stated subcase.
        """
        random.seed(seed)

        # Split the pattern into two and place into two messages
        split_pattern = [self._pattern[0:len(self._pattern)//2],
                         self._pattern[len(self._pattern)//2:]]
        stream_data = [self._get_sample_values(2*len(self._pattern)) for
                       _ in range(2)]

        messages = [
            {"opcode": "sample", "data": stream_data[0] + split_pattern[0]}]
        if subcase == "sample_only":
            # sample_only is a valid subcase, but nothing needs to be done
            pass
        elif subcase == "with_time":
            messages.append({
                "opcode": "time",
                "data": random.uniform(self.TIME_MIN,
                                       float(self.TIME_MAX))})
        elif subcase == "with_sample_interval":
            messages.append({
                "opcode": "sample_interval",
                "data": random.uniform(self.SAMPLE_INTERVAL_MIN,
                                       float(self.SAMPLE_INTERVAL_MAX))})
        elif subcase == "with_flush":
            messages.append({"opcode": "flush", "data": None})
        elif subcase == "with_discontinuity":
            messages.append({"opcode": "discontinuity", "data": None})
        elif subcase == "with_metadata":
            messages.append({
                "opcode": "metadata",
                "data": {
                    "id": random.randint(0, self.METADATA_ID_MAX),
                    "value": random.randint(
                        0, self.METADATA_VALUE_MAX)}})
        else:
            raise ValueError(
                f"subcase of {subcase} is not supported for a " +
                "pattern_cross_messages test case")

        messages.append(
            {"opcode": "sample", "data": split_pattern[1] + stream_data[1]})
        return messages

    def ignored_pattern_bits(self, seed, subcase):
        """Generate messages with pattern only matching when masked.

        While the mask and pattern property case will test a match, this test
        case is to test when the data does not match the pattern but incorrect
        bits are set to be ignored by the mask.

        Args:
            seed (int): Seed for the random number generator.
            subcase (str): Subcase label, which is ignored.

        Returns:
            list: A list of opcodes, with pattern inserted.
        """
        random.seed(seed)
        data = self._pattern + self._get_sample_values(2*len(self._pattern))

        for index, mask_bit in enumerate(self._bitmask):
            if mask_bit is False:
                data[index] = not (data[index])

        return [{"opcode": "sample", "data": data}]

    def _add_pattern(self, seed, messages, must_insert=True):
        """Adds bit pattern into one of the sample opcode.

        Args:
            seed (int): Seed for the random number generator.
            messages (list): The opcode messages to add the pattern in to.
            must_insert (bool, optional): Throw exception if a pattern cannot be inserted. Defaults to True.

        Raises:
            RuntimeError: Raised if the pattern is too long to insert into
                          any of the passed sample opcode and if
                          must_insert is true.

        Returns:
            list: The list of opcodes, with pattern inserted.
        """
        # For a random position in a message insert the pattern to be
        # matched against
        pattern_length = len(self._pattern)
        possible_messages = [
            index for index, message in enumerate(messages)
            if message["opcode"] == "sample" and
            len(message["data"]) >= pattern_length]
        if len(possible_messages) > 0:
            random.seed(seed)
            modify_message = random.choice(possible_messages)
            pattern_index = random.randint(
                0, len(messages[modify_message]["data"]) - pattern_length)
            messages[modify_message]["data"][
                pattern_index: pattern_index + pattern_length] = self._pattern
            print(f"INFO: Inserted pattern ({pattern_length}) into "
                  f"message {modify_message} at {pattern_index} bits")
        else:
            print("WARNING: FAILED TO INSERT PATTERN")
            if must_insert:
                raise RuntimeError("Data not long enough to insert pattern")
        return messages

    def _add_large_pattern_into_messages(self, messages):
        """Add pattern that might not fit in one sample opcode into several new sample opcodes.

        Args:
            messages (List): Opcode tuples

        Returns:
            list: The list of opcodes, with sample opcodes containing the pattern appended.
        """
        index = 0
        while index < len(self._pattern):
            messages.append({
                "opcode": "sample",
                "data": self._pattern[index:index+self.SAMPLE_DATA_LENGTH]})
            index += self.SAMPLE_DATA_LENGTH
        return messages


arguments = ocpi_testing.get_generate_arguments(
    ["pattern_cross_messages", "ignored_pattern_bits"])

subcase = os.environ["OCPI_TEST_subcase"]
pattern_length_property = int(os.environ.get("OCPI_TEST_pattern_length"))
metadata_value_property = int(os.environ.get("OCPI_TEST_metadata_value"))
metadata_value_id = int(os.environ.get("OCPI_TEST_metadata_id"))

# Need to read pattern/bitmask from file because OCPI truncates array inputs
# when they end with zeros - and that leads to not having the correct pattern.
pattern_input = os.environ.get("OCPI_TESTFILE_pattern")
bitmask_input = os.environ.get("OCPI_TESTFILE_bitmask")
with open(pattern_input) as in_file:
    pattern_property = [(value.strip().lower() == "true")
                        for value in in_file.readlines()]
with open(bitmask_input) as in_file:
    bitmask_property = [(value.strip().lower() == "true")
                        for value in in_file.readlines()]

if len(pattern_property) != pattern_length_property:
    raise RuntimeError(f"ERROR! Pattern (length: {len(pattern_property)}) does "
                       f"not match pattern_length_property ({pattern_length_property})")
if len(bitmask_property) != pattern_length_property:
    raise RuntimeError(f"ERROR! Bitmask (length: {len(bitmask_property)}) does "
                       f"not match pattern_length_property ({pattern_length_property})")

seed = ocpi_testing.get_test_seed(arguments.case, subcase,
                                  pattern_length_property,
                                  metadata_value_property,
                                  pattern_property,
                                  bitmask_property)

generator = LargePatternDetectorGenerator(pattern_property, bitmask_property)

# Generate the test data messages
messages = generator.generate(seed, arguments.case, subcase,
                              arguments.case_number, arguments.subcase_number)
with ocpi_protocols.WriteMessagesFile(
        arguments.save_path, "bool_timed_sample") as file_id:
    file_id.write_dict_messages(messages)
