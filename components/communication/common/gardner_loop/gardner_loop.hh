// Gardner Loop Implementation
//
// This file is protected by Copyright. Please refer to the COPYRIGHT file
// distributed with this source distribution.
//
// This file is part of OpenCPI <http://www.opencpi.org>
//
// OpenCPI is free software: you can redistribute it and/or modify it under the
// terms of the GNU Lesser General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option) any
// later version.
//
// OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
// more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

// LINT EXCEPTION: cpp_005: 2: Define use allowed in include guard
#ifndef COMPONENTS_COMMUNICATION_COMMON_GARDNER_LOOP_GARDNER_LOOP_HH_
#define COMPONENTS_COMMUNICATION_COMMON_GARDNER_LOOP_GARDNER_LOOP_HH_

// LINT EXCEPTION: cpp_008: 4: Include allowed after include guard
// LINT EXCEPTION: cpp_008: 4: Include allowed after include guard
// LINT EXCEPTION: cpp_008: 4: Include allowed after include guard
// LINT EXCEPTION: cpp_008: 4: Include allowed after include guard
#include <cstddef>
#include <cmath>
#include <type_traits>
#include <algorithm>

// Templates are expecting to be numerical types only
// LINT EXCEPTION: cpp_011: 3: Not a class definition, but template wrapped
// onto two lines
template <class input_t = OCPI::RCC::RCCFloat,
          class coefficient_t = OCPI::RCC::RCCFloat>
// LINT EXCEPTION: cpp_011: 2: New class definition allowed in common
// implementation
class gardner_loop {
  static_assert(std::is_arithmetic<input_t>::value,
                "input_t must be a numeric type");
  static_assert(std::is_arithmetic<coefficient_t>::value,
                "coefficient_t must be a numeric type");

 private:
  static const size_t history_length = 4;
  coefficient_t in_data_history[history_length];
  size_t index_0 = 0;  // Current/last index into circular history buffer
  size_t index_1 = history_length - 1;  // Index to "1 before last" in buffer
  size_t index_2 = history_length - 2;  // Index to "2 before last" in buffer
  size_t index_3 = history_length - 3;  // Index to "3 before last" in buffer
  size_t samples_per_symbol;
  size_t sample_offset;
  input_t ted_val;  // Timing error detector value

  // Alter circular indices and add sample into circular history buffer
  void update_input_history(input_t data) {
    index_0 = (index_0 + 1) % history_length;
    index_1 = (index_1 + 1) % history_length;
    index_2 = (index_2 + 1) % history_length;
    index_3 = (index_3 + 1) % history_length;
    in_data_history[index_0] = static_cast<coefficient_t>(data);
  }

  input_t gardner() const {
    // The Gardner loop has the form:
    //    Timing Error Detector value = Prompt x (Late - Early)
    coefficient_t ted = in_data_history[index_1] *
                        (in_data_history[index_0] - in_data_history[index_2]);

    // The data stream should be centred around zero, if zero crossing is
    // shifted by one position, the timing error detector produces an error
    // with the opposite sign. Adjust for this.
    if ((abs(in_data_history[index_0]) > abs(in_data_history[index_1])) &&
        (abs(in_data_history[index_2]) > abs(in_data_history[index_3]))) {
      ted = 1.0 - ted;
    }
    return static_cast<input_t>(ted);
  }

  // Returns true if a value is generated
  bool generate_gardner() {
    if ((std::signbit(in_data_history[index_0]) ==
         std::signbit(in_data_history[index_1])) &&
        (std::signbit(in_data_history[index_1]) ==
         std::signbit(in_data_history[index_2]))) {
      // No zero crossing in history, set to 0 to prevent runs of false
      // errors
      ted_val = 0;
    } else if (sample_offset % samples_per_symbol == 0) {
      // Normal Gardner, runs every 2nd sample
      ted_val = gardner();

      return true;
    } else {
      // On the alternating sample the value held and we return false
      ;
    }
    return false;
  }

 public:
  gardner_loop() { reset(); }

  void reset() {
    std::fill(in_data_history, in_data_history + history_length, 0);
    samples_per_symbol = 2;
    sample_offset = 0;
    ted_val = 0;
  }

  bool step(input_t sample) {
    update_input_history(sample);
    bool ok = generate_gardner();
    sample_offset = (sample_offset + 1) % samples_per_symbol;
    return ok;
  }

  input_t get_ted_val() const { return ted_val; }
};

// specialisations
template <>
// LINT EXCEPTION: cpp_002: 1: Function cannot be made static as templated
int8_t gardner_loop<int8_t, int16_t>::gardner() const {
  // The Gardner loop has the form:
  //    Timing Error Detector value = Prompt x (Late - Early)
  int16_t ted = (in_data_history[index_1] *
                 (in_data_history[index_0] - in_data_history[index_2]));
  if (ted < 0) {
    ted -= UINT8_MAX;
  }
  ted /= (static_cast<int16_t>(UINT8_MAX) + 1);

  // The data stream should be centred around zero, if zero crossing is
  //  shifted by one position, the timing error detector produces an error
  // with the opposite sign. Adjust for this.
  if ((abs(in_data_history[index_0]) > abs(in_data_history[index_1])) &&
      (abs(in_data_history[index_2]) > abs(in_data_history[index_3]))) {
    ted = INT8_MAX - ted;
  }
  return static_cast<int8_t>(ted & UINT8_MAX);
}

template <>
// LINT EXCEPTION: cpp_002: 1: Function cannot be made static as templated
int16_t gardner_loop<int16_t, int32_t>::gardner() const {
  // The Gardner loop has the form:
  //    Timing Error Detector value = Prompt x (Late - Early)
  int32_t ted = (in_data_history[index_1] *
                 (in_data_history[index_0] - in_data_history[index_2]));
  if (ted < 0) {
    ted -= UINT16_MAX;
  }
  ted /= (static_cast<int32_t>(UINT16_MAX) + 1);

  // The data stream should be centred around zero, if zero crossing is
  //  shifted by one position, the timing error detector produces an error
  // with the opposite sign. Adjust for this.
  if ((abs(in_data_history[index_0]) > abs(in_data_history[index_1])) &&
      (abs(in_data_history[index_2]) > abs(in_data_history[index_3]))) {
    ted = INT16_MAX - ted;
  }
  return static_cast<int16_t>(ted & UINT16_MAX);
}

template <>
// LINT EXCEPTION: cpp_002: 1: Function cannot be made static as templated
int32_t gardner_loop<int32_t, int64_t>::gardner() const {
  // The Gardner loop has the form:
  //    Timing Error Detector value = Prompt x (Late - Early)
  int64_t ted = (in_data_history[index_1] *
                 (in_data_history[index_0] - in_data_history[index_2]));
  if (ted < 0) {
    ted -= UINT32_MAX;
  }
  ted /= (static_cast<int64_t>(UINT32_MAX) + 1);

  // The data stream should be centred around zero, if zero crossing is
  //  shifted by one position, the timing error detector produces an error
  // with the opposite sign. Adjust for this.
  if ((abs(in_data_history[index_0]) > abs(in_data_history[index_1])) &&
      (abs(in_data_history[index_2]) > abs(in_data_history[index_3]))) {
    ted = INT32_MAX - ted;
  }
  return static_cast<int32_t>(ted & UINT32_MAX);
}

#endif  // COMPONENTS_COMMUNICATION_COMMON_GARDNERLOOP_GARDNERLOOP_HH_
