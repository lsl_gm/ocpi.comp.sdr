// RCC implementation of large_pattern_detector_b worker.
//
// This file is protected by Copyright. Please refer to the COPYRIGHT file
// distributed with this source distribution.
//
// This file is part of OpenCPI <http://www.opencpi.org>
//
// OpenCPI is free software: you can redistribute it and/or modify it under the
// terms of the GNU Lesser General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option) any
// later version.
//
// OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
// more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#include "large_pattern_detector_b-worker.hh"
#include <cstdint>
#include <bitset>
#include <algorithm>

using namespace OCPI::RCC;
using namespace Large_pattern_detector_bWorkerTypes;

class Large_pattern_detector_bWorker
    : public Large_pattern_detector_bWorkerBase {
  // Set pattern and bitmask lengths
  static const uint16_t pattern_length =
      LARGE_PATTERN_DETECTOR_B_PATTERN_LENGTH;
  std::bitset<pattern_length> pattern = 0;
  std::bitset<pattern_length> bitmask = 0;
  std::bitset<pattern_length> shift_register = 0;
  uint32_t metadata_id = 0;
  uint64_t metadata_value = 0;
  uint16_t samples_index = 0;
  bool insert_metadata = false;
  size_t bits_needed = pattern_length;

  RCCResult pattern_written() {
    for (uint16_t i = 0; i < this->pattern_length; i++) {
      this->pattern[i] = properties().pattern[i];
    }
    return RCC_OK;
  }

  RCCResult bitmask_written() {
    for (uint16_t i = 0; i < this->pattern_length; i++) {
      this->bitmask[i] = properties().bitmask[i];
    }
    return RCC_OK;
  }

  RCCResult metadata_value_written() {
    this->metadata_value = properties().metadata_value;
    return RCC_OK;
  }

  RCCResult metadata_id_written() {
    this->metadata_id = properties().metadata_id;
    return RCC_OK;
  }

  RCCResult run(bool) {
    if (this->insert_metadata) {
      // Insert metadata message
      output.setOpCode(Bool_timed_sampleMetadata_OPERATION);
      output.metadata().id() = this->metadata_id;
      output.metadata().value() = this->metadata_value;
      this->insert_metadata = false;
      if (this->samples_index == 0) {
        return RCC_ADVANCE;
      } else {
        output.advance();
        return RCC_OK;
      }
    }
    if (input.opCode() == Bool_timed_sampleSample_OPERATION) {
      uint16_t length = input.sample().data().size();
      const bool *inData =
          reinterpret_cast<const bool *>(input.sample().data().data());
      bool *outData = reinterpret_cast<bool *>(output.sample().data().data());
      output.setOpCode(Bool_timed_sampleSample_OPERATION);

      bool is_match = false;
      uint16_t samples_index_start = this->samples_index;
      auto pattern_masked = this->pattern & this->bitmask;
      while (this->samples_index < length) {
        this->shift_register >>= 1;
        this->shift_register[this->pattern_length - 1] =
            inData[this->samples_index++];
        if (this->bits_needed > 0) {
          this->bits_needed--;
        }
        // Check for match, if received enough bits since reset
        if (this->bits_needed == 0 && this->bitmask != 0) {
          auto shift_register_masked = this->shift_register & this->bitmask;
          if (shift_register_masked == pattern_masked) {
            is_match = true;
            break;
          }
        }
      }
      uint16_t samples = this->samples_index - samples_index_start;
      output.sample().data().resize(samples);
      std::copy_n(inData + samples_index_start, samples, outData);
      if (this->samples_index >= length) {
        this->samples_index = 0;
      }
      if (is_match) {
        this->insert_metadata = true;
        output.advance();
        return RCC_OK;
      } else {
        return RCC_ADVANCE;
      }
    } else if (input.opCode() == Bool_timed_sampleTime_OPERATION) {
      // Pass through time opcode and time data
      output.setOpCode(Bool_timed_sampleTime_OPERATION);
      output.time().fraction() = input.time().fraction();
      output.time().seconds() = input.time().seconds();
      return RCC_ADVANCE;
    } else if (input.opCode() == Bool_timed_sampleSample_interval_OPERATION) {
      // Pass through sample interval opcode and sample interval data
      output.setOpCode(Bool_timed_sampleSample_interval_OPERATION);
      output.sample_interval().fraction() = input.sample_interval().fraction();
      output.sample_interval().seconds() = input.sample_interval().seconds();
      return RCC_ADVANCE;
    } else if (input.opCode() == Bool_timed_sampleFlush_OPERATION) {
      // Pass through flush opcode and reset number of bits needed
      // before a pattern can be matched
      this->bits_needed = this->pattern_length;
      output.setOpCode(Bool_timed_sampleFlush_OPERATION);
      return RCC_ADVANCE;
    } else if (input.opCode() == Bool_timed_sampleDiscontinuity_OPERATION) {
      // Pass through discontinuity opcode and reset number of bits needed
      // before a pattern can be matched
      this->bits_needed = this->pattern_length;
      output.setOpCode(Bool_timed_sampleDiscontinuity_OPERATION);
      return RCC_ADVANCE;
    } else if (input.opCode() == Bool_timed_sampleMetadata_OPERATION) {
      // Pass through metadata opcode, id, and data
      output.setOpCode(Bool_timed_sampleMetadata_OPERATION);
      output.metadata().id() = input.metadata().id();
      output.metadata().value() = input.metadata().value();
      return RCC_ADVANCE;
    } else {
      setError("Unknown OpCode received");
      return RCC_FATAL;
    }
  }
};

LARGE_PATTERN_DETECTOR_B_START_INFO
// Insert any static info assignments here (memSize, memSizes, portInfo)
// e.g.: info.memSize = sizeof(MyMemoryStruct);
LARGE_PATTERN_DETECTOR_B_END_INFO
