.. polyphase_clock_synchroniser_dependencies

.. This file is protected by Copyright. Please refer to the COPYRIGHT file
   distributed with this source distribution.

   This file is part of OpenCPI <http://www.opencpi.org>

   OpenCPI is free software: you can redistribute it and/or modify it under the
   terms of the GNU Lesser General Public License as published by the Free
   Software Foundation, either version 3 of the License, or (at your option) any
   later version.

   OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>.

The dependencies to other elements in OpenCPI are:

 * :ref:`Down-sample protocol interface delay primitive v2 <downsample_protocol_interface_delay_v2-primitive>`

 * :ref:`Flush inserter primitive v2 <flush_inserter_v2-primitive>`

 * :ref:`Polyphase FIR Interpolator primitive <polyphase_interpolator-primitive>`

 * :ref:`Gardner Timing Error Detector primitive <gardner_timing_error_detector-primitive>`

 * ``components/communication/common/gardner_loop/gardner_loop.hh``

 * ``components/dsp/common/polyphase_interpolator/polyphase_interpolator.hh``

There is also dependencies on:

 * ``ieee.std_logic_1164``

 * ``ieee.numeric_std``

 * ``ieee.math_real``
