#!/usr/bin/env python3

# Generates the input binary file for Convolutioal Interleaver testing.
#
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
# more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

"""Generate script."""

import struct
import numpy as np
import sys
import os.path
import conv_inter_deinterleave_utils as cid_utils

if len(sys.argv) != 2:
    print("Don't run this script manually, it is called by 'ocpidev test' or 'make test'")
    sys.exit(1)

N = int(os.environ.get("OCPI_TEST_N"))
D = int(os.environ.get("OCPI_TEST_D"))
bit_width = int(os.environ.get("OCPI_TEST_bit_width"))

num_symbols = 2**bit_width
num_zeros_pad = 0

if (num_symbols > 1):
    num_zeros_pad = D*N*(N-1)
else:
    num_zeros_pad = 0

idata = cid_utils.generate_input_data(num_symbols, num_zeros_pad)
idata_length = len(idata)
ofilename = sys.argv[1]
with open(ofilename, "wb") as f:
    f.write(struct.pack("<I", idata_length*2))
    f.write(struct.pack("<I", 0))
    for i in range(0, idata_length):
        f.write(struct.pack("<H", idata[i]))
