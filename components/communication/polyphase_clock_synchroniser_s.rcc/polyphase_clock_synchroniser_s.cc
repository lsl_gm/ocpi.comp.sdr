// RCC implementation of polyphase_clock_synchroniser_s worker.
//
// This file is protected by Copyright. Please refer to the COPYRIGHT file
// distributed with this source distribution.
//
// This file is part of OpenCPI <http://www.opencpi.org>
//
// OpenCPI is free software: you can redistribute it and/or modify it under the
// terms of the GNU Lesser General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option) any
// later version.
//
// OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
// more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#include <memory>
#include "polyphase_clock_synchroniser_s-worker.hh"
#include "../../dsp/common/polyphase_interpolator/polyphase_interpolator.hh"
#include "../../math/common/scale_and_round.hh"
#include "../../math/common/time_utils.hh"
#include "../common/gardner_loop/gardner_loop.hh"
#include "OcpiDebugApi.hh"  // OCPI_LOG_INFO

using namespace OCPI::RCC;  // for easy access to RCC data types and constants
using namespace dsp;
using namespace Polyphase_clock_synchroniser_sWorkerTypes;

typedef dsp::polyphase_interpolator<int16_t, int64_t> poly_interpolator;

class Polyphase_clock_synchroniser_sWorker
    : public Polyphase_clock_synchroniser_sWorkerBase {

 public:
  // ***************************************************
  // Component Properties
  const uint16_t samples_per_symbol = 2;
  uint8_t interpolation_factor = properties().interpolation_factor;
  uint8_t branch_bit_depth =
      static_cast<uint8_t>(std::ceil(std::log2(interpolation_factor)));

  std::unique_ptr<poly_interpolator> real_interpolator;

  // Coefficients for the loop filter (Proportional, Integrator)
  // and NCO (Numerically-Controlled Oscillator) branch selection
  // N.B. Values are in S5.10 format (dividing by 2**10 or 1024 is their float
  // value), so defaults set that are consistent with user-settable options.
  int16_t k_p = 1536;      // Default is 1.5;
  int16_t k_i = 51;        // Default is 0.049804688 ~= 0.05;
  int16_t k_branch = 512;  // Default is 0.5;

  size_t number_of_taps = POLYPHASE_CLOCK_SYNCHRONISER_S_MAX_NUMBER_OF_TAPS;

  const enum ::Rounding_type rounding_type =
      POLYPHASE_CLOCK_SYNCHRONISER_S_ROUNDING_TYPE;

  // ***************************************************
  // Property updated callbacks

  RCCResult taps_written() {
    // No limits on range, so inputs not bounds checked
    // But check have the right number of taps
    size_t number_of_taps_passed_in =
        sizeof(properties().taps) / sizeof(properties().taps[0]);

    if (number_of_taps_passed_in > this->number_of_taps) {
      setError(
          "Invalid number of taps supplied, expected %lu, "
          "but given %lu",
          this->number_of_taps, number_of_taps_passed_in);
      return RCC_FATAL;
    }

    // Taps are in the range (-1, 1) and passed in S0.15 format - i.e. 1 sign
    // bit and 15 fractional bits.  i.e. a float multiplied by 2**15 and cast
    // to an integer.
    if (this->real_interpolator != nullptr) {
      for (size_t index = 0; index < number_of_taps_passed_in; index++) {
        int16_t tap_value = properties().taps[index];
        this->real_interpolator->set_tap(index, tap_value);
      }
    }

    return RCC_OK;
  }

  RCCResult loop_coefficients_written() {
    // No limits on range, so inputs not bounds checked
    // But will check there are 3 coefficients.
    size_t num_coefficients = sizeof(properties().loop_coefficients) /
                              sizeof(properties().loop_coefficients[0]);
    if (num_coefficients != 3) {
      setError("Invalid number of coefficients, expected 3.");
      return RCC_FATAL;
    }

    // Values are in S5.10 format (dividing by 2**10 or 1024 is their float
    // value)
    this->k_p = properties().loop_coefficients[0];
    this->k_i = properties().loop_coefficients[1];
    this->k_branch = properties().loop_coefficients[2];
    return RCC_OK;
  }

  // ***************************************************
  // Internal state
  gardner_loop<int16_t, int32_t> timing_error_detector;
  sdr_math::rounding_type polyphase_rounding_type =
      sdr_math::rounding_type::MATH_TRUNCATE;

  int16_t interpolator_value = 0;  // From the polyphase interpolation filter
  int16_t timing_error_value = 0;  // From Gardner loop timing error detector
  int32_t error_i = 0;             // Ei from loop filter
  int32_t error_value = 0;         // Err from loop filter
  bool skip_symbol = false;
  bool stuff_symbol = false;

  const size_t downsample_ratio = 2;  // Ratio by which to downsample
  size_t downsample_index = 0;        // Downsampling index used on output
  size_t sample_offset = 0;           // Count of samples processed (up to sps)
  bool received_data = false;         // Samples received.

  int32_t branch_nco = 0;

  uint32_t interval_seconds = 0;   // Outgoing Sample_Interval seconds part
  uint64_t interval_fraction = 0;  // Outgoing Sample_Interval fractional part

  uint32_t time_seconds = 0;   // Outgoing timestamp seconds part
  uint64_t time_fraction = 0;  // Outgoing timestamp fractional part

  // ***************************************************
  // ControlOperations Functions

  RCCResult start() {
    this->interpolation_factor = properties().interpolation_factor;
    this->branch_bit_depth =
        static_cast<uint8_t>(std::ceil(std::log2(interpolation_factor)));
    this->number_of_taps = properties().number_of_taps;
    size_t taps_per_branch = static_cast<uint16_t>(
        std::ceil(static_cast<RCCFloat>(this->number_of_taps) /
                  static_cast<RCCFloat>(this->interpolation_factor)));

    switch (this->rounding_type) {
      case POLYPHASE_CLOCK_SYNCHRONISER_S_ROUNDING_TYPE_TRUNCATE:
        polyphase_rounding_type = sdr_math::rounding_type::MATH_TRUNCATE;
        break;
      case POLYPHASE_CLOCK_SYNCHRONISER_S_ROUNDING_TYPE_HALF_UP:
        polyphase_rounding_type = sdr_math::rounding_type::MATH_HALF_UP;
        break;
      case POLYPHASE_CLOCK_SYNCHRONISER_S_ROUNDING_TYPE_HALF_EVEN:
        polyphase_rounding_type = sdr_math::rounding_type::MATH_HALF_EVEN;
        break;
      default:
        polyphase_rounding_type = sdr_math::rounding_type::MATH_TRUNCATE;
    }

    this->real_interpolator =
        std::unique_ptr<poly_interpolator>(new (std::nothrow) poly_interpolator(
            this->interpolation_factor, taps_per_branch,
            polyphase_rounding_type, sdr_math::overflow_type::MATH_WRAP));

    if (this->real_interpolator != nullptr) {
      for (size_t index = 0; index < this->number_of_taps; index++) {
        int16_t tap_value = properties().taps[index];
        this->real_interpolator->set_tap(index, tap_value);
      }
    } else {
      setError("Failed to allocate interpolator");
      return RCC_FATAL;
    }

    // Reset/Initialise state, interpolator and gardner loop
    this->real_interpolator->set_branch(0);
    this->branch_nco = this->real_interpolator->get_branch();
    reset();
    return RCC_OK;
  }

  RCCResult run(bool) {
    // Determine opcode
    if (input.opCode() == Short_timed_sampleSample_OPERATION) {
      // Incoming samples are consumed, leading to the internal
      // counter and buffers to step on each sample
      // Outgoing symbols are produced at the rate defined through
      // the samples per symbol property.

      const size_t input_length = input.sample().data().size();

      output.setOpCode(Short_timed_sampleSample_OPERATION);

      const int16_t* input_data_pointer = input.sample().data().data();

      if (input_length > 0) {
        this->received_data = true;
      }

      std::vector<int16_t> output_values =
          process_samples(input_data_pointer, input_length);
      downsample_into_output(output_values);
    } else if (input.opCode() == Short_timed_sampleTime_OPERATION) {
      // Time is time of next sample, so output time now
      output.setOpCode(Short_timed_sampleTime_OPERATION);
      if ((this->downsample_index % downsample_ratio) != 1) {
        output.time().seconds() =
            input.time().seconds() + this->interval_seconds;
        output.time().fraction() =
            input.time().fraction() + this->interval_fraction;
      } else {
        output.time().seconds() = input.time().seconds();
        output.time().fraction() = input.time().fraction();
      }
      log(OCPI_LOG_DEBUG, "Time to output:  %u, %lu", output.time().seconds(),
          output.time().fraction());
      return RCC_ADVANCE;
    } else if (input.opCode() == Short_timed_sampleSample_interval_OPERATION) {
      // Calculate and store outgoing interval use for later time calculations.
      // Outgoing Sample Interval is set to be multiple of
      // Incoming Sample_Interval (where the sample per symbol
      //                              is the multiplicand)
      this->interval_seconds = input.sample_interval().seconds();
      this->interval_fraction = input.sample_interval().fraction();
      time_utils::multiply(&this->interval_seconds, &this->interval_fraction,
                           this->samples_per_symbol);
      // Forward on the Outgoing Sample Interval
      output.setOpCode(Short_timed_sampleSample_interval_OPERATION);
      output.sample_interval().seconds() = this->interval_seconds;
      output.sample_interval().fraction() = this->interval_fraction;
    } else if (input.opCode() == Short_timed_sampleFlush_OPERATION) {
      const size_t flush_length =
          (POLYPHASE_CLOCK_SYNCHRONISER_S_MAX_NUMBER_OF_TAPS /
           this->interpolation_factor);
      // Pad input with zeros
      if (this->received_data) {
        int16_t empty_buffer[flush_length];
        std::fill_n(empty_buffer, flush_length, 0);

        output.setOpCode(Short_timed_sampleSample_OPERATION);
        std::vector<int16_t> output_values =
            process_samples(empty_buffer, flush_length);
        downsample_into_output(output_values);

        // Return buffer now, flush will be forwarded on next run
        this->received_data = false;
        output.advance();
        return RCC_OK;
      }

      // Forward flush
      output.setOpCode(Short_timed_sampleFlush_OPERATION);
      this->downsample_index = 0;
      this->sample_offset = 0;
      this->timing_error_detector.reset();
      this->real_interpolator->reset();
    } else if (input.opCode() == Short_timed_sampleDiscontinuity_OPERATION) {
      output.setOpCode(Short_timed_sampleDiscontinuity_OPERATION);
      reset();
    } else if (input.opCode() == Short_timed_sampleMetadata_OPERATION) {
      // Forward metadata
      output.setOpCode(Short_timed_sampleMetadata_OPERATION);
      output.metadata().id() = input.metadata().id();
      output.metadata().value() = input.metadata().value();
    } else {
      // Unexpected opcode
      setError("Unexpected OpCode (%d)", input.opCode());
      return RCC_FATAL;
    }

    return RCC_ADVANCE;
  }

  // ***************************************************
  // Functions

  void reset() {
    this->timing_error_value = 0;
    this->error_i = 0;
    this->error_value = 0;

    this->skip_symbol = false;
    this->stuff_symbol = false;

    this->downsample_index = 0;
    this->sample_offset = 0;
    this->received_data = false;

    this->branch_nco = 0;

    this->timing_error_detector.reset();
    this->real_interpolator->reset();
  }

  // Write the time-of-next-sample timestamp into the time opcode output buffer
  void write_timestamp() {
    uint32_t interval_seconds_scaled = this->interval_seconds;
    uint64_t interval_fraction_scaled = this->interval_fraction;
    time_utils::multiply(&interval_seconds_scaled, &interval_fraction_scaled,
                         this->samples_per_symbol);
    time_utils::add(&this->time_seconds, &this->time_fraction,
                    interval_seconds_scaled, interval_fraction_scaled);
    output.time().seconds() = this->time_seconds;
    output.time().fraction() = this->time_fraction;
  }

  // Round the calculated branch value into a integer in the correct range
  int8_t round_branch(int32_t value) const {
    return (value >> (32 - (this->branch_bit_depth))) &
           static_cast<int8_t>(
               (static_cast<int16_t>(1) << this->branch_bit_depth) - 1);
  }

  // Process the given array of samples, passing them through the polyphase
  // interpolator, performing timing error detection and branch interpolation
  // branch selection.  Returns a vector containing the to-be-output samples
  std::vector<int16_t> process_samples(const int16_t* samples, size_t length) {
    // Create array to hold output
    std::vector<int16_t> output_values;
    // For efficiency, reserve space now (assuming 75% of input will
    // be sufficient for most conditions).
    output_values.reserve(length * 3 / 4);

    for (size_t i = 0; i < length; i++) {
      int16_t value = samples[i];

      if (this->stuff_symbol) {
        this->stuff_symbol = false;

        // Generate two values
        int16_t p1 = this->real_interpolator->step(value);
        this->real_interpolator->set_branch(round_branch(this->branch_nco));
        int16_t p2 = this->real_interpolator->_step();

        // Choose the peak value, not the zero crossing value
        if (std::abs(p1) > std::abs(p2)) {
          this->interpolator_value = p1;
        } else {
          this->interpolator_value = p2;
        }

        // As there are only 2 SPS then there is no purpose to rotating 1
        // symbol backwards and instead need to ADD 2 additional samples
        // (ie. one peak, and one zero)
        output_values.push_back(p1);
        output_values.push_back(p2);
        log(OCPI_LOG_DEBUG,
            "sample: %d, pif: %d(None), ted: None, nco: %d : %d - STUFF", value,
            this->interpolator_value, this->branch_nco,
            round_branch(this->branch_nco));
        continue;

      } else if (this->skip_symbol) {
        // Skip this sample, but still update the interpolator
        this->skip_symbol = false;
        this->real_interpolator->step(value);
        // Move onto next sample without adding to output
        log(OCPI_LOG_DEBUG,
            "sample: %d, pif: %d(None), ted: None, nco: %d : %d - SKIP", value,
            this->interpolator_value, this->branch_nco,
            round_branch(this->branch_nco));
        continue;
      }

      // Update the interpolator to the desired branch, then calculate
      // the new interpolant
      this->real_interpolator->set_branch(round_branch(this->branch_nco));
      this->interpolator_value = this->real_interpolator->step(value);

      // Keep count of the sample number
      this->sample_offset =
          (this->sample_offset + 1) % this->samples_per_symbol;

      // Output the interpolant.
      output_values.push_back(this->interpolator_value);

      // Calculate Timing Error
      if (this->timing_error_detector.step(this->interpolator_value)) {
        this->timing_error_value = this->timing_error_detector.get_ted_val();
      } else {
        log(OCPI_LOG_DEBUG, "sample: %d, pif: %d, ted: None, nco: %d : %d",
            value, this->interpolator_value, this->branch_nco,
            round_branch(this->branch_nco));
        continue;
      }

      // Do Loop Filter and branch selection
      loop_filter_and_branch_selection();
      log(OCPI_LOG_DEBUG, "sample: %d, pif: %d, ted: %d, nco: %d : %d", value,
          this->interpolator_value, this->timing_error_value, this->branch_nco,
          round_branch(this->branch_nco));
    }

    return output_values;
  }

  // Perform a second order phase-locker loop filter to smooth the variation
  // in output of the timing error detector, and select the appropriate branch
  // of the polyphase interpolator
  void loop_filter_and_branch_selection() {
    // Loop Filter (PI)
    this->error_i += (static_cast<int32_t>(this->timing_error_value) *
                      static_cast<int32_t>(this->k_i));
    this->error_value = (static_cast<int32_t>(this->timing_error_value) *
                         static_cast<int32_t>(this->k_p)) +
                        this->error_i;

    // Error Accumulator, used to select the active branch
    this->branch_nco +=
        (this->error_value >> 16) * static_cast<int32_t>(this->k_branch);

    // Drift - Handling wrap around events at the edge of the interpolation
    // range. Mask the glitch in the error corrector through skip and stuffing
    // wrap the branch, with a slight margin, to handle rounding error
    uint8_t selected_branch = round_branch(this->branch_nco);
    if (selected_branch == (this->interpolation_factor - 1) &&
        this->real_interpolator->get_branch() == 0) {
      this->stuff_symbol = true;
    } else if (this->real_interpolator->get_branch() ==
                   (this->interpolation_factor - 1) &&
               selected_branch == 0) {
      this->skip_symbol = true;
    }
  }

  // Given a vector of to-be-output samples, copy them into the sample opcode
  // output buffer, but only those that are selected by the downsample ratio.
  void downsample_into_output(std::vector<int16_t> samples) {
    const size_t out_len =
        (samples.size() + (this->downsample_index % downsample_ratio)) /
        downsample_ratio;

    output.sample().data().resize(out_len);
    int16_t* out_data_pointer = output.sample().data().data();

    // Copy values into output
    for (auto it = samples.begin(); it != samples.end(); ++it) {
      if (this->downsample_index++ % downsample_ratio == 1) {
        *out_data_pointer++ = *it;
      }
    }
  }
};

POLYPHASE_CLOCK_SYNCHRONISER_S_START_INFO
// Insert any static info assignments here (memSize, memSizes, portInfo)
// e.g.: info.memSize = sizeof(MyMemoryStruct);
POLYPHASE_CLOCK_SYNCHRONISER_S_END_INFO
