<?xml version="1.0"?>
<!-- This file is protected by Copyright. Please refer to the COPYRIGHT file
     distributed with this source distribution.

     This file is part of OpenCPI <http://www.opencpi.org>

     OpenCPI is free software: you can redistribute it and/or modify it under
     the terms of the GNU Lesser General Public License as published by the Free
     Software Foundation, either version 3 of the License, or (at your option)
     any later version.

     OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
     WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
     FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
     more details.

     You should have received a copy of the GNU Lesser General Public License
     along with this program. If not, see <http://www.gnu.org/licenses/>. -->
<tests usehdlfileio="true">
  <property name="subcase" test="true" type="string"/>
  <case>
    <input port="input" script="generate.py --case typical" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="samples_per_symbol" value="5"/>
    <property name="sample_point" value="2"/>
  </case>
  <case>
    <input port="input" script="generate.py --case property" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="samples_per_symbol" values="2,3,4,5,6,7,8,9,10,11,12,15,16,31,32,127,128,255,256,512"/>
    <property name="sample_point" value="1"/>
    <property name="subcase" value="samples_per_symbol"/>
  </case>
  <case>
    <input port="input" script="generate.py --case property" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="samples_per_symbol" value="63"/>
    <property name="sample_point" values="0,1,62,31,56,16,11,29,2,51"/>
    <property name="subcase" value="sample_point"/>
  </case>
  <!-- Cannot currently verify as no way to determine when property changed in
       relation to data. -->
  <!--
  <case>
  <input port="input" script="generate.py -c property_change" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="samples_per_symbol">
      <set delay="0" value="5"/>
      <set delay="1" value="6"/>
    </property>
    <property name="sample_point" value="2"/>
    <property name="subcase" value="samples_per_symbol"/>
  </case>
  <case>
  <input port="input" script="generate.py -c property_change" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="samples_per_symbol" value="5"/>
    <property name="sample_point">
      <set delay="0" value="2"/>
      <set delay="1" value="4"/>
    </property>
    <property name="subcase" value="sample_point"/>
  </case>
  -->
  <case>
    <input port="input" script="generate.py --case sample" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="samples_per_symbol" value="5"/>
    <property name="sample_point" value="2"/>
    <property name="subcase" values="all_zero,all_maximum"/>
  </case>
  <case>
    <input port="input" script="generate.py --case input_stressing" messagesinfile="true" messagesize="16384" stressormode="full"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="samples_per_symbol" value="5"/>
    <property name="sample_point" value="2"/>
  </case>
  <case>
    <input port="input" script="generate.py -c message_size" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="samples_per_symbol" value="5"/>
    <property name="sample_point" value="1"/>
    <property name="subcase" values="shortest,longest"/>
  </case>
  <!-- Custom Test - Symbol clock slower and faster than set value -->
  <case>
    <input port="input" script="generate.py --case custom" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="samples_per_symbol" value="9"/>
    <property name="sample_point" value="4"/>
    <property name="subcase" values="slower,faster"/>
  </case>
  <!-- Custom Test - Check timestamp is correctly advanced -->
  <case>
    <input port="input" script="generate.py --case custom" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="samples_per_symbol" value="5"/>
    <property name="sample_point" value="2"/>
    <property name="subcase" value="timestamp"/>
  </case>
  <case>
    <input port="input" script="generate.py --case time" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="samples_per_symbol" value="5"/>
    <property name="sample_point" value="2"/>
    <property name="subcase" values="zero,positive,maximum,consecutive"/>
  </case>
  <case>
    <input port="input" script="generate.py --case sample_interval" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="samples_per_symbol" value="5"/>
    <property name="sample_point" value="2"/>
    <property name="subcase" values="zero,positive,maximum,consecutive"/>
  </case>
  <case>
    <input port="input" script="generate.py --case flush" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="samples_per_symbol" value="5"/>
    <property name="sample_point" value="2"/>
    <property name="subcase" values="single,consecutive"/>
  </case>
  <case>
    <input port="input" script="generate.py --case discontinuity" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="samples_per_symbol" value="5"/>
    <property name="sample_point" value="2"/>
    <property name="subcase" values="single,consecutive"/>
  </case>
  <case>
    <input port="input" script="generate.py --case metadata" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="samples_per_symbol" value="5"/>
    <property name="sample_point" value="2"/>
    <property name="subcase" values="zero,positive,maximum,consecutive"/>
  </case>
  <case>
    <input port="input" script="generate.py --case soak" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="samples_per_symbol" values="745,83,20,444"/>
    <property name="sample_point" values="1,10"/>
    <property name="subcase" values="sample_only,all_opcodes"/>
  </case>
</tests>
