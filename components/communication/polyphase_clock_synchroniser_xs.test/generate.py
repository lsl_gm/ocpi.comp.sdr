#!/usr/bin/env python3

# Use this file to generate your input data.
#
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
# more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

"""Generator for the Input port for Polyphase Clock Synchroniser."""

import os
import random
import decimal
import numpy as np

import opencpi.ocpi_protocols as ocpi_protocols
import opencpi.ocpi_testing as ocpi_testing

from sdr_communication._polyphase_clock_synchroniser.symbol_generator import SymbolGenerator


class PolyphaseClockSyncGenerator(ocpi_testing.generator.ComplexShortGenerator):
    """Polyphase Clock Synchroniser Generator Class."""

    def __init__(self, samples_per_symbol=2, mod_order=2, **kwds):
        """Initialise generator class for a polyphase clock synchroniser.

        Defines the default values for the variables that control the values
        and size of messages that are generated.

        Returns:
            An initialised ComplexShortGenerator instance.
        """
        super().__init__()

        # Sample range is reduced to prevent out of range samples after
        # FIR taps are applied
        self.SHORT_MINIMUM += 2500
        self.SHORT_MAXIMUM -= 2500

        # The component multiplies the sample rate by the samples per symbol
        # while passing through this component, as such, ensure that the
        # defined maximum rate begins at half the element maximum.
        self.SAMPLE_INTERVAL_MAX /= decimal.Decimal(2)

        if mod_order == 2:
            symbols = [-self.TYPICAL_AMPLITUDE_MEAN,
                       self.TYPICAL_AMPLITUDE_MEAN]
        elif mod_order == 4:
            symbols = [-self.TYPICAL_AMPLITUDE_MEAN,
                       -self.TYPICAL_AMPLITUDE_MEAN / 3,
                       self.TYPICAL_AMPLITUDE_MEAN / 3,
                       self.TYPICAL_AMPLITUDE_MEAN]
        else:
            raise ValueError(
                "Symbol order {} is not recognised.".format(mod_order))

        n_symbols = np.ceil(self.SAMPLE_DATA_LENGTH/samples_per_symbol)
        self._n_symbols = n_symbols
        self._symgen = SymbolGenerator(
            n_symbols=n_symbols, symbols=symbols, **kwds)

    def _get_sample_values(self, number_of_samples=None):
        """Generate a random symbol sequence of complex shorts.

        The values generated are a subset of the whole supported range that
        shorts can represent, this range size is set by
        self.LIMITED_SCALE_FACTOR.

        Args:
            number_of_samples (int, optional): The number of random values to
                be generated. If not set will default to None, which means the
                number of samples defined by self.SAMPLE_DATA_LENGTH will be
                generated.

        Returns:
            List of the generated random values.
        """
        if number_of_samples is None:
            number_of_samples = self.SAMPLE_DATA_LENGTH

        real_data = [int(x) for x in np.floor(self._symgen.generate())]
        imag_data = [int(x) for x in np.floor(self._symgen.generate())]
        data = []
        for index in range(0, len(real_data)):
            data.append(np.complex(real_data[index], imag_data[index]))
        return data

    def typical(self, seed, subcase):
        """Generate a sample message with typical data inputs.

        Typical behaviour for clock sync edge detector is x repeated values,
        where x is equal to the number of symbols per sample, so override
        the default typical behaviour for this particular case.


        Args:
            seed (int): The seed value to use for random number generation.
            subcase (str): Name of the subcase messages are to be generated
                for.

        Returns:
            Messages for the typical case and the stated subcase.
        """
        self._symgen.seed = seed
        return [
            {"opcode": "sample", "data": self._get_sample_values()}]

    def soak(self, seed, subcase):
        """Generate messages for soak test cases.

        Args:
            seed (int): The seed value to use for random number generation.
            subcase (str): Name of the subcase messages are to be generated
                for.

        Returns:
            Messages for the requested soak test subcase.
        """
        self._symgen.seed = seed
        random.seed(seed)

        # Set the number and opcode of messages
        number_of_messages = round(random.gauss(
            self.SOAK_ALL_OPCODE_AVERAGE_NUMBER_OF_MESSAGES,
            self.SOAK_ALL_OPCODE_STANDARD_DEVIATION_NUMBER_OF_MESSAGES))
        if number_of_messages > 8:
            number_of_messages = 8
        elif number_of_messages < 1:
            number_of_messages = 1

        messages = []

        if subcase == "sample_only":
            message_lengths = [0] * number_of_messages

            # The total of all messages should be self.SAMPLE_DATA_LENGTH
            for index in range(number_of_messages - 1):
                remaining_messages = number_of_messages - index - 1
                # Set a maximum which ensures that all messages have at least
                # 5 symbols.
                minimum_message_length = self._symgen.samples_per_symbol*5
                maximum_message_length = (self.SAMPLE_DATA_LENGTH -
                                          sum(message_lengths) -
                                          (remaining_messages * minimum_message_length))
                message_lengths[index] = random.randint(
                    minimum_message_length, maximum_message_length)
            # The final message will have a length to ensure that all sample
            # messages have a length which totals self.SAMPLE_DATA_LENGTH.
            message_lengths[-1] = self.SAMPLE_DATA_LENGTH - sum(
                message_lengths[0:-1])

            if message_lengths[-1] == 0:
                del message_lengths[-1]

            values = self._get_sample_values()
            n = 0
            for length in message_lengths:
                messages.append({
                    "opcode": "sample",
                    "data": values[n:n+length]})
                n += length

        elif subcase == "all_opcodes":
            sample_data_included = 0
            sample_message_size = self.SAMPLE_DATA_LENGTH // number_of_messages

            values = self._get_sample_values()
            n = 0

            for opcode in random.choices(list(ocpi_protocols.OPCODES.keys()),
                                         k=number_of_messages):
                # As opcode keys are opcode names and values, if an int this
                # will be the value. In which case get the name.
                if isinstance(opcode, int):
                    opcode = ocpi_protocols.OPCODES[opcode]

                if opcode == "sample":
                    messages.append({
                        "opcode": "sample",
                        "data": values[n:n+sample_message_size]})
                    n += sample_message_size
                    sample_data_included = (sample_data_included +
                                            sample_message_size)
                elif opcode == "time":
                    messages.append({
                        "opcode": "time",
                        "data": random.uniform(self.TIME_MIN,
                                               float(self.TIME_MAX))})
                elif opcode == "sample_interval":
                    messages.append({
                        "opcode": "sample_interval",
                        "data": random.uniform(self.SAMPLE_INTERVAL_MIN,
                                               float(self.SAMPLE_INTERVAL_MAX))
                    })
                elif opcode == "flush":
                    messages.append({"opcode": "flush", "data": None})
                elif opcode == "discontinuity":
                    messages.append({"opcode": "discontinuity", "data": None})
                elif opcode == "metadata":
                    messages.append({
                        "opcode": "metadata",
                        "data": {
                            "id": random.randint(0, self.METADATA_ID_MAX),
                            "value": random.randint(
                                0, self.METADATA_VALUE_MAX)}})
                else:
                    raise ValueError(
                        f"Unexpected opcode of {opcode} for soak()")

            # Add a final sample message to ensure the total length of all
            # sample messages is self.SAMPLE_DATA_LENGTH. But don't add
            # a zero length message.
            if (self.SAMPLE_DATA_LENGTH - sample_data_included):
                messages.append({
                    "opcode": "sample",
                    "data": values[n:len(values)]})

        else:
            raise ValueError(f"Unexpected subcase of {subcase} for soak()")

        return messages


arguments = ocpi_testing.get_generate_arguments()

subcase = os.environ["OCPI_TEST_subcase"]
phase_offset = float(os.environ["OCPI_TEST_phase_offset"])
frequency_offset = float(os.environ["OCPI_TEST_frequency_offset"])
awgn_level = float(os.environ["OCPI_TEST_awgn_level"])

seed = ocpi_testing.get_test_seed(
    arguments.case, subcase, phase_offset,
    frequency_offset, awgn_level)

generator = PolyphaseClockSyncGenerator(
    samples_per_symbol=2, phase_offset=phase_offset,
    frequency_offset=frequency_offset,
    awgn_level=awgn_level)

# Generate the test data messages
messages = generator.generate(seed, arguments.case, subcase,
                              arguments.case_number, arguments.subcase_number)

with ocpi_protocols.WriteMessagesFile(
        arguments.save_path, "complex_short_timed_sample") as file_id:
    file_id.write_dict_messages(messages)
