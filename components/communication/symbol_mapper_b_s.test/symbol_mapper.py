#!/usr/bin/env python3

# Python implementation of symbol mapper block for verification
#
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
# more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

"""Python implementation of symbol mapper block for verification."""

import opencpi.ocpi_protocols as ocpi_protocols
import opencpi.ocpi_testing as ocpi_testing


class SymbolMapper(ocpi_testing.Implementation):
    """The symbol mapper class."""

    def __init__(self, space_symbol, mark_symbol):
        """Symbol Mapper.

        Args:
            space_symbol (int): Value to use for space symbol in output stream.
            mark_symbol (int): Value to use for mark symbol in output stream.
        """
        super().__init__(space_symbol=space_symbol, mark_symbol=mark_symbol)

    def reset(self):
        """Re-initialises internal state."""
        pass

    def sample(self, values):
        """Processes the sample opcode, and generates output data.

        Args:
            values (list of bool): Sample input values

        Returns:
            Formatted message for output containing one or more sample
            opcodes containing mark and space values.
        """
        output_values = [self.space_symbol] * len(values)
        for index, value in enumerate(values):
            if value is True:
                output_values[index] = self.mark_symbol

        max_message_length = ocpi_protocols.PROTOCOLS[
            "short_timed_sample"].max_sample_length
        messages = []
        for index in range(0, len(values), max_message_length):
            messages.append(
                {"opcode": "sample",
                 "data": output_values[index: index + max_message_length]})

        return self.output_formatter(messages)
