#!/usr/bin/env python3

# Use this script to validate your output data against your input data.
#
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
# more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

"""Verifier script for the Polyphase Clock Synchroniser (Char)."""

import os
import sys
import numpy as np

import opencpi.ocpi_testing as ocpi_testing

from sdr_communication.polyphase_clock_synchroniser import PolyphaseClockSynchroniserInt


taps_fractional_bits = 8
coeff_fractional_bits = 4

log_level = int(os.environ.get("OCPI_LOG_LEVEL", 0))
phase_offset = float(os.environ["OCPI_TEST_phase_offset"])
frequency_offset = float(os.environ["OCPI_TEST_frequency_offset"])
number_of_taps = int(os.environ["OCPI_TEST_number_of_taps"])
interpolation_factor = int(os.environ["OCPI_TEST_interpolation_factor"])
rounding_type = os.environ.get("OCPI_TEST_rounding_type", "truncate")

filter_taps = [0] * (number_of_taps)
# OCPI adds a 0 at the end of the array, (and then stops reading when it finds a string of zeros)
for i, x in enumerate((os.environ["OCPI_TEST_taps"]).split(",")):
    if i < (number_of_taps):
        filter_taps[i] = int(x)
    else:
        if int(x) != 0:
            raise ValueError("Taps contains too many values (expected: {}, got: {})\n{}"
                             .format(number_of_taps,
                                     len((os.environ["OCPI_TEST_taps"]).split(
                                         ",")),
                                     (os.environ["OCPI_TEST_taps"]).split(",")))


assert (number_of_taps) == len(filter_taps), \
    "There have not been enough taps provided"

loop_coefficients = [int(x) for x in (
    os.environ["OCPI_TEST_loop_coefficients"]).split(",")]

assert np.all([l < 2**7 for l in loop_coefficients]), \
    "Loop coefficient out of char range"

if log_level > 9:
    verify_log_level = "DEBUG"
elif log_level > 5:
    verify_log_level = "INFO"
else:
    verify_log_level = "WARNING"

assert np.nonzero(filter_taps)[0][-1] < number_of_taps, (
    "The expected number of taps does not match the tap array, "
    f"Expected {number_of_taps}, "
    f"Actual: {len(filter_taps)} \n{filter_taps}")

input_file_path = str(sys.argv[-1])
output_file_path = str(sys.argv[-2])

test_id = ocpi_testing.get_test_case()

clock_sync_implementation = PolyphaseClockSynchroniserInt(
    samples_per_symbol=2,
    max_interpolation=interpolation_factor,
    filter_taps=filter_taps, loop_coefficients=loop_coefficients,
    logging_level=verify_log_level, case=test_id, rounding_type=rounding_type,
    output_depth=8, input_depth=8, internal_depth=16)


verifier = ocpi_testing.Verifier(clock_sync_implementation)
# Bounded with exception needed due to differing latencies on interpolation
# branch being updated between the different models.
verifier.set_port_types(
    ["char_timed_sample"], ["char_timed_sample"], ["bounded_with_exception"])
verifier.comparison[0].BOUND = 1
verifier.comparison[0].EXCEPTION_BOUND = 128
verifier.comparison[0].ALLOWED_EXCEPTION_RATE = 0.1
if verifier.verify(test_id, [input_file_path], [output_file_path]) is True:
    sys.exit(0)
else:
    sys.exit(1)
