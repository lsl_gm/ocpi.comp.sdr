// divider_s RCC worker.
//
// This file is protected by Copyright. Please refer to the COPYRIGHT file
// distributed with this source distribution.
//
// This file is part of OpenCPI <http://www.opencpi.org>
//
// OpenCPI is free software: you can redistribute it and/or modify it under the
// terms of the GNU Lesser General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option) any
// later version.
//
// OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
// more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#include "divider_s-worker.hh"
#include <algorithm>
#include <cmath>

using namespace OCPI::RCC;  // for easy access to RCC data types and constants
using namespace Divider_sWorkerTypes;

class Divider_sWorker : public Divider_sWorkerBase {
  size_t input_unprocessed_data = 0;
  size_t denominator_unprocessed_data = 0;

  bool output_is_connected;
  bool remainder_is_connected;
  int16_t *output_data;
  int16_t *remainder_data;
  const int16_t *input_data;
  const int16_t *denominator_data;

  RCCResult start() {
    output_is_connected = (output.isConnected());
    remainder_is_connected = (remainder.isConnected());
    return RCC_OK;
  }

  // The actual division methodology to use, for a single value
  inline std::pair<int16_t, int16_t> division(int16_t numerator,
                                              int16_t denominator) {
    int16_t quotient;
    int16_t remainder;
    // The divide by 0 case is altered to allow simpler implementation for a
    // carry shift architecture (i.e. -0 equating to -1)
    if (properties().method == METHOD_FLOOR) {
      if (denominator == 0) {
        quotient = numerator < 0 ? 0 : -1;
        remainder = numerator;
      } else {
        quotient = std::floor(static_cast<RCCFloat>(numerator) /
                              static_cast<RCCFloat>(denominator));
        if (numerator < 0) {
          remainder = ((numerator % denominator) + denominator) % denominator;
        } else {
          remainder = numerator % denominator;
          if (remainder != 0 && denominator < 0) {
            remainder += denominator;
          }
        }
      }
    } else {  // METHOD_TRUNCATE
      if (denominator == 0) {
        quotient = numerator < 0 ? 1 : -1;
        remainder = numerator;
      } else {
        div_t a = div(numerator, denominator);
        quotient = a.quot;
        remainder = a.rem;
      }
    }
    return std::make_pair(quotient, remainder);
  }

  RCCResult run(bool) {
    if (input.opCode() == Short_timed_sampleSample_OPERATION &&
        denominator.opCode() == Short_timed_sampleSample_OPERATION) {
      // As output messages must be the same length as messages on input
      // port this controls the size of the output
      if (input_unprocessed_data == 0) {
        input_data = input.sample().data().data();
        input_unprocessed_data = input.sample().data().size();
        if (output_is_connected) {
          output_data = output.sample().data().data();
          output.setOpCode(Short_timed_sampleSample_OPERATION);
          output.sample().data().resize(input_unprocessed_data);
        }
        if (remainder_is_connected) {
          remainder_data = remainder.sample().data().data();
          remainder.setOpCode(Short_timed_sampleSample_OPERATION);
          remainder.sample().data().resize(input_unprocessed_data);
        }
      }

      if (denominator_unprocessed_data == 0) {
        denominator_data = denominator.sample().data().data();
        denominator_unprocessed_data = denominator.sample().data().size();
      }

      size_t samples_available = denominator_unprocessed_data;
      if (input_unprocessed_data < denominator_unprocessed_data) {
        samples_available = input_unprocessed_data;
      }

      // Process input data
      for (size_t i = 0; i < samples_available; i++) {
        std::pair<int16_t, int16_t> result =
            division(*input_data++, *denominator_data++);
        if (output_is_connected) {
          *output_data++ = result.first;
        }
        if (remainder_is_connected) {
          *remainder_data++ = result.second;
        }
      }

      input_unprocessed_data = input_unprocessed_data - samples_available;
      denominator_unprocessed_data =
          denominator_unprocessed_data - samples_available;

      // Input controls the size of the output messages, so when input needs
      // advancing so will the output
      if (input_unprocessed_data == 0) {
        input.advance();
        if (output_is_connected) {
          output.advance();
        }
        if (remainder_is_connected) {
          remainder.advance();
        }
      }
      if (denominator_unprocessed_data == 0) {
        denominator.advance();
      }
    } else {
      // Passthrough any non-sample opcodes of input
      if (input.opCode() != Short_timed_sampleSample_OPERATION) {
        if (input.opCode() == Short_timed_sampleTime_OPERATION) {
          // Pass through time opcode and time data
          if (output_is_connected) {
            output.setOpCode(Short_timed_sampleTime_OPERATION);
            output.time().fraction() = input.time().fraction();
            output.time().seconds() = input.time().seconds();
            output.advance();
          }
          if (remainder_is_connected) {
            remainder.setOpCode(Short_timed_sampleTime_OPERATION);
            remainder.time().fraction() = input.time().fraction();
            remainder.time().seconds() = input.time().seconds();
            remainder.advance();
          }
          input.advance();
        } else if (input.opCode() ==
                   Short_timed_sampleSample_interval_OPERATION) {
          // Pass through sample interval opcode and sample interval data
          if (output_is_connected) {
            output.setOpCode(Short_timed_sampleSample_interval_OPERATION);
            output.sample_interval().fraction() =
                input.sample_interval().fraction();
            output.sample_interval().seconds() =
                input.sample_interval().seconds();
            output.advance();
          }
          if (remainder_is_connected) {
            remainder.setOpCode(Short_timed_sampleSample_interval_OPERATION);
            remainder.sample_interval().fraction() =
                input.sample_interval().fraction();
            remainder.sample_interval().seconds() =
                input.sample_interval().seconds();
            remainder.advance();
          }
          input.advance();
        } else if (input.opCode() == Short_timed_sampleFlush_OPERATION) {
          // Pass through flush opcode
          if (output_is_connected) {
            output.setOpCode(Short_timed_sampleFlush_OPERATION);
            output.advance();
          }
          if (remainder_is_connected) {
            remainder.setOpCode(Short_timed_sampleFlush_OPERATION);
            remainder.advance();
          }
          input.advance();
        } else if (input.opCode() ==
                   Short_timed_sampleDiscontinuity_OPERATION) {
          // Pass through discontinuity opcode
          if (output_is_connected) {
            output.setOpCode(Short_timed_sampleDiscontinuity_OPERATION);
            output.advance();
          }
          if (remainder_is_connected) {
            remainder.setOpCode(Short_timed_sampleDiscontinuity_OPERATION);
            remainder.advance();
          }
          input.advance();
        } else if (input.opCode() == Short_timed_sampleMetadata_OPERATION) {
          // Pass through metadata opcode, id, and data
          if (output_is_connected) {
            output.setOpCode(Short_timed_sampleMetadata_OPERATION);
            output.metadata().id() = input.metadata().id();
            output.metadata().value() = input.metadata().value();
            output.advance();
          }
          if (remainder_is_connected) {
            remainder.setOpCode(Short_timed_sampleMetadata_OPERATION);
            remainder.metadata().id() = input.metadata().id();
            remainder.metadata().value() = input.metadata().value();
            remainder.advance();
          }
          input.advance();
        } else {
          setError("Unknown OpCode received");
          return RCC_FATAL;
        }
      }
      // Discard any non-sample opcodes on denominator
      if (denominator.opCode() != Short_timed_sampleSample_OPERATION) {
        denominator.advance();
      }
    }
    return RCC_OK;
  }
};

DIVIDER_S_START_INFO
// Insert any static info assignments here (memSize, memSizes, portInfo)
// e.g.: info.memSize = sizeof(MyMemoryStruct);
DIVIDER_S_END_INFO
