#!/usr/bin/env python3

# Python implementation of shift right
#
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
# more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

"""Python implementation of shift right."""

import numpy as np

import opencpi.ocpi_testing as ocpi_testing


class ShiftRight(ocpi_testing.Implementation):
    """Testing implementation for ShiftRight."""

    def __init__(self, shift):
        """Initialise."""
        super().__init__(shift=shift)

    def reset(self):
        """Reset the state to the same state as at initialisation.

        There is no state so this function is a no-op, but it is required by
        the ocpi_testing.Implementation base class.
        """
        pass

    def sample(self, values):
        """Handle input sample opcode.

        Args:
            values (list): Sample data on input port.

        Returns:
            Formatted messages.
        """
        output_values = []
        for value in values:
            real = np.int16(int(value.real) >> self.shift)
            imag = np.int16(int(value.imag) >> self.shift)
            output_values.append(complex(real, imag))
        return self.output_formatter(
            [{"opcode": "sample", "data": output_values}])
