// RCC implementation of complex_multiplier_xf worker.
//
// This file is protected by Copyright. Please refer to the COPYRIGHT file
// distributed with this source distribution.
//
// This file is part of OpenCPI <http://www.opencpi.org>
//
// OpenCPI is free software: you can redistribute it and/or modify it under the
// terms of the GNU Lesser General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option) any
// later version.
//
// OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
// more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#include "complex_multiplier_xf-worker.hh"

using namespace OCPI::RCC;  // for easy access to RCC data types and constants
using namespace Complex_multiplier_xfWorkerTypes;

class Complex_multiplier_xfWorker : public Complex_multiplier_xfWorkerBase {
  Complex_float_timed_sampleSampleData *output_data;
  const Complex_float_timed_sampleSampleData *input_1_data;
  const Complex_float_timed_sampleSampleData *input_2_data;

  size_t input_1_unprocessed_data = 0;
  size_t input_2_unprocessed_data = 0;

  RCCResult run(bool) {
    if (input_1.opCode() == Complex_float_timed_sampleSample_OPERATION &&
        input_2.opCode() == Complex_float_timed_sampleSample_OPERATION) {
      // As output messages must be the same length as messages on input_1
      // port this controls the size of the output
      if (input_1_unprocessed_data == 0) {
        input_1_data = input_1.sample().data().data();
        input_1_unprocessed_data = input_1.sample().data().size();
        output_data = output.sample().data().data();
        output.setOpCode(Complex_float_timed_sampleSample_OPERATION);
        output.sample().data().resize(input_1_unprocessed_data);
      }

      if (input_2_unprocessed_data == 0) {
        input_2_data = input_2.sample().data().data();
        input_2_unprocessed_data = input_2.sample().data().size();
      }

      size_t samples_available = input_2_unprocessed_data;
      if (input_1_unprocessed_data < input_2_unprocessed_data) {
        samples_available = input_1_unprocessed_data;
      }

      for (size_t i = 0; i < samples_available; i++) {
        // a+ib * c+id = (ac-bd) + i(bc+ad)
        output_data->real = (input_1_data->real * input_2_data->real) -
                            (input_1_data->imaginary * input_2_data->imaginary);
        output_data->imaginary =
            (input_1_data->imaginary * input_2_data->real) +
            (input_2_data->imaginary * input_1_data->real);

        input_1_data++;
        input_2_data++;
        output_data++;
      }

      input_1_unprocessed_data = input_1_unprocessed_data - samples_available;
      input_2_unprocessed_data = input_2_unprocessed_data - samples_available;

      // Input 1 controls the size of the output messages, so when input_1 needs
      // advancing so will the output
      if (input_1_unprocessed_data == 0) {
        input_1.advance();
        output.advance();
      }
      if (input_2_unprocessed_data == 0) {
        input_2.advance();
      }
    } else {
      // Pass through all non-sample input 1 opcodes
      if (input_1.opCode() != Complex_float_timed_sampleSample_OPERATION) {
        if (input_1.opCode() == Complex_float_timed_sampleTime_OPERATION) {
          // Pass through time opcode and time data
          output.setOpCode(Complex_float_timed_sampleTime_OPERATION);
          output.time().fraction() = input_1.time().fraction();
          output.time().seconds() = input_1.time().seconds();
          input_1.advance();
          output.advance();
        } else if (input_1.opCode() ==
                   Complex_float_timed_sampleSample_interval_OPERATION) {
          // Pass through sample interval opcode and sample interval data
          output.setOpCode(Complex_float_timed_sampleSample_interval_OPERATION);
          output.sample_interval().fraction() =
              input_1.sample_interval().fraction();
          output.sample_interval().seconds() =
              input_1.sample_interval().seconds();
          input_1.advance();
          output.advance();
        } else if (input_1.opCode() ==
                   Complex_float_timed_sampleFlush_OPERATION) {
          // Pass through flush opcode
          output.setOpCode(Complex_float_timed_sampleFlush_OPERATION);
          input_1.advance();
          output.advance();
        } else if (input_1.opCode() ==
                   Complex_float_timed_sampleDiscontinuity_OPERATION) {
          // Pass through discontinuity opcode
          output.setOpCode(Complex_float_timed_sampleDiscontinuity_OPERATION);
          input_1.advance();
          output.advance();
        } else if (input_1.opCode() ==
                   Complex_float_timed_sampleMetadata_OPERATION) {
          // Pass through metadata opcode, id, and data
          output.setOpCode(Complex_float_timed_sampleMetadata_OPERATION);
          output.metadata().id() = input_1.metadata().id();
          output.metadata().value() = input_1.metadata().value();
          input_1.advance();
          output.advance();
        } else {
          setError("Unknown OpCode received");
          return RCC_FATAL;
        }
      }
      // Discard input 2 opcodes
      if (input_2.opCode() != Complex_float_timed_sampleSample_OPERATION) {
        input_2.advance();
      }
    }

    return RCC_OK;
  }
};

COMPLEX_MULTIPLIER_XF_START_INFO
// Insert any static info assignments here (memSize, memSizes, portInfo)
// e.g.: info.memSize = sizeof(MyMemoryStruct);
COMPLEX_MULTIPLIER_XF_END_INFO
