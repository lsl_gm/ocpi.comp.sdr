// Multiplier implementation.
//
// This file is protected by Copyright. Please refer to the COPYRIGHT file
// distributed with this source distribution.
//
// This file is part of OpenCPI <http://www.opencpi.org>
//
// OpenCPI is free software: you can redistribute it and/or modify it under the
// terms of the GNU Lesser General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option) any
// later version.
//
// OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
// more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#include "multiplier_s_l-worker.hh"

using namespace OCPI::RCC;  // for easy access to RCC data types and constants
using namespace Multiplier_s_lWorkerTypes;

class Multiplier_s_lWorker : public Multiplier_s_lWorkerBase {
  size_t input_1_processed_samples = 0;
  size_t input_2_processed_samples = 0;
  size_t output_processed_samples = 0;

  RCCResult start() {
    input_1_processed_samples = 0;
    input_2_processed_samples = 0;
    output_processed_samples = 0;
    return RCC_OK;
  }

  RCCResult run(bool) {
    if (input_1.opCode() == Short_timed_sampleSample_OPERATION &&
        input_2.opCode() == Short_timed_sampleSample_OPERATION) {
      const int16_t *input_1_data = input_1.sample().data().data();
      const int16_t *input_2_data = input_2.sample().data().data();
      int32_t *output_data = output.sample().data().data();

      // Advance the inputs to the next sample point to be handled
      input_1_data = &input_1_data[input_1_processed_samples];
      input_2_data = &input_2_data[input_2_processed_samples];

      size_t input_1_length = input_1.sample().data().size();
      size_t input_2_length = input_2.sample().data().size();
      size_t input_1_unprocessed_samples =
          input_1_length - input_1_processed_samples;
      size_t input_2_unprocessed_samples =
          input_2_length - input_2_processed_samples;
      size_t max_output_length =
          MULTIPLIER_S_L_OCPI_MAX_BYTES_OUTPUT / sizeof(*output_data);

      // New output buffer is being used
      if (output_processed_samples == 0) {
        output.setOpCode(Long_timed_sampleSample_OPERATION);
        output.sample().data().resize(
            std::min(input_1_unprocessed_samples, max_output_length));
      }

      size_t output_unprocessed_samples =
          output.sample().data().size() - output_processed_samples;
      output_data = &output_data[output_processed_samples];

      // Output buffer space is restricting total samples that can be processed
      if (output_unprocessed_samples <= input_1_unprocessed_samples &&
          output_unprocessed_samples <= input_2_unprocessed_samples) {
        for (size_t i = 0; i < output_unprocessed_samples; i++) {
          *output_data = *input_1_data * *input_2_data;
          output_data++;
          input_1_data++;
          input_2_data++;
        }
        input_1_processed_samples += output_unprocessed_samples;
        input_2_processed_samples += output_unprocessed_samples;
        output.advance();
        output_processed_samples = 0;

      } else {
        size_t available_input_samples =
            std::min(input_1_unprocessed_samples, input_2_unprocessed_samples);
        for (size_t i = 0; i < available_input_samples; i++) {
          *output_data = *input_1_data * *input_2_data;
          ;
          output_data++;
          input_1_data++;
          input_2_data++;
        }
        input_1_processed_samples += available_input_samples;
        input_2_processed_samples += available_input_samples;
        // Do not advance output as input data limited
        output_processed_samples += available_input_samples;
      }
      if (input_1_processed_samples == input_1_length) {
        input_1.advance();
        input_1_processed_samples = 0;
      }
      if (input_2_processed_samples == input_2_length) {
        input_2.advance();
        input_2_processed_samples = 0;
      }
      return RCC_OK;
    } else {
      // Passthrough any non-sample opcodes of input_1
      if (input_1.opCode() != Short_timed_sampleSample_OPERATION) {
        if (input_1.opCode() == Short_timed_sampleTime_OPERATION) {
          // Pass through time opcode and time data
          output.setOpCode(Long_timed_sampleTime_OPERATION);
          output.time().fraction() = input_1.time().fraction();
          output.time().seconds() = input_1.time().seconds();
          input_1.advance();
          output.advance();
        } else if (input_1.opCode() ==
                   Short_timed_sampleSample_interval_OPERATION) {
          // Pass through sample interval opcode and sample interval data
          output.setOpCode(Long_timed_sampleSample_interval_OPERATION);
          output.sample_interval().fraction() =
              input_1.sample_interval().fraction();
          output.sample_interval().seconds() =
              input_1.sample_interval().seconds();
          input_1.advance();
          output.advance();
        } else if (input_1.opCode() == Short_timed_sampleFlush_OPERATION) {
          // Pass through flush opcode
          output.setOpCode(Long_timed_sampleFlush_OPERATION);
          input_1.advance();
          output.advance();
        } else if (input_1.opCode() ==
                   Short_timed_sampleDiscontinuity_OPERATION) {
          // Pass through discontinuity opcode
          output.setOpCode(Long_timed_sampleDiscontinuity_OPERATION);
          input_1.advance();
          output.advance();
        } else if (input_1.opCode() == Short_timed_sampleMetadata_OPERATION) {
          // Pass through metadata opcode, id, and data
          output.setOpCode(Long_timed_sampleMetadata_OPERATION);
          output.metadata().id() = input_1.metadata().id();
          output.metadata().value() = input_1.metadata().value();
          input_1.advance();
          output.advance();
        } else {
          setError("Unknown OpCode received");
          return RCC_FATAL;
        }
      }
      // Discard any non-sample opcodes on input_2
      if (input_2.opCode() != Short_timed_sampleSample_OPERATION) {
        input_2.advance();
      }
    }
    return RCC_OK;
  }
};

MULTIPLIER_S_L_START_INFO

MULTIPLIER_S_L_END_INFO
