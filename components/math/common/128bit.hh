// 128 bit math routines
//
// This file is protected by Copyright. Please refer to the COPYRIGHT file
// distributed with this source distribution.
//
// This file is part of OpenCPI <http://www.opencpi.org>
//
// OpenCPI is free software: you can redistribute it and/or modify it under the
// terms of the GNU Lesser General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option) any
// later version.
//
// OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
// more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

// LINT EXCEPTION: cpp_005: 2: Define use allowed in include guard
#ifndef COMPONENTS_MATH_COMMON_128BIT_HH_
#define COMPONENTS_MATH_COMMON_128BIT_HH_

// LINT EXCEPTION: cpp_008: 1: Include allowed after include guard
#include <cstdint>
// LINT EXCEPTION: cpp_008: 1: Include allowed after include guard
#include <sstream>
// LINT EXCEPTION: cpp_008: 1: Include allowed after include guard
#include <type_traits>
// LINT EXCEPTION: cpp_008: 1: Include allowed after include guard
#include <cmath>
// LINT EXCEPTION: cpp_008: 1: Include allowed after include guard
#include "multiword_integer_divide.hh"

// uint128_t and int128_t implementation for platforms/compilers
// without native support.
//
// Public Interface is provided by sdr_math::uint128_t and
// sdr_math::int128_t typedefs. The namespace detail should
// not be accessed directly by client code.
//
// Implementation notes
// --------------------
// The 128 bits are stored as 2 uint64_t variables. This assumes the
// platform provides fast 64 bit arithmetic operations.
// The most significant word matches the sign of the 128 bit value.
//   This allows a fast negative check for the signed variant by
//   testing the most significant word is less than zero.
// The least significant word is always unsigned.
//   This ensures zero bits in for shift operations.
//
// The most common unary and binary operators are provided as
// overloaded operator methods using textbook C++ value class
// methods. Of note:
// * Binary operators that have an assignment version, the assignment
//   is implemented as a class method and the binary operator as a
//   non-class function calling the class method.
// * operator>,<=,>= are wrappers to operator<
// * operator!= is a wrapper to operator==
//
// The following functions are also provided:
// * abs() for the signed version only
// * sqrtl() - Simple algorithm (probably not the fastest).
// * (u)int64_t cast - Truncates to 64 bits.
// * bool cast - For use in if(T128) and similar.
// * multiply (static function) - Helper to multiply 2 64 bit
//   numbers returning a 128 bit result.
// * multiply_and_shift (static function) - Helper to multiply
//   2 64 bit numbers and shift right to scale down.
//
// As the majority of functionality is common to both the signed
// and unsigned version, this is implemented as a template. Where
// the implementation differs, template specialisation is used.
//
// For speed there is no overflow or error checking. This matches
// the behaviour of the built-in integer types.
//
// This is not intended as a full integral number class but as a
// helper function to perform dsp 128 bit maths.

namespace sdr_math {
namespace detail {

static_assert(__cplusplus >= 201103L, "128bit.hh requires c++11 or later");

// LINT EXCEPTION: cpp_011: 3: New class definition allowed in common
// implementation
template <class T64>
class T128 {
 private:
  // Member variables.
  T64 _hi;
  uint64_t _lo;

  // Private constructor from 2 64-bit values.
  T128(T64 hi, uint64_t lo) : _hi{hi}, _lo{lo} {}

  // Helper method to negate the held number (2s complement negate by
  // inverting all bits and adding one).
  T128 &_negate() {
    _hi = ~_hi;
    _lo = ~_lo;
    operator++();
    return *this;
  }

  // Helper method to extend sign of 64 bit into hi part.
  template <typename T>
  static T64 _sign_extend(const T &lo) {
    return (std::is_signed<T64>::value && std::is_signed<T>::value && (lo < 0))
               ? ~0ll
               : 0ll;
  }

  // Alternative negative sign check for when 'if (hi < 0)'
  // generates a compiler warning (always false) when
  // instantiating the unsigned version.
  static const uint64_t _sign_bit{0x8000000000000000};

  // Helper to determine significant bits.
  int _bit_width() const {
    T128 word = abs(*this);
    int bits{0};
    // Binary chop to find highest set bit.
    for (int bit_test = 128 / 2; bit_test != 0; bit_test >>= 1) {
      if (word >> bit_test != 0) {
        bits += bit_test;
        word >>= bit_test;
      }
    }
    return bits + word._lo;
  }

 public:
  // Default Constructor: Set to zero.
  T128() : _hi{0}, _lo{0} {}

  // Copy Constructor
  T128(const T128 &rhs) : _hi{rhs._hi}, _lo{rhs._lo} {}

  // Construct from fundamental integral type
  template <
      typename T,
      typename = typename std::enable_if<
          std::is_fundamental<T>::value &&std::is_integral<T>::value, T>::type>
  // LINT EXCEPTION: cpp_002: 1: implicit constructor is OK for numeric types
  // cppcheck-suppress noExplicitConstructor
  T128(const T rhs)
      : _hi(_sign_extend(static_cast<T64>(rhs))),
        _lo(static_cast<uint64_t>(rhs)) {}

  // Construct from long double
  // Only supports upto 64 bits unsigned/63 bits signed.
  // Required for scale_and_round gtest.
  // LINT EXCEPTION: cpp_002: 1: implicit constructor is OK for numeric types
  // cppcheck-suppress noExplicitConstructor
  T128(const long double &rhs) : _hi(0), _lo(0) {
    long double pos_rhs = std::abs(rhs);
    long double f_hi = std::trunc(pos_rhs / std::pow(2.0L, 64.0L));
    long double f_lo = pos_rhs - f_hi * std::pow(2.0L, 64.0L);
    _lo = uint64_t(f_lo);
    _hi = T64(f_hi);
    if (rhs < 0) {
      _negate();
    }
  }

  // Construct signed <-> unsigned
  template <typename T, typename = typename std::enable_if<
                            !std::is_same<T, T64>::value>::type>
  // LINT EXCEPTION: cpp_002: 1: implicit constructor is OK for numeric types
  // cppcheck-suppress noExplicitConstructor
  T128(T128<T> rhs)
      : _hi(static_cast<T64>(rhs.hi())), _lo(rhs.lo()) {}

  // Copy Assignment
  T128 &operator=(const T128 &rhs) {
    _hi = rhs._hi;
    _lo = rhs._lo;
    return *this;
  }

  // Assign from any fundamental integral type
  template <
      typename T,
      typename = typename std::enable_if<
          std::is_fundamental<T>::value &&std::is_integral<T>::value, T>::type>
  T128 &operator=(const T &rhs) {
    _hi = _sign_extend(T64(rhs));
    _lo = uint64_t(T64(rhs));
    return *this;
  }

  // Types
  typedef T64 value_type;

  // Constants
  static const T128 MAX;
  static const T128 MIN;  // (signed only)

  // Overloaded operators
  T128 &operator+=(const T128 &rhs) {
    uint64_t lo_sum = _lo + rhs._lo;
    T64 carry = lo_sum < _lo ? 1 : 0;
    _lo = lo_sum;
    _hi += rhs._hi + carry;
    return *this;
  }

  template <class T>
  T128 &operator+=(const T &rhs) {
    return operator+=(T128(rhs));
  }

  T128 &operator-=(const T128 &rhs) {
    uint64_t lo_sub = _lo - rhs._lo;
    T64 borrow = lo_sub > _lo ? 1 : 0;
    _lo = lo_sub;
    _hi -= rhs._hi + borrow;
    return *this;
  }

  template <class T>
  T128 &operator-=(const T &rhs) {
    return operator-=(T128(rhs));
  }

  T128 &operator++() {
    ++_lo;
    if (_lo == 0) {
      ++_hi;
    }
    return *this;
  }

  T128 operator++(int) {
    T128 ret{*this};
    operator++();
    return ret;
  }

  T128 &operator--() {
    if (_lo-- == 0) {
      --_hi;
    }
    return *this;
  }

  T128 operator--(int) {
    T128 ret{*this};
    operator--();
    return ret;
  }

  T128 &operator*=(const T128 &rhs) {
    T128 lo_lo = multiply(_lo, rhs._lo);
    T128 hi_lo = multiply(_hi, rhs._lo) << 64;
    T128 lo_hi = multiply(_lo, rhs._hi) << 64;
    *this = lo_lo + hi_lo + lo_hi;
    return *this;
  }

  T128 &operator/=(const T128 &rhs);

  template <
      typename T,
      typename = typename std::enable_if<
          std::is_fundamental<T>::value &&std::is_integral<T>::value, T>::type>
  T128 operator/=(const T &rhs) {
    return operator/=(T128(rhs));
  }

  T128 &operator%=(const T128 &rhs);

  template <
      typename T,
      typename = typename std::enable_if<
          std::is_fundamental<T>::value &&std::is_integral<T>::value, T>::type>
  T128 operator%=(const T &rhs) {
    return operator%=(T128(rhs));
  }

  template <
      typename T,
      typename = typename std::enable_if<
          std::is_fundamental<T>::value &&std::is_integral<T>::value, T>::type>
  T128 &operator>>=(const T &shift) {
    if (shift >= 64) {
      _lo = uint64_t(_hi >> (shift - 64));
      _hi = 0;
      if (std::is_signed<T64>::value) {
        if (_lo & _sign_bit) {
          //-ve sign extend
          _hi = ~T64(0);
        }
      }
      return *this;
    }
    T rshift{shift};  // Copy to suppress lint shiftTooManyBitsSigned
    uint64_t carry = _hi & (~(UINT64_MAX << rshift));
    _lo = (_lo >> rshift) | (carry << (64 - rshift));
    _hi = (_hi >> rshift);
    return *this;
  }

  template <
      typename T,
      typename = typename std::enable_if<
          std::is_fundamental<T>::value &&std::is_integral<T>::value, T>::type>
  T128 &operator<<=(const T &shift) {
    if (shift >= 64) {
      _hi = T64(_lo << (shift - 64));
      _lo = 0;
      return *this;
    }
    T lshift{shift};  // Copy to suppress lint shiftTooManyBitsSigned
    uint64_t carry = (_lo & (~(UINT64_MAX >> lshift))) >> (64 - lshift);
    _lo = (_lo << lshift);
    _hi = (_hi << lshift) | T64(carry);
    return *this;
  }

  T128 &operator|=(const T128 &rhs) {
    _hi |= rhs._hi;
    _lo |= rhs._lo;
    return *this;
  }

  template <typename T>
  T128 &operator|=(const T &rhs) {
    _hi |= _sign_extend(rhs);
    _lo |= rhs;
    return *this;
  }

  T128 &operator&=(const T128 &rhs) {
    _hi &= rhs._hi;
    _lo &= rhs._lo;
    return *this;
  }

  template <typename T>
  T128 &operator&=(const T &rhs) {
    _hi &= _sign_extend(rhs);
    _lo &= static_cast<uint64_t>(rhs);
    return *this;
  }

  T128 &operator^=(const T128 &rhs) {
    _hi ^= rhs._hi;
    _lo ^= rhs._lo;
    return *this;
  }

  template <typename T>
  T128 &operator^=(const T &rhs) {
    _hi ^= _sign_extend(rhs);
    _lo ^= rhs;
    return *this;
  }

  T128 operator~() const { return T128(~_hi, ~_lo); }

  T128 operator-() const { return T128(*this)._negate(); }

  // Overloaded cast operators
  operator T64() const { return static_cast<T64>(_lo); }
  // Allow returning of smaller integer segments
  template <
      typename T,
      typename = typename std::enable_if<
          std::is_fundamental<T>::value &&std::is_integral<T>::value, T>::type>
  operator T() const {
    return static_cast<T>(_lo);
  }

  explicit operator bool() const { return _lo || _hi; }
  // Only supports upto 64 bits unsigned/63 bits signed.
  // Required for scale_and_round gtest.
  explicit operator long double() const {
    int significant_bits{_bit_width()};
    int scale{0};
    T128 number{*this};
    if (significant_bits > std::numeric_limits<long double>::digits) {
      scale = significant_bits - std::numeric_limits<long double>::digits;
      number >>= scale;
    }
    long double result{static_cast<long double>(number._hi) *
                       std::pow(2.0L, 64)};
    result += static_cast<long double>(number._lo);
    if (scale) {
      result *= std::pow(2.0L, scale);
    }
    return result;
  }
  // Static member functions taking 2 64 bit values and returning
  //  a 128 bit value.
  static T128 multiply(T64 lhs, T64 rhs);

  template <
      typename T,
      typename = typename std::enable_if<
          std::is_fundamental<T>::value &&std::is_integral<T>::value, T>::type>
  static T128 multiply_and_shift(T64 lhs, T64 rhs, const T &shift) {
    T128 product = multiply(lhs, rhs);
    if (shift == 0) {
      return product;
    }
    return product >> shift;
  }

  // Helper function to convert uint128 to int128.
  static T128 make_signed(const T128<uint64_t> &arg);

  // Read only access to hi and lo parts.
  T64 hi() const {
    return _hi;
  };
  uint64_t lo() const {
    return _lo;
  };

  // Helper conversion functions
  std::string to_string(const std::ios_base::fmtflags &flags =
                            std::ios_base::fmtflags()) const {
    static const char hexdigits[] = "0123456789abcdef";
    T128 number{*this};
    int radix{10};
    int shift{0};
    uint64_t hi_mask{~0lu >> 1};
    uint64_t lo_mask{0};
    std::string prefix{};
    if (flags & std::ios_base::oct) {
      radix = 8;
      shift = 3;
      hi_mask = ~0lu >> 3;
      lo_mask = 07;
      prefix = "0";
    }
    if (flags & std::ios_base::hex) {
      radix = 16;
      shift = 4;
      hi_mask = ~0lu >> 4;
      lo_mask = 0xf;
      prefix = "x0";  // Will be reversed at last step.
    }
    std::stringstream output;
    output.flags(flags);
    output.unsetf(std::ios_base::showbase);
    if (std::is_signed<T64>::value) {
      if (radix == 10 && (_hi & _sign_bit)) {
        number._negate();
      }
    }
    do {
      if (radix == 10) {
        if (std::is_signed<T64>::value && (number._hi & _sign_bit)) {
          std::operator<<(output, hexdigits[(number % -radix)._lo]);
          number /= -radix;
        } else {
          std::operator<<(output, hexdigits[(number % radix)._lo]);
          number /= radix;
        }
      } else {
        std::operator<<(output, hexdigits[number._lo & lo_mask]);
        output.flush();
        number >>= shift;
        number._hi &= T64(hi_mask);
      }
    } while (number);
    if (std::is_signed<T64>::value) {
      if (radix == 10 && (_hi & _sign_bit)) {
        std::operator<<(output, '-');
      }
    }
    if (flags & std::ios_base::showbase) {
      std::operator<<(output, prefix);
    }
    std::string result(output.str());
    std::reverse(result.begin(), result.end());
    return result;
  }

  // Numeric Limits
  static constexpr T128 max() {
    return T128(std::numeric_limits<T64>::max(), uint64_t(-1));
  }
  static constexpr T128 min() {
    return T128(std::numeric_limits<T64>::min(), 0);
  }
  static constexpr T128 lowest() {
    return T128(std::numeric_limits<T64>::min(), 0);
  }
  static constexpr int digits{std::numeric_limits<T64>::digits +
                              std::numeric_limits<uint64_t>::digits};
  static constexpr int digits10{
      std::numeric_limits<T128>::digits *std::log10(2)};
  static constexpr bool is_signed{std::numeric_limits<T64>::is_signed};
  static constexpr bool is_exact{std::numeric_limits<T64>::is_exact};
  static constexpr bool is_iec559{std::numeric_limits<T64>::is_iec559};
};

// Non-class methods.
template <class T128>
inline bool operator<(const T128 &lhs, const T128 &rhs) {
  if (lhs.hi() == rhs.hi()) {
    return lhs.lo() < rhs.lo();
  }
  return lhs.hi() < rhs.hi();
}

template <class T128>
inline bool operator>(const T128 &lhs, const T128 &rhs) {
  return operator<(rhs, lhs);
}

template <class T128>
inline bool operator<=(const T128 &lhs, const T128 &rhs) {
  return !operator>(lhs, rhs);
}

template <class T128>
inline bool operator>=(const T128 &lhs, const T128 &rhs) {
  return !operator<(lhs, rhs);
}

template <class T128, class T>
T128 operator+(T128 lhs, const T &rhs) {
  return lhs += rhs;
}

template <class T128, class T>
T128 operator-(T128 lhs, const T &rhs) {
  return lhs -= rhs;
}

template <class T128>
T128 operator*(T128 lhs, const T128 &rhs) {
  return lhs *= rhs;
}

template <class T128>
T128 operator/(T128 lhs, const T128 &rhs) {
  return lhs /= rhs;
}

template <class T128>
T128 operator%(T128 lhs, const T128 &rhs) {
  return lhs %= rhs;
}

template <class T128>
inline bool operator==(const T128 &lhs, const T128 &rhs) {
  return lhs.hi() == rhs.hi() && lhs.lo() == rhs.lo();
}

template <class T128>
inline bool operator!=(const T128 &lhs, const T128 &rhs) {
  return !operator==(lhs, rhs);
}

template <
    typename T128, typename T,
    typename = typename std::enable_if<
        std::is_fundamental<T>::value &&std::is_integral<T>::value, T>::type>
T128 operator>>(T128 lhs, const T &shift) {
  return lhs >>= shift;
}

template <
    typename T128, typename T,
    typename = typename std::enable_if<
        std::is_fundamental<T>::value &&std::is_integral<T>::value, T>::type>
T128 operator<<(T128 lhs, const T &shift) {
  return lhs <<= shift;
}

template <class T128>
T128 abs(const T128 &arg) {
  return arg.hi() < 0 ? -arg : arg;
}

template <class T128, class T>
T128 operator|(T128 lhs, const T &rhs) {
  return lhs |= rhs;
}

template <class T128, class T>
T128 operator&(T128 lhs, const T &rhs) {
  return lhs &= rhs;
}

template <class T128, class T>
T128 operator^(T128 lhs, const T &rhs) {
  return lhs ^= rhs;
}

template <class T128>
T128 sqrtl(const T128 &arg) {
  T128 lower{0};
  T128 upper{arg};
  T128 result;
  for (;;) {
    result = (lower + upper) >> 1;
    T128 square = result * result;
    if (square == arg) {
      return result;
    }
    if (square > arg) {
      upper = result;
    } else {
      lower = result;
    }
  }
  return result;
}

// Converting compare overloads
template <
    typename T128, typename T,
    typename = typename std::enable_if<
        std::is_fundamental<T>::value &&std::is_integral<T>::value, T>::type>
bool operator==(const T128 &lhs, const T &rhs) {
  return lhs == T128(rhs);
}

template <
    typename T128, typename T,
    typename = typename std::enable_if<
        std::is_fundamental<T>::value &&std::is_integral<T>::value, T>::type>
bool operator!=(const T128 &lhs, const T &rhs) {
  return lhs != T128(rhs);
}

template <
    typename T128, typename T,
    typename = typename std::enable_if<
        std::is_fundamental<T>::value &&std::is_integral<T>::value, T>::type>
bool operator<(const T128 &lhs, const T &rhs) {
  return lhs < T128(rhs);
}

template <
    typename T128, typename T,
    typename = typename std::enable_if<
        std::is_fundamental<T>::value &&std::is_integral<T>::value, T>::type>
bool operator>(const T128 &lhs, const T &rhs) {
  return lhs > T128(rhs);
}

template <
    typename T128, typename T,
    typename = typename std::enable_if<
        std::is_fundamental<T>::value &&std::is_integral<T>::value, T>::type>
bool operator<=(const T128 &lhs, const T &rhs) {
  return lhs <= T128(rhs);
}

template <
    typename T128, typename T,
    typename = typename std::enable_if<
        std::is_fundamental<T>::value &&std::is_integral<T>::value, T>::type>
bool operator>=(const T128 &lhs, const T &rhs) {
  return lhs >= T128(rhs);
}

// Constants (signed only)
template <>
const T128<int64_t> T128<int64_t>::MAX(int64_t(0x7fffffffffffffff),
                                       uint64_t(0xffffffffffffffff));
template <>
const T128<uint64_t> T128<uint64_t>::MAX(uint64_t(0xffffffffffffffff),
                                         uint64_t(0xffffffffffffffff));
template <>
const T128<int64_t> T128<int64_t>::MIN(int64_t(0x8000000000000000),
                                       uint64_t(0x0000000000000000));

// cast unsigned->signed helper
template <>
T128<int64_t> T128<int64_t>::make_signed(const T128<uint64_t> &arg) {
  T128<int64_t> signed_arg{static_cast<int64_t>(arg.hi()), arg.lo()};
  return signed_arg;
}

// Divide and multiply sign helper:
// Ensure both operands are positive and determine sign of result.
template <class T>
// LINT EXCEPTION: cpp_001: 1: Allow non-const reference.
bool normalise_signs(T &lhs, T &rhs) {
  bool negate = false;
  if (lhs < 0) {
    negate = true;
    lhs = -lhs;
  }
  if (rhs < 0) {
    negate = !negate;
    rhs = -rhs;
  }
  return negate;
}

// Multiply
template <>
T128<uint64_t> T128<uint64_t>::multiply(uint64_t lhs, uint64_t rhs) {
  uint64_t lhs_lo = lhs & UINT32_MAX;
  uint64_t lhs_hi = (lhs >> 32) & UINT32_MAX;
  uint64_t rhs_lo = rhs & UINT32_MAX;
  uint64_t rhs_hi = (rhs >> 32) & UINT32_MAX;

  uint64_t lo_lo = lhs_lo * rhs_lo;
  uint64_t lo_hi = lhs_lo * rhs_hi;
  uint64_t hi_lo = lhs_hi * rhs_lo;
  uint64_t hi_hi = lhs_hi * rhs_hi;

  uint64_t carry =
      ((lo_lo >> 32) + (lo_hi & UINT32_MAX) + (hi_lo & UINT32_MAX)) >> 32;

  T128<uint64_t> product;
  product._lo = lo_lo + (lo_hi << 32) + (hi_lo << 32);
  product._hi = hi_hi + (lo_hi >> 32) + (hi_lo >> 32) + carry;
  return product;
}

template <>
T128<int64_t> T128<int64_t>::multiply(int64_t lhs, int64_t rhs) {
  bool negate = normalise_signs(lhs, rhs);
  T128<uint64_t> uproduct{
      T128<uint64_t>::multiply(uint64_t(lhs), uint64_t(rhs))};
  T128<int64_t> sproduct{make_signed(uproduct)};
  if (negate) {
    return -sproduct;
  }
  return sproduct;
}

template <>
T128<int64_t> &T128<int64_t>::operator*=(const T128<int64_t> &rhs) {
  T128<int64_t> u_rhs{rhs};
  bool negate = normalise_signs(*this, u_rhs);
  T128<uint64_t> lo_lo = T128<uint64_t>::multiply(_lo, u_rhs._lo);
  T128<uint64_t> hi_lo = T128<uint64_t>::multiply(uint64_t(_hi), u_rhs._lo)
                         << 64;
  T128<uint64_t> lo_hi = T128<uint64_t>::multiply(_lo, uint64_t(u_rhs._hi))
                         << 64;
  *this = make_signed(lo_lo + hi_lo + lo_hi);
  if (negate) {
    _negate();
  }
  return *this;
}

template <
    typename T128, typename T,
    typename = typename std::enable_if<
        std::is_fundamental<T>::value &&std::is_integral<T>::value, T>::type>
T128 operator*(const T128 &lhs, const T &rhs) {
  return lhs * T128(rhs);
}

template <
    typename T128, typename T,
    typename = typename std::enable_if<
        std::is_fundamental<T>::value &&std::is_integral<T>::value, T>::type>
T128 operator*(const T &lhs, const T128 &rhs) {
  return T128(lhs) * rhs;
}

// Divide
// These are wrappers to the 32 bit multiword divide routine converting
// between 2x64bit signed/unsigned and unsigned 4x32bit words.
// multiword_unsigned_divide() does the heavy lifting.
template <>
T128<uint64_t> &T128<uint64_t>::operator/=(const T128<uint64_t> &rhs) {
  uint32_t dividend[4]{uint32_t(_lo), uint32_t(_lo >> 32),
                       uint32_t(_hi), uint32_t(_hi >> 32)};
  uint32_t divisor[4]{uint32_t(rhs._lo), uint32_t(rhs._lo >> 32),
                      uint32_t(rhs._hi), uint32_t(rhs._hi >> 32)};
  uint32_t quotient[4];

  multiword_unsigned_divide(dividend, 4, divisor, 4, quotient);

  _hi = uint64_t(quotient[3]) << 32 | quotient[2];
  _lo = uint64_t(quotient[1]) << 32 | quotient[0];
  return *this;
}

template <>
T128<uint64_t> &T128<uint64_t>::operator%=(const T128<uint64_t> &rhs) {
  uint32_t dividend[4]{uint32_t(_lo), uint32_t(_lo >> 32),
                       uint32_t(_hi), uint32_t(_hi >> 32)};
  uint32_t divisor[4]{uint32_t(rhs._lo), uint32_t(rhs._lo >> 32),
                      uint32_t(rhs._hi), uint32_t(rhs._hi >> 32)};
  uint32_t quotient[4];
  uint32_t remainder[4];
  multiword_unsigned_divide(dividend, 4, divisor, 4, quotient, remainder);
  _hi = uint64_t(remainder[3]) << 32 | remainder[2];
  _lo = uint64_t(remainder[1]) << 32 | remainder[0];
  return *this;
}

template <>
T128<int64_t> &T128<int64_t>::operator/=(const T128<int64_t> &rhs) {
  T128 u_rhs{rhs};
  bool negate = normalise_signs(*this, u_rhs);
  uint32_t dividend[4]{uint32_t(_lo), uint32_t(_lo >> 32),
                       uint32_t(_hi), uint32_t(_hi >> 32)};
  uint32_t divisor[4]{uint32_t(u_rhs.lo()), uint32_t(u_rhs.lo() >> 32),
                      uint32_t(u_rhs.hi()), uint32_t(u_rhs.hi() >> 32)};
  uint32_t quotient[4];
  multiword_unsigned_divide(dividend, 4, divisor, 4, quotient);
  _hi = int64_t(quotient[3]) << 32 | quotient[2];
  _lo = uint64_t(quotient[1]) << 32 | quotient[0];
  if (negate) {
    _negate();
  }
  return *this;
}

template <>
T128<int64_t> &T128<int64_t>::operator%=(const T128<int64_t> &rhs) {
  T128 u_rhs{rhs};
  bool negate = normalise_signs(*this, u_rhs);
  uint32_t dividend[4]{uint32_t(_lo), uint32_t(_lo >> 32),
                       uint32_t(_hi), uint32_t(_hi >> 32)};
  uint32_t divisor[4]{uint32_t(u_rhs.lo()), uint32_t(u_rhs.lo() >> 32),
                      uint32_t(u_rhs.hi()), uint32_t(u_rhs.hi() >> 32)};
  uint32_t quotient[4];
  uint32_t remainder[4];
  multiword_unsigned_divide(dividend, 4, divisor, 4, quotient, remainder);
  _hi = int64_t(remainder[3]) << 32 | remainder[2];
  _lo = uint64_t(remainder[1]) << 32 | remainder[0];
  if (negate) {
    _negate();
  }
  return *this;
}

template <
    typename T128, typename T,
    typename = typename std::enable_if<
        std::is_fundamental<T>::value &&std::is_integral<T>::value, T>::type>
T128 operator/(const T128 &lhs, const T &rhs) {
  return lhs / T128(rhs);
}

template <
    typename T128, typename T,
    typename = typename std::enable_if<
        std::is_fundamental<T>::value &&std::is_integral<T>::value, T>::type>
T128 operator%(const T128 &lhs, const T &rhs) {
  return lhs % T128(rhs);
}

// Stream I/O operators
template <class T128>
std::ostream &operator<<(std::ostream &os, const T128 &rhs) {
  std::operator<<(os, rhs.to_string(os.flags()));
  return os;
}

// as overloading of to_string is not allowed, add in argument-dependent-lookup
// function through:
template <class T128>
std::string to_string(const T128 &rhs) {
  return rhs.to_string();
}

template <class T128>
std::istream &operator>>(std::istream &is, T128 &rhs) {
  // Note: Does not support std::ios_base::showbase,
  // std::ios_base::showpos. Fill and align not tested.
  bool negate{false};
  T128 radix{10};
  if (is.flags() & std::ios_base::oct) radix = 8;
  if (is.flags() & std::ios_base::hex) radix = 16;
  std::stringstream converter;
  converter.flags(is.flags());
  if (std::is_signed<typename T128::value_type>::value) {
    if (is.peek() == '-') {
      negate = true;
      is.get();
    }
  }
  if (is.peek() == '0') {
    converter >> std::oct;
    radix = 8;
    is.get();
    if (tolower(is.peek()) == 'x') {
      converter >> std::hex;
      radix = 16;
      is.get();
    }
  }
  rhs = 0;
  while (!is.eof()) {
    int digit = -1;
    converter.clear();
    converter.put(is.peek());
    converter.operator>>(digit);
    if (converter.fail()) {
      break;
    }
    is.get();
    rhs *= radix;
    rhs += T128(digit);
  }
  if (negate) {
    rhs = -rhs;
  }
  return is;
}

}  // namespace detail

// Exported unsigned and signed types.
typedef detail::T128<uint64_t> uint128_t;
typedef detail::T128<int64_t> int128_t;
static_assert(sizeof(uint128_t) == 128 / 8, "uint128_t is not 128 bits");
static_assert(sizeof(int128_t) == 128 / 8, "int128_t is not 128 bits");

}  // namespace sdr_math

namespace std {
// Ensure that number traits are set appropriately.
// This is conforming to C++11:
// "A program may add a template specialization for any standard library
// template to namespace std only if the declaration depends on a user-defined
// type and the specialization meets the standard library requirements for the
// original template and is not explicitly prohibited."
template <class T64>
struct numeric_limits<sdr_math::detail::T128<T64>> : public numeric_limits<
                                                         T64> {
  static const sdr_math::detail::T128<T64> max() {
    return sdr_math::detail::T128<T64>::max();
  };
  static const sdr_math::detail::T128<T64> min() {
    return sdr_math::detail::T128<T64>::min();
  };
  static const sdr_math::detail::T128<T64> lowest() {
    return sdr_math::detail::T128<T64>::lowest();
  };
  static constexpr int digits{sdr_math::detail::T128<T64>::digits};
  static constexpr int digits10{sdr_math::detail::T128<T64>::digits10};
};
// std:: specialisations not supported in g++ 4.8.1
#if 0
template <class T64>
constexpr int numeric_limits<sdr_math::detail::T128<T64>> digits;
template <class T64>
constexpr int numeric_limits<sdr_math::detail::T128<T64>> digits10;
template <class T64>
struct is_fundamental<sdr_math::detail::T128<T64>> : false_type {};
template <T64>
struct is_integral<sdr_math::detail::T128<T64>> : true_type {};
template <T64>
constexpr struct is_signed<sdr_math::detail::T128<T64>> : is_signed<T64> {};
template <T64>
constexpr struct is_exact<sdr_math::detail::T128<T64>> : true_type {};
template <T64>
constexpr struct is_iec559<sdr_math::detail::T128<T64>> : false_type {};
#endif
template <class T64>
struct make_unsigned<sdr_math::detail::T128<T64>> {
  typedef sdr_math::detail::T128<typename make_unsigned<T64>::type> type;
};
template <class T64>
struct make_signed<sdr_math::detail::T128<T64>> {
  typedef sdr_math::detail::T128<typename make_signed<T64>::type> type;
};
}  // namespace std

#ifndef INT128_MAX
const sdr_math::int128_t INT128_MAX{
    std::numeric_limits<sdr_math::int128_t>::max()};
const sdr_math::int128_t INT128_MIN{
    std::numeric_limits<sdr_math::int128_t>::min()};
const sdr_math::uint128_t UINT128_MAX{
    std::numeric_limits<sdr_math::uint128_t>::max()};
const sdr_math::uint128_t UINT128_MIN{
    std::numeric_limits<sdr_math::uint128_t>::min()};
#endif

#endif  // COMPONENTS_MATH_COMMON_128BIT_HH_
