// Unit tests for the scale_and_round common functions for 128 bit types
//
// This file is protected by Copyright. Please refer to the COPYRIGHT file
// distributed with this source distribution.
//
// This file is part of OpenCPI <http://www.opencpi.org>
//
// OpenCPI is free software: you can redistribute it and/or modify it under the
// terms of the GNU Lesser General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option) any
// later version.
//
// OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
// more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#include <gmp.h>
#include <cmath>
#include <cfenv>
#include "gtest/gtest.h"
#include "../scale_and_round.hh"
#include "../128bit.hh"

// LINT EXCEPTION: cpp_006: -7: There is no "C++" style header for GMP
// Full testing of 128 bit requires a long double with precision of more than
// 64 bits. Typically a long double is 80 bits with 64 bit mantissa which is
// insufficient.  So using the GMP library to test.

using sdr_math::int128_t;
using sdr_math::uint128_t;

// g++ -I/opt/opencpi/prerequisites/gtest/include -std=c++11
// scale_and_round_128_gtest.cc -L/opt/opencpi/prerequisites/gtest/centos7/lib/
// -lgtest -lgmp -lgmpxx -o scale_and_round_gtest
// LD_LIBRARY_PATH=/opt/opencpi/prerequisites/gtest/centos7/lib/
// ./scale_and_round_gtest

// ***********************************************************************
// Define common input values

namespace inputs {
// TODO(add more test data): No longer need to avoid testing larger numbers
//       as these unit test are now using GMP for high precision floating point.
// Assume that the 128 bit type is tested elsewhere for >64 arithmetic and
// restrict scale and round testing to < 64 bit cases.
static_assert(sizeof(long double) > 8, "long double > 64 bits required");
const int128_t positive_128bit[] = {
    int128_t(0),                  int128_t(1),
    int128_t(7),                  int128_t(8),
    int128_t(9),                  int128_t(21),
    int128_t(2453),               int128_t(0x5555555555555555),
    int128_t(0x7777777777777777), int128_t(INT64_MAX / 2 - 1),
    int128_t(INT64_MAX / 2),      int128_t(INT64_MAX / 2 + 1),
    int128_t(INT64_MAX - 2),      int128_t(INT64_MAX - 1),
    int128_t(INT64_MAX),          int128_t(INT64_MAX) + 1,
    int128_t(INT64_MAX) + 2,      int128_t(INT64_MAX) * 2,
    int128_t(INT64_MAX) * 2 + 1,  int128_t(UINT64_MAX / 2),
    int128_t(UINT64_MAX / 2 + 1), int128_t(UINT64_MAX - 2),
    int128_t(UINT64_MAX - 1),     int128_t(UINT64_MAX),
    int128_t(UINT64_MAX) + 1,     int128_t(UINT64_MAX) + 2,
    int128_t(UINT64_MAX) * 2,     int128_t(UINT64_MAX) * 2 + 1};
const int128_t negative_128bit[]{
    int128_t(-1),                int128_t(-2),
    int128_t(-7),                int128_t(-8),
    int128_t(-9),                int128_t(-21),
    int128_t(-2453),             int128_t(0x7777777777777777),
    int128_t(INT64_MIN / 2 + 1), int128_t(INT64_MIN / 2),
    int128_t(INT64_MIN / 2 - 1), int128_t(INT64_MIN + 2),
    int128_t(INT64_MIN + 1),     int128_t(INT64_MIN),
    int128_t(INT64_MIN) - 1,     int128_t(INT64_MIN) - 2,
    int128_t(INT64_MIN) * 2,     int128_t(INT64_MIN) * 2 + 1};
const uint128_t unsigned_128bit[]{
    uint128_t(0),                  uint128_t(1),
    uint128_t(7),                  uint128_t(8),
    uint128_t(9),                  uint128_t(21),
    uint128_t(2453),               uint128_t(0x5555555555555555),
    uint128_t(0x7777777777777777), uint128_t(UINT64_MAX / 2 - 1),
    uint128_t(UINT64_MAX / 2),     uint128_t(UINT64_MAX / 2 + 1),
    uint128_t(UINT64_MAX - 2),     uint128_t(UINT64_MAX - 1),
    uint128_t(UINT64_MAX),         uint128_t(UINT64_MAX) + 1};
}

// ***********************************************************************
// Define test functions

const uint8_t min_shift = 1;
const uint8_t low_shift = 32;
const uint8_t mid_shift = 64;
const uint8_t max_shift = 132;

// LINT EXCEPTION: cpp_001: 2: Using a non-const reference
// Load a value in into mpf float type
void load_into_mpf(sdr_math::int128_t value, mpf_t& output) {
  mpf_set_si(output, value.hi());
  mpf_mul_2exp(output, output, 64);
  mpf_add_ui(output, output, value.lo());
}
// LINT EXCEPTION: cpp_001: 1: Using a non-const reference
void load_into_mpf(sdr_math::uint128_t value, mpf_t& output) {
  mpf_set_ui(output, value.hi());
  mpf_mul_2exp(output, output, 64);
  mpf_add_ui(output, output, value.lo());
}

template <class T>
void do_half_up_test(const T& input, uint8_t bit_size) {

  auto rounding = sdr_math::rounding_type::MATH_HALF_UP;
  T actual = sdr_math::scale_and_round<T, T>(
      input, rounding, bit_size, sdr_math::overflow_type::MATH_WRAP);

  mpf_t actual_mpf;
  mpf_t expected_mpf;
  mpf_init2(expected_mpf, 256);  // precision at least 256 bits
  mpf_init2(actual_mpf, 256);    // precision at least 256 bits

  // Load actual into mpf float
  load_into_mpf(actual, actual_mpf);

  // Load input into mpf float
  load_into_mpf(input, expected_mpf);

  // Scale
  if (bit_size != 0) {
    mpf_div_2exp(expected_mpf, expected_mpf, bit_size);
  }

  // Add half
  mpf_t half_float;
  mpf_init2(half_float, 256);
  mpf_set_d(half_float, 0.5);
  mpf_add(expected_mpf, expected_mpf, half_float);
  mpf_clear(half_float);

  // Round
  mpf_floor(expected_mpf, expected_mpf);

  // Compare
  int cmp_result = mpf_cmp(actual_mpf, expected_mpf);
  EXPECT_EQ(cmp_result, 0) << "Half-up(" << int(input) << "/2^" << int(bit_size)
                           << ") = " << actual_mpf << " (expected "
                           << expected_mpf << ")";
  mpf_clear(actual_mpf);
  mpf_clear(expected_mpf);
}

template <class T, class S>
void do_half_up_saturate_test(const T& input, uint8_t bit_size) {

  auto rounding = sdr_math::rounding_type::MATH_HALF_UP;
  T actual = sdr_math::scale_and_round<T, S>(
      input, rounding, bit_size, sdr_math::overflow_type::MATH_SATURATE);

  mpf_t actual_mpf;
  mpf_t expected_mpf;
  mpf_init2(actual_mpf, 256);    // precision at least 256 bits
  mpf_init2(expected_mpf, 256);  // precision at least 256 bits

  // Load actual into mpf float
  load_into_mpf(actual, actual_mpf);

  // Load input into mpf float
  load_into_mpf(input, expected_mpf);

  // Scale
  if (bit_size != 0) {
    mpf_div_2exp(expected_mpf, expected_mpf, bit_size);
  }

  // Add half
  {
    mpf_t half_float;
    mpf_init2(half_float, 256);
    mpf_set_d(half_float, 0.5);
    mpf_add(expected_mpf, expected_mpf, half_float);
    mpf_clear(half_float);
  }

  // Round
  mpf_floor(expected_mpf, expected_mpf);

  // Saturate
  mpf_t saturate_mpf;
  mpf_init2(saturate_mpf, 256);

  load_into_mpf(static_cast<T>(std::numeric_limits<S>::max()), saturate_mpf);
  int cmp_result = mpf_cmp(expected_mpf, saturate_mpf);
  if (cmp_result > 0) {
    mpf_set(expected_mpf, saturate_mpf);
  }
  load_into_mpf(static_cast<T>(std::numeric_limits<S>::min()), saturate_mpf);
  cmp_result = mpf_cmp(expected_mpf, saturate_mpf);
  if (cmp_result < 0) {
    mpf_set(expected_mpf, saturate_mpf);
  }
  mpf_clear(saturate_mpf);

  // Compare
  cmp_result = mpf_cmp(actual_mpf, expected_mpf);
  EXPECT_EQ(cmp_result, 0) << "Half-up-saturate(" << input << "/2^"
                           << int(bit_size) << ") = " << actual_mpf
                           << " (expected " << expected_mpf << ")";

  mpf_clear(actual_mpf);
  mpf_clear(expected_mpf);
}

template <class T>
void do_truncate_test(const T& input, uint8_t bit_size) {

  auto rounding = sdr_math::rounding_type::MATH_TRUNCATE;
  T actual = sdr_math::scale_and_round<T, T>(
      input, rounding, bit_size, sdr_math::overflow_type::MATH_WRAP);

  mpf_t actual_mpf;
  mpf_t expected_mpf;
  mpf_init2(expected_mpf, 256);  // precision at least 256 bits
  mpf_init2(actual_mpf, 256);    // precision at least 256 bits

  // Load actual into mpf float
  load_into_mpf(actual, actual_mpf);

  // Load input into mpf float
  load_into_mpf(input, expected_mpf);

  // Scale
  if (bit_size != 0) {
    mpf_div_2exp(expected_mpf, expected_mpf, bit_size);
  }

  if (std::numeric_limits<T>::is_signed) {
    if (bit_size >= ((sizeof(T) * 8) - 1)) {
      mpf_set_d(expected_mpf, 0.0L);
    }
  } else {
    if (bit_size >= (sizeof(T) * 8)) {
      mpf_set_d(expected_mpf, 0.0L);
    }
  }

  // std::trunc rounds towards zero, but truncate in twos-compliment should
  // round to negative infinity.
  mpf_floor(expected_mpf, expected_mpf);

  // Compare
  int cmp_result = mpf_cmp(actual_mpf, expected_mpf);
  EXPECT_EQ(cmp_result, 0) << "Truncate(" << input << "/2^" << int(bit_size)
                           << ") = " << actual_mpf << " (expected "
                           << expected_mpf << ")";
  mpf_clear(actual_mpf);
  mpf_clear(expected_mpf);
}

template <class T, class S>
void do_truncate_saturate_test(const T& input, uint8_t bit_size) {

  auto rounding = sdr_math::rounding_type::MATH_TRUNCATE;
  T actual = sdr_math::scale_and_round<T, S>(
      input, rounding, bit_size, sdr_math::overflow_type::MATH_SATURATE);

  mpf_t actual_mpf;
  mpf_t expected_mpf;
  mpf_init2(actual_mpf, 256);    // precision at least 256 bits
  mpf_init2(expected_mpf, 256);  // precision at least 256 bits

  // Load actual into mpf float
  load_into_mpf(actual, actual_mpf);

  // Load input into mpf float
  load_into_mpf(input, expected_mpf);

  // Scale
  if (bit_size != 0) {
    mpf_div_2exp(expected_mpf, expected_mpf, bit_size);
  }

  if (std::numeric_limits<T>::is_signed) {
    if (bit_size >= ((sizeof(T) * 8) - 1)) {
      mpf_set_d(expected_mpf, 0.0L);
    }
  } else {
    if (bit_size >= (sizeof(T) * 8)) {
      mpf_set_d(expected_mpf, 0.0L);
    }
  }

  // Round, truncate in twos-compliment should
  // round to negative infinity.
  mpf_floor(expected_mpf, expected_mpf);

  // Saturate
  mpf_t saturate_mpf;
  mpf_init2(saturate_mpf, 256);

  load_into_mpf(static_cast<T>(std::numeric_limits<S>::max()), saturate_mpf);
  int cmp_result = mpf_cmp(expected_mpf, saturate_mpf);
  if (cmp_result > 0) {
    mpf_set(expected_mpf, saturate_mpf);
  }
  load_into_mpf(static_cast<T>(std::numeric_limits<S>::min()), saturate_mpf);
  cmp_result = mpf_cmp(expected_mpf, saturate_mpf);
  if (cmp_result < 0) {
    mpf_set(expected_mpf, saturate_mpf);
  }
  mpf_clear(saturate_mpf);

  // Compare
  cmp_result = mpf_cmp(actual_mpf, expected_mpf);
  EXPECT_EQ(cmp_result, 0) << "Truncate-saturate(" << input << "/2^"
                           << int(bit_size) << ") = " << actual_mpf
                           << " (expected " << expected_mpf << ")";

  mpf_clear(actual_mpf);
  mpf_clear(expected_mpf);
}

template <class T>
void do_half_even_test(const T& input, uint8_t bit_size) {

  auto rounding = sdr_math::rounding_type::MATH_HALF_EVEN;
  T actual = sdr_math::scale_and_round<T, T>(
      input, rounding, bit_size, sdr_math::overflow_type::MATH_WRAP);

  mpf_t actual_mpf;
  mpf_t expected_mpf;
  mpf_init2(expected_mpf, 256);  // precision at least 256 bits
  mpf_init2(actual_mpf, 256);    // precision at least 256 bits

  // Load actual into mpf float
  load_into_mpf(actual, actual_mpf);

  // Load input into mpf float
  load_into_mpf(input, expected_mpf);

  // Scale
  if (bit_size != 0) {
    mpf_div_2exp(expected_mpf, expected_mpf, bit_size);
  }

  // Test if whole is even and exactly half
  bool exact_half = false;
  bool whole_even = false;
  {
    mpf_t test_half;
    mpf_init2(test_half, 256);
    mpf_div_2exp(test_half, expected_mpf, 1);
    exact_half = mpf_integer_p(test_half);

    mpf_t test_even;
    mpf_init2(test_even, 256);
    mpf_floor(test_even, expected_mpf);     // Get integer value
    mpf_div_ui(test_even, test_even, 2u);   // Divide by 2
    whole_even = mpf_integer_p(test_even);  // Check is integer

    mpf_clear(test_half);
    mpf_clear(test_even);
  }

  if (exact_half && whole_even) {
    // Round down
    mpf_floor(expected_mpf, expected_mpf);
  } else {
    // Round half up
    mpf_t half_float;
    mpf_init2(half_float, 256);
    mpf_set_d(half_float, 0.5);
    mpf_add(expected_mpf, expected_mpf, half_float);
    mpf_floor(expected_mpf, expected_mpf);
    mpf_clear(half_float);
  }

  // Compare
  int cmp_result = mpf_cmp(actual_mpf, expected_mpf);
  EXPECT_EQ(cmp_result, 0) << "Half-even(" << input << "/2^" << int(bit_size)
                           << ") = " << actual_mpf << " (expected "
                           << expected_mpf << ")";
  mpf_clear(actual_mpf);
  mpf_clear(expected_mpf);
}

template <class T, class S>
void do_half_even_saturate_test(const T& input, uint8_t bit_size) {

  auto rounding = sdr_math::rounding_type::MATH_HALF_EVEN;
  T actual = sdr_math::scale_and_round<T, S>(
      input, rounding, bit_size, sdr_math::overflow_type::MATH_SATURATE);

  mpf_t actual_mpf;
  mpf_t expected_mpf;
  mpf_init2(actual_mpf, 256);    // precision at least 256 bits
  mpf_init2(expected_mpf, 256);  // precision at least 256 bits

  // Load actual into mpf float
  load_into_mpf(actual, actual_mpf);

  // Load input into mpf float
  load_into_mpf(input, expected_mpf);

  // Scale
  if (bit_size != 0) {
    mpf_div_2exp(expected_mpf, expected_mpf, bit_size);
  }

  if (std::numeric_limits<T>::is_signed) {
    if (bit_size >= ((sizeof(T) * 8) - 1)) {
      mpf_set_d(expected_mpf, 0.0L);
    }
  } else {
    if (bit_size >= (sizeof(T) * 8)) {
      mpf_set_d(expected_mpf, 0.0L);
    }
  }

  // Test if whole is even and exactly half
  bool exact_half = false;
  bool whole_even = false;
  {
    mpf_t test_half;
    mpf_init2(test_half, 256);
    mpf_div_2exp(test_half, expected_mpf, 1);
    exact_half = mpf_integer_p(test_half);

    mpf_t test_even;
    mpf_init2(test_even, 256);
    mpf_floor(test_even, expected_mpf);     // Get integer value
    mpf_div_ui(test_even, test_even, 2u);   // Divide by 2
    whole_even = mpf_integer_p(test_even);  // Check is integer

    mpf_clear(test_half);
    mpf_clear(test_even);
  }

  if (exact_half && whole_even) {
    // Round down
    mpf_floor(expected_mpf, expected_mpf);
  } else {
    // Round half up
    mpf_t half_float;
    mpf_init2(half_float, 256);
    mpf_set_d(half_float, 0.5);
    mpf_add(expected_mpf, expected_mpf, half_float);
    mpf_floor(expected_mpf, expected_mpf);
    mpf_clear(half_float);
  }

  // Saturate
  mpf_t saturate_mpf;
  mpf_init2(saturate_mpf, 256);

  load_into_mpf(static_cast<T>(std::numeric_limits<S>::max()), saturate_mpf);
  int cmp_result = mpf_cmp(expected_mpf, saturate_mpf);
  if (cmp_result > 0) {
    mpf_set(expected_mpf, saturate_mpf);
  }
  load_into_mpf(static_cast<T>(std::numeric_limits<S>::min()), saturate_mpf);
  cmp_result = mpf_cmp(expected_mpf, saturate_mpf);
  if (cmp_result < 0) {
    mpf_set(expected_mpf, saturate_mpf);
  }
  mpf_clear(saturate_mpf);

  // Compare
  cmp_result = mpf_cmp(actual_mpf, expected_mpf);
  EXPECT_EQ(cmp_result, 0) << "Half-even-saturate(" << input << "/2^"
                           << int(bit_size) << ") = " << actual_mpf
                           << " (expected " << expected_mpf << ")";

  mpf_clear(actual_mpf);
  mpf_clear(expected_mpf);
}

// ***********************************************************************
// Half Up tests

TEST(testScale, HalfUpPositive128LowShift) {
  for (sdr_math::int128_t const& val : inputs::positive_128bit) {
    for (uint8_t bit_size = min_shift; bit_size <= low_shift; bit_size++) {
      do_half_up_test(val, bit_size);
      do_half_up_saturate_test<int128_t, int8_t>(val, bit_size);
      do_half_up_saturate_test<int128_t, int16_t>(val, bit_size);
      do_half_up_saturate_test<int128_t, int32_t>(val, bit_size);
      do_half_up_saturate_test<int128_t, int64_t>(val, bit_size);
      do_half_up_saturate_test<int128_t, int128_t>(val, bit_size);
    }
  }
}
TEST(testScale, HalfUpPositive128MidShift) {
  for (sdr_math::int128_t const& val : inputs::positive_128bit) {
    for (uint8_t bit_size = low_shift; bit_size <= mid_shift; bit_size++) {
      do_half_up_test(val, bit_size);
      do_half_up_saturate_test<int128_t, int8_t>(val, bit_size);
      do_half_up_saturate_test<int128_t, int16_t>(val, bit_size);
      do_half_up_saturate_test<int128_t, int32_t>(val, bit_size);
      do_half_up_saturate_test<int128_t, int64_t>(val, bit_size);
      do_half_up_saturate_test<int128_t, int128_t>(val, bit_size);
    }
  }
}
TEST(testScale, HalfUpPositive128MaxShift) {
  for (sdr_math::int128_t const& val : inputs::positive_128bit) {
    for (uint8_t bit_size = mid_shift; bit_size <= max_shift; bit_size++) {
      do_half_up_test(val, bit_size);
      do_half_up_saturate_test<int128_t, int8_t>(val, bit_size);
      do_half_up_saturate_test<int128_t, int16_t>(val, bit_size);
      do_half_up_saturate_test<int128_t, int32_t>(val, bit_size);
      do_half_up_saturate_test<int128_t, int64_t>(val, bit_size);
      do_half_up_saturate_test<int128_t, int128_t>(val, bit_size);
    }
  }
}
TEST(testScale, HalfUpNegative128LowShift) {
  for (sdr_math::int128_t const& val : inputs::negative_128bit) {
    for (uint8_t bit_size = min_shift; bit_size <= low_shift; bit_size++) {
      do_half_up_test(val, bit_size);
      do_half_up_saturate_test<int128_t, int8_t>(val, bit_size);
      do_half_up_saturate_test<int128_t, int16_t>(val, bit_size);
      do_half_up_saturate_test<int128_t, int32_t>(val, bit_size);
      do_half_up_saturate_test<int128_t, int64_t>(val, bit_size);
      do_half_up_saturate_test<int128_t, int128_t>(val, bit_size);
    }
  }
}
TEST(testScale, HalfUpNegative128MidShift) {
  for (sdr_math::int128_t const& val : inputs::negative_128bit) {
    for (uint8_t bit_size = low_shift; bit_size <= mid_shift; bit_size++) {
      do_half_up_test(val, bit_size);
      do_half_up_saturate_test<int128_t, int8_t>(val, bit_size);
      do_half_up_saturate_test<int128_t, int16_t>(val, bit_size);
      do_half_up_saturate_test<int128_t, int32_t>(val, bit_size);
      do_half_up_saturate_test<int128_t, int64_t>(val, bit_size);
      do_half_up_saturate_test<int128_t, int128_t>(val, bit_size);
    }
  }
}
TEST(testScale, HalfUpNegative128MaxShift) {
  for (sdr_math::int128_t const& val : inputs::negative_128bit) {
    for (uint8_t bit_size = mid_shift; bit_size <= max_shift; bit_size++) {
      do_half_up_test(val, bit_size);
      do_half_up_saturate_test<int128_t, int8_t>(val, bit_size);
      do_half_up_saturate_test<int128_t, int16_t>(val, bit_size);
      do_half_up_saturate_test<int128_t, int32_t>(val, bit_size);
      do_half_up_saturate_test<int128_t, int64_t>(val, bit_size);
      do_half_up_saturate_test<int128_t, int128_t>(val, bit_size);
    }
  }
}
TEST(testScale, HalfUpUnsigned128LowShift) {
  for (sdr_math::uint128_t const& val : inputs::unsigned_128bit) {
    for (uint8_t bit_size = min_shift; bit_size <= low_shift; bit_size++) {
      do_half_up_test(val, bit_size);
      do_half_up_saturate_test<uint128_t, uint8_t>(val, bit_size);
      do_half_up_saturate_test<uint128_t, uint16_t>(val, bit_size);
      do_half_up_saturate_test<uint128_t, uint32_t>(val, bit_size);
      do_half_up_saturate_test<uint128_t, uint64_t>(val, bit_size);
      do_half_up_saturate_test<uint128_t, uint128_t>(val, bit_size);
    }
  }
}
TEST(testScale, HalfUpUnsigned128MidShift) {
  for (sdr_math::uint128_t const& val : inputs::unsigned_128bit) {
    for (uint8_t bit_size = low_shift; bit_size <= mid_shift; bit_size++) {
      do_half_up_test(val, bit_size);
      do_half_up_saturate_test<uint128_t, uint8_t>(val, bit_size);
      do_half_up_saturate_test<uint128_t, uint16_t>(val, bit_size);
      do_half_up_saturate_test<uint128_t, uint32_t>(val, bit_size);
      do_half_up_saturate_test<uint128_t, uint64_t>(val, bit_size);
      do_half_up_saturate_test<uint128_t, uint128_t>(val, bit_size);
    }
  }
}
TEST(testScale, HalfUpUnsigned128MaxShift) {
  for (sdr_math::uint128_t const& val : inputs::unsigned_128bit) {
    for (uint8_t bit_size = mid_shift; bit_size <= max_shift; bit_size++) {
      do_half_up_test(val, bit_size);
      do_half_up_saturate_test<uint128_t, uint8_t>(val, bit_size);
      do_half_up_saturate_test<uint128_t, uint16_t>(val, bit_size);
      do_half_up_saturate_test<uint128_t, uint32_t>(val, bit_size);
      do_half_up_saturate_test<uint128_t, uint64_t>(val, bit_size);
      do_half_up_saturate_test<uint128_t, uint128_t>(val, bit_size);
    }
  }
}

// ***********************************************************************
// Truncate tests

TEST(testScale, TruncatePositive128LowShift) {
  for (sdr_math::int128_t const& val : inputs::positive_128bit) {
    for (uint8_t bit_size = min_shift; bit_size <= low_shift; bit_size++) {
      do_truncate_test(val, bit_size);
      do_truncate_saturate_test<int128_t, int8_t>(val, bit_size);
      do_truncate_saturate_test<int128_t, int16_t>(val, bit_size);
      do_truncate_saturate_test<int128_t, int32_t>(val, bit_size);
      do_truncate_saturate_test<int128_t, int64_t>(val, bit_size);
      do_truncate_saturate_test<int128_t, int128_t>(val, bit_size);
    }
  }
}
TEST(testScale, TruncatePositive128MidShift) {
  for (sdr_math::int128_t const& val : inputs::positive_128bit) {
    for (uint8_t bit_size = low_shift; bit_size <= mid_shift; bit_size++) {
      do_truncate_test(val, bit_size);
      do_truncate_saturate_test<int128_t, int8_t>(val, bit_size);
      do_truncate_saturate_test<int128_t, int16_t>(val, bit_size);
      do_truncate_saturate_test<int128_t, int32_t>(val, bit_size);
      do_truncate_saturate_test<int128_t, int64_t>(val, bit_size);
      do_truncate_saturate_test<int128_t, int128_t>(val, bit_size);
    }
  }
}
TEST(testScale, TruncatePositive128MaxShift) {
  for (sdr_math::int128_t const& val : inputs::positive_128bit) {
    for (uint8_t bit_size = mid_shift; bit_size <= max_shift; bit_size++) {
      do_truncate_test(val, bit_size);
      do_truncate_saturate_test<int128_t, int8_t>(val, bit_size);
      do_truncate_saturate_test<int128_t, int16_t>(val, bit_size);
      do_truncate_saturate_test<int128_t, int32_t>(val, bit_size);
      do_truncate_saturate_test<int128_t, int64_t>(val, bit_size);
      do_truncate_saturate_test<int128_t, int128_t>(val, bit_size);
    }
  }
}

TEST(testScale, TruncateNegative128LowShift) {
  for (sdr_math::int128_t const& val : inputs::negative_128bit) {
    for (uint8_t bit_size = min_shift; bit_size <= low_shift; bit_size++) {
      do_truncate_test(val, bit_size);
      do_truncate_saturate_test<int128_t, int8_t>(val, bit_size);
      do_truncate_saturate_test<int128_t, int16_t>(val, bit_size);
      do_truncate_saturate_test<int128_t, int32_t>(val, bit_size);
      do_truncate_saturate_test<int128_t, int64_t>(val, bit_size);
      do_truncate_saturate_test<int128_t, int128_t>(val, bit_size);
    }
  }
}
TEST(testScale, TruncateNegative128MidShift) {
  for (sdr_math::int128_t const& val : inputs::negative_128bit) {
    for (uint8_t bit_size = low_shift; bit_size <= mid_shift; bit_size++) {
      do_truncate_test(val, bit_size);
      do_truncate_saturate_test<int128_t, int8_t>(val, bit_size);
      do_truncate_saturate_test<int128_t, int16_t>(val, bit_size);
      do_truncate_saturate_test<int128_t, int32_t>(val, bit_size);
      do_truncate_saturate_test<int128_t, int64_t>(val, bit_size);
      do_truncate_saturate_test<int128_t, int128_t>(val, bit_size);
    }
  }
}
TEST(testScale, TruncateNegative128MaxShift) {
  for (sdr_math::int128_t const& val : inputs::negative_128bit) {
    for (uint8_t bit_size = mid_shift; bit_size <= max_shift; bit_size++) {
      do_truncate_test(val, bit_size);
      do_truncate_saturate_test<int128_t, int8_t>(val, bit_size);
      do_truncate_saturate_test<int128_t, int16_t>(val, bit_size);
      do_truncate_saturate_test<int128_t, int32_t>(val, bit_size);
      do_truncate_saturate_test<int128_t, int64_t>(val, bit_size);
      do_truncate_saturate_test<int128_t, int128_t>(val, bit_size);
    }
  }
}

TEST(testScale, TruncateUnsigned128LowShift) {
  for (sdr_math::uint128_t const& val : inputs::unsigned_128bit) {
    for (uint8_t bit_size = min_shift; bit_size <= low_shift; bit_size++) {
      do_truncate_test(val, bit_size);
      do_truncate_saturate_test<uint128_t, uint8_t>(val, bit_size);
      do_truncate_saturate_test<uint128_t, uint16_t>(val, bit_size);
      do_truncate_saturate_test<uint128_t, uint32_t>(val, bit_size);
      do_truncate_saturate_test<uint128_t, uint64_t>(val, bit_size);
      do_truncate_saturate_test<uint128_t, uint128_t>(val, bit_size);
    }
  }
}
TEST(testScale, TruncateUnsigned128MidShift) {
  for (sdr_math::uint128_t const& val : inputs::unsigned_128bit) {
    for (uint8_t bit_size = low_shift; bit_size <= mid_shift; bit_size++) {
      do_truncate_test(val, bit_size);
      do_truncate_saturate_test<uint128_t, uint8_t>(val, bit_size);
      do_truncate_saturate_test<uint128_t, uint16_t>(val, bit_size);
      do_truncate_saturate_test<uint128_t, uint32_t>(val, bit_size);
      do_truncate_saturate_test<uint128_t, uint64_t>(val, bit_size);
      do_truncate_saturate_test<uint128_t, uint128_t>(val, bit_size);
    }
  }
}
TEST(testScale, TruncateUnsigned128MaxShift) {
  for (sdr_math::uint128_t const& val : inputs::unsigned_128bit) {
    for (uint8_t bit_size = mid_shift; bit_size <= max_shift; bit_size++) {
      do_truncate_test(val, bit_size);
      do_truncate_saturate_test<uint128_t, uint8_t>(val, bit_size);
      do_truncate_saturate_test<uint128_t, uint16_t>(val, bit_size);
      do_truncate_saturate_test<uint128_t, uint32_t>(val, bit_size);
      do_truncate_saturate_test<uint128_t, uint64_t>(val, bit_size);
      do_truncate_saturate_test<uint128_t, uint128_t>(val, bit_size);
    }
  }
}

// ***********************************************************************
// Half Even tests

TEST(testScale, HalfEvenPositive128LowShift) {
  for (sdr_math::int128_t const& val : inputs::positive_128bit) {
    for (uint8_t bit_size = min_shift; bit_size <= low_shift; bit_size++) {
      do_half_even_test(val, bit_size);
      do_half_even_saturate_test<int128_t, int8_t>(val, bit_size);
      do_half_even_saturate_test<int128_t, int16_t>(val, bit_size);
      do_half_even_saturate_test<int128_t, int32_t>(val, bit_size);
      do_half_even_saturate_test<int128_t, int64_t>(val, bit_size);
      do_half_even_saturate_test<int128_t, int128_t>(val, bit_size);
    }
  }
}
TEST(testScale, HalfEvenPositive128MidShift) {
  for (sdr_math::int128_t const& val : inputs::positive_128bit) {
    for (uint8_t bit_size = low_shift; bit_size <= mid_shift; bit_size++) {
      do_half_even_test(val, bit_size);
      do_half_even_saturate_test<int128_t, int8_t>(val, bit_size);
      do_half_even_saturate_test<int128_t, int16_t>(val, bit_size);
      do_half_even_saturate_test<int128_t, int32_t>(val, bit_size);
      do_half_even_saturate_test<int128_t, int64_t>(val, bit_size);
      do_half_even_saturate_test<int128_t, int128_t>(val, bit_size);
    }
  }
}
TEST(testScale, HalfEvenPositive128MaxShift) {
  for (sdr_math::int128_t const& val : inputs::positive_128bit) {
    for (uint8_t bit_size = mid_shift; bit_size <= max_shift; bit_size++) {
      do_half_even_test(val, bit_size);
      do_half_even_saturate_test<int128_t, int8_t>(val, bit_size);
      do_half_even_saturate_test<int128_t, int16_t>(val, bit_size);
      do_half_even_saturate_test<int128_t, int32_t>(val, bit_size);
      do_half_even_saturate_test<int128_t, int64_t>(val, bit_size);
      do_half_even_saturate_test<int128_t, int128_t>(val, bit_size);
    }
  }
}

TEST(testScale, HalfEvenNegative128LowShift) {
  for (sdr_math::int128_t const& val : inputs::negative_128bit) {
    for (uint8_t bit_size = min_shift; bit_size <= low_shift; bit_size++) {
      do_half_even_test(val, bit_size);
      do_half_even_saturate_test<int128_t, int8_t>(val, bit_size);
      do_half_even_saturate_test<int128_t, int16_t>(val, bit_size);
      do_half_even_saturate_test<int128_t, int32_t>(val, bit_size);
      do_half_even_saturate_test<int128_t, int64_t>(val, bit_size);
      do_half_even_saturate_test<int128_t, int128_t>(val, bit_size);
    }
  }
}
TEST(testScale, HalfEvenNegative128MidShift) {
  for (sdr_math::int128_t const& val : inputs::negative_128bit) {
    for (uint8_t bit_size = low_shift; bit_size <= mid_shift; bit_size++) {
      do_half_even_test(val, bit_size);
      do_half_even_saturate_test<int128_t, int8_t>(val, bit_size);
      do_half_even_saturate_test<int128_t, int16_t>(val, bit_size);
      do_half_even_saturate_test<int128_t, int32_t>(val, bit_size);
      do_half_even_saturate_test<int128_t, int64_t>(val, bit_size);
      do_half_even_saturate_test<int128_t, int128_t>(val, bit_size);
    }
  }
}
TEST(testScale, HalfEvenNegative128MaxShift) {
  for (sdr_math::int128_t const& val : inputs::negative_128bit) {
    for (uint8_t bit_size = mid_shift; bit_size <= max_shift; bit_size++) {
      do_half_even_test(val, bit_size);
      do_half_even_saturate_test<int128_t, int8_t>(val, bit_size);
      do_half_even_saturate_test<int128_t, int16_t>(val, bit_size);
      do_half_even_saturate_test<int128_t, int32_t>(val, bit_size);
      do_half_even_saturate_test<int128_t, int64_t>(val, bit_size);
      do_half_even_saturate_test<int128_t, int128_t>(val, bit_size);
    }
  }
}

TEST(testScale, HalfEvenUnsigned128LowShift) {
  for (sdr_math::uint128_t const& val : inputs::unsigned_128bit) {
    for (uint8_t bit_size = min_shift; bit_size <= low_shift; bit_size++) {
      do_half_even_test(val, bit_size);
      do_half_even_saturate_test<uint128_t, uint8_t>(val, bit_size);
      do_half_even_saturate_test<uint128_t, uint16_t>(val, bit_size);
      do_half_even_saturate_test<uint128_t, uint32_t>(val, bit_size);
      do_half_even_saturate_test<uint128_t, uint64_t>(val, bit_size);
      do_half_even_saturate_test<uint128_t, uint128_t>(val, bit_size);
    }
  }
}
TEST(testScale, HalfEvenUnsigned128MidShift) {
  for (sdr_math::uint128_t const& val : inputs::unsigned_128bit) {
    for (uint8_t bit_size = low_shift; bit_size <= mid_shift; bit_size++) {
      do_half_even_test(val, bit_size);
      do_half_even_saturate_test<uint128_t, uint8_t>(val, bit_size);
      do_half_even_saturate_test<uint128_t, uint16_t>(val, bit_size);
      do_half_even_saturate_test<uint128_t, uint32_t>(val, bit_size);
      do_half_even_saturate_test<uint128_t, uint64_t>(val, bit_size);
      do_half_even_saturate_test<uint128_t, uint128_t>(val, bit_size);
    }
  }
}
TEST(testScale, HalfEvenUnsigned128MaxShift) {
  for (sdr_math::uint128_t const& val : inputs::unsigned_128bit) {
    for (uint8_t bit_size = mid_shift; bit_size <= max_shift; bit_size++) {
      do_half_even_test(val, bit_size);
      do_half_even_saturate_test<uint128_t, uint8_t>(val, bit_size);
      do_half_even_saturate_test<uint128_t, uint16_t>(val, bit_size);
      do_half_even_saturate_test<uint128_t, uint32_t>(val, bit_size);
      do_half_even_saturate_test<uint128_t, uint64_t>(val, bit_size);
      do_half_even_saturate_test<uint128_t, uint128_t>(val, bit_size);
    }
  }
}

// ***********************************************************************
// Define main

int main(int argc, char** argv) {
  ::testing::InitGoogleTest(&argc, argv);
  std::cout << std::hex;
  return RUN_ALL_TESTS();
}
