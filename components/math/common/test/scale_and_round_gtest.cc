// Unit tests for the scale_and_round common functions
//
// This file is protected by Copyright. Please refer to the COPYRIGHT file
// distributed with this source distribution.
//
// This file is part of OpenCPI <http://www.opencpi.org>
//
// OpenCPI is free software: you can redistribute it and/or modify it under the
// terms of the GNU Lesser General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option) any
// later version.
//
// OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
// more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#include <cmath>
#include <cfenv>
#include "gtest/gtest.h"
#include "../scale_and_round.hh"

// g++ -I/opt/opencpi/prerequisites/gtest/include -std=c++11
// scale_and_round_gtest.cc -L/opt/opencpi/prerequisites/gtest/centos7/lib/
// -lgtest -o scale_and_round_gtest
// LD_LIBRARY_PATH=/opt/opencpi/prerequisites/gtest/centos7/lib/
// ./scale_and_round_gtest

// ***********************************************************************
// Define common input values

namespace inputs {
const int8_t positive_8bit[]{
    0,                  1,              7,                  8,
    9,                  21,             0x55,               0x77,
    (INT8_MAX / 2) - 1, (INT8_MAX / 2), (INT8_MAX / 2) + 1, INT8_MAX - 2,
    INT8_MAX - 1,       INT8_MAX};
const int8_t negative_8bit[]{
    -1,                 -2,             -7,                 -8,
    -9,                 -21,            int8_t(0xBB),       int8_t(0xF7),
    (INT8_MIN / 2) + 1, (INT8_MIN / 2), (INT8_MIN / 2) - 1, INT8_MIN + 2,
    INT8_MIN + 1,       INT8_MIN};
const uint8_t unsigned_8bit[]{
    0,             1,                   7,               8,
    9,             21,                  245,             uint8_t(0x55),
    0x77,          (UINT8_MAX / 2) - 1, (UINT8_MAX / 2), (UINT8_MAX / 2) + 1,
    UINT8_MAX - 2, UINT8_MAX - 1,       UINT8_MAX};

const int16_t positive_16bit[]{
    0,             1,                   7,               8,
    9,             21,                  2453,            int16_t(0x5555),
    0x7777,        (INT16_MAX / 2) - 1, (INT16_MAX / 2), (INT16_MAX / 2) + 1,
    INT16_MAX - 2, INT16_MAX - 1,       INT16_MAX};
const int16_t negative_16bit[]{
    -1,              -2,                  -7,              -8,
    -9,              -21,                 -2453,           int16_t(0xBBBB),
    int16_t(0xF777), (INT16_MIN / 2) + 1, (INT16_MIN / 2), (INT16_MIN / 2) - 1,
    INT16_MIN + 2,   INT16_MIN + 1,       INT16_MIN};
const uint16_t unsigned_16bit[]{
    0,                    1,                7,
    8,                    9,                21,
    2453,                 0x5555,           0x7777,
    (UINT16_MAX / 2) - 1, (UINT16_MAX / 2), (UINT16_MAX / 2) + 1,
    UINT16_MAX - 2,       UINT16_MAX - 1,   UINT16_MAX};

const int32_t positive_32bit[]{
    0,             1,                   7,               8,
    9,             21,                  2453,            0x55555555,
    0x77777777,    (INT32_MAX / 2) - 1, (INT32_MAX / 2), (INT32_MAX / 2) + 1,
    INT32_MAX - 2, INT32_MAX - 1,       INT32_MAX};
const int32_t negative_32bit[]{
    -1,                  -2,                  -7,
    -8,                  -9,                  -21,
    -2453,               int32_t(0xBBBBBBBB), int32_t(0xF7777777),
    (INT32_MIN / 2) + 1, (INT32_MIN / 2),     (INT32_MIN / 2) - 1,
    INT32_MIN + 2,       INT32_MIN + 1,       INT32_MIN};
const uint32_t unsigned_32bit[]{
    0,                    1,                7,
    8,                    9,                21,
    2453,                 0x55555555,       0x77777777,
    (UINT32_MAX / 2) - 1, (UINT32_MAX / 2), (UINT32_MAX / 2) + 1,
    UINT32_MAX - 2,       UINT32_MAX - 1,   UINT32_MAX};

const int64_t positive_64bit[]{
    0,                           1,
    7,                           8,
    9,                           21,
    2453,                        int64_t(0x5555555555555555),
    int64_t(0x7777777777777777), (INT64_MAX / 2) - 1,
    (INT64_MAX / 2),             (INT64_MAX / 2) + 1,
    INT64_MAX - 2,               INT64_MAX - 1,
    INT64_MAX};
const int64_t negative_64bit[]{
    -1,                          -2,
    -7,                          -8,
    -9,                          -21,
    -2453,                       int64_t(0xbbbbBBBBbbbbBBBB),
    int64_t(0xF777777777777777), (INT64_MIN / 2) + 1,
    (INT64_MIN / 2),             (INT64_MIN / 2) - 1,
    INT64_MIN + 2,               INT64_MIN + 1,
    INT64_MIN};
const uint64_t unsigned_64bit[]{
    0,                    1,                  7,
    8,                    9,                  21,
    2453,                 0x5555555555555555, 0x7777777777777777,
    (UINT64_MAX / 2) - 1, (UINT64_MAX / 2),   (UINT64_MAX / 2) + 1,
    UINT64_MAX - 2,       UINT64_MAX - 1,     UINT64_MAX};
}

// ***********************************************************************
// Define test functions

const uint8_t min_shift = 1;
const uint8_t low_shift = 8;
const uint8_t mid_shift = 16;
const uint8_t max_shift = 68;  // Test shifting too much

template <class T>
void do_half_up_test(const T& input, uint8_t bit_size) {
  auto rounding = sdr_math::rounding_type::MATH_HALF_UP;

  T actual = sdr_math::scale_and_round<T, T>(
      input, rounding, bit_size, sdr_math::overflow_type::MATH_WRAP);

  long double expected_double{0};
  if (bit_size == 0) {
    expected_double = (static_cast<long double>(input));
  } else {
    expected_double =
        (static_cast<long double>(input)) / std::pow(2.0L, bit_size);

    if ((!std::numeric_limits<T>::is_signed) && (bit_size > (sizeof(T) * 8))) {
      expected_double = 0.0L;
    }
  }

  T expected = static_cast<T>(std::floor(expected_double + 0.5));
  EXPECT_EQ(actual, expected) << "Half-up(" << input << "/2^" << int(bit_size)
                              << ") = " << expected_double;
}

template <class T, class S>
void do_half_up_saturate_test(const T& input, uint8_t bit_size) {
  auto rounding = sdr_math::rounding_type::MATH_HALF_UP;

  T actual = sdr_math::scale_and_round<T, S>(
      input, rounding, bit_size, sdr_math::overflow_type::MATH_SATURATE);

  long double expected_double =
      (static_cast<long double>(input)) / std::pow(2.0L, bit_size);

  if ((!std::numeric_limits<T>::is_signed) && (bit_size > (sizeof(T) * 8))) {
    expected_double = 0.0L;
  }

  T expected = static_cast<T>(std::floor(expected_double + 0.5));
  if (expected > std::numeric_limits<S>::max()) {
    expected = std::numeric_limits<S>::max();
  } else if (expected < std::numeric_limits<S>::min()) {
    expected = std::numeric_limits<S>::min();
  }
  EXPECT_EQ(actual, expected) << "Half-up-saturate(" << input << "/2^"
                              << int(bit_size) << ") = " << expected_double;
}

template <class T>
void do_truncate_test(const T& input, uint8_t bit_size) {
  auto rounding = sdr_math::rounding_type::MATH_TRUNCATE;

  T actual = sdr_math::scale_and_round<T, T>(
      input, rounding, bit_size, sdr_math::overflow_type::MATH_WRAP);

  long double expected_double =
      (static_cast<long double>(input)) / std::pow(2.0L, bit_size);

  if (std::numeric_limits<T>::is_signed) {
    if (bit_size >= ((sizeof(T) * 8) - 1)) {
      expected_double = 0.0L;
    }
  } else {

    if (bit_size >= (sizeof(T) * 8)) {
      expected_double = 0.0L;
    }
  }

  // std::trunc rounds towards zero, but truncate in twos-compliment should
  // round to negative infinity.
  int original_mode = std::fegetround();
  std::fesetround(FE_DOWNWARD);
  T expected = static_cast<T>(std::nearbyint(expected_double));
  std::fesetround(original_mode);

  EXPECT_EQ(actual, expected) << "Truncate(" << input << "/2^" << int(bit_size)
                              << ") = " << expected_double;
}

template <class T, class S>
void do_truncate_saturate_test(const T& input, uint8_t bit_size) {
  auto rounding = sdr_math::rounding_type::MATH_TRUNCATE;

  T actual = sdr_math::scale_and_round<T, S>(
      input, rounding, bit_size, sdr_math::overflow_type::MATH_SATURATE);

  long double expected_double =
      (static_cast<long double>(input)) / std::pow(2.0L, bit_size);

  // std::trunc rounds towards zero, but truncate in twos-compliment should
  // round to negative infinity.
  int original_mode = std::fegetround();
  std::fesetround(FE_DOWNWARD);
  T expected = static_cast<T>(std::nearbyint(expected_double));
  if (expected > std::numeric_limits<S>::max()) {
    expected = std::numeric_limits<S>::max();
  } else if (expected < std::numeric_limits<S>::min()) {
    expected = std::numeric_limits<S>::min();
  }
  std::fesetround(original_mode);

  if (std::numeric_limits<T>::is_signed) {
    if (bit_size >= ((sizeof(T) * 8) - 1)) {
      expected = 0;
    }
  } else {

    if (bit_size >= (sizeof(T) * 8)) {
      expected = 0;
    }
  }

  EXPECT_EQ(actual, expected) << "Truncate-saturate(" << input << "/2^"
                              << int(bit_size) << ") = " << expected_double;
}

template <class T>
void do_half_even_test(const T& input, uint8_t bit_size) {
  auto rounding = sdr_math::rounding_type::MATH_HALF_EVEN;

  T actual = sdr_math::scale_and_round<T, T>(
      input, rounding, bit_size, sdr_math::overflow_type::MATH_WRAP);

  long double expected_double =
      static_cast<long double>(input) / std::pow(2.0L, bit_size);

  if (std::numeric_limits<T>::is_signed) {
    if (bit_size >= ((sizeof(T) * 8) - 1)) {
      expected_double = 0.0L;
    }
  } else {

    if (bit_size >= (sizeof(T) * 8)) {
      expected_double = 0.0L;
    }
  }

  int original_mode = std::fegetround();
  std::fesetround(FE_TONEAREST);
  T expected = static_cast<T>(std::nearbyint(expected_double));
  std::fesetround(original_mode);

  ASSERT_EQ(actual, expected) << "Half-even(" << input << "/2^" << int(bit_size)
                              << ") = " << expected_double;
}

template <class T, class S>
void do_half_even_saturate_test(const T& input, uint8_t bit_size) {
  auto rounding = sdr_math::rounding_type::MATH_HALF_EVEN;

  T actual = sdr_math::scale_and_round<T, S>(
      input, rounding, bit_size, sdr_math::overflow_type::MATH_SATURATE);

  long double expected_double =
      static_cast<long double>(input) / std::pow(2.0L, bit_size);

  int original_mode = std::fegetround();
  std::fesetround(FE_TONEAREST);
  T expected = static_cast<T>(std::nearbyint(expected_double));
  if (expected > std::numeric_limits<S>::max()) {
    expected = std::numeric_limits<S>::max();
  } else if (expected < std::numeric_limits<S>::min()) {
    expected = std::numeric_limits<S>::min();
  }
  std::fesetround(original_mode);

  if (std::numeric_limits<T>::is_signed) {
    if (bit_size >= ((sizeof(T) * 8) - 1)) {
      expected = 0;
    }
  } else {

    if (bit_size >= (sizeof(T) * 8u)) {
      expected = 0;
    }
  }

  ASSERT_EQ(actual, expected) << "Half-even-saturate(" << input << "/2^"
                              << int(bit_size) << ") = " << expected_double;
}

// ***********************************************************************
// Half Up tests

TEST(testScale, HalfUpPositive8LowShift) {
  for (int8_t const& val : inputs::positive_8bit) {
    for (uint8_t bit_size = min_shift; bit_size <= low_shift; bit_size++) {
      do_half_up_test(val, bit_size);
      do_half_up_saturate_test<int8_t, int8_t>(val, bit_size);
    }
  }
}
TEST(testScale, HalfUpPositive8MidShift) {
  for (int8_t const& val : inputs::positive_8bit) {
    for (uint8_t bit_size = low_shift; bit_size <= mid_shift; bit_size++) {
      do_half_up_test(val, bit_size);
      do_half_up_saturate_test<int8_t, int8_t>(val, bit_size);
    }
  }
}
TEST(testScale, HalfUpPositive8MaxShift) {
  for (int8_t const& val : inputs::positive_8bit) {
    for (uint8_t bit_size = mid_shift; bit_size <= max_shift; bit_size++) {
      do_half_up_test(val, bit_size);
      do_half_up_saturate_test<int8_t, int8_t>(val, bit_size);
    }
  }
}

TEST(testScale, HalfUpNegative8LowShift) {
  for (int8_t const& val : inputs::negative_8bit) {
    for (uint8_t bit_size = min_shift; bit_size <= low_shift; bit_size++) {
      do_half_up_test(val, bit_size);
      do_half_up_saturate_test<int8_t, int8_t>(val, bit_size);
    }
  }
}
TEST(testScale, HalfUpNegative8MidShift) {
  for (int8_t const& val : inputs::negative_8bit) {
    for (uint8_t bit_size = low_shift; bit_size <= mid_shift; bit_size++) {
      do_half_up_test(val, bit_size);
      do_half_up_saturate_test<int8_t, int8_t>(val, bit_size);
    }
  }
}
TEST(testScale, HalfUpNegative8MaxShift) {
  for (int8_t const& val : inputs::negative_8bit) {
    for (uint8_t bit_size = mid_shift; bit_size <= max_shift; bit_size++) {
      do_half_up_test(val, bit_size);
      do_half_up_saturate_test<int8_t, int8_t>(val, bit_size);
    }
  }
}

TEST(testScale, HalfUpUnsigned8LowShift) {
  for (uint8_t const& val : inputs::unsigned_8bit) {
    for (uint8_t bit_size = min_shift; bit_size <= low_shift; bit_size++) {
      do_half_up_test(val, bit_size);
      do_half_up_saturate_test<uint8_t, uint8_t>(val, bit_size);
    }
  }
}
TEST(testScale, HalfUpUnsigned8MidShift) {
  for (uint8_t const& val : inputs::unsigned_8bit) {
    for (uint8_t bit_size = low_shift; bit_size <= mid_shift; bit_size++) {
      do_half_up_test(val, bit_size);
      do_half_up_saturate_test<uint8_t, uint8_t>(val, bit_size);
    }
  }
}
TEST(testScale, HalfUpUnsigned8MaxShift) {
  for (uint8_t const& val : inputs::unsigned_8bit) {
    for (uint8_t bit_size = mid_shift; bit_size <= max_shift; bit_size++) {
      do_half_up_test(val, bit_size);
      do_half_up_saturate_test<uint8_t, uint8_t>(val, bit_size);
    }
  }
}

TEST(testScale, HalfUpPositive16LowShift) {
  for (int16_t const& val : inputs::positive_16bit) {
    for (uint8_t bit_size = min_shift; bit_size <= low_shift; bit_size++) {
      do_half_up_test(val, bit_size);
      do_half_up_saturate_test<int16_t, int8_t>(val, bit_size);
      do_half_up_saturate_test<int16_t, int16_t>(val, bit_size);
    }
  }
}
TEST(testScale, HalfUpPositive16MidShift) {
  for (int16_t const& val : inputs::positive_16bit) {
    for (uint8_t bit_size = low_shift; bit_size <= mid_shift; bit_size++) {
      do_half_up_test(val, bit_size);
      do_half_up_saturate_test<int16_t, int8_t>(val, bit_size);
      do_half_up_saturate_test<int16_t, int16_t>(val, bit_size);
    }
  }
}
TEST(testScale, HalfUpPositive16MaxShift) {
  for (int16_t const& val : inputs::positive_16bit) {
    for (uint8_t bit_size = mid_shift; bit_size <= max_shift; bit_size++) {
      do_half_up_test(val, bit_size);
      do_half_up_saturate_test<int16_t, int8_t>(val, bit_size);
      do_half_up_saturate_test<int16_t, int16_t>(val, bit_size);
    }
  }
}

TEST(testScale, HalfUpNegative16LowShift) {
  for (int16_t const& val : inputs::negative_16bit) {
    for (uint8_t bit_size = min_shift; bit_size <= low_shift; bit_size++) {
      do_half_up_test(val, bit_size);
      do_half_up_saturate_test<int16_t, int8_t>(val, bit_size);
      do_half_up_saturate_test<int16_t, int16_t>(val, bit_size);
    }
  }
}
TEST(testScale, HalfUpNegative16MidShift) {
  for (int16_t const& val : inputs::negative_16bit) {
    for (uint8_t bit_size = low_shift; bit_size <= mid_shift; bit_size++) {
      do_half_up_test(val, bit_size);
      do_half_up_saturate_test<int16_t, int8_t>(val, bit_size);
      do_half_up_saturate_test<int16_t, int16_t>(val, bit_size);
    }
  }
}
TEST(testScale, HalfUpNegative16MaxShift) {
  for (int16_t const& val : inputs::negative_16bit) {
    for (uint8_t bit_size = mid_shift; bit_size <= max_shift; bit_size++) {
      do_half_up_test(val, bit_size);
      do_half_up_saturate_test<int16_t, int8_t>(val, bit_size);
      do_half_up_saturate_test<int16_t, int16_t>(val, bit_size);
    }
  }
}
TEST(testScale, HalfUpUnsigned16LowShift) {
  for (uint16_t const& val : inputs::unsigned_16bit) {
    for (uint8_t bit_size = min_shift; bit_size <= low_shift; bit_size++) {
      do_half_up_test(val, bit_size);
      do_half_up_saturate_test<uint16_t, uint8_t>(val, bit_size);
      do_half_up_saturate_test<uint16_t, uint16_t>(val, bit_size);
    }
  }
}
TEST(testScale, HalfUpUnsigned16MidShift) {
  for (uint16_t const& val : inputs::unsigned_16bit) {
    for (uint8_t bit_size = low_shift; bit_size <= mid_shift; bit_size++) {
      do_half_up_test(val, bit_size);
      do_half_up_saturate_test<uint16_t, uint8_t>(val, bit_size);
      do_half_up_saturate_test<uint16_t, uint16_t>(val, bit_size);
    }
  }
}
TEST(testScale, HalfUpUnsigned16MaxShift) {
  for (uint16_t const& val : inputs::unsigned_16bit) {
    for (uint8_t bit_size = mid_shift; bit_size <= max_shift; bit_size++) {
      do_half_up_test(val, bit_size);
      do_half_up_saturate_test<uint16_t, uint8_t>(val, bit_size);
      do_half_up_saturate_test<uint16_t, uint16_t>(val, bit_size);
    }
  }
}

TEST(testScale, HalfUpPositive32LowShift) {
  for (int32_t const& val : inputs::positive_32bit) {
    for (uint8_t bit_size = min_shift; bit_size <= low_shift; bit_size++) {
      do_half_up_test(val, bit_size);
      do_half_up_saturate_test<int32_t, int8_t>(val, bit_size);
      do_half_up_saturate_test<int32_t, int16_t>(val, bit_size);
      do_half_up_saturate_test<int32_t, int32_t>(val, bit_size);
    }
  }
}
TEST(testScale, HalfUpPositive32MidShift) {
  for (int32_t const& val : inputs::positive_32bit) {
    for (uint8_t bit_size = low_shift; bit_size <= mid_shift; bit_size++) {
      do_half_up_test(val, bit_size);
    }
  }
}
TEST(testScale, HalfUpPositive32MaxShift) {
  for (int32_t const& val : inputs::positive_32bit) {
    for (uint8_t bit_size = mid_shift; bit_size <= max_shift; bit_size++) {
      do_half_up_test(val, bit_size);
      do_half_up_saturate_test<int32_t, int8_t>(val, bit_size);
      do_half_up_saturate_test<int32_t, int16_t>(val, bit_size);
      do_half_up_saturate_test<int32_t, int32_t>(val, bit_size);
    }
  }
}
TEST(testScale, HalfUpNegative32LowShift) {
  for (int32_t const& val : inputs::negative_32bit) {
    for (uint8_t bit_size = min_shift; bit_size <= low_shift; bit_size++) {
      do_half_up_test(val, bit_size);
      do_half_up_saturate_test<int32_t, int8_t>(val, bit_size);
      do_half_up_saturate_test<int32_t, int16_t>(val, bit_size);
      do_half_up_saturate_test<int32_t, int32_t>(val, bit_size);
    }
  }
}
TEST(testScale, HalfUpNegative32MidShift) {
  for (int32_t const& val : inputs::negative_32bit) {
    for (uint8_t bit_size = low_shift; bit_size <= mid_shift; bit_size++) {
      do_half_up_test(val, bit_size);
      do_half_up_saturate_test<int32_t, int8_t>(val, bit_size);
      do_half_up_saturate_test<int32_t, int16_t>(val, bit_size);
      do_half_up_saturate_test<int32_t, int32_t>(val, bit_size);
    }
  }
}
TEST(testScale, HalfUpNegative32MaxShift) {
  for (int32_t const& val : inputs::negative_32bit) {
    for (uint8_t bit_size = mid_shift; bit_size <= max_shift; bit_size++) {
      do_half_up_test(val, bit_size);
      do_half_up_saturate_test<int32_t, int8_t>(val, bit_size);
      do_half_up_saturate_test<int32_t, int16_t>(val, bit_size);
      do_half_up_saturate_test<int32_t, int32_t>(val, bit_size);
    }
  }
}
TEST(testScale, HalfUpUnsigned32LowShift) {
  for (uint32_t const& val : inputs::unsigned_32bit) {
    for (uint8_t bit_size = min_shift; bit_size <= low_shift; bit_size++) {
      do_half_up_test(val, bit_size);
      do_half_up_saturate_test<uint32_t, uint8_t>(val, bit_size);
      do_half_up_saturate_test<uint32_t, uint16_t>(val, bit_size);
      do_half_up_saturate_test<uint32_t, uint32_t>(val, bit_size);
    }
  }
}
TEST(testScale, HalfUpUnsigned32MidShift) {
  for (uint32_t const& val : inputs::unsigned_32bit) {
    for (uint8_t bit_size = low_shift; bit_size <= mid_shift; bit_size++) {
      do_half_up_test(val, bit_size);
      do_half_up_saturate_test<uint32_t, uint8_t>(val, bit_size);
      do_half_up_saturate_test<uint32_t, uint16_t>(val, bit_size);
      do_half_up_saturate_test<uint32_t, uint32_t>(val, bit_size);
    }
  }
}
TEST(testScale, HalfUpUnsigned32MaxShift) {
  for (uint32_t const& val : inputs::unsigned_32bit) {
    for (uint8_t bit_size = mid_shift; bit_size <= max_shift; bit_size++) {
      do_half_up_test(val, bit_size);
      do_half_up_saturate_test<uint32_t, uint8_t>(val, bit_size);
      do_half_up_saturate_test<uint32_t, uint16_t>(val, bit_size);
      do_half_up_saturate_test<uint32_t, uint32_t>(val, bit_size);
    }
  }
}

TEST(testScale, HalfUpPositive64LowShift) {
  for (int64_t const& val : inputs::positive_64bit) {
    for (uint8_t bit_size = min_shift; bit_size <= low_shift; bit_size++) {
      do_half_up_test(val, bit_size);
      do_half_up_saturate_test<int64_t, int8_t>(val, bit_size);
      do_half_up_saturate_test<int64_t, int16_t>(val, bit_size);
      do_half_up_saturate_test<int64_t, int32_t>(val, bit_size);
      do_half_up_saturate_test<int64_t, int64_t>(val, bit_size);
    }
  }
}
TEST(testScale, HalfUpPositive64MidShift) {
  for (int64_t const& val : inputs::positive_64bit) {
    for (uint8_t bit_size = low_shift; bit_size <= mid_shift; bit_size++) {
      do_half_up_test(val, bit_size);
      do_half_up_saturate_test<int64_t, int8_t>(val, bit_size);
      do_half_up_saturate_test<int64_t, int16_t>(val, bit_size);
      do_half_up_saturate_test<int64_t, int32_t>(val, bit_size);
      do_half_up_saturate_test<int64_t, int64_t>(val, bit_size);
    }
  }
}
TEST(testScale, HalfUpPositive64MaxShift) {
  for (int64_t const& val : inputs::positive_64bit) {
    for (uint8_t bit_size = mid_shift; bit_size <= max_shift; bit_size++) {
      do_half_up_test(val, bit_size);
      do_half_up_saturate_test<int64_t, int8_t>(val, bit_size);
      do_half_up_saturate_test<int64_t, int16_t>(val, bit_size);
      do_half_up_saturate_test<int64_t, int32_t>(val, bit_size);
      do_half_up_saturate_test<int64_t, int64_t>(val, bit_size);
    }
  }
}
TEST(testScale, HalfUpNegative64LowShift) {
  for (int64_t const& val : inputs::negative_64bit) {
    for (uint8_t bit_size = min_shift; bit_size <= low_shift; bit_size++) {
      do_half_up_test(val, bit_size);
      do_half_up_saturate_test<int64_t, int8_t>(val, bit_size);
      do_half_up_saturate_test<int64_t, int16_t>(val, bit_size);
      do_half_up_saturate_test<int64_t, int32_t>(val, bit_size);
      do_half_up_saturate_test<int64_t, int64_t>(val, bit_size);
    }
  }
}
TEST(testScale, HalfUpNegative64MidShift) {
  for (int64_t const& val : inputs::negative_64bit) {
    for (uint8_t bit_size = low_shift; bit_size <= mid_shift; bit_size++) {
      do_half_up_test(val, bit_size);
      do_half_up_saturate_test<int64_t, int8_t>(val, bit_size);
      do_half_up_saturate_test<int64_t, int16_t>(val, bit_size);
      do_half_up_saturate_test<int64_t, int32_t>(val, bit_size);
      do_half_up_saturate_test<int64_t, int64_t>(val, bit_size);
    }
  }
}
TEST(testScale, HalfUpNegative64MaxShift) {
  for (int64_t const& val : inputs::negative_64bit) {
    for (uint8_t bit_size = mid_shift; bit_size <= max_shift; bit_size++) {
      do_half_up_test(val, bit_size);
      do_half_up_saturate_test<int64_t, int8_t>(val, bit_size);
      do_half_up_saturate_test<int64_t, int16_t>(val, bit_size);
      do_half_up_saturate_test<int64_t, int32_t>(val, bit_size);
      do_half_up_saturate_test<int64_t, int64_t>(val, bit_size);
    }
  }
}
TEST(testScale, HalfUpUnsigned64LowShift) {
  for (uint64_t const& val : inputs::unsigned_64bit) {
    for (uint8_t bit_size = min_shift; bit_size <= low_shift; bit_size++) {
      do_half_up_test(val, bit_size);
      do_half_up_saturate_test<uint64_t, uint8_t>(val, bit_size);
      do_half_up_saturate_test<uint64_t, uint16_t>(val, bit_size);
      do_half_up_saturate_test<uint64_t, uint32_t>(val, bit_size);
      do_half_up_saturate_test<uint64_t, uint64_t>(val, bit_size);
    }
  }
}
TEST(testScale, HalfUpUnsigned64MidShift) {
  for (uint64_t const& val : inputs::unsigned_64bit) {
    for (uint8_t bit_size = low_shift; bit_size <= mid_shift; bit_size++) {
      do_half_up_test(val, bit_size);
      do_half_up_saturate_test<uint64_t, uint8_t>(val, bit_size);
      do_half_up_saturate_test<uint64_t, uint16_t>(val, bit_size);
      do_half_up_saturate_test<uint64_t, uint32_t>(val, bit_size);
      do_half_up_saturate_test<uint64_t, uint64_t>(val, bit_size);
    }
  }
}
TEST(testScale, HalfUpUnsigned64MaxShift) {
  for (uint64_t const& val : inputs::unsigned_64bit) {
    for (uint8_t bit_size = mid_shift; bit_size <= max_shift; bit_size++) {
      do_half_up_test(val, bit_size);
      do_half_up_saturate_test<uint64_t, uint8_t>(val, bit_size);
      do_half_up_saturate_test<uint64_t, uint16_t>(val, bit_size);
      do_half_up_saturate_test<uint64_t, uint32_t>(val, bit_size);
      do_half_up_saturate_test<uint64_t, uint64_t>(val, bit_size);
    }
  }
}

// ***********************************************************************
// Truncate tests

TEST(testScale, TruncatePositive8LowShift) {
  for (int8_t const& val : inputs::positive_8bit) {
    for (uint8_t bit_size = min_shift; bit_size <= low_shift; bit_size++) {
      do_truncate_test(val, bit_size);
      do_truncate_saturate_test<int8_t, int8_t>(val, bit_size);
    }
  }
}
TEST(testScale, TruncatePositive8MidShift) {
  for (int8_t const& val : inputs::positive_8bit) {
    for (uint8_t bit_size = low_shift; bit_size <= mid_shift; bit_size++) {
      do_truncate_test(val, bit_size);
      do_truncate_saturate_test<int8_t, int8_t>(val, bit_size);
    }
  }
}
TEST(testScale, TruncatePositive8MaxShift) {
  for (int8_t const& val : inputs::positive_8bit) {
    for (uint8_t bit_size = mid_shift; bit_size <= max_shift; bit_size++) {
      do_truncate_test(val, bit_size);
      do_truncate_saturate_test<int8_t, int8_t>(val, bit_size);
    }
  }
}

TEST(testScale, TruncateNegative8LowShift) {
  for (int8_t const& val : inputs::negative_8bit) {
    for (uint8_t bit_size = min_shift; bit_size <= low_shift; bit_size++) {
      do_truncate_test(val, bit_size);
      do_truncate_saturate_test<int8_t, int8_t>(val, bit_size);
    }
  }
}
TEST(testScale, TruncateNegative8MidShift) {
  for (int8_t const& val : inputs::negative_8bit) {
    for (uint8_t bit_size = low_shift; bit_size <= mid_shift; bit_size++) {
      do_truncate_test(val, bit_size);
      do_truncate_saturate_test<int8_t, int8_t>(val, bit_size);
    }
  }
}
TEST(testScale, TruncateNegative8MaxShift) {
  for (int8_t const& val : inputs::negative_8bit) {
    for (uint8_t bit_size = mid_shift; bit_size <= max_shift; bit_size++) {
      do_truncate_test(val, bit_size);
      do_truncate_saturate_test<int8_t, int8_t>(val, bit_size);
    }
  }
}

TEST(testScale, TruncateUnsigned8LowShift) {
  for (uint8_t const& val : inputs::unsigned_8bit) {
    for (uint8_t bit_size = min_shift; bit_size <= low_shift; bit_size++) {
      do_truncate_test(val, bit_size);
      do_truncate_saturate_test<uint8_t, uint8_t>(val, bit_size);
    }
  }
}
TEST(testScale, TruncateUnsigned8MidShift) {
  for (uint8_t const& val : inputs::unsigned_8bit) {
    for (uint8_t bit_size = low_shift; bit_size <= mid_shift; bit_size++) {
      do_truncate_test(val, bit_size);
      do_truncate_saturate_test<uint8_t, uint8_t>(val, bit_size);
    }
  }
}
TEST(testScale, TruncateUnsigned8MaxShift) {
  for (uint8_t const& val : inputs::unsigned_8bit) {
    for (uint8_t bit_size = mid_shift; bit_size <= max_shift; bit_size++) {
      do_truncate_test(val, bit_size);
      do_truncate_saturate_test<uint8_t, uint8_t>(val, bit_size);
    }
  }
}

TEST(testScale, TruncatePositive16LowShift) {
  for (int16_t const& val : inputs::positive_16bit) {
    for (uint8_t bit_size = min_shift; bit_size <= low_shift; bit_size++) {
      do_truncate_test(val, bit_size);
      do_truncate_saturate_test<int16_t, int8_t>(val, bit_size);
      do_truncate_saturate_test<int16_t, int16_t>(val, bit_size);
    }
  }
}
TEST(testScale, TruncatePositive16MidShift) {
  for (int16_t const& val : inputs::positive_16bit) {
    for (uint8_t bit_size = low_shift; bit_size <= mid_shift; bit_size++) {
      do_truncate_test(val, bit_size);
      do_truncate_saturate_test<int16_t, int8_t>(val, bit_size);
      do_truncate_saturate_test<int16_t, int16_t>(val, bit_size);
    }
  }
}
TEST(testScale, TruncatePositive16MaxShift) {
  for (int16_t const& val : inputs::positive_16bit) {
    for (uint8_t bit_size = mid_shift; bit_size <= max_shift; bit_size++) {
      do_truncate_test(val, bit_size);
      do_truncate_saturate_test<int16_t, int8_t>(val, bit_size);
      do_truncate_saturate_test<int16_t, int16_t>(val, bit_size);
    }
  }
}

TEST(testScale, TruncateNegative16LowShift) {
  for (int16_t const& val : inputs::negative_16bit) {
    for (uint8_t bit_size = min_shift; bit_size <= low_shift; bit_size++) {
      do_truncate_test(val, bit_size);
      do_truncate_saturate_test<int16_t, int8_t>(val, bit_size);
      do_truncate_saturate_test<int16_t, int16_t>(val, bit_size);
    }
  }
}
TEST(testScale, TruncateNegative16MidShift) {
  for (int16_t const& val : inputs::negative_16bit) {
    for (uint8_t bit_size = low_shift; bit_size <= mid_shift; bit_size++) {
      do_truncate_test(val, bit_size);
      do_truncate_saturate_test<int16_t, int8_t>(val, bit_size);
      do_truncate_saturate_test<int16_t, int16_t>(val, bit_size);
    }
  }
}
TEST(testScale, TruncateNegative16MaxShift) {
  for (int16_t const& val : inputs::negative_16bit) {
    for (uint8_t bit_size = mid_shift; bit_size <= max_shift; bit_size++) {
      do_truncate_test(val, bit_size);
      do_truncate_saturate_test<int16_t, int8_t>(val, bit_size);
      do_truncate_saturate_test<int16_t, int16_t>(val, bit_size);
    }
  }
}

TEST(testScale, TruncateUnsigned16LowShift) {
  for (uint16_t const& val : inputs::unsigned_16bit) {
    for (uint8_t bit_size = min_shift; bit_size <= low_shift; bit_size++) {
      do_truncate_test(val, bit_size);
      do_truncate_saturate_test<uint16_t, uint8_t>(val, bit_size);
      do_truncate_saturate_test<uint16_t, uint16_t>(val, bit_size);
    }
  }
}
TEST(testScale, TruncateUnsigned16MidShift) {
  for (uint16_t const& val : inputs::unsigned_16bit) {
    for (uint8_t bit_size = low_shift; bit_size <= mid_shift; bit_size++) {
      do_truncate_test(val, bit_size);
      do_truncate_saturate_test<uint16_t, uint8_t>(val, bit_size);
      do_truncate_saturate_test<uint16_t, uint16_t>(val, bit_size);
    }
  }
}
TEST(testScale, TruncateUnsigned16MaxShift) {
  for (uint16_t const& val : inputs::unsigned_16bit) {
    for (uint8_t bit_size = mid_shift; bit_size <= max_shift; bit_size++) {
      do_truncate_test(val, bit_size);
      do_truncate_saturate_test<uint16_t, uint8_t>(val, bit_size);
      do_truncate_saturate_test<uint16_t, uint16_t>(val, bit_size);
    }
  }
}

TEST(testScale, TruncatePositive32LowShift) {
  for (int32_t const& val : inputs::positive_32bit) {
    for (uint8_t bit_size = min_shift; bit_size <= low_shift; bit_size++) {
      do_truncate_test(val, bit_size);
      do_truncate_saturate_test<int32_t, int8_t>(val, bit_size);
      do_truncate_saturate_test<int32_t, int16_t>(val, bit_size);
      do_truncate_saturate_test<int32_t, int32_t>(val, bit_size);
    }
  }
}
TEST(testScale, TruncatePositive32MidShift) {
  for (int32_t const& val : inputs::positive_32bit) {
    for (uint8_t bit_size = low_shift; bit_size <= mid_shift; bit_size++) {
      do_truncate_test(val, bit_size);
      do_truncate_saturate_test<int32_t, int8_t>(val, bit_size);
      do_truncate_saturate_test<int32_t, int16_t>(val, bit_size);
      do_truncate_saturate_test<int32_t, int32_t>(val, bit_size);
    }
  }
}
TEST(testScale, TruncatePositive32MaxShift) {
  for (int32_t const& val : inputs::positive_32bit) {
    for (uint8_t bit_size = mid_shift; bit_size <= max_shift; bit_size++) {
      do_truncate_test(val, bit_size);
      do_truncate_saturate_test<int32_t, int8_t>(val, bit_size);
      do_truncate_saturate_test<int32_t, int16_t>(val, bit_size);
      do_truncate_saturate_test<int32_t, int32_t>(val, bit_size);
    }
  }
}

TEST(testScale, TruncateNegative32LowShift) {
  for (int32_t const& val : inputs::negative_32bit) {
    for (uint8_t bit_size = min_shift; bit_size <= low_shift; bit_size++) {
      do_truncate_test(val, bit_size);
      do_truncate_saturate_test<int32_t, int8_t>(val, bit_size);
      do_truncate_saturate_test<int32_t, int16_t>(val, bit_size);
      do_truncate_saturate_test<int32_t, int32_t>(val, bit_size);
    }
  }
}
TEST(testScale, TruncateNegative32MidShift) {
  for (int32_t const& val : inputs::negative_32bit) {
    for (uint8_t bit_size = low_shift; bit_size <= mid_shift; bit_size++) {
      do_truncate_test(val, bit_size);
      do_truncate_saturate_test<int32_t, int8_t>(val, bit_size);
      do_truncate_saturate_test<int32_t, int16_t>(val, bit_size);
      do_truncate_saturate_test<int32_t, int32_t>(val, bit_size);
    }
  }
}
TEST(testScale, TruncateNegative32MaxShift) {
  for (int32_t const& val : inputs::negative_32bit) {
    for (uint8_t bit_size = mid_shift; bit_size <= max_shift; bit_size++) {
      do_truncate_test(val, bit_size);
      do_truncate_saturate_test<int32_t, int8_t>(val, bit_size);
      do_truncate_saturate_test<int32_t, int16_t>(val, bit_size);
      do_truncate_saturate_test<int32_t, int32_t>(val, bit_size);
    }
  }
}

TEST(testScale, TruncateUnsigned32LowShift) {
  for (uint32_t const& val : inputs::unsigned_32bit) {
    for (uint8_t bit_size = min_shift; bit_size <= low_shift; bit_size++) {
      do_truncate_test(val, bit_size);
      do_truncate_saturate_test<uint32_t, uint8_t>(val, bit_size);
      do_truncate_saturate_test<uint32_t, uint16_t>(val, bit_size);
      do_truncate_saturate_test<uint32_t, uint32_t>(val, bit_size);
    }
  }
}
TEST(testScale, TruncateUnsigned32MidShift) {
  for (uint32_t const& val : inputs::unsigned_32bit) {
    for (uint8_t bit_size = low_shift; bit_size <= mid_shift; bit_size++) {
      do_truncate_test(val, bit_size);
      do_truncate_saturate_test<uint32_t, uint8_t>(val, bit_size);
      do_truncate_saturate_test<uint32_t, uint16_t>(val, bit_size);
      do_truncate_saturate_test<uint32_t, uint32_t>(val, bit_size);
    }
  }
}
TEST(testScale, TruncateUnsigned32MaxShift) {
  for (uint32_t const& val : inputs::unsigned_32bit) {
    for (uint8_t bit_size = mid_shift; bit_size <= max_shift; bit_size++) {
      do_truncate_test(val, bit_size);
      do_truncate_saturate_test<uint32_t, uint8_t>(val, bit_size);
      do_truncate_saturate_test<uint32_t, uint16_t>(val, bit_size);
      do_truncate_saturate_test<uint32_t, uint32_t>(val, bit_size);
    }
  }
}

TEST(testScale, TruncatePositive64LowShift) {
  for (int64_t const& val : inputs::positive_64bit) {
    for (uint8_t bit_size = min_shift; bit_size <= low_shift; bit_size++) {
      do_truncate_test(val, bit_size);
      do_truncate_saturate_test<int64_t, int8_t>(val, bit_size);
      do_truncate_saturate_test<int64_t, int16_t>(val, bit_size);
      do_truncate_saturate_test<int64_t, int32_t>(val, bit_size);
      do_truncate_saturate_test<int64_t, int64_t>(val, bit_size);
    }
  }
}
TEST(testScale, TruncatePositive64MidShift) {
  for (int64_t const& val : inputs::positive_64bit) {
    for (uint8_t bit_size = low_shift; bit_size <= mid_shift; bit_size++) {
      do_truncate_test(val, bit_size);
      do_truncate_saturate_test<int64_t, int8_t>(val, bit_size);
      do_truncate_saturate_test<int64_t, int16_t>(val, bit_size);
      do_truncate_saturate_test<int64_t, int32_t>(val, bit_size);
      do_truncate_saturate_test<int64_t, int64_t>(val, bit_size);
    }
  }
}
TEST(testScale, TruncatePositive64MaxShift) {
  for (int64_t const& val : inputs::positive_64bit) {
    for (uint8_t bit_size = mid_shift; bit_size <= max_shift; bit_size++) {
      do_truncate_test(val, bit_size);
      do_truncate_saturate_test<int64_t, int8_t>(val, bit_size);
      do_truncate_saturate_test<int64_t, int16_t>(val, bit_size);
      do_truncate_saturate_test<int64_t, int32_t>(val, bit_size);
      do_truncate_saturate_test<int64_t, int64_t>(val, bit_size);
    }
  }
}

TEST(testScale, TruncateNegative64LowShift) {
  for (int64_t const& val : inputs::negative_64bit) {
    for (uint8_t bit_size = min_shift; bit_size <= low_shift; bit_size++) {
      do_truncate_test(val, bit_size);
      do_truncate_saturate_test<int64_t, int8_t>(val, bit_size);
      do_truncate_saturate_test<int64_t, int16_t>(val, bit_size);
      do_truncate_saturate_test<int64_t, int32_t>(val, bit_size);
      do_truncate_saturate_test<int64_t, int64_t>(val, bit_size);
    }
  }
}
TEST(testScale, TruncateNegative64MidShift) {
  for (int64_t const& val : inputs::negative_64bit) {
    for (uint8_t bit_size = low_shift; bit_size <= mid_shift; bit_size++) {
      do_truncate_test(val, bit_size);
      do_truncate_saturate_test<int64_t, int8_t>(val, bit_size);
      do_truncate_saturate_test<int64_t, int16_t>(val, bit_size);
      do_truncate_saturate_test<int64_t, int32_t>(val, bit_size);
      do_truncate_saturate_test<int64_t, int64_t>(val, bit_size);
    }
  }
}
TEST(testScale, TruncateNegative64MaxShift) {
  for (int64_t const& val : inputs::negative_64bit) {
    for (uint8_t bit_size = mid_shift; bit_size <= max_shift; bit_size++) {
      do_truncate_test(val, bit_size);
      do_truncate_saturate_test<int64_t, int8_t>(val, bit_size);
      do_truncate_saturate_test<int64_t, int16_t>(val, bit_size);
      do_truncate_saturate_test<int64_t, int32_t>(val, bit_size);
      do_truncate_saturate_test<int64_t, int64_t>(val, bit_size);
    }
  }
}

TEST(testScale, TruncateUnsigned64LowShift) {
  for (uint64_t const& val : inputs::unsigned_64bit) {
    for (uint8_t bit_size = min_shift; bit_size <= low_shift; bit_size++) {
      do_truncate_test(val, bit_size);
      do_truncate_saturate_test<uint64_t, uint8_t>(val, bit_size);
      do_truncate_saturate_test<uint64_t, uint16_t>(val, bit_size);
      do_truncate_saturate_test<uint64_t, uint32_t>(val, bit_size);
      do_truncate_saturate_test<uint64_t, uint64_t>(val, bit_size);
    }
  }
}
TEST(testScale, TruncateUnsigned64MidShift) {
  for (uint64_t const& val : inputs::unsigned_64bit) {
    for (uint8_t bit_size = low_shift; bit_size <= mid_shift; bit_size++) {
      do_truncate_test(val, bit_size);
      do_truncate_saturate_test<uint64_t, uint8_t>(val, bit_size);
      do_truncate_saturate_test<uint64_t, uint16_t>(val, bit_size);
      do_truncate_saturate_test<uint64_t, uint32_t>(val, bit_size);
      do_truncate_saturate_test<uint64_t, uint64_t>(val, bit_size);
    }
  }
}
TEST(testScale, TruncateUnsigned64MaxShift) {
  for (uint64_t const& val : inputs::unsigned_64bit) {
    for (uint8_t bit_size = mid_shift; bit_size <= max_shift; bit_size++) {
      do_truncate_test(val, bit_size);
      do_truncate_saturate_test<uint64_t, uint8_t>(val, bit_size);
      do_truncate_saturate_test<uint64_t, uint16_t>(val, bit_size);
      do_truncate_saturate_test<uint64_t, uint32_t>(val, bit_size);
      do_truncate_saturate_test<uint64_t, uint64_t>(val, bit_size);
    }
  }
}

// ***********************************************************************
// Half Even tests

TEST(testScale, HalfEvenPositive8LowShift) {
  for (int8_t const& val : inputs::positive_8bit) {
    for (uint8_t bit_size = min_shift; bit_size <= low_shift; bit_size++) {
      do_half_even_test(val, bit_size);
      do_half_even_saturate_test<int8_t, int8_t>(val, bit_size);
    }
  }
}
TEST(testScale, HalfEvenPositive8MidShift) {
  for (int8_t const& val : inputs::positive_8bit) {
    for (uint8_t bit_size = low_shift; bit_size <= mid_shift; bit_size++) {
      do_half_even_test(val, bit_size);
      do_half_even_saturate_test<int8_t, int8_t>(val, bit_size);
    }
  }
}
TEST(testScale, HalfEvenPositive8MaxShift) {
  for (int8_t const& val : inputs::positive_8bit) {
    for (uint8_t bit_size = mid_shift; bit_size <= max_shift; bit_size++) {
      do_half_even_test(val, bit_size);
      do_half_even_saturate_test<int8_t, int8_t>(val, bit_size);
    }
  }
}

TEST(testScale, HalfEvenNegative8LowShift) {
  for (int8_t const& val : inputs::negative_8bit) {
    for (uint8_t bit_size = min_shift; bit_size <= low_shift; bit_size++) {
      do_half_even_test(val, bit_size);
      do_half_even_saturate_test<int8_t, int8_t>(val, bit_size);
    }
  }
}
TEST(testScale, HalfEvenNegative8MidShift) {
  for (int8_t const& val : inputs::negative_8bit) {
    for (uint8_t bit_size = low_shift; bit_size <= mid_shift; bit_size++) {
      do_half_even_test(val, bit_size);
      do_half_even_saturate_test<int8_t, int8_t>(val, bit_size);
    }
  }
}
TEST(testScale, HalfEvenNegative8MaxShift) {
  for (int8_t const& val : inputs::negative_8bit) {
    for (uint8_t bit_size = mid_shift; bit_size <= max_shift; bit_size++) {
      do_half_even_test(val, bit_size);
      do_half_even_saturate_test<int8_t, int8_t>(val, bit_size);
    }
  }
}
TEST(testScale, HalfEvenUnsigned8LowShift) {
  for (uint8_t const& val : inputs::unsigned_8bit) {
    for (uint8_t bit_size = min_shift; bit_size <= low_shift; bit_size++) {
      do_half_even_test(val, bit_size);
      do_half_even_saturate_test<uint8_t, uint8_t>(val, bit_size);
    }
  }
}
TEST(testScale, HalfEvenUnsigned8MidShift) {
  for (uint8_t const& val : inputs::unsigned_8bit) {
    for (uint8_t bit_size = low_shift; bit_size <= mid_shift; bit_size++) {
      do_half_even_test(val, bit_size);
      do_half_even_saturate_test<uint8_t, uint8_t>(val, bit_size);
    }
  }
}
TEST(testScale, HalfEvenUnsigned8MaxShift) {
  for (uint8_t const& val : inputs::unsigned_8bit) {
    for (uint8_t bit_size = mid_shift; bit_size <= max_shift; bit_size++) {
      do_half_even_test(val, bit_size);
      do_half_even_saturate_test<uint8_t, uint8_t>(val, bit_size);
    }
  }
}

TEST(testScale, HalfEvenPositive16LowShift) {
  for (int16_t const& val : inputs::positive_16bit) {
    for (uint8_t bit_size = min_shift; bit_size <= low_shift; bit_size++) {
      do_half_even_test(val, bit_size);
      do_half_even_saturate_test<int16_t, int8_t>(val, bit_size);
      do_half_even_saturate_test<int16_t, int16_t>(val, bit_size);
    }
  }
}
TEST(testScale, HalfEvenPositive16MidShift) {
  for (int16_t const& val : inputs::positive_16bit) {
    for (uint8_t bit_size = low_shift; bit_size <= mid_shift; bit_size++) {
      do_half_even_test(val, bit_size);
      do_half_even_saturate_test<int16_t, int8_t>(val, bit_size);
      do_half_even_saturate_test<int16_t, int16_t>(val, bit_size);
    }
  }
}
TEST(testScale, HalfEvenPositive16MaxShift) {
  for (int16_t const& val : inputs::positive_16bit) {
    for (uint8_t bit_size = mid_shift; bit_size <= max_shift; bit_size++) {
      do_half_even_test(val, bit_size);
      do_half_even_saturate_test<int16_t, int8_t>(val, bit_size);
      do_half_even_saturate_test<int16_t, int16_t>(val, bit_size);
    }
  }
}

TEST(testScale, HalfEvenNegative16LowShift) {
  for (int16_t const& val : inputs::negative_16bit) {
    for (uint8_t bit_size = min_shift; bit_size <= low_shift; bit_size++) {
      do_half_even_test(val, bit_size);
      do_half_even_saturate_test<int16_t, int8_t>(val, bit_size);
      do_half_even_saturate_test<int16_t, int16_t>(val, bit_size);
    }
  }
}
TEST(testScale, HalfEvenNegative16MidShift) {
  for (int16_t const& val : inputs::negative_16bit) {
    for (uint8_t bit_size = low_shift; bit_size <= mid_shift; bit_size++) {
      do_half_even_test(val, bit_size);
      do_half_even_saturate_test<int16_t, int8_t>(val, bit_size);
      do_half_even_saturate_test<int16_t, int16_t>(val, bit_size);
    }
  }
}
TEST(testScale, HalfEvenNegative16MaxShift) {
  for (int16_t const& val : inputs::negative_16bit) {
    for (uint8_t bit_size = mid_shift; bit_size <= max_shift; bit_size++) {
      do_half_even_test(val, bit_size);
      do_half_even_saturate_test<int16_t, int8_t>(val, bit_size);
      do_half_even_saturate_test<int16_t, int16_t>(val, bit_size);
    }
  }
}

TEST(testScale, HalfEvenUnsigned16LowShift) {
  for (uint16_t const& val : inputs::unsigned_16bit) {
    for (uint8_t bit_size = min_shift; bit_size <= low_shift; bit_size++) {
      do_half_even_test(val, bit_size);
      do_half_even_saturate_test<uint16_t, uint8_t>(val, bit_size);
      do_half_even_saturate_test<uint16_t, uint16_t>(val, bit_size);
    }
  }
}
TEST(testScale, HalfEvenUnsigned16MidShift) {
  for (uint16_t const& val : inputs::unsigned_16bit) {
    for (uint8_t bit_size = low_shift; bit_size <= mid_shift; bit_size++) {
      do_half_even_test(val, bit_size);
      do_half_even_saturate_test<uint16_t, uint8_t>(val, bit_size);
      do_half_even_saturate_test<uint16_t, uint16_t>(val, bit_size);
    }
  }
}
TEST(testScale, HalfEvenUnsigned16MaxShift) {
  for (uint16_t const& val : inputs::unsigned_16bit) {
    for (uint8_t bit_size = mid_shift; bit_size <= max_shift; bit_size++) {
      do_half_even_test(val, bit_size);
      do_half_even_saturate_test<uint16_t, uint8_t>(val, bit_size);
      do_half_even_saturate_test<uint16_t, uint16_t>(val, bit_size);
    }
  }
}

TEST(testScale, HalfEvenPositive32LowShift) {
  for (int32_t const& val : inputs::positive_32bit) {
    for (uint8_t bit_size = min_shift; bit_size <= low_shift; bit_size++) {
      do_half_even_test(val, bit_size);
      do_half_even_saturate_test<int32_t, int8_t>(val, bit_size);
      do_half_even_saturate_test<int32_t, int16_t>(val, bit_size);
      do_half_even_saturate_test<int32_t, int32_t>(val, bit_size);
    }
  }
}
TEST(testScale, HalfEvenPositive32MidShift) {
  for (int32_t const& val : inputs::positive_32bit) {
    for (uint8_t bit_size = low_shift; bit_size <= mid_shift; bit_size++) {
      do_half_even_test(val, bit_size);
      do_half_even_saturate_test<int32_t, int8_t>(val, bit_size);
      do_half_even_saturate_test<int32_t, int16_t>(val, bit_size);
      do_half_even_saturate_test<int32_t, int32_t>(val, bit_size);
    }
  }
}
TEST(testScale, HalfEvenPositive32MaxShift) {
  for (int32_t const& val : inputs::positive_32bit) {
    for (uint8_t bit_size = mid_shift; bit_size <= max_shift; bit_size++) {
      do_half_even_test(val, bit_size);
      do_half_even_saturate_test<int32_t, int8_t>(val, bit_size);
      do_half_even_saturate_test<int32_t, int16_t>(val, bit_size);
      do_half_even_saturate_test<int32_t, int32_t>(val, bit_size);
    }
  }
}

TEST(testScale, HalfEvenNegative32LowShift) {
  for (int32_t const& val : inputs::negative_32bit) {
    for (uint8_t bit_size = min_shift; bit_size <= low_shift; bit_size++) {
      do_half_even_test(val, bit_size);
      do_half_even_saturate_test<int32_t, int8_t>(val, bit_size);
      do_half_even_saturate_test<int32_t, int16_t>(val, bit_size);
      do_half_even_saturate_test<int32_t, int32_t>(val, bit_size);
    }
  }
}
TEST(testScale, HalfEvenNegative32MidShift) {
  for (int32_t const& val : inputs::negative_32bit) {
    for (uint8_t bit_size = low_shift; bit_size <= mid_shift; bit_size++) {
      do_half_even_test(val, bit_size);
      do_half_even_saturate_test<int32_t, int8_t>(val, bit_size);
      do_half_even_saturate_test<int32_t, int16_t>(val, bit_size);
      do_half_even_saturate_test<int32_t, int32_t>(val, bit_size);
    }
  }
}
TEST(testScale, HalfEvenNegative32MaxShift) {
  for (int32_t const& val : inputs::negative_32bit) {
    for (uint8_t bit_size = mid_shift; bit_size <= max_shift; bit_size++) {
      do_half_even_test(val, bit_size);
      do_half_even_saturate_test<int32_t, int8_t>(val, bit_size);
      do_half_even_saturate_test<int32_t, int16_t>(val, bit_size);
      do_half_even_saturate_test<int32_t, int32_t>(val, bit_size);
    }
  }
}

TEST(testScale, HalfEvenUnsigned32LowShift) {
  for (uint32_t const& val : inputs::unsigned_32bit) {
    for (uint8_t bit_size = min_shift; bit_size <= low_shift; bit_size++) {
      do_half_even_test(val, bit_size);
      do_half_even_saturate_test<uint32_t, uint8_t>(val, bit_size);
      do_half_even_saturate_test<uint32_t, uint16_t>(val, bit_size);
      do_half_even_saturate_test<uint32_t, uint32_t>(val, bit_size);
    }
  }
}
TEST(testScale, HalfEvenUnsigned32MidShift) {
  for (uint32_t const& val : inputs::unsigned_32bit) {
    for (uint8_t bit_size = low_shift; bit_size <= mid_shift; bit_size++) {
      do_half_even_test(val, bit_size);
      do_half_even_saturate_test<uint32_t, uint8_t>(val, bit_size);
      do_half_even_saturate_test<uint32_t, uint16_t>(val, bit_size);
      do_half_even_saturate_test<uint32_t, uint32_t>(val, bit_size);
    }
  }
}
TEST(testScale, HalfEvenUnsigned32MaxShift) {
  for (uint32_t const& val : inputs::unsigned_32bit) {
    for (uint8_t bit_size = mid_shift; bit_size <= max_shift; bit_size++) {
      do_half_even_test(val, bit_size);
      do_half_even_saturate_test<uint32_t, uint8_t>(val, bit_size);
      do_half_even_saturate_test<uint32_t, uint16_t>(val, bit_size);
      do_half_even_saturate_test<uint32_t, uint32_t>(val, bit_size);
    }
  }
}

TEST(testScale, HalfEvenPositive64LowShift) {
  for (int64_t const& val : inputs::positive_64bit) {
    for (uint8_t bit_size = min_shift; bit_size <= low_shift; bit_size++) {
      do_half_even_test(val, bit_size);
      do_half_even_saturate_test<int64_t, int8_t>(val, bit_size);
      do_half_even_saturate_test<int64_t, int16_t>(val, bit_size);
      do_half_even_saturate_test<int64_t, int32_t>(val, bit_size);
      do_half_even_saturate_test<int64_t, int64_t>(val, bit_size);
    }
  }
}
TEST(testScale, HalfEvenPositive64MidShift) {
  for (int64_t const& val : inputs::positive_64bit) {
    for (uint8_t bit_size = low_shift; bit_size <= mid_shift; bit_size++) {
      do_half_even_test(val, bit_size);
      do_half_even_saturate_test<int64_t, int8_t>(val, bit_size);
      do_half_even_saturate_test<int64_t, int16_t>(val, bit_size);
      do_half_even_saturate_test<int64_t, int32_t>(val, bit_size);
      do_half_even_saturate_test<int64_t, int64_t>(val, bit_size);
    }
  }
}
TEST(testScale, HalfEvenPositive64MaxShift) {
  for (int64_t const& val : inputs::positive_64bit) {
    for (uint8_t bit_size = mid_shift; bit_size <= max_shift; bit_size++) {
      do_half_even_test(val, bit_size);
      do_half_even_saturate_test<int64_t, int8_t>(val, bit_size);
      do_half_even_saturate_test<int64_t, int16_t>(val, bit_size);
      do_half_even_saturate_test<int64_t, int32_t>(val, bit_size);
      do_half_even_saturate_test<int64_t, int64_t>(val, bit_size);
    }
  }
}

TEST(testScale, HalfEvenNegative64LowShift) {
  for (int64_t const& val : inputs::negative_64bit) {
    for (uint8_t bit_size = min_shift; bit_size <= low_shift; bit_size++) {
      do_half_even_test(val, bit_size);
      do_half_even_saturate_test<int64_t, int8_t>(val, bit_size);
      do_half_even_saturate_test<int64_t, int16_t>(val, bit_size);
      do_half_even_saturate_test<int64_t, int32_t>(val, bit_size);
      do_half_even_saturate_test<int64_t, int64_t>(val, bit_size);
    }
  }
}
TEST(testScale, HalfEvenNegative64MidShift) {
  for (int64_t const& val : inputs::negative_64bit) {
    for (uint8_t bit_size = low_shift; bit_size <= mid_shift; bit_size++) {
      do_half_even_test(val, bit_size);
      do_half_even_saturate_test<int64_t, int8_t>(val, bit_size);
      do_half_even_saturate_test<int64_t, int16_t>(val, bit_size);
      do_half_even_saturate_test<int64_t, int32_t>(val, bit_size);
      do_half_even_saturate_test<int64_t, int64_t>(val, bit_size);
    }
  }
}
TEST(testScale, HalfEvenNegative64MaxShift) {
  for (int64_t const& val : inputs::negative_64bit) {
    for (uint8_t bit_size = mid_shift; bit_size <= max_shift; bit_size++) {
      do_half_even_test(val, bit_size);
      do_half_even_saturate_test<int64_t, int8_t>(val, bit_size);
      do_half_even_saturate_test<int64_t, int16_t>(val, bit_size);
      do_half_even_saturate_test<int64_t, int32_t>(val, bit_size);
      do_half_even_saturate_test<int64_t, int64_t>(val, bit_size);
    }
  }
}

TEST(testScale, HalfEvenUnsigned64LowShift) {
  for (uint64_t const& val : inputs::unsigned_64bit) {
    for (uint8_t bit_size = min_shift; bit_size <= low_shift; bit_size++) {
      do_half_even_test(val, bit_size);
      do_half_even_saturate_test<uint64_t, uint8_t>(val, bit_size);
      do_half_even_saturate_test<uint64_t, uint16_t>(val, bit_size);
      do_half_even_saturate_test<uint64_t, uint32_t>(val, bit_size);
      do_half_even_saturate_test<uint64_t, uint64_t>(val, bit_size);
    }
  }
}
TEST(testScale, HalfEvenUnsigned64MidShift) {
  for (uint64_t const& val : inputs::unsigned_64bit) {
    for (uint8_t bit_size = low_shift; bit_size <= mid_shift; bit_size++) {
      do_half_even_test(val, bit_size);
      do_half_even_saturate_test<uint64_t, uint8_t>(val, bit_size);
      do_half_even_saturate_test<uint64_t, uint16_t>(val, bit_size);
      do_half_even_saturate_test<uint64_t, uint32_t>(val, bit_size);
      do_half_even_saturate_test<uint64_t, uint64_t>(val, bit_size);
    }
  }
}
TEST(testScale, HalfEvenUnsigned64MaxShift) {
  for (uint64_t const& val : inputs::unsigned_64bit) {
    for (uint8_t bit_size = mid_shift; bit_size <= max_shift; bit_size++) {
      do_half_even_test(val, bit_size);
      do_half_even_saturate_test<uint64_t, uint8_t>(val, bit_size);
      do_half_even_saturate_test<uint64_t, uint16_t>(val, bit_size);
      do_half_even_saturate_test<uint64_t, uint32_t>(val, bit_size);
      do_half_even_saturate_test<uint64_t, uint64_t>(val, bit_size);
    }
  }
}

// ***********************************************************************
// Define main

int main(int argc, char** argv) {
  ::testing::InitGoogleTest(&argc, argv);
  std::cout << std::hex;
  return RUN_ALL_TESTS();
}
