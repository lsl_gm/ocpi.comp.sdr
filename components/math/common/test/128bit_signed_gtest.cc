// 128 bit math routines: gtest
//
// This file is protected by Copyright. Please refer to the COPYRIGHT file
// distributed with this source distribution.
//
// This file is part of OpenCPI <http://www.opencpi.org>
//
// OpenCPI is free software: you can redistribute it and/or modify it under the
// terms of the GNU Lesser General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option) any
// later version.
//
// OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
// more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#include <csignal>
#include <memory>
#include "gtest/gtest.h"
#include "../128bit.hh"

using sdr_math::int128_t;

// g++ -I/opt/opencpi/prerequisites/gtest/include -std=c++11 -Wno-narrowing
// 128bit_signed_gtest.cpp -L/opt/opencpi/prerequisites/gtest/centos7/lib/
// -lgtest -o 128bit_signed_gtest
// LD_LIBRARY_PATH=/opt/opencpi/prerequisites/gtest/centos7/lib/ ./128bit_gtest

typedef struct {
  int64_t lhs, rhs, hi, lo;
} binary;

typedef struct {
  int64_t lhs, rhs, rhs2, hi, lo;
} binary2;

TEST(test128BitSigned, Constructor) {
  int128_t def;
  EXPECT_EQ(def.hi(), 0) << "default ctor";
  EXPECT_EQ(def.lo(), 0) << "default ctor";
  int128_t zero = 0;
  EXPECT_EQ(zero.hi(), 0) << "assign from 0";
  EXPECT_EQ(zero.lo(), 0) << "assign from 0";
  int128_t minuslongtwo = -2L;
  EXPECT_EQ(minuslongtwo.hi(), -1) << "assign from negative long";
  EXPECT_EQ(minuslongtwo.lo(), -2) << "assign from negative long";
  int128_t unsignedten = 10u;
  EXPECT_EQ(unsignedten.hi(), 0) << "assign from unsigned";
  EXPECT_EQ(unsignedten.lo(), 10) << "assign from unsigned";
  int128_t from64{int64_t(1)};
  EXPECT_EQ(from64.hi(), 0) << "ctor(int64_t)";
  EXPECT_EQ(from64.lo(), 1) << "ctor(int64_t)";
  int128_t from64n{int64_t(-2)};
  EXPECT_EQ(from64n.hi(), -1) << "ctor(-int64_t)";
  EXPECT_EQ(from64n.lo(), -2) << "ctor(-int64_t)";
  int128_t from128{from64n};
  EXPECT_EQ(from128.hi(), -1) << "copy ctor";
  EXPECT_EQ(from128.lo(), -2) << "copy ctor";
  int128_t from_positive_f{2.0L};
  EXPECT_EQ(from_positive_f.hi(), 0) << "+ve long double ctor";
  EXPECT_EQ(from_positive_f.lo(), 2) << "+ve long double ctor";
  int128_t from_negative_f{-2.0L};
  EXPECT_EQ(from_negative_f.hi(), -1) << "-ve long double ctor";
  EXPECT_EQ(from_negative_f.lo(), -2) << "-ve long double ctor";
}

TEST(test128BitSigned, Casts) {
  int128_t positive{42};
  int128_t negative{-42};
  EXPECT_EQ(static_cast<char>(positive), 42);
  EXPECT_EQ(static_cast<char>(negative), -42);
  EXPECT_EQ(static_cast<int>(positive), 42);
  EXPECT_EQ(static_cast<int>(negative), -42);
  EXPECT_EQ(static_cast<long>(positive), 42);
  EXPECT_EQ(static_cast<long>(negative), -42);
}

TEST(test128BitSigned, Add) {
  binary2 data[] = {
      {0, 0, 0, 0, 0},
      {0x7fffffffffffffff, 1, 1, 0, int64_t(0x8000000000000001)},
      {0x2aaaaaaaaaaaaaaa, 0x5555555555555555, 0, 0, 0x7fffffffffffffff},
      {0x5555555555555555, 1,                          0x2aaaaaaaaaaaaaaa,
       0,                  int64_t(0x8000000000000000)},
      {0x7fffffffffffffff, 0x7fffffffffffffff, 0x7fffffffffffffff,
       1,                  0x7ffffffffffffffd},
      {-1, -1, 0, -1, -2}, {-1, 1, 0, 0, 0}};

  for (binary2 *t = data; t != data + sizeof(data) / sizeof(data[0]); ++t) {
    int128_t lhs{t->lhs};
    int128_t rhs{t->rhs};
    int128_t rhs2{t->rhs2};
    // operator+
    int128_t plus = lhs + rhs + rhs2;
    EXPECT_EQ(plus.hi(), t->hi) << t->lhs << "+" << t->rhs << "+" << t->rhs2;
    EXPECT_EQ(plus.lo(), t->lo) << t->lhs << "+" << t->rhs << "+" << t->rhs2;
    // operator+=
    lhs += rhs;
    lhs += rhs2;
    EXPECT_EQ(lhs.hi(), t->hi) << t->lhs << "+=" << t->rhs << "+=" << t->rhs2;
    EXPECT_EQ(lhs.lo(), t->lo) << t->lhs << "+=" << t->rhs << "+=" << t->rhs2;
  }
}

TEST(test128BitSigned, Subtract) {
  binary data[]{
      {0, 0, 0, 0},
      {0, 1, int64_t(0xffffffffffffffff), int64_t(0xffffffffffffffff)},
      {2, 1, 0, 1},
      {int64_t(0xffffffffffffffff), int64_t(0xffffffffffffffff), 0, 0}};
  for (binary *t = data; t != data + sizeof(data) / sizeof(data[0]); ++t) {
    int128_t lhs{t->lhs};
    int128_t rhs{t->rhs};
    // operator-
    int128_t minus = lhs - rhs;
    EXPECT_EQ(minus.hi(), t->hi) << t->lhs << "-" << t->rhs;
    EXPECT_EQ(minus.lo(), t->lo) << t->lhs << "-" << t->rhs;
    // operator-=
    lhs -= rhs;
    EXPECT_EQ(lhs.hi(), t->hi) << t->lhs << "-" << t->rhs;
    EXPECT_EQ(lhs.lo(), t->lo) << t->lhs << "-" << t->rhs;
  }
}

typedef struct {
  int64_t lhs;
  int rhs;
  int64_t hi, lo;
} shift;

TEST(test128BitSigned, Shift) {
  shift data[]{{0, 1, 0, 0}, {1, 1, 0, 2},
               {int64_t(0x5a5a5a5a5a5a5a5a), 15,
                int64_t(0x0000000000002d2d), int64_t(0x2d2d2d2d2d2d0000)},
               {int64_t(0xa0a0a0a0a0a0a0a0), 15,
                int64_t(0xffffffffffffd050), int64_t(0x5050505050500000)},
               {int64_t(0x9999999999999999), 64,
                int64_t(0x9999999999999999), int64_t(0)},
               {int64_t(0x000000000000000f), 96,
                int64_t(0x0000000f00000000), int64_t(0)}};
  for (shift *t = data; t != data + sizeof(data) / sizeof(data[0]); ++t) {
    int128_t lhs{t->lhs};
    int128_t shifted = lhs << t->rhs;
    EXPECT_EQ(shifted.hi(), t->hi);
    EXPECT_EQ(shifted.lo(), t->lo);
    shifted >>= t->rhs;
    EXPECT_EQ(shifted.hi(), lhs.hi());
    EXPECT_EQ(shifted.lo(), lhs.lo());
  }
}

typedef struct {
  int64_t lhs, rhs, op_and, op_or, op_xor, op_not;
} bitwise;

TEST(test128BitSigned, Bitwise) {
  // Shift operands and result to ensure hi and lo contain different patterns.
  bitwise data[]{
      {0, 0, 0, 0, 0, -1}, {-1, -1, -1, -1, 0, 0},
      {int64_t(0xaaaaaaaaaaaaaaaa), 0x5555555555555555, 0x0000000000000000,
       -1,                          -1,                 0x5555555555555555}};
  const int shift_up{29};
  const int shift_down{64 - shift_up};
  for (bitwise *t = data; t != data + sizeof(data) / sizeof(data[0]); ++t) {
    int128_t lhs{t->lhs};
    int128_t rhs{t->rhs};
    lhs <<= shift_up;
    rhs <<= shift_up;
    int128_t bitwise_and = lhs & rhs;
    int128_t bitwise_or = lhs | rhs;
    int128_t bitwise_xor = lhs ^ rhs;
    int128_t bitwise_not = ~lhs;
    EXPECT_EQ(bitwise_and.hi(), t->op_and >> shift_down) << t->lhs << "&"
                                                         << t->rhs;
    EXPECT_EQ(bitwise_and.lo(), t->op_and << shift_up) << t->lhs << "&"
                                                       << t->rhs;
    EXPECT_EQ(bitwise_or.hi(), t->op_or >> shift_down) << t->lhs << "|"
                                                       << t->rhs;
    EXPECT_EQ(bitwise_or.lo(), t->op_or << shift_up) << t->lhs << "|" << t->rhs;
    EXPECT_EQ(bitwise_xor.hi(), t->op_xor >> shift_down) << t->lhs << "^"
                                                         << t->rhs;
    EXPECT_EQ(bitwise_xor.lo(), t->op_xor << shift_up) << t->lhs << "^"
                                                       << t->rhs;
    EXPECT_EQ(bitwise_not.hi(), t->op_not >> shift_down) << "~" << t->rhs;
    EXPECT_EQ(bitwise_not.lo(), (t->op_not << shift_up) | 0x1fffffff) << "~"
                                                                      << t->rhs;
  }
}

TEST(test128BitSigned, IncDec) {
  // Pre-increment.
  int128_t test{0};
  int128_t pre{--test};
  EXPECT_EQ(pre.hi(), -1) << "--0 (pre)";
  EXPECT_EQ(pre.lo(), -1) << "--0 (pre)";
  EXPECT_EQ(test.hi(), -1) << "--0 (post)";
  EXPECT_EQ(test.lo(), -1) << "--0 (post)";
  pre = ++test;
  EXPECT_EQ(pre.hi(), 0) << "++0 (pre)";
  EXPECT_EQ(pre.lo(), 0) << "++0 (pre)";
  EXPECT_EQ(test.hi(), 0) << "++0 (post)";
  EXPECT_EQ(test.lo(), 0) << "++0 (post)";
  // Post-increment
  pre = test--;
  EXPECT_EQ(pre.hi(), 0) << "--0 (pre)";
  EXPECT_EQ(pre.lo(), 0) << "--0 (pre)";
  EXPECT_EQ(test.hi(), -1) << "--0 (post)";
  EXPECT_EQ(test.lo(), -1) << "--0 (post)";
  pre = test++;
  EXPECT_EQ(pre.hi(), -1) << "++0 (pre)";
  EXPECT_EQ(pre.lo(), -1) << "++0 (pre)";
  EXPECT_EQ(test.hi(), 0) << "++0 (post)";
  EXPECT_EQ(test.lo(), 0) << "++0 (post)";
}

typedef struct {
  int64_t lhs, rhs;
  bool eq, ne, lt, gt, le, ge;
} compare;

TEST(test128BitSigned, Compare) {
  compare data[]{{255, 255, true, false, false, false, true, true},
                 {511, 255, false, true, false, true, false, true},
                 {255, 511, false, true, true, false, true, false},
                 {-255, -255, true, false, false, false, true, true},
                 {-511, -255, false, true, true, false, true, false},
                 {-255, -511, false, true, false, true, false, true}};
  for (compare *t = data; t != data + sizeof(data) / sizeof(data[0]); ++t) {
    int128_t lhs{t->lhs};
    int128_t rhs{t->rhs};
    // Compare 128,128
    EXPECT_EQ(lhs == rhs, t->eq) << "equal";
    EXPECT_EQ(lhs != rhs, t->ne) << "not equal";
    EXPECT_EQ(lhs < rhs, t->lt);
    EXPECT_EQ(lhs > rhs, t->gt);
    EXPECT_EQ(lhs <= rhs, t->le);
    EXPECT_EQ(lhs >= rhs, t->ge);
    // Compare 128,other
    EXPECT_EQ(lhs == t->rhs, t->eq) << "equal";
    EXPECT_EQ(lhs != int(t->rhs), t->ne) << "not equal";
    EXPECT_EQ(lhs < t->rhs, t->lt);
    EXPECT_EQ(lhs > int(t->rhs), t->gt);
    EXPECT_EQ(lhs <= t->rhs, t->le);
    EXPECT_EQ(lhs >= int(t->rhs), t->ge);
    // Test in hi.
    lhs <<= 64;
    rhs <<= 64;
    EXPECT_EQ(lhs == rhs, t->eq) << "equal";
    EXPECT_EQ(lhs != rhs, t->ne) << "not equal";
    EXPECT_EQ(lhs < rhs, t->lt);
    EXPECT_EQ(lhs > rhs, t->gt);
    EXPECT_EQ(lhs <= rhs, t->le);
    EXPECT_EQ(lhs >= rhs, t->ge);
    // Test in hi+lo
    lhs >>= 3;
    rhs >>= 3;
    EXPECT_EQ(lhs == rhs, t->eq) << "equal";
    EXPECT_EQ(lhs != rhs, t->ne) << "not equal";
    EXPECT_EQ(lhs < rhs, t->lt);
    EXPECT_EQ(lhs > rhs, t->gt);
    EXPECT_EQ(lhs <= rhs, t->le);
    EXPECT_EQ(lhs >= rhs, t->ge);
  }
}

TEST(test128BitSigned, Multiply) {
  // Basic signed test
  binary data[]{{0, 0, 0, 0}, {1, 1, 0, 1}, {-1, 1, -1, -1}, {1, -1, -1, -1},
                {0x7fffffffffffffff, 0x7fffffffffffffff,
                 0x3fffffffffffffff, 1}};
  for (binary *t = data; t != data + sizeof(data) / sizeof(data[0]); ++t) {
    int128_t lhs{t->lhs};
    int128_t rhs{t->rhs};
    // operator*
    int128_t product{lhs *rhs};
    EXPECT_EQ(product.hi(), t->hi) << t->lhs << "*" << t->rhs;
    EXPECT_EQ(product.lo(), t->lo) << t->lhs << "*" << t->rhs;
    // operator*=
    lhs *= rhs;
    EXPECT_EQ(lhs.hi(), t->hi) << t->lhs << "*" << t->rhs;
    EXPECT_EQ(lhs.lo(), t->lo) << t->lhs << "*" << t->rhs;
  }
}

typedef struct {
  int64_t lhs_hi, lhs_lo, rhs_hi, rhs_lo;
  int64_t q_hi, q_lo, r_hi, r_lo;
} divide;

TEST(test128BitSigned, Divide) {
  divide data[]{{0, 0, 0, 1, 0, 0, 0, 0},        // 0/1=0r0
                {0, 1, 0, 1, 0, 1, 0, 0},        // 1/1=1r0
                {0, 3, 0, 2, 0, 1, 0, 1},        // 3/2=1r1
                {-1, -3, 0, 2, -1, -1, -1, -1},  // -3/2=-1r-1
                {0, 1, 0, 0, 0, 0, 0, 0}         // divide by zero test
  };
  for (divide *t = data; t != data + sizeof(data) / sizeof(data[0]); ++t) {
    int128_t lhs{(int128_t(t->lhs_hi) << 64) | int128_t(uint64_t(t->lhs_lo))};
    int128_t rhs{(int128_t(t->rhs_hi) << 64) | int128_t(uint64_t(t->rhs_lo))};
    // Set SIGFPE to throw. Restore on exit from scope.
    std::shared_ptr<void(int)> handler(
        signal(SIGFPE, [](int) { throw 0; }),
        [](__sighandler_t f) { signal(SIGFPE, f); });
    try {
#ifdef GCC_OPTIMISE_ENABLED
      // If the gcc optimise level (-On) is set above 0, the try...catch
      // SIGFPE -> throw does not work. Using gcc 4.8.
      if (rhs == 0) {
        throw 0;
      }
#endif
      int128_t quotient = lhs / rhs;
      EXPECT_NE(rhs, 0) << "Divide by zero should have thrown";
      EXPECT_EQ(quotient.hi(), t->q_hi) << "quotient";
      EXPECT_EQ(quotient.lo(), t->q_lo) << "quotient";
    }
    catch (int ex) {
      EXPECT_EQ(t->rhs_lo, ex);
      continue;
    }
    int128_t remainder = lhs % rhs;
    EXPECT_EQ(remainder.hi(), t->r_hi) << "remainder";
    EXPECT_EQ(remainder.lo(), t->r_lo) << "remainder";
  }
  std::signal(SIGFPE, SIG_DFL);
}

TEST(test128BitSigned, SoakMultDiv) {
  std::random_device rd;
  std::uniform_int_distribution<int64_t> incr(1ll, 2ll << 45);
  std::uniform_int_distribution<int64_t> other(-INT64_MIN / 2, INT64_MAX / 2);
  for (int64_t lhs64{-INT64_MIN / 2}; lhs64 < INT64_MAX / 2;
       lhs64 += incr(rd)) {
    int64_t rhs64{other(rd)};
    int128_t lhs{lhs64};
    int128_t rhs{rhs64};
    int128_t product{lhs *rhs};
    int128_t quotient{product / lhs};
    int64_t hi{quotient < 0 ? -1 : 0};
    EXPECT_EQ(quotient.hi(), hi) << lhs64 << "*" << rhs64 << "/" << lhs64
                                 << " quotient";
    EXPECT_EQ(quotient.lo(), rhs64) << lhs64 << "*" << rhs64 << "/" << lhs64
                                    << " quotient";
    int128_t remainder{product % lhs};
    EXPECT_EQ(remainder.hi(), 0) << lhs64 << "*" << rhs64 << "/" << lhs64
                                 << " remainder";
    EXPECT_EQ(remainder.lo(), 0) << lhs64 << "*" << rhs64 << "/" << lhs64
                                 << " remainder";
    quotient = product / rhs;
    hi = quotient < 0 ? -1 : 0;
    EXPECT_EQ(quotient.hi(), hi) << lhs64 << "*" << rhs64 << "/" << lhs64
                                 << " quotient";
    EXPECT_EQ(quotient.lo(), lhs64) << lhs64 << "*" << rhs64 << "/" << lhs64
                                    << " quotient";
    remainder = product % lhs;
    EXPECT_EQ(remainder.hi(), 0) << lhs64 << "*" << rhs64 << "/" << lhs64
                                 << " remainder";
    EXPECT_EQ(remainder.lo(), 0) << lhs64 << "*" << rhs64 << "/" << lhs64
                                 << " remainder";
  }
}

typedef struct {
  int64_t hi, lo;
  const char *dec, *oct, *hex;
} streamio;

TEST(test128BitSigned, StreamIO) {
  streamio data[]{
      {0, 0, "0", "0", "0"}, {0, 12, "12", "14", "c"},
      {-1,
       -1,
       "-1",
       "3777777777777777777777777777777777777777777",
       "ffffffffffffffffffffffffffffffff"},
      {0, 123456789, "123456789", "726746425", "75bcd15"},
      {1,                        0,                  "18446744073709551616",
       "2000000000000000000000", "10000000000000000"},
      {123456789,                      1,
       "2277375790844960561141121025", "1655715052000000000000000000001",
       "75bcd150000000000000001"},
      {INT64_MIN,
       0,
       "-170141183460469231731687303715884105728",
       "2000000000000000000000000000000000000000000",
       "80000000000000000000000000000000"}};
  for (streamio *t = data; t != data + sizeof(data) / sizeof(data[0]); ++t) {
    int128_t value{(int128_t(t->hi) << 64) | int128_t(uint64_t(t->lo))};
    std::stringstream decimal;
    decimal << value;
    EXPECT_STREQ(decimal.str().c_str(), t->dec);
    decimal << "test";
    std::stringstream octal;
    octal << std::oct << value;
    EXPECT_STREQ(octal.str().c_str(), t->oct);
    std::stringstream hexadecimal;
    hexadecimal << std::hex << value;
    EXPECT_STREQ(hexadecimal.str().c_str(), t->hex);
    int128_t result;
    decimal >> result;
    EXPECT_EQ(result, value);
    std::string suffix;
    decimal >> suffix;
    EXPECT_STREQ(suffix.c_str(), "test");
    octal >> result;
    EXPECT_EQ(result, value);
    hexadecimal >> result;
    EXPECT_EQ(result, value);
    if (value > 0) {
      std::stringstream pre_octal;
      pre_octal << "0" << std::oct << value;
      pre_octal >> std::dec >> result;
      EXPECT_EQ(result, value);
      std::stringstream pre_hexadecimal;
      pre_hexadecimal << "0x" << std::hex << value;
      pre_hexadecimal >> std::dec >> result;
      EXPECT_EQ(result, value);
    }
  }
}

TEST(test128BitSigned, Functions) {
  int128_t pos{123456789};
  int128_t neg{-123456789};
  EXPECT_EQ(abs(pos), pos);
  EXPECT_EQ(abs(neg), pos);
  EXPECT_EQ(sqrtl(pos * pos), pos);
}

TEST(test128BitSigned, Traits) {
  EXPECT_EQ(sizeof(int128_t) * 8, 128) << "sizeof(int128_t)";
  EXPECT_EQ(int128_t::MAX.hi(), 0x7fffffffffffffff);
  EXPECT_EQ(int128_t::MAX.lo(), 0xffffffffffffffff);
  EXPECT_EQ(int128_t::MIN.hi(), 0x8000000000000000);
  EXPECT_EQ(int128_t::MIN.lo(), 0x0000000000000000);
  EXPECT_TRUE(std::numeric_limits<int128_t>::is_integer);
  EXPECT_TRUE(std::numeric_limits<int128_t>::is_signed);
}

TEST(test128BitSigned, Conversions) {
  EXPECT_TRUE((std::is_same<typename std::make_unsigned<int128_t>::type,
                            sdr_math::uint128_t>::value));
  EXPECT_TRUE((std::is_same<typename std::make_signed<int128_t>::type,
                            int128_t>::value));
  // Using 32 bit precision numbers should convert perfectly for both
  // 64-bit and 80-bit long doubles.
  for (int power = 0; power < 90; power += 3) {
    int128_t ivalue{0x87654321};  // 32 bits of precision.
    ivalue <<= power;
    long double f_from_i = static_cast<long double>(ivalue);
    EXPECT_EQ(static_cast<int128_t>(f_from_i), ivalue);

    long double fvalue{-3435934515.0L};  // 32 bits of precision.
    fvalue *= std::pow(2.0L, power);
    int128_t i_from_f = static_cast<int128_t>(fvalue);
    EXPECT_EQ(static_cast<long double>(i_from_f), fvalue);
  }
}

int main(int argc, char **argv) {
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
