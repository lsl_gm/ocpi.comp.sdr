// multiword integer multiply gtest
//
// This file is protected by Copyright. Please refer to the COPYRIGHT file
// distributed with this source distribution.
//
// This file is part of OpenCPI <http://www.opencpi.org>
//
// OpenCPI is free software: you can redistribute it and/or modify it under the
// terms of the GNU Lesser General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option) any
// later version.
//
// OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
// more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#include "multiword_integer_multiply.hh"
#include "gtest/gtest.h"

// g++ -I/opt/opencpi/prerequisites/gtest/include -std=c++11 -Wno-narrowing
// multiply_gtest.cpp -L/opt/opencpi/prerequisites/gtest/centos7/lib/
// -lgtest -o multiply_gtest
// LD_LIBRARY_PATH=/opt/opencpi/prerequisites/gtest/centos7/lib/
// ./multiply_gtest
//

// Test with one or both of the operands zero. The result will be zero
// with no overrun of result array.
TEST(MultiplyTest, Zero) {
  uint32_t z[]{0, 0, 0};
  uint32_t n[]{-1, -1, -1};
  uint32_t p[]{1, 2, 3, 4, 5, 6, 42};

  static_assert(sizeof(p) >= sizeof(z) + sizeof(n), "p too small");
  // Test x all zero.
  multiword_unsigned_multiply(z, 3, n, 3, p);
  for (int i = 0; i < 6; ++i) {
    ASSERT_EQ(p[i], 0);
    p[i] = i;
  }
  ASSERT_EQ(p[6], 42);
  // Test y all zero.
  multiword_unsigned_multiply(n, 3, z, 3, p);
  for (int i = 0; i < 6; ++i) {
    ASSERT_EQ(p[i], 0);
    p[i] = i;
  }
  ASSERT_EQ(p[6], 42);
  static_assert(sizeof(p) >= sizeof(z) * 2, "p too small");
  // Test x and y all zero.
  multiword_unsigned_multiply(z, 3, z, 3, p);
  for (int i = 0; i < 6; ++i) {
    ASSERT_EQ(p[i], 0);
    p[i] = i;
  }
  ASSERT_EQ(p[6], 42);
}

// Test with mixed value words in operands. This will test the individual word
// multiplies with all bits, some bits and zero bits. Intermediate results will
// test the add+carry in the multiply core.
TEST(MultiplyTest, Values) {
  uint32_t lhs[]{0xffffffff, 0, 0xaaaaaaaa, 0x55555555};
  uint32_t rhs[]{0xaaaaaaaa, 0xffffffff, 0, 0x55555555};
  uint32_t result[9];

  // Initialise result with fixed non-zero values
  std::fill(result, result + 9, 42);
  multiword_unsigned_multiply(lhs, 4, rhs, 4, result);
  EXPECT_EQ(result[0], 0x55555556);
  EXPECT_EQ(result[1], 0xaaaaaaaa);
  EXPECT_EQ(result[2], 0xe38e38e2);
  EXPECT_EQ(result[3], 0xe38e38e4);
  EXPECT_EQ(result[4], 0xe38e38e1);
  EXPECT_EQ(result[5], 0xc71c71c7);
  EXPECT_EQ(result[6], 0x71c71c71);
  EXPECT_EQ(result[7], 0x1c71c71c);
  // Check no overrun of product.
  EXPECT_EQ(result[8], 42);
}

// Test large operands. The operands are set to an incrementing sequence which
// produces a product that can be calculated by multiplying and adding adjacent
// words.
TEST(MultiplyTest, LargeOperands) {
  constexpr size_t size{29};

  uint32_t lhs[size]{0};
  uint32_t rhs[size + 1]{0};
  uint32_t prod[size * 2 + 1]{0};

  // lhs: 1,2,3,4,5,6,...,28,0
  // rhs: 0,1,2,3,4,5....,27,28
  // expected product: 0,1,4,10,20,35...,2185,1512,(28*28),0
  for (size_t n{1}; n != size; ++n) {
    lhs[n - 1] = n;
    rhs[n] = n;
    prod[n] = n * n;
    if (n > 2) {
      prod[n] += prod[n - 2];
    }
  }
  for (size_t n{2}; n != size; ++n) {
    prod[n + size - 2] = n * n;
  }
  prod[size * 2 - 2] = 0;
  for (size_t n{size * 2 - 3}, e{0}; n != size - 1; --n, ++e) {
    prod[n] += prod[n + 1] - e * (e + 1) / 2;
  }
  uint32_t result[size * 2 + 1];
  multiword_unsigned_multiply(lhs, size, rhs, size + 1, result);
  for (size_t n{0}; n != size * 2 + 1; ++n) {
    EXPECT_EQ(result[n], prod[n]) << "result[" << n << "]";
  }
}

int main(int argc, char **argv) {
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
