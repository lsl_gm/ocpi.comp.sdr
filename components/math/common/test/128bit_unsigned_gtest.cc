// 128 bit math routines: gtest
//
// This file is protected by Copyright. Please refer to the COPYRIGHT file
// distributed with this source distribution.
//
// This file is part of OpenCPI <http://www.opencpi.org>
//
// OpenCPI is free software: you can redistribute it and/or modify it under the
// terms of the GNU Lesser General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option) any
// later version.
//
// OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
// more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#include <csignal>
#include <memory>
#include "gtest/gtest.h"
#include "../128bit.hh"

using sdr_math::uint128_t;

// g++ -I/opt/opencpi/prerequisites/gtest/include -std=c++11 -Wno-narrowing
// 128bit_unsigned_gtest.cc -L/opt/opencpi/prerequisites/gtest/centos7/lib/
// -lgtest -o 128bit_unsigned_gtest
// LD_LIBRARY_PATH=/opt/opencpi/prerequisites/gtest/centos7/lib/
// ./128bit_unsigned_gtest

typedef struct {
  uint64_t lhs, rhs, hi, lo;
} binary;

typedef struct {
  uint64_t lhs, rhs, rhs2, hi, lo;
} binary2;

TEST(test128BitUnsigned, Constructor) {
  uint128_t def;
  EXPECT_EQ(def.hi(), 0) << "default ctor";
  EXPECT_EQ(def.lo(), 0) << "default ctor";
  uint128_t zero = 0u;
  EXPECT_EQ(zero.hi(), 0) << "assign from 0";
  EXPECT_EQ(zero.lo(), 0) << "assign from 0";
  uint128_t longtwo = 2Lu;
  EXPECT_EQ(longtwo.hi(), 0) << "assign from long";
  EXPECT_EQ(longtwo.lo(), 2) << "assign from long";
  uint128_t from64{uint64_t(1)};
  EXPECT_EQ(from64.hi(), 0) << "ctor(uint64_t)";
  EXPECT_EQ(from64.lo(), 1) << "ctor(uint64_t)";
  uint128_t from128{from64};
  EXPECT_EQ(from128.hi(), 0) << "copy ctor";
  EXPECT_EQ(from128.lo(), 1) << "copy ctor";
  uint128_t from_f{2.0L};
  EXPECT_EQ(from_f.hi(), 0) << "long double ctor";
  EXPECT_EQ(from_f.lo(), 2) << "long double ctor";
}

TEST(test128BitUnsigned, Add) {
  binary2 data[] = {
      {0u, 0u, 0u, 0u, 0u},
      {0x7fffffffffffffff, 1u, 1u, 0u, 0x8000000000000001},
      {0x2aaaaaaaaaaaaaaa, 0x5555555555555555, 0u, 0u, 0x7fffffffffffffff},
      {0x5555555555555555, 1u, 0x2aaaaaaaaaaaaaaa, 0u, 0x8000000000000000},
      {0xffffffffffffffff, 1u, 0u, 1u, 0u}};

  for (binary2 *t = data; t != data + sizeof(data) / sizeof(data[0]); ++t) {
    uint128_t lhs{t->lhs};
    uint128_t rhs{t->rhs};
    uint128_t rhs2{t->rhs2};
    // operator+
    uint128_t plus = lhs + rhs + rhs2;
    EXPECT_EQ(plus.hi(), t->hi) << t->lhs << "+" << t->rhs << "+" << t->rhs2;
    EXPECT_EQ(plus.lo(), t->lo) << t->lhs << "+" << t->rhs << "+" << t->rhs2;
    // operator+=
    lhs += rhs;
    lhs += rhs2;
    EXPECT_EQ(lhs.hi(), t->hi) << t->lhs << "+=" << t->rhs << "+=" << t->rhs2;
    EXPECT_EQ(lhs.lo(), t->lo) << t->lhs << "+=" << t->rhs << "+=" << t->rhs2;
  }
}

TEST(test128BitUnsigned, Subtract) {
  binary data[]{{0u, 0u, 0u, 0u},
                {0u, 1u, 0xffffffffffffffff, 0xffffffffffffffff},
                {2u, 1u, 0u, 1u},
                {0xffffffffffffffff, 0xffffffffffffffff, 0u, 0u}};
  for (binary *t = data; t != data + sizeof(data) / sizeof(data[0]); ++t) {
    uint128_t lhs{t->lhs};
    uint128_t rhs{t->rhs};
    // operator-
    uint128_t minus = lhs - rhs;
    EXPECT_EQ(minus.hi(), t->hi) << t->lhs << "-" << t->rhs;
    EXPECT_EQ(minus.lo(), t->lo) << t->lhs << "-" << t->rhs;
    // operator-=
    lhs -= rhs;
    EXPECT_EQ(lhs.hi(), t->hi) << t->lhs << "-" << t->rhs;
    EXPECT_EQ(lhs.lo(), t->lo) << t->lhs << "-" << t->rhs;
  }
}

typedef struct {
  uint64_t lhs;
  int rhs;
  uint64_t hi, lo;
} shift;

TEST(test128BitUnsigned, Shift) {
  shift data[]{{0, 1, 0, 0},
               {1, 1, 0, 2},
               {0x5a5a5a5a5a5a5a5a, 15, 0x2d2d, 0x2d2d2d2d2d2d0000},
               {0xa0a0a0a0a0a0a0a0, 15, 0x0000000000005050, 0x5050505050500000},
               {0x9999999999999999, 64, 0x9999999999999999, 0},
               {0x000000000000000f, 96, 0x0000000f00000000, 0}};
  for (shift *t = data; t != data + sizeof(data) / sizeof(data[0]); ++t) {
    uint128_t lhs{t->lhs};
    uint128_t shifted = lhs << t->rhs;
    EXPECT_EQ(shifted.hi(), t->hi);
    EXPECT_EQ(shifted.lo(), t->lo);
    shifted >>= t->rhs;
    EXPECT_EQ(shifted.hi(), lhs.hi());
    EXPECT_EQ(shifted.lo(), lhs.lo());
  }
}

typedef struct {
  uint64_t lhs, rhs, op_and, op_or, op_xor, op_not;
} bitwise;

TEST(test128BitUnsigned, Bitwise) {
  // Shift operands and result to ensure hi and lo contain different patterns.
  bitwise data[]{{0, 0, 0, 0, 0, 0xffffffffffffffff},
                 {0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff,
                  0xffffffffffffffff, 0,                  0},
                 {0xaaaaaaaaaaaaaaaa, 0x5555555555555555, 0x0000000000000000,
                  0xffffffffffffffff, 0xffffffffffffffff, 0x5555555555555555}};
  const int shift_up{29};
  const int shift_down{64 - shift_up};
  for (bitwise *t = data; t != data + sizeof(data) / sizeof(data[0]); ++t) {
    uint128_t lhs{t->lhs};
    uint128_t rhs{t->rhs};
    lhs <<= shift_up;
    rhs <<= shift_up;
    uint64_t lhs_hi{t->lhs >> shift_down};
    uint64_t lhs_lo{t->lhs << shift_up};
    uint64_t rhs_hi{t->rhs >> shift_down};
    uint64_t rhs_lo{t->rhs << shift_up};
    uint128_t bitwise_and = lhs & rhs;
    uint128_t bitwise_or = lhs | rhs;
    uint128_t bitwise_xor = lhs ^ rhs;
    uint128_t bitwise_not = ~lhs;
    EXPECT_EQ(bitwise_and.hi(), lhs_hi & rhs_hi) << t->lhs << "&" << t->rhs;
    EXPECT_EQ(bitwise_and.lo(), lhs_lo & rhs_lo) << t->lhs << "&" << t->rhs;
    EXPECT_EQ(bitwise_or.hi(), lhs_hi | rhs_hi) << t->lhs << "|" << t->rhs;
    EXPECT_EQ(bitwise_or.lo(), lhs_lo | rhs_lo) << t->lhs << "|" << t->rhs;
    EXPECT_EQ(bitwise_xor.hi(), lhs_hi ^ rhs_hi) << t->lhs << "^" << t->rhs;
    EXPECT_EQ(bitwise_xor.lo(), lhs_lo ^ rhs_lo) << t->lhs << "^" << t->rhs;
    EXPECT_EQ(bitwise_not.hi(), ~lhs_hi) << "~" << t->rhs;
    EXPECT_EQ(bitwise_not.lo(), ~lhs_lo) << "~" << t->rhs;
  }
}

TEST(test128BitUnsigned, IncDec) {
  // Pre-increment.
  uint128_t test{0u};
  uint128_t pre{--test};
  EXPECT_EQ(pre.hi(), -1) << "--0 (pre)";
  EXPECT_EQ(pre.lo(), -1) << "--0 (pre)";
  EXPECT_EQ(test.hi(), -1) << "--0 (post)";
  EXPECT_EQ(test.lo(), -1) << "--0 (post)";
  pre = ++test;
  EXPECT_EQ(pre.hi(), 0) << "++0 (pre)";
  EXPECT_EQ(pre.lo(), 0) << "++0 (pre)";
  EXPECT_EQ(test.hi(), 0) << "++0 (post)";
  EXPECT_EQ(test.lo(), 0) << "++0 (post)";
  // Post-increment
  pre = test--;
  EXPECT_EQ(pre.hi(), 0) << "--0 (pre)";
  EXPECT_EQ(pre.lo(), 0) << "--0 (pre)";
  EXPECT_EQ(test.hi(), -1) << "--0 (post)";
  EXPECT_EQ(test.lo(), -1) << "--0 (post)";
  pre = test++;
  EXPECT_EQ(pre.hi(), -1) << "++0 (pre)";
  EXPECT_EQ(pre.lo(), -1) << "++0 (pre)";
  EXPECT_EQ(test.hi(), 0) << "++0 (post)";
  EXPECT_EQ(test.lo(), 0) << "++0 (post)";
}

typedef struct {
  uint64_t lhs, rhs;
  bool eq, ne, lt, gt, le, ge;
} compare;

TEST(test128BitUnsigned, Compare) {
  compare data[]{
      {255, 255, true, false, false, false, true, true},
      {511, 255, false, true, false, true, false, true},
      {255, 511, false, true, true, false, true, false},
      {uint64_t(-255), uint64_t(-255), true, false, false, false, true, true},
      {uint64_t(-511), uint64_t(-255), false, true, true, false, true, false},
      {uint64_t(-255), uint64_t(-511), false, true, false, true, false, true}};
  for (compare *t = data; t != data + sizeof(data) / sizeof(data[0]); ++t) {
    uint128_t lhs{t->lhs};
    uint128_t rhs{t->rhs};
    // Compare 128,128
    EXPECT_EQ(lhs == rhs, t->eq) << "equal";
    EXPECT_EQ(lhs != rhs, t->ne) << "not equal";
    EXPECT_EQ(lhs < rhs, t->lt);
    EXPECT_EQ(lhs > rhs, t->gt);
    EXPECT_EQ(lhs <= rhs, t->le);
    EXPECT_EQ(lhs >= rhs, t->ge);
    // Compare 128,other
    EXPECT_EQ(lhs == t->rhs, t->eq) << "equal";
    EXPECT_EQ(lhs != int(t->rhs), t->ne) << "not equal";
    EXPECT_EQ(lhs < t->rhs, t->lt);
    EXPECT_EQ(lhs > int(t->rhs), t->gt);
    EXPECT_EQ(lhs <= t->rhs, t->le);
    EXPECT_EQ(lhs >= int(t->rhs), t->ge);
    // Test in hi.
    lhs <<= 64;
    rhs <<= 64;
    EXPECT_EQ(lhs == rhs, t->eq) << "equal";
    EXPECT_EQ(lhs != rhs, t->ne) << "not equal";
    EXPECT_EQ(lhs < rhs, t->lt);
    EXPECT_EQ(lhs > rhs, t->gt);
    EXPECT_EQ(lhs <= rhs, t->le);
    EXPECT_EQ(lhs >= rhs, t->ge);
    // Test in hi+lo
    lhs >>= 3;
    rhs >>= 3;
    EXPECT_EQ(lhs == rhs, t->eq) << "equal";
    EXPECT_EQ(lhs != rhs, t->ne) << "not equal";
    EXPECT_EQ(lhs < rhs, t->lt);
    EXPECT_EQ(lhs > rhs, t->gt);
    EXPECT_EQ(lhs <= rhs, t->le);
    EXPECT_EQ(lhs >= rhs, t->ge);
  }
}

TEST(test128BitUnsigned, Multiply) {
  // Basic signed test
  binary data[]{
      {0u, 0u, 0u, 0u}, {1u, 1u, 0u, 1u},
      {0x7fffffffffffffff, 0x7fffffffffffffff, 0x3fffffffffffffff, 1u}};
  for (binary *t = data; t != data + sizeof(data) / sizeof(data[0]); ++t) {
    uint128_t lhs{t->lhs};
    uint128_t rhs{t->rhs};
    // operator*
    uint128_t product{lhs *rhs};
    EXPECT_EQ(product.hi(), t->hi) << t->lhs << "*" << t->rhs;
    EXPECT_EQ(product.lo(), t->lo) << t->lhs << "*" << t->rhs;
    // operator*=
    lhs *= rhs;
    EXPECT_EQ(lhs.hi(), t->hi) << t->lhs << "*" << t->rhs;
    EXPECT_EQ(lhs.lo(), t->lo) << t->lhs << "*" << t->rhs;
  }
}

typedef struct {
  uint64_t lhs_hi, lhs_lo, rhs_hi, rhs_lo;
  uint64_t q_hi, q_lo, r_hi, r_lo;
} divide;

TEST(test128BitUnsigned, Divide) {
  divide data[]{{0, 0, 0, 1, 0, 0, 0, 0},                    // 0/1=0r0
                {0, 1, 0, 1, 0, 1, 0, 0},                    // 1/1=1r0
                {0, 3, 0, 2, 0, 1, 0, 1},                    // 3/2=1r1
                {0xffff, 0xffff, 0xff, 0xff, 0, 257, 0, 0},  // hi bit test.
                {0, 1, 0, 0, 0, 0, 0, 0}  // divide by zero test
  };
  for (divide *t = data; t != data + sizeof(data) / sizeof(data[0]); ++t) {
    uint128_t lhs{(uint128_t(t->lhs_hi) << 64) | uint128_t(t->lhs_lo)};
    uint128_t rhs{(uint128_t(t->rhs_hi) << 64) | uint128_t(t->rhs_lo)};
    // Set SIGFPE to throw. Restore on exit from scope.
    std::shared_ptr<void(int)> handler(
        signal(SIGFPE, [](int) { throw 0; }),
        [](__sighandler_t f) { signal(SIGFPE, f); });
    try {
#ifdef GCC_OPTIMISE_ENABLED
      // If the gcc optimise level (-On) is set above 0, the try...catch
      // SIGFPE -> throw does not work. Using gcc 4.8.
      if (rhs == 0) {
        throw 0;
      }
#endif
      uint128_t quotient = lhs / rhs;
      EXPECT_NE(rhs, 0) << "Divide by zero should have thrown";
      EXPECT_EQ(quotient.hi(), t->q_hi) << "quotient";
      EXPECT_EQ(quotient.lo(), t->q_lo) << "quotient";
    }
    catch (int ex) {
      EXPECT_EQ(t->rhs_lo, ex);
      continue;
    }
    uint128_t remainder = lhs % rhs;
    EXPECT_EQ(remainder.hi(), t->r_hi) << "remainder";
    EXPECT_EQ(remainder.lo(), t->r_lo) << "remainder";
  }
  std::signal(SIGFPE, SIG_DFL);
}

TEST(test128BitUnsigned, SoakMultDiv) {
  std::random_device rd;
  std::uniform_int_distribution<uint64_t> incr(1ll, 2ll << 45);
  std::uniform_int_distribution<uint64_t> other(1, UINT64_MAX / 2);
  // LINT EXCEPTION: cpp_002: 1: Checking if unsigned expression 'lhs64' < 0
  for (uint64_t lhs64 = 1u; lhs64 < UINT64_MAX / 2u; lhs64 += incr(rd)) {
    uint64_t rhs64{other(rd)};
    uint128_t lhs{lhs64};
    uint128_t rhs{rhs64};
    uint128_t product{lhs *rhs};
    uint128_t quotient{product / lhs};
    uint64_t hi{quotient < 0 ? uint64_t(-1) : 0};
    EXPECT_EQ(quotient.hi(), hi) << lhs64 << "*" << rhs64 << "/" << lhs64
                                 << " quotient";
    EXPECT_EQ(quotient.lo(), rhs64) << lhs64 << "*" << rhs64 << "/" << lhs64
                                    << " quotient";
    uint128_t remainder{product % lhs};
    EXPECT_EQ(remainder.hi(), 0) << lhs64 << "*" << rhs64 << "/" << lhs64
                                 << " remainder";
    EXPECT_EQ(remainder.lo(), 0) << lhs64 << "*" << rhs64 << "/" << lhs64
                                 << " remainder";
    quotient = product / rhs;
    hi = quotient < 0 ? uint64_t(-1) : 0;
    EXPECT_EQ(quotient.hi(), hi) << lhs64 << "*" << rhs64 << "/" << lhs64
                                 << " quotient";
    EXPECT_EQ(quotient.lo(), lhs64) << lhs64 << "*" << rhs64 << "/" << lhs64
                                    << " quotient";
    remainder = product % lhs;
    EXPECT_EQ(remainder.hi(), 0) << lhs64 << "*" << rhs64 << "/" << lhs64
                                 << " remainder";
    EXPECT_EQ(remainder.lo(), 0) << lhs64 << "*" << rhs64 << "/" << lhs64
                                 << " remainder";
  }
}

typedef struct {
  uint64_t hi, lo;
  const char *dec, *oct, *hex;
} streamio;

TEST(test128BitUnsigned, StreamIO) {
  streamio data[]{
      {0u, 0u, "0", "0", "0"}, {0, 12, "12", "14", "c"},
      {0u, 123456789u, "123456789", "726746425", "75bcd15"},
      {1u,                       0u,                 "18446744073709551616",
       "2000000000000000000000", "10000000000000000"},
      {123456789u,                     1u,
       "2277375790844960561141121025", "1655715052000000000000000000001",
       "75bcd150000000000000001"},
      {0xffffffffffffffff,
       0xffffffffffffffff,
       "340282366920938463463374607431768211455",
       "3777777777777777777777777777777777777777777",
       "ffffffffffffffffffffffffffffffff"},
      {uint64_t(INT64_MIN),
       0u,
       "170141183460469231731687303715884105728",
       "2000000000000000000000000000000000000000000",
       "80000000000000000000000000000000"}};
  for (streamio *t = data; t != data + sizeof(data) / sizeof(data[0]); ++t) {
    uint128_t value{(uint128_t(t->hi) << 64) | uint128_t(t->lo)};
    std::stringstream decimal;
    decimal << value;
    EXPECT_STREQ(decimal.str().c_str(), t->dec);
    decimal << "test";
    std::stringstream octal;
    octal << std::oct << value;
    EXPECT_STREQ(octal.str().c_str(), t->oct);
    std::stringstream hexadecimal;
    hexadecimal << std::hex << value;
    EXPECT_STREQ(hexadecimal.str().c_str(), t->hex);
    uint128_t result;
    decimal >> result;
    EXPECT_EQ(result, value);
    std::string suffix;
    decimal >> suffix;
    EXPECT_STREQ(suffix.c_str(), "test");
    octal >> result;
    EXPECT_EQ(result, value);
    hexadecimal >> result;
    EXPECT_EQ(result, value);
    if (value > 0) {
      std::stringstream pre_octal;
      pre_octal << "0" << std::oct << value;
      pre_octal >> std::dec >> result;
      EXPECT_EQ(result, value);
      std::stringstream pre_hexadecimal;
      pre_hexadecimal << "0x" << std::hex << value;
      pre_hexadecimal >> std::dec >> result;
      EXPECT_EQ(result, value);
    }
  }
}

TEST(test128BitUnsigned, Functions) {
  uint128_t pos{123456789};
  EXPECT_EQ(abs(pos), pos);
  EXPECT_EQ(sqrtl(pos * pos), pos);
}

TEST(test128BitUnsigned, Traits) {
  EXPECT_EQ(sizeof(uint128_t) * 8, 128) << "sizeof(uint128_t)";
  EXPECT_TRUE(std::numeric_limits<uint128_t>::is_integer);
  EXPECT_FALSE(std::numeric_limits<uint128_t>::is_signed);
}

TEST(test128BitUnsigned, Conversions) {
  EXPECT_TRUE((std::is_same<typename std::make_signed<uint128_t>::type,
                            sdr_math::int128_t>::value));
  EXPECT_TRUE((std::is_same<typename std::make_unsigned<uint128_t>::type,
                            uint128_t>::value));
  // Using 32 bit precision numbers should convert perfectly for both
  // 64-bit and 80-bit long doubles.
  for (int power = 0; power < 90; power += 3) {
    uint128_t ivalue{0x87654321};  // 32 bits of precision.
    ivalue <<= power;
    long double f_from_i = static_cast<long double>(ivalue);
    EXPECT_EQ(static_cast<uint128_t>(f_from_i), ivalue);

    long double fvalue{3435934515.0L};  // 32 bits of precision.
    fvalue *= std::pow(2.0L, power);
    uint128_t i_from_f = static_cast<uint128_t>(fvalue);
    EXPECT_EQ(static_cast<long double>(i_from_f), fvalue);
  }
}

int main(int argc, char **argv) {
  ::testing::InitGoogleTest(&argc, argv);
  std::cout << std::hex;
  return RUN_ALL_TESTS();
}
