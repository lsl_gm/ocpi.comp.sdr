// Common rounding functions
//
// This file is protected by Copyright. Please refer to the COPYRIGHT file
// distributed with this source distribution.
//
// This file is part of OpenCPI <http://www.opencpi.org>
//
// OpenCPI is free software: you can redistribute it and/or modify it under the
// terms of the GNU Lesser General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option) any
// later version.
//
// OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
// more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

// LINT EXCEPTION: cpp_005: 2: Define use allowed in include guard
#ifndef COMPONENTS_MATH_COMMON_SCALE_AND_ROUND_HH_
#define COMPONENTS_MATH_COMMON_SCALE_AND_ROUND_HH_

// LINT EXCEPTION: cpp_008: 1: Include allowed after include guard
#include <type_traits>
// LINT EXCEPTION: cpp_008: 1: Include allowed after include guard
#include <cstdint>
// LINT EXCEPTION: cpp_008: 1: Include allowed after include guard
#include <limits>
// LINT EXCEPTION: cpp_008: 1: Include allowed after include guard
#include "128bit.hh"

namespace sdr_math {

// rounding_type
//
// Choices for how to round fractional values into integers.
enum struct rounding_type {
  MATH_TRUNCATE,
  MATH_HALF_UP,
  MATH_HALF_EVEN
};

// overflow_type
//
// Choices for how to cope with overflow.
// MATH_WRAP    : Value will wrap around into bounds.
// MATH_SATURATE: Value should clip to max/min bounds.
enum struct overflow_type {
  MATH_WRAP,
  MATH_SATURATE
};

// constrain_value
//
// Template parameters:
//   accum_t : Integer type, e.g. int32_t
//   output_t : Integer type, e.g. int16_t
//
// Args:
//  value    : Value to be constrained to be within output_t range.
//  overflow : Whether to clip to max/min bounds or wrap around into bounds.
template <class accum_t, class output_t>
inline output_t constrain_value(accum_t value, const overflow_type overflow) {

  if (overflow == overflow_type::MATH_SATURATE) {
    value = std::max(
        static_cast<accum_t>(std::numeric_limits<output_t>::min()),
        std::min(value,
                 static_cast<accum_t>(std::numeric_limits<output_t>::max())));
  }

  // Return value constrained to output_t bits.
  return static_cast<output_t>(value);
}

// arithmetic_right_shift
//
// Template parameters:
//   int_type : Integer type, e.g. int32_t
//   hardened : Boolean type, includes additional safety checks.
//
// Args:
//  input_value  : Value to be shifted
//  binary_point : Power of 2 position of binary point.
//
//  Note: The hardened parameter enables / disables additional
//        checks against the binary point.
template <class int_type, bool hardened = false>
inline int_type arithmetic_right_shift(int_type input_value,
                                       const uint8_t binary_point) {
  if (hardened && (binary_point >= std::numeric_limits<int_type>::digits)) {
    return 0;
  }
  // SEI Guidance int13 : Use bitwise operators only on unsigned operands.
  // Therefore an arithmetic right shift function is defined locally
  return input_value < 0 ? ~(~input_value >> binary_point)
                         : input_value >> binary_point;
}

// truncate_rounding
//
// Template parameters:
//   accum_t : Integer type, e.g. int32_t
//   hardened : Boolean type, includes additional safety checks.
//
// Args:
//  value    : Value to scale and round.
//  binary_point : Power of 2 position of binary point.
//
//  Note: The hardened parameter instructs arithmetic_right_shift to
//        enable / disable additional checks against the binary point.
template <class accum_t, bool hardened = true>
inline accum_t truncate_rounding(accum_t value, const uint8_t binary_point) {
  return arithmetic_right_shift<accum_t, hardened>(value, binary_point);
}

// half_up_rounding
//
// Template parameters:
//   accum_t : Integer type, e.g. int32_t
//   hardened : Boolean type, includes additional safety checks.
//
// Args:
//  value    : Value to scale and round.
//  binary_point : Power of 2 position of binary point.
//
//  Note: The hardened parameter enables / disables additional
//        checks against the binary point.
template <class accum_t, bool hardened = true>
accum_t half_up_rounding(accum_t value, const uint8_t binary_point) {

  if (hardened && (binary_point > std::numeric_limits<accum_t>::digits)) {
    return 0;
  }

  accum_t rounded{value};
  // Binary Point has already been checked for 0 therefore always
  // greater than 0.
  if (binary_point == 1U) {
    rounded = arithmetic_right_shift(value, 1U);
    rounded += value & static_cast<accum_t>(1U);
  } else {
    rounded = arithmetic_right_shift(value, (binary_point - 1));
    rounded += 1;
    rounded = arithmetic_right_shift(rounded, 1U);
  }

  return rounded;
}

// half_even_rounding
//
// Template parameters:
//   accum_t : Integer type, e.g. int32_t
//   hardened : Boolean type, includes additional safety checks.
//
// Args:
//  value    : Value to scale and round.
//  binary_point : Power of 2 position of binary point.
//
//  Note: The hardened parameter enables / disables additional
//        checks against the binary point.
template <class accum_t, bool hardened = true>
accum_t half_even_rounding(accum_t value, const uint8_t binary_point) {

  if (hardened && (binary_point >= std::numeric_limits<accum_t>::digits)) {
    return 0;
  }

  typedef typename std::make_unsigned<accum_t>::type uint_accum_t;
  uint_accum_t and_mask =
      (uint_accum_t(-1) >> ((sizeof(accum_t) * 8) - binary_point));
  uint_accum_t test_bit = (uint_accum_t(1) << (binary_point - 1));

  bool fract_is_half =
      ((static_cast<uint_accum_t>(value) & and_mask) == test_bit);
  bool whole_is_even =
      ((arithmetic_right_shift<accum_t, true>(value, binary_point) &
        static_cast<accum_t>(1U)) == static_cast<accum_t>(0U));

  accum_t rounded{value};
  if (binary_point == 1U) {
    rounded = arithmetic_right_shift(rounded, 1U);
    rounded += (value & static_cast<accum_t>(1U)) +
               ((static_cast<accum_t>(whole_is_even) &
                 static_cast<accum_t>(fract_is_half)) *
                static_cast<accum_t>(-1));
  } else {
    rounded = arithmetic_right_shift(rounded, (binary_point - 1));
    rounded +=
        static_cast<accum_t>(1U) + ((static_cast<accum_t>(whole_is_even) &
                                     static_cast<accum_t>(fract_is_half)) *
                                    static_cast<accum_t>(-1));
    rounded = arithmetic_right_shift(rounded, 1U);
  }

  return rounded;
}

// scale_and_round
//
// Template parameters:
//   accum_t : Integer type, e.g. int32_t
//   output_t : Integer type, e.g. int32_t
//
// Args:
//  val      : Value to scale and round.
//  rounding : Type of rounding to perform.
//  binary_point : Power of 2 position of binary point.
//  overflow : Whether to clip to max/min bounds or wrap around into bounds.
//
//  Note: Components using a "number of bits to shift" parameter for
//        scaling will need to add one on the call to this function
//        to get the required scaling factor.
template <class accum_t, class output_t>
output_t scale_and_round(const accum_t val,
                         const sdr_math::rounding_type rounding,
                         const uint8_t binary_point,
                         const sdr_math::overflow_type overflow) {
  static_assert(std::numeric_limits<accum_t>::is_integer,
                "Integer class required.");

  // Scale and Round should return value right shifted
  // by binary_point - 1. If binary point is 0 return
  // the value as received.
  if (binary_point == 0) {
    return constrain_value<accum_t, output_t>(val, overflow);
  }

  accum_t rounded = val;
  // Do scaling divide and round
  switch (rounding) {
    case rounding_type::MATH_TRUNCATE: {
      rounded = truncate_rounding(val, binary_point);
      break;
    }
    case rounding_type::MATH_HALF_UP: {
      rounded = half_up_rounding(val, binary_point);
      break;
    }
    // Default to half even
    default:
    case rounding_type::MATH_HALF_EVEN: {
      rounded = half_even_rounding(val, binary_point);
    }
  }
  output_t output = constrain_value<accum_t, output_t>(rounded, overflow);
  return output;
}

}  // namespace sdr_math
#endif  // COMPONENTS_MATH_COMMON_SCALE_AND_ROUND_HH_
