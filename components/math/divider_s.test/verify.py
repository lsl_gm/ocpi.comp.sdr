#!/usr/bin/env python3

# Runs checks for divider_s testing
#
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
# more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

"""Verification script for the divider_s component."""

import os
import sys

import opencpi.ocpi_testing as ocpi_testing

from divider_s import Divider

method_property = os.environ.get("OCPI_TEST_method")

input_file = str(sys.argv[-2])
denominator_file = str(sys.argv[-1])
output_file = str(sys.argv[-3])

port_names = {
    "output": 0,
    "remainder": 1
}

port_index = port_names[output_file.split(".")[-2]]
test_id = ocpi_testing.get_test_case()

division_implementation = Divider(
    division_method=method_property,
    bit_length=16,
    case=test_id,
    logging_level="DEBUG")

verifier = ocpi_testing.Verifier(division_implementation)
verifier.set_port_types(["short_timed_sample", "short_timed_sample"],
                        ["short_timed_sample", "short_timed_sample"],
                        ["equal", "equal"])
if not verifier.verify(test_id, [input_file, denominator_file],
                       [output_file], port_select=port_index):
    sys.exit(1)
