-- log_us HDL implementation.
--
-- This file is protected by Copyright. Please refer to the COPYRIGHT file
-- distributed with this source distribution.
--
-- This file is part of OpenCPI <http://www.opencpi.org>
--
-- OpenCPI is free software: you can redistribute it and/or modify it under the
-- terms of the GNU Lesser General Public License as published by the Free
-- Software Foundation, either version 3 of the License, or (at your option) any
-- later version.
--
-- OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
-- WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
-- A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
-- more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;
library ocpi;
use ocpi.types.all;
library sdr_math;
use sdr_math.sdr_math.logarithm;
library sdr_dsp;
use sdr_dsp.sdr_dsp.rounding_halfup;

architecture rtl of worker is

  constant base_c            : real    := from_float(base);  -- Logarithm base
  constant input_size_c      : integer := input_in.data'length;  -- data input width
  constant output_size_c     : integer := output_out.data'length;  -- data output width
  constant fractional_bits_c : integer := 16;  -- number of fraction bits
  constant max_log_val_c     : real    := log2(2.0**input_size_c - 1.0) / log2(base_c);
  constant log_size_c        : integer := integer(ceil(max_log_val_c)) + input_size_c + 1;
  constant delay_c           : integer := 24;  -- delay for flow control signals

  signal valid_data   : std_logic;      -- true when input data is valid
  signal resized_data : unsigned(input_size_c + fractional_bits_c -1 downto 0);  -- resized data
  signal log_valid    : std_logic;      -- log output data valid signal
  signal log_data     : signed(log_size_c - 1 downto 0);  -- log output data
  signal binary_point : integer range 0 to log_size_c - 1;  -- o/p binary point
  signal log_out      : signed(log_size_c - 1 downto 0);  -- scaled log result
  signal data_out     : unsigned(output_size_c - 1 downto 0);  -- data output
  signal data_out_slv : std_logic_vector(output_size_c - 1 downto 0);

begin

  input_out.take <= output_in.ready;

  -- Signal for valid input data
  valid_data <= '1' when input_in.valid = '1' and input_in.opcode = ushort_timed_sample_sample_op_e else '0';

  -- Interface delay module
  -- Delays streaming interface signals to align with the delay introduced by
  -- the logarithm module.
  interface_delay_i : entity work.unsigned_short_protocol_delay
    generic map (
      delay_g              => delay_c,
      data_in_width_g      => input_in.data'length,
      processed_data_mux_g => '1'
      )
    port map (
      clk                 => ctl_in.clk,
      reset               => ctl_in.reset,
      enable              => output_in.ready,
      take_in             => '1',
      input_in            => input_in,
      processed_stream_in => data_out_slv,
      output_out          => output_out
      );

  -- Resize input data for 16 fractional bits and convert any zero to a one
  resized_data <= x"00010000" when valid_data = '1' and input_in.data = x"0000"
                  else unsigned(input_in.data) & x"0000";

  -- Log instantiation
  log_inst : logarithm
    generic map (
      input_size_g  => input_size_c + fractional_bits_c,
      output_size_g => log_size_c,
      base_g        => base_c
      )
    port map (
      clk            => ctl_in.clk,
      rst            => ctl_in.reset,
      clk_en         => output_in.ready,
      data_valid_in  => valid_data,
      data_in        => resized_data,
      data_valid_out => log_valid,
      data_out       => log_data
      );

  -- Set output binary point
  binary_point <= fractional_bits_c - to_integer(props_in.scale_output);

  -- Halfup Rounder instantiation
  halfup_rounder_inst : rounding_halfup
    generic map (
      input_width_g   => log_size_c,
      output_width_g  => log_size_c,
      saturation_en_g => false
      )
    port map(
      clk            => ctl_in.clk,
      reset          => ctl_in.reset,
      clk_en         => output_in.ready,
      data_in        => log_data,
      data_valid_in  => log_valid,
      binary_point   => binary_point,
      data_out       => log_out,
      data_valid_out => open
      );

  -- Ensure log values aren't less than 0 and rail if greater than 65535
  data_out <= x"0000" when log_out < 0 else
              x"ffff" when log_out > 65535 else
              unsigned(log_out(output_size_c - 1 downto 0));

  data_out_slv <= std_logic_vector(data_out);

end rtl;
