-- divider worker for the short protocol
--
-- This file is protected by Copyright. Please refer to the COPYRIGHT file
-- distributed with this source distribution.
--
-- This file is part of OpenCPI <http://www.opencpi.org>
--
-- OpenCPI is free software: you can redistribute it and/or modify it under the
-- terms of the GNU Lesser General Public License as published by the Free
-- Software Foundation, either version 3 of the License, or (at your option) any
-- later version.
--
-- OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
-- WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
-- A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
-- more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.


library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
library ocpi;
use ocpi.types.all;  -- remove this to avoid all ocpi name collisions
library sdr_math;
use sdr_math.sdr_math.non_restoring_divider;

architecture rtl of worker is

  function to_string(method : work.divider_s_constants.method_t) return string is
  begin
    case (method) is
      when truncate_e => return "truncate";
      when floor_e    => return "floor";
      when others =>
        report "Bad division method requested" severity failure;
        return "floor";
    end case;
  end function;

  constant method_c : string  := to_string(method);
  constant delay_c  : natural := input_in.data'length + 2;

  signal input_take        : std_logic;
  signal denominator_take  : std_logic;
  signal input_delayed     : worker_input_in_t;
  signal valid_data        : std_logic;
  signal valid_data_enable : std_logic;
  signal numerator         : signed(input_in.data'range);
  signal denominator       : signed(denominator_in.data'range);
  signal get_next          : std_logic;
  signal divider_out_valid : std_logic;
  signal output_ready      : std_logic;
  signal output_give       : std_logic;
  signal output_valid      : std_logic;
  signal output_eof        : std_logic_vector(delay_c-1 downto 0);
  signal output_data       : signed(input_in.data'range);
  signal remainder_data    : signed(input_in.data'range);

begin

  --------
  -- Input port logic
  --------
  input_out.take       <= input_take;
  denominator_out.take <= denominator_take;

  valid_data <= '1' when (input_in.valid = '1' and denominator_in.valid = '1' and
                          input_in.opcode = short_timed_sample_sample_op_e and
                          denominator_in.opcode = short_timed_sample_sample_op_e)
                else '0';

  valid_data_enable <= valid_data and get_next;

  -- Output ready is only true when all connected ports are reporting ready
  output_ready <= (output_in.ready or not output_in.is_connected) and
                  (remainder_in.ready or not remainder_in.is_connected);

  -- Always take non-sample opcodes from both inputs.
  -- Only take sample opcodes if both ports have a sample opcode.
  input_take_logic_p : process (output_ready, valid_data_enable, get_next,
                                input_in.opcode, denominator_in.opcode)
  begin
    input_take       <= '0';
    denominator_take <= '0';
    -- If inputs and output are ready
    if output_ready = '1' then
      -- Always take when a non-sample opcode
      if input_in.opcode /= short_timed_sample_sample_op_e and get_next = '1' then
        input_take <= '1';
      end if;
      -- Always take when a non-sample opcode, but this will be discarded
      if denominator_in.opcode /= short_timed_sample_sample_op_e then
        denominator_take <= '1';
      end if;
      -- Only take a sample opcode if both inputs are valid,
      if valid_data_enable = '1' then
        input_take       <= '1';
        denominator_take <= '1';
      end if;
    end if;
  end process;

  numerator   <= signed(input_in.data);
  denominator <= signed(denominator_in.data);

  division_i : non_restoring_divider
    generic map (
      data_width_g => input_in.data'length,
      method_g     => method_c
      )
    port map (
      clk            => ctl_in.clk,
      reset          => ctl_in.reset,
      clk_en         => output_ready,
      data_in_valid  => valid_data_enable,
      data_in_ready  => get_next,
      numerator      => numerator,
      denominator    => denominator,
      data_valid_out => divider_out_valid,
      quotient       => output_data,
      remainder      => remainder_data
      );

  --------
  -- Output port logic
  --------
  output_ports_p : process (ctl_in.clk)
  begin
    if rising_edge(ctl_in.clk) then
      if ctl_in.reset = '1' then
        output_eof(0) <= '0';
      elsif output_ready = '1' then
        output_eof <= output_eof(output_eof'high-1 downto 0) & input_in.eof;
        if input_take = '1' then
          input_delayed <= input_in;
        end if;
      end if;
    end if;
  end process;

  -- As the ready state of the 2 output ports are treated jointly, the output
  -- signals are ANDed with this common ready state.
  output_give <= divider_out_valid and output_ready when input_delayed.opcode = short_timed_sample_sample_op_e
                 else input_delayed.ready and output_ready;
  output_valid <= divider_out_valid and output_ready when input_delayed.opcode = short_timed_sample_sample_op_e
                  else input_delayed.valid and output_ready;


  output_out.som         <= input_delayed.som;
  output_out.eom         <= input_delayed.eom;
  output_out.eof         <= output_eof(output_eof'high);
  output_out.valid       <= output_valid;
  output_out.give        <= output_give;
  output_out.byte_enable <= input_delayed.byte_enable;
  output_out.opcode      <= input_delayed.opcode;
  output_out.data        <= std_logic_vector(output_data) when input_delayed.opcode = short_timed_sample_sample_op_e
                            else input_delayed.data;

  remainder_out.som         <= input_delayed.som;
  remainder_out.eom         <= input_delayed.eom;
  remainder_out.eof         <= output_eof(output_eof'high);
  remainder_out.valid       <= output_valid;
  remainder_out.give        <= output_give;
  remainder_out.byte_enable <= input_delayed.byte_enable;
  remainder_out.opcode      <= input_delayed.opcode;
  remainder_out.data        <= std_logic_vector(remainder_data) when input_delayed.opcode = short_timed_sample_sample_op_e
                               else input_delayed.data;

end rtl;
