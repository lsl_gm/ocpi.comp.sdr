#!/usr/bin/env python3

# Runs checks for real testing
#
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
# more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

"""Runs checks for real testing."""

import os
import sys

import opencpi.ocpi_testing as ocpi_testing

from real import Real


input_file_path = str(sys.argv[-1])
output_file_path = str(sys.argv[-2])

real_implementation = Real()

test_id = ocpi_testing.get_test_case()

verifier = ocpi_testing.Verifier(real_implementation)
verifier.set_port_types(
    ["complex_short_timed_sample"], ["short_timed_sample"], ["equal"])
if verifier.verify(test_id, [input_file_path], [output_file_path]) is True:
    sys.exit(0)
else:
    sys.exit(1)
