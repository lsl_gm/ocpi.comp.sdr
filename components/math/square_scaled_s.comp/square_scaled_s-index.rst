.. square_scaled_s documentation

.. This file is protected by Copyright. Please refer to the COPYRIGHT file
   distributed with this source distribution.

   This file is part of OpenCPI <http://www.opencpi.org>

   OpenCPI is free software: you can redistribute it and/or modify it under the
   terms of the GNU Lesser General Public License as published by the Free
   Software Foundation, either version 3 of the License, or (at your option) any
   later version.

   OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>.


.. _square_scaled_s:


Square Scaled (``square_scaled_s``)
===================================
Squares input values and right shifts the result.

Design
------
Square an the values in a sample opcode message and then right shift the result of the square operation.

The mathematical representation of the implementation is given in :eq:`square_scaled_s-equation`.

.. math::
   :label: square_scaled_s-equation

   y[n] = \lfloor \frac{x[n] * x[n]}{2^N} \rfloor

In :eq:`square_scaled_s-equation`:

 * :math:`x[n]` is the input values.

 * :math:`y[n]` is the output values.

 * :math:`N` is the number of bit the result of the square is right shifted by.

A block diagram representation of the implementation is given in :numref:`square_scaled_s-diagram`.

.. _square_scaled_s-diagram:

.. figure:: square_scaled_s.svg
   :alt: Block diagram outlining square scaled implementation.
   :align: center

   Square scaled implementation.

Interface
---------
.. literalinclude:: ../specs/square_scaled_s-spec.xml
   :language: xml
   :lines: 1,19-

Ports
~~~~~
.. ocpi_documentation_ports::

   input: Primary input samples port.
   output: Primary output samples port.

Opcode Handling
~~~~~~~~~~~~~~~
The square and scaling is only applied to values in a sample opcode message. All other opcodes messages received are passed through this component without any effect.

Properties
~~~~~~~~~~
.. ocpi_documentation_properties::

   scale_output: Setting `scale_output` to the default value of 15 ensures that the output never overflows for full scale 16-bit input values.

Implementations
---------------
.. ocpi_documentation_implementations:: ../square_scaled_s.hdl ../square_scaled_s.rcc

Example Application
-------------------
.. literalinclude:: example_app.xml
   :language: xml
   :lines: 1,19-

Dependencies
------------
The dependencies to other elements in OpenCPI are:

 * None

There is also a dependency on:

 * ``ieee.std_logic_1164``

 * ``ieee.numeric_std``

Limitations
-----------
Limitations of ``square_scaled_s`` are:

 * None.

Testing
-------
.. ocpi_documentation_test_platforms::

.. ocpi_documentation_test_result_summary::
