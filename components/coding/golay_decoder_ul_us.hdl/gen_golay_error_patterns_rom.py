#!/usr/bin/env python3

# Script is used to generate the golay_error_patterns_rom.vhd
#
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
# more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

"""This script is used to generate the golay_error_patterns_rom.vhd."""

import os.path
import sys
import numpy as np
import golay_utils
from datetime import datetime, timezone

error_patterns = golay_utils.gen_error_patterns()
syndromes = golay_utils.gen_syndromes(error_patterns)
syndromes_sorted = sorted(syndromes)

num_syndromes = len(syndromes)
bit_width = 23
binary_length = "0" + str(bit_width) + "b"
syndrome_bit_width = 11
entity_name = "golay_error_patterns_rom"
filename = "gen/" + entity_name + ".vhd"

syndrome_error_pattern_dict = {}
for i in range(0, num_syndromes):
    syndrome_error_pattern_dict[syndromes[i]] = error_patterns[i]


with open(filename, "w") as f:
    # Write header
    now = datetime.now(timezone.utc).astimezone()
    current_time = now.strftime("%a %b %-d %H:%M:%S %Y ")
    current_time = current_time + now.tzname()

    f.write("-- THIS FILE WAS GENERATED ON " + current_time + "\n")
    f.write("-- YOU PROBABLY SHOULD NOT EDIT IT" + "\n\n")

    # Write libraries
    f.write("library ieee; use ieee.std_logic_1164.all, ieee.numeric_std.all;\n\n")

    # Write Description
    f.write("-------------------------------------------------------------------------------\n")
    f.write("-- Golay Error Patterns ROM\n")
    f.write("-------------------------------------------------------------------------------\n")
    f.write("--\n")
    f.write("-- Description\n")
    f.write("-- Golay error patterns ROM. The ROM contains precomputed error patterns for 1 bit,\n")
    f.write("-- 2 bit, and 3 bit errors. \n")
    f.write("--\n")
    f.write("-- The 11 bit syndrome is used as the address for the ROM.\n")
    f.write("--\n")
    f.write("-- References:\n")
    f.write("-- https://www.researchgate.net/profile/Satyabrata-Sarangi/publication/264977426_Efficient_Hardware_Implementation_of_Encoder_and_Decoder_for_Golay_Code/links/5405fdd00cf23d9765a7cbca/Efficient-Hardware-Implementation-of-Encoder-and-Decoder-for-Golay-Code.pdf\n")
    f.write("-- https://www.sciencedirect.com/science/article/pii/S1665642313715438\n")
    f.write("-- https://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.323.9943&rep=rep1&type=pdf\n\n")

    # Write entity
    f.write("entity " + entity_name + " is\n")
    f.write("    port (\n")
    f.write("        clk           : in  std_logic;\n")
    f.write("        ivld          : in  std_logic;\n")
    f.write("        syndrome      : in  std_logic_vector(" +
            str(syndrome_bit_width - 1) + " downto 0);\n")
    f.write("        error_pattern : out std_logic_vector(" +
            str(bit_width - 1) + " downto 0));\n")
    f.write("end " + entity_name + ";\n\n")

    f.write("architecture rtl of " + entity_name + " is\n\n")

    # Write ROM
    count = 0
    f.write("type rom_type is array (0 to " + str(num_syndromes-1) +
            ") of std_logic_vector(" + str(bit_width-1) + " downto 0);\n")
    f.write("signal ROM : rom_type := (\n")
    for i in range(0, num_syndromes):
        if count == 0:
            f.write("  ")
        if count < 3:
            f.write(
                "\"" + format(syndrome_error_pattern_dict[syndromes_sorted[i]], binary_length) + "\", ")
            count += 1
        elif count == 3:
            if i == num_syndromes-1:
                f.write(
                    "\"" + format(syndrome_error_pattern_dict[syndromes_sorted[i]], binary_length) + "\");" + "\n\n")
            else:
                f.write(
                    "\"" + format(syndrome_error_pattern_dict[syndromes_sorted[i]], binary_length) + "\"," + "\n")
            count = 0

    # Write process
    f.write("begin\n\n")
    f.write("  process(clk)\n")
    f.write("  begin\n")
    f.write("    if rising_edge(clk) then\n")
    f.write("      if ivld = '1' then\n")
    f.write("        error_pattern <= ROM(to_integer(unsigned(syndrome)));\n")
    f.write("      end if;\n")
    f.write("    end if;\n")
    f.write("  end process;\n\n")
    f.write("end rtl;\n")
