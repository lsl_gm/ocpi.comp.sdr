#!/usr/bin/env python3

# Script used to generate the golay_codewords_rom.vhd
#
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
# more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

"""This script is used to generate the golay_codewords_rom.vhd."""

import os.path
import sys
import numpy as np
import golay_utils
from datetime import datetime, timezone

codewords = golay_utils.gen_codewords()
num_codewords = len(codewords)
bit_width = 24
binary_length = "0" + str(bit_width) + "b"
din_bit_width = 12
entity_name = "golay_codewords_rom"
filename = "gen/" + entity_name + ".vhd"

with open(filename, "w") as f:
    # Write header
    now = datetime.now(timezone.utc).astimezone()
    current_time = now.strftime("%a %b %-d %H:%M:%S %Y ")
    current_time = current_time + now.tzname()

    f.write("-- THIS FILE WAS GENERATED ON " + current_time + "\n")
    f.write("-- YOU PROBABLY SHOULD NOT EDIT IT" + "\n\n")

    # Write libraries
    f.write("library ieee; use ieee.std_logic_1164.all, ieee.numeric_std.all;\n\n")

    # Write Description
    f.write("-------------------------------------------------------------------------------\n")
    f.write("-- Golay Codewords ROM\n")
    f.write("-------------------------------------------------------------------------------\n")
    f.write("--\n")
    f.write("-- Description\n")
    f.write("-- Golay codewords ROM. The ROM contains precomputed Golay codewords.\n")
    f.write("--\n")
    f.write(
        "-- The 12 bit input data is used as the address for the ROM. These precomputed\n")
    f.write("-- codewords were generated with the algorithm referenced here:\n")
    f.write("-- http://aqdi.com/articles/using-the-golay-error-detection-and-correction-code-3/\n\n")

    # Write entity
    f.write("entity " + entity_name + " is\n")
    f.write("    port (\n")
    f.write("        clk      : in  std_logic;\n")
    f.write("        ivld     : in  std_logic;\n")
    f.write("        din      : in  std_logic_vector(" +
            str(din_bit_width - 1) + " downto 0);\n")
    f.write("        codeword : out std_logic_vector(" +
            str(bit_width - 1) + " downto 0));\n")
    f.write("end " + entity_name + ";\n\n")

    f.write("architecture rtl of " + entity_name + " is\n\n")

    # Write ROM
    count = 0
    f.write("type rom_type is array (0 to " + str(num_codewords-1) +
            ") of std_logic_vector(" + str(bit_width-1) + " downto 0);\n")
    f.write("signal ROM : rom_type := (\n")
    for i in range(0, num_codewords):
        if count == 0:
            f.write("  ")
        if count < 3:
            f.write("\"" + format(codewords[i], binary_length) + "\", ")
            count += 1
        elif count == 3:
            if i == num_codewords-1:
                f.write("\"" + format(codewords[i],
                                      binary_length) + "\");" + "\n\n")
            else:
                f.write("\"" + format(codewords[i],
                                      binary_length) + "\"," + "\n")
            count = 0

    # Write process
    f.write("begin\n\n")
    f.write("  process(clk)\n")
    f.write("  begin\n")
    f.write("    if rising_edge(clk) then\n")
    f.write("      if ivld = '1' then\n")
    f.write("        codeword <= ROM(to_integer(unsigned(din)));\n")
    f.write("      end if;\n")
    f.write("    end if;\n")
    f.write("  end process;\n\n")
    f.write("end rtl;\n")
