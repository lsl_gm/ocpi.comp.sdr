#!/usr/bin/env python3

# Python implementation of splitter block for verification
#
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
# more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

"""Python implementation of splitter block for verification."""

import opencpi.ocpi_testing as ocpi_testing


class Splitter(ocpi_testing.Implementation):
    """Python implementation of splitter block for verification."""

    def __init__(self):
        """Initialise the Splitter class."""
        super().__init__()
        self.output_ports = ["output_1", "output_2", "output_3", "output_4"]

    def reset(self):
        """Reset the state to the same state as at initialisation.

        There is no state so this function is a no-op, but it is required by
        the ocpi_testing.Implementation base class.
        """
        pass

    def splitter(self, opcode, data):
        """Split an opcode over the output ports.

        Args:
            opcode (str): The opcode name to output.
            data (various): The opcode data to output.

        Returns:
            Formatted output opcode messages for each output port.
        """
        return self.output_formatter(
            [{"opcode": opcode, "data": data}],
            [{"opcode": opcode, "data": data}],
            [{"opcode": opcode, "data": data}],
            [{"opcode": opcode, "data": data}])

    def sample(self, values):
        """Handle incoming Sample opcode.

        Args:
            values (list): Incoming sample values from the input port.

        Returns:
            Formatted output opcode messages.
        """
        return self.splitter("sample", values)

    def time(self, value):
        """Handle incoming Time opcode.

        Args:
            value (decimal): Incoming time value from the input port.

        Returns:
            Formatted output opcode messages.
        """
        return self.splitter("time", value)

    def sample_interval(self, value):
        """Handle incoming Sample Interval opcode.

        Args:
            value (decimal): Incoming interval value from the input port.

        Returns:
            Formatted output opcode messages.
        """
        return self.splitter("sample_interval", value)

    def discontinuity(self, indicator):
        """Handle incoming Discontinuity opcode.

        Args:
            indicator (bool): True if discontinuity present on the input port.

        Returns:
            Formatted output opcode messages.
        """
        if indicator is True:
            return self.splitter("discontinuity", None)
        else:
            return self.output_formatter([], [], [], [])

    def flush(self, indicator):
        """Handle incoming Flush opcode.

        Args:
            indicator (bool): True if flush present on the input port.

        Returns:
            Formatted output opcode messages.
        """
        if indicator is True:
            return self.splitter("flush", None)
        else:
            return self.output_formatter([], [], [], [])

    def metadata(self, value):
        """Handle incoming Metadata opcode.

        Args:
            value (tuple): Metadata id/value tuple from the input port.

        Returns:
            Formatted output opcode messages.
        """
        return self.splitter("metadata", value)
