.. message_length_adjuster documentation

.. This file is protected by Copyright. Please refer to the COPYRIGHT file
   distributed with this source distribution.

   This file is part of OpenCPI <http://www.opencpi.org>

   OpenCPI is free software: you can redistribute it and/or modify it under the
   terms of the GNU Lesser General Public License as published by the Free
   Software Foundation, either version 3 of the License, or (at your option) any
   later version.

   OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>.


.. _message_length_adjuster:


Message Length Adjuster (``message_length_adjuster``)
=====================================================
Combines multiple smaller sample messages into larger ones, or splits larger sample messages into smaller ones.

Design
------
The component is capable of taking successive sample messages received at the input and combining them to produce output messages with up to a maximum of ``message_size`` samples, where ``message_size`` is a property. Conversely the component is also capable of taking larger messages received at the input and splitting them to produce multiple smaller output sample messages.

In the event that the number of samples received at the input is less than ``message_size``, the ``timeout`` property allows the output message to be sent. This prevents samples being buffered indefinitely. Setting ``timeout`` to 0 disables the time-out.

If the time interface is unavailable, the component will operate as normal but the time-out function will not work, and messages will be held indefinitely until the message length is received.

The original message order is preserved which means that if a non-sample opcode is received and samples are already buffered waiting for ``message_size`` samples to be received, these buffered samples will be output before the non-sample opcode.

Interface
---------
.. literalinclude:: ../specs/message_length_adjuster-spec.xml
   :language: xml
   :lines: 1,19-

Ports
~~~~~
.. ocpi_documentation_ports::

   input: Primary input samples port.
   output: Primary output samples port.

Opcode Handling
~~~~~~~~~~~~~~~
All received message types are passed through this component and the ordering unchanged. Two or more consecutive input sample opcodes may be condensed into fewer larger output sample opcodes or larger samples may be split into smaller output sample opcodes.

Properties
~~~~~~~~~~
.. ocpi_documentation_properties::

   width: Must be a non-zero multiple of 8.
   message_size: If ``message_size`` times ``width`` is greater than ``ocpi_buffer_size_output`` the maximum number of samples per message will be limited to ``ocpi_buffer_size_output``.

Implementations
---------------
.. ocpi_documentation_implementations:: ../message_length_adjuster.hdl ../message_length_adjuster.rcc

Example Application
-------------------
.. literalinclude:: example_app.xml
   :language: xml
   :lines: 1,19-

Dependencies
------------
The dependencies to other elements in OpenCPI are:

 * :ref:`Opcode enumeration type <opcode_t-type>`

 * :ref:`Opcode to SLV function <opcode_to_slv-function>`

 * :ref:`Timeout Monitor Primitive <timeout_monitor-primitive>`

There is also a dependency on:

 * ``ieee.std_logic_1164``

 * ``ieee.numeric_std``

 * ``ieee.math_real``

Limitations
-----------

  * The setting of the ``timeout`` property has been tested but limitations of the testing framework means that the timing of the timeout trigger has not been fully verified.

  * Very small values for the `timeout` property may cause the time-out to always trigger before any messages are read - resulting in the RCC implementation only processing time-outs and not messages, meaning no data is processed through the component.

Testing
-------
.. ocpi_documentation_test_platforms::

.. ocpi_documentation_test_result_summary::
