// RCC implementation of splitter_b worker.
//
// This file is protected by Copyright. Please refer to the COPYRIGHT file
// distributed with this source distribution.
//
// This file is part of OpenCPI <http://www.opencpi.org>
//
// OpenCPI is free software: you can redistribute it and/or modify it under the
// terms of the GNU Lesser General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option) any
// later version.
//
// OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
// more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#include "splitter_b-worker.hh"

using namespace OCPI::RCC;  // for easy access to RCC data types and constants
using namespace Splitter_bWorkerTypes;

class Splitter_bWorker : public Splitter_bWorkerBase {

  RCCResult run(bool) {

    if (output_2.isConnected()) {
      if (!output_2.hasBuffer()) return RCC_OK;
    }

    if (output_3.isConnected()) {
      if (!output_3.hasBuffer()) return RCC_OK;
    }

    if (output_4.isConnected()) {
      if (!output_4.hasBuffer()) return RCC_OK;
    }

    if (input.opCode() == Bool_timed_sampleSample_OPERATION) {
      uint8_t *output_data;
      size_t length = input.sample().data().size();
      const uint8_t *input_data = input.sample().data().data();
      // Output 1
      output_data = output_1.sample().data().data();
      output_1.sample().data().resize(length);
      output_1.setOpCode(Bool_timed_sampleSample_OPERATION);
      std::copy(input_data, input_data + length, output_data);
      // Output 2
      if (output_2.isConnected()) {
        output_data = output_2.sample().data().data();
        output_2.sample().data().resize(length);
        output_2.setOpCode(Bool_timed_sampleSample_OPERATION);
        std::copy(input_data, input_data + length, output_data);
      }
      // Output 3
      if (output_3.isConnected()) {
        output_data = output_3.sample().data().data();
        output_3.sample().data().resize(length);
        output_3.setOpCode(Bool_timed_sampleSample_OPERATION);
        std::copy(input_data, input_data + length, output_data);
      }
      // Output 4
      if (output_4.isConnected()) {
        output_data = output_4.sample().data().data();
        output_4.sample().data().resize(length);
        output_4.setOpCode(Bool_timed_sampleSample_OPERATION);
        std::copy(input_data, input_data + length, output_data);
      }
      return RCC_ADVANCE;
    } else if (input.opCode() == Bool_timed_sampleTime_OPERATION) {
      // Pass through time opcode and time data
      output_1.setOpCode(Bool_timed_sampleTime_OPERATION);
      output_1.time().fraction() = input.time().fraction();
      output_1.time().seconds() = input.time().seconds();
      if (output_2.isConnected()) {
        output_2.setOpCode(Bool_timed_sampleTime_OPERATION);
        output_2.time().fraction() = input.time().fraction();
        output_2.time().seconds() = input.time().seconds();
      }
      if (output_3.isConnected()) {
        output_3.setOpCode(Bool_timed_sampleTime_OPERATION);
        output_3.time().fraction() = input.time().fraction();
        output_3.time().seconds() = input.time().seconds();
      }
      if (output_4.isConnected()) {
        output_4.setOpCode(Bool_timed_sampleTime_OPERATION);
        output_4.time().fraction() = input.time().fraction();
        output_4.time().seconds() = input.time().seconds();
      }
      return RCC_ADVANCE;
    } else if (input.opCode() == Bool_timed_sampleSample_interval_OPERATION) {
      // Pass through sample interval opcode and sample interval data
      output_1.setOpCode(Bool_timed_sampleSample_interval_OPERATION);
      output_1.sample_interval().fraction() =
          input.sample_interval().fraction();
      output_1.sample_interval().seconds() = input.sample_interval().seconds();
      if (output_2.isConnected()) {
        output_2.setOpCode(Bool_timed_sampleSample_interval_OPERATION);
        output_2.sample_interval().fraction() =
            input.sample_interval().fraction();
        output_2.sample_interval().seconds() =
            input.sample_interval().seconds();
      }
      if (output_3.isConnected()) {
        output_3.setOpCode(Bool_timed_sampleSample_interval_OPERATION);
        output_3.sample_interval().fraction() =
            input.sample_interval().fraction();
        output_3.sample_interval().seconds() =
            input.sample_interval().seconds();
      }
      if (output_4.isConnected()) {
        output_4.setOpCode(Bool_timed_sampleSample_interval_OPERATION);
        output_4.sample_interval().fraction() =
            input.sample_interval().fraction();
        output_4.sample_interval().seconds() =
            input.sample_interval().seconds();
      }
      return RCC_ADVANCE;
    } else if (input.opCode() == Bool_timed_sampleFlush_OPERATION) {
      // Pass through flush opcode
      output_1.setOpCode(Bool_timed_sampleFlush_OPERATION);
      if (output_2.isConnected()) {
        output_2.setOpCode(Bool_timed_sampleFlush_OPERATION);
      }
      if (output_3.isConnected()) {
        output_3.setOpCode(Bool_timed_sampleFlush_OPERATION);
      }
      if (output_4.isConnected()) {
        output_4.setOpCode(Bool_timed_sampleFlush_OPERATION);
      }
      return RCC_ADVANCE;
    } else if (input.opCode() == Bool_timed_sampleDiscontinuity_OPERATION) {
      // Pass through discontinuity opcode
      output_1.setOpCode(Bool_timed_sampleDiscontinuity_OPERATION);
      if (output_2.isConnected()) {
        output_2.setOpCode(Bool_timed_sampleDiscontinuity_OPERATION);
      }
      if (output_3.isConnected()) {
        output_3.setOpCode(Bool_timed_sampleDiscontinuity_OPERATION);
      }
      if (output_4.isConnected()) {
        output_4.setOpCode(Bool_timed_sampleDiscontinuity_OPERATION);
      }
      return RCC_ADVANCE;
    } else if (input.opCode() == Bool_timed_sampleMetadata_OPERATION) {
      // Pass through metadata opcode, id, and data
      output_1.setOpCode(Bool_timed_sampleMetadata_OPERATION);
      output_1.metadata().id() = input.metadata().id();
      output_1.metadata().value() = input.metadata().value();
      if (output_2.isConnected()) {
        output_2.setOpCode(Bool_timed_sampleMetadata_OPERATION);
        output_2.metadata().id() = input.metadata().id();
        output_2.metadata().value() = input.metadata().value();
      }
      if (output_3.isConnected()) {
        output_3.setOpCode(Bool_timed_sampleMetadata_OPERATION);
        output_3.metadata().id() = input.metadata().id();
        output_3.metadata().value() = input.metadata().value();
      }
      if (output_4.isConnected()) {
        output_4.setOpCode(Bool_timed_sampleMetadata_OPERATION);
        output_4.metadata().id() = input.metadata().id();
        output_4.metadata().value() = input.metadata().value();
      }
      return RCC_ADVANCE;
    } else {
      setError("Unknown OpCode received");
      return RCC_FATAL;
    }
  }
};
SPLITTER_B_START_INFO
// Insert any static info assignments here (memSize, memSizes, portInfo)
// e.g.: info.memSize = sizeof(MyMemoryStruct);
SPLITTER_B_END_INFO
