-- HDL Implementation of an interface throttle.
--
-- This file is protected by Copyright. Please refer to the COPYRIGHT file
-- distributed with this source distribution.
--
-- This file is part of OpenCPI <http://www.opencpi.org>
--
-- OpenCPI is free software: you can redistribute it and/or modify it under the
-- terms of the GNU Lesser General Public License as published by the Free
-- Software Foundation, either version 3 of the License, or (at your option) any
-- later version.
--
-- OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
-- WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
-- A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
-- more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library ocpi;
use ocpi.types.all;  -- remove this to avoid all ocpi name collisions

architecture rtl of worker is

  constant counter_full_scale_c : std_logic_vector(props_in.step_size'length - 1 downto 0) := (others => '1');

  signal counter   : unsigned(props_in.step_size'length - 1 downto 0);
  signal counter_r : unsigned(props_in.step_size'length - 1 downto 0);
  signal throttle  : std_logic;
  signal take      : std_logic;
  signal zero_pad  : std_logic;

begin

  take <= throttle and output_in.ready
          when props_in.enable = '1' and input_in.opcode = bool_timed_sample_sample_op_e
          else output_in.ready;

  input_out.take <= take;

  zero_pad <= '1' when (props_in.zero_pad_when_not_ready = '1'
                        and input_in.ready = '0' and output_in.ready = '1'
                        and throttle = '1')
              else '0';

  phase_accum_p : process(ctl_in.clk)
  begin
    if rising_edge(ctl_in.clk) then
      if ctl_in.reset = '1' then
        counter <= (others => '0');
      else
        counter <= counter + props_in.step_size;
      end if;
      counter_r <= counter;
    end if;
  end process;

  throttle <= '1' when (counter_r > counter
                        or props_in.step_size = unsigned(counter_full_scale_c))
              else '0';

  -- Give when both the input and output are ready
  output_out.give <= input_in.ready and take;

  -- Pass input data through when input ready, else zero pad
  output_out.data <= input_in.data when zero_pad = '0'
                     else (others => '0');

  -- Output data is always valid when zero padding, else output data is
  -- valid when the input data is valid.
  output_out.valid <= input_in.valid and take
                      when zero_pad = '0' or output_in.reset = '1'
                      else '1';

  -- Pass through input EOM
  output_out.eom <= input_in.eom;

  -- Pass through opcode and byte enable signal when not zero padding.
  output_out.byte_enable <= input_in.byte_enable when zero_pad = '0'
                            else (others => '1');
  output_out.opcode <= input_in.opcode when zero_pad = '0'
                       else bool_timed_sample_sample_op_e;

end rtl;
