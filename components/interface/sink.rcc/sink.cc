// Sink implementation.
//
// This file is protected by Copyright. Please refer to the COPYRIGHT file
// distributed with this source distribution.
//
// This file is part of OpenCPI <http://www.opencpi.org>
//
// OpenCPI is free software: you can redistribute it and/or modify it under the
// terms of the GNU Lesser General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option) any
// later version.
//
// OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
// more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#include "sink-worker.hh"

using namespace OCPI::RCC;
using namespace SinkWorkerTypes;

class SinkWorker : public SinkWorkerBase {
  RCCResult run(bool) {
    if (properties().take_data) {
      if (input.eof()) {
        return RCC_ADVANCE_FINISHED;
      } else {
        return RCC_ADVANCE;
      }
    } else {
      return RCC_OK;
    }
  }
};

SINK_START_INFO

SINK_END_INFO
