#!/usr/bin/env python3

# Generates the input binary file for metadata inserter testing
#
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
# more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

"""OpenCPI generator for metadata_inserter tests."""

import os

import opencpi.ocpi_protocols as ocpi_protocols
import opencpi.ocpi_testing as ocpi_testing

arguments = ocpi_testing.get_generate_arguments()

subcase = os.environ["OCPI_TEST_subcase"]
metadata_id = int(os.environ["OCPI_TEST_metadata_id"])
metadata_value = int(os.environ["OCPI_TEST_metadata_value"])
increment_value = os.environ["OCPI_TEST_increment_value"].upper() == "TRUE"
insert_metadata_message = (os.environ["OCPI_TEST_insert_metadata_message"]
                           .upper() == "TRUE")


port_width = int(os.environ["OCPI_TEST_port_width"])

seed = ocpi_testing.get_test_seed(arguments.case, subcase, port_width,
                                  metadata_id, metadata_value, increment_value,
                                  insert_metadata_message)

if port_width == 8:
    protocol = "uchar_timed_sample"
    generator = ocpi_testing.generator.UnsignedCharacterGenerator()
elif port_width == 16:
    protocol = "ushort_timed_sample"
    generator = ocpi_testing.generator.UnsignedShortGenerator()
elif port_width == 32:
    protocol = "ulong_timed_sample"
    generator = ocpi_testing.generator.UnsignedLongGenerator()
elif port_width == 64:
    protocol = "ulonglong_timed_sample"
    generator = ocpi_testing.generator.UnsignedLongLongGenerator()
else:
    raise ValueError(f"Unsupported port length: {port_width}")

# Increase number of messages
generator.SOAK_ALL_OPCODE_AVERAGE_NUMBER_OF_MESSAGES *= 8

# Generate the test data messages
messages = generator.generate(seed, arguments.case, subcase,
                              arguments.case_number, arguments.subcase_number)
with ocpi_protocols.WriteMessagesFile(
        arguments.save_path, protocol) as file_id:
    file_id.write_dict_messages(messages)
