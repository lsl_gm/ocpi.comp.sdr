#!/usr/bin/env python3

# Python implementation of square wave generator for testing
#
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
# more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

"""Python implementation of square wave generator for testing."""

import math
import numpy as np

import opencpi.ocpi_testing as ocpi_testing


class SquareWaveGenerator(ocpi_testing.Implementation):
    """A component to generate a square wave signal."""

    def __init__(self, enable, message_length,
                 on_samples, off_samples,
                 on_amplitude, off_amplitude,
                 discontinuity_on_amplitude_change,
                 discontinuity_on_cycle_change):
        """Initialise the square wave generator class.

        Args:
            enable (bool): When false the component output is disabled.
            message_length (int): Output message length.
            on_samples (int):     Number of samples in the on portion of a cycle.
            off_samples (int):    Number of samples in the off portion of a cycle.
            on_amplitude (int):   Amplitude of the output when it is set to on.
            off_amplitude (int):  Amplitude of the output when it is set to off.
            discontinuity_on_amplitude_change (bool): Whether to send a
                                  discontinuity message at the point the
                                  amplitude changes at the output.
            discontinuity_on_cycle_change (bool): Whether to send a
                                  discontinuity message at the point
                                  ``on_samples`` or ``off_samples``
                                  changes at the output.
        """
        super().__init__(
            enable=enable, message_length=message_length,
            on_samples=on_samples, off_samples=off_samples,
            on_amplitude=on_amplitude, off_amplitude=off_amplitude,
            discontinuity_on_amplitude_change=discontinuity_on_amplitude_change,
            discontinuity_on_cycle_change=discontinuity_on_cycle_change)

        self.on_amplitude = np.int32(self.on_amplitude)
        self.off_amplitude = np.int32(self.off_amplitude)

        self.input_ports = []

    def reset(self):
        """Reset the state to the same state as at initialisation.

        There is no state so this function is a no-op, but it is required by
        the ocpi_testing.Implementation base class.
        """
        pass

    def sample(self, *args):
        """Handle incoming Sample opcode.

        Raises:
            TypeError: Square wave generator does not have any input ports.
        """
        raise TypeError("Square wave generator does not have any input ports")

    def time(self, *args):
        """Handle incoming Time opcode.

        Raises:
            TypeError: Carrier generator does not have any input ports.
        """
        raise TypeError("Square wave generator does not have any input ports")

    def sample_interval(self, *args):
        """Handle incoming Sample Interval opcode.

        Raises:
            TypeError: Carrier generator does not have any input ports.
        """
        raise TypeError("Square wave generator does not have any input ports")

    def flush(self, *args):
        """Handle incoming Flush opcode.

        Raises:
            TypeError: Carrier generator does not have any input ports.
        """
        raise TypeError("Square wave generator does not have any input ports")

    def discontinuity(self, *args):
        """Handle incoming Discontinuity opcode.

        Raises:
            TypeError: Carrier generator does not have any input ports.
        """
        raise TypeError("Square wave generator does not have any input ports")

    def metadata(self, *args):
        """Handle incoming Metadata opcode.

        Raises:
            TypeError: Carrier generator does not have any input ports.
        """
        raise TypeError("Square wave generator does not have any input ports")

    def generate(self, total_output_length):
        """Generate output samples.

        Args:
            total_output_length (int): Number of output samples to generate.

        Returns:
            Formatted messages
        """
        if self.enable:

            output = np.zeros(total_output_length, dtype=np.int32)
            counter = 0

            # Set output values
            for sample in range(total_output_length):
                if counter < self.on_samples:
                    output[sample] = self.on_amplitude
                    counter += 1
                elif self.on_samples <= counter < (
                        self.on_samples + self.off_samples):
                    output[sample] = self.off_amplitude
                    counter += 1
                else:
                    output[sample] = self.on_amplitude
                    counter = 1

            # Split output data into message sizes
            number_of_messages = math.ceil(len(output) / self.message_length)
            message_data = [[]] * number_of_messages
            for index in range(number_of_messages):
                message_data[index] = output[index * self.message_length:
                                             (index + 1) * self.message_length]

            return self.output_formatter([
                {"opcode": "sample", "data": data} for data in message_data])

        else:
            return self.output_formatter([])
