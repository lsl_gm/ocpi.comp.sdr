#!/usr/bin/env python3

# Python implementation of pseudo-random bit sequence generator for testing
#
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
# more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

"""Python implementation of pseudo-random bit sequence generator for testing."""
import math

import scipy.signal

import opencpi.ocpi_testing as ocpi_testing


class PrbsGenerator(ocpi_testing.Implementation):
    """A Python pseudo-random bit sequence generator implementation."""

    def __init__(self, enable, invert_output, initial_seed, polynomial_taps):
        """Initialise the PRBS class.

        Args:
            enable (bool): When true the PRBS generator is enabled.
            invert_output (bool): When true the output is inverted.
            initial_seed (int): Initial seed for the PRBS generator.
            polynomial_taps (list): Taps of PRBS polynomial.
        """
        self.shift_register_length = polynomial_taps[0]

        # Convert taps from Fibonacci LFSR to Galois LFSR
        polynomial_taps = [(self.shift_register_length - tap) for
                           tap in polynomial_taps[1:]]

        # Limit initial seed to values supported by polynomial taps
        initial_seed_limit = initial_seed & (
            2**(self.shift_register_length) - 1)

        # Convert initial seed to binary string and extend to width of linear
        # feedback shift register.
        format_string = "{:0" + str(self.shift_register_length) + "b}"
        initial_seed_binary = format_string.format(initial_seed_limit)
        # Convert to integer list
        if invert_output:
            initial_seed = [1 - int(bit) for bit in initial_seed_binary]
        else:
            initial_seed = [int(bit) for bit in initial_seed_binary]

        if (not invert_output and initial_seed_limit == 0) or \
                (invert_output and
                 initial_seed_limit == (2**(self.shift_register_length) - 1)):
            self.blank_output = True
        else:
            self.blank_output = False

        self._message_size = 16384

        super().__init__(enable=enable, invert_output=invert_output,
                         initial_seed=initial_seed,
                         polynomial_taps=polynomial_taps)
        self.input_ports = []

    def reset(self):
        """Reset the state to the same state as at initialisation.

        There is no state so this function is a no-op, but it is required by
        the ocpi_testing.Implementation base class.
        """
        pass

    def sample(self, *args):
        """Handle incoming Sample opcode.

        Raises:
            TypeError: Carrier generator does not have any input ports.
        """
        raise TypeError("Pseudo-random binary sequence generator does not " +
                        "have any input ports")

    def time(self, *args):
        """Handle incoming Time opcode.

        Raises:
            TypeError: Carrier generator does not have any input ports.
        """
        raise TypeError("Pseudo-random binary sequence generator does not " +
                        "have any input ports")

    def sample_interval(self, *args):
        """Handle incoming Sample Interval opcode.

        Raises:
            TypeError: Carrier generator does not have any input ports.
        """
        raise TypeError("Pseudo-random binary sequence generator does not " +
                        "have any input ports")

    def flush(self, *args):
        """Handle incoming Flush opcode.

        Raises:
            TypeError: Carrier generator does not have any input ports.
        """
        raise TypeError("Pseudo-random binary sequence generator does not " +
                        "have any input ports")

    def discontinuity(self, *args):
        """Handle incoming Discontinuity opcode.

        Raises:
            TypeError: Carrier generator does not have any input ports.
        """
        raise TypeError("Pseudo-random binary sequence generator does not  " +
                        "have any input ports")

    def metadata(self, *args):
        """Handle incoming Metadata opcode.

        Raises:
            TypeError: Carrier generator does not have any input ports.
        """
        raise TypeError("Pseudo-random binary sequence generator does not " +
                        "have any input ports")

    def generate(self, total_output_length):
        """Generate output samples.

        Args:
            total_output_length (int): Number of output samples to generate.

        Returns:
            Formatted messages
        """
        if self.enable:
            if self.blank_output:
                if self.invert_output:
                    output = [True] * total_output_length
                else:
                    output = [False] * total_output_length

            else:
                # Get PRBS sequence
                binary_sequence = scipy.signal.max_len_seq(
                    nbits=self.shift_register_length, state=self.initial_seed,
                    length=total_output_length, taps=self.polynomial_taps)[0]

                # Convert to boolean list
                if self.invert_output:
                    output = [not bool(bit) for bit in binary_sequence]
                else:
                    output = [bool(bit) for bit in binary_sequence]

            # Split output data into message sizes
            number_of_messages = math.ceil(len(output) / self._message_size)
            message_data = [[]] * number_of_messages
            for index in range(number_of_messages):
                message_data[index] = output[index * self._message_size:
                                             (index + 1) * self._message_size]

            return self.output_formatter([
                {"opcode": "sample", "data": data} for data in message_data])

        else:
            return self.output_formatter([])
