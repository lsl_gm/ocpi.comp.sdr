-- HDL Implementation of constant unsigned long generator
--
-- This file is protected by Copyright. Please refer to the COPYRIGHT file
-- distributed with this source distribution.
--
-- This file is part of OpenCPI <http://www.opencpi.org>
--
-- OpenCPI is free software: you can redistribute it and/or modify it under the
-- terms of the GNU Lesser General Public License as published by the Free
-- Software Foundation, either version 3 of the License, or (at your option) any
-- later version.
--
-- OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
-- WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
-- A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
-- more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;

library ocpi;
use ocpi.types.all;
library sdr_interface;
use sdr_interface.sdr_interface.protocol_interface_generator_v2;

architecture rtl of worker is

  constant delay_c        : integer := 1;
  constant opcode_width_c : integer := integer(ceil(log2(real(ulong_timed_sample_opcode_t'pos(ulong_timed_sample_opcode_t'high)))));

  function slv_to_opcode(inslv : in std_logic_vector(opcode_width_c - 1 downto 0)) return ulong_timed_sample_opcode_t is
  begin
    return ulong_timed_sample_opcode_t'val(to_integer(unsigned(inslv)));
  end function;

  -- Generator signals
  signal discontinuity_trigger   : std_logic;
  signal output_interface_opcode : std_logic_vector(opcode_width_c - 1 downto 0);
  signal output_data             : std_logic_vector(output_out.data'range);

begin

  discontinuity_trigger <= props_in.discontinuity_on_value_change and
                           props_in.value_written;

  -- Instantiate generator
  interface_generator_i : protocol_interface_generator_v2
    generic map (
      delay_g                 => delay_c,
      data_width_g            => output_out.data'length,
      opcode_width_g          => opcode_width_c,
      byte_enable_width_g     => output_out.byte_enable'length,
      processed_data_opcode_g => "000",
      discontinuity_opcode_g  => "100"
      )
    port map (
      clk                   => ctl_in.clk,
      reset                 => ctl_in.reset,
      output_ready          => output_in.ready,
      -- High when discontinuity event occurs
      discontinuity_trigger => discontinuity_trigger,
      generator_enable      => props_in.enable,
      generator_reset       => ctl_in.reset,
      -- Connect output from data processing module
      processed_stream_in   => std_logic_vector(props_in.value),
      -- Specifies the length that the output message should be
      message_length        => props_in.message_length,
      -- High when processing module should be disabled.
      output_hold           => open,
      -- Output interface signals
      output_som            => output_out.som,
      output_eom            => output_out.eom,
      output_valid          => output_out.valid,
      output_give           => output_out.give,
      output_byte_enable    => output_out.byte_enable,
      output_opcode         => output_interface_opcode,
      output_data           => output_data
      );

  output_out.opcode <= slv_to_opcode(output_interface_opcode);
  output_out.data   <= output_data;

end rtl;
