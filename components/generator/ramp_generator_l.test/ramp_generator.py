#!/usr/bin/env python3

# Python implementation of ramp generator for testing
#
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
# more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

"""Python implementation of ramp generator for testing."""

import math
import numpy as np

import opencpi.ocpi_testing as ocpi_testing


class RampGenerator(ocpi_testing.Implementation):
    """Component to generate a ramping sawtooth signal."""

    def __init__(self, enable, message_length,
                 step_size,
                 start_amplitude, stop_amplitude,
                 discontinuity_on_amplitude_change,
                 discontinuity_on_step_size_change):
        """Initialise the ramp generator class.

        Args:
            enable (bool): When false the component output is disabled.
            message_length (int): Output message length.
            step_size (int): Amount the amplitude changes between each sample.
            start_amplitude (int): Amplitude value at which the sawtooth starts.
            stop_amplitude (int): Amplitude value at which the sawtooth stops.
            discontinuity_on_amplitude_change (bool): Controls if a discontinuity
                    is sent when the start/stop amplitude changes.
            discontinuity_on_step_size_change (bool): Control if a discontinuity
                    is sent when the step_size changes.
        """
        super().__init__(
            enable=enable, message_length=message_length,
            step_size=step_size,
            start_amplitude=start_amplitude, stop_amplitude=stop_amplitude,
            discontinuity_on_amplitude_change=discontinuity_on_amplitude_change,
            discontinuity_on_step_size_change=discontinuity_on_step_size_change)

        self.start_amplitude = np.int32(self.start_amplitude)
        self.stop_amplitude = np.int32(self.stop_amplitude)
        self.step_size = np.int32(self.step_size)

        self.count = self.start_amplitude

        self.input_ports = []

    def reset(self):
        """Reset the state to the same state as at initialisation."""
        self.count = self.start_amplitude

    def sample(self, *args):
        """Handle incoming Sample opcode.

        Raises:
            TypeError: Carrier generator does not have any input ports.
        """
        raise TypeError("Ramp generator does not have any input ports")

    def time(self, *args):
        """Handle incoming Time opcode.

        Raises:
            TypeError: Carrier generator does not have any input ports.
        """
        raise TypeError("Ramp generator does not have any input ports")

    def sample_interval(self, *args):
        """Handle incoming Sample Interval opcode.

        Raises:
            TypeError: Carrier generator does not have any input ports.
        """
        raise TypeError("Ramp generator does not have any input ports")

    def flush(self, *args):
        """Handle incoming Flush opcode.

        Raises:
            TypeError: Carrier generator does not have any input ports.
        """
        raise TypeError("Ramp generator does not have any input ports")

    def discontinuity(self, *args):
        """Handle incoming Discontinuity opcode.

        Raises:
            TypeError: Carrier generator does not have any input ports.
        """
        raise TypeError("Ramp generator does not have any input ports")

    def metadata(self, *args):
        """Handle incoming Metadata opcode.

        Raises:
            TypeError: Carrier generator does not have any input ports.
        """
        raise TypeError("Ramp generator does not have any input ports")

    def generate(self, total_output_length):
        """Generate output samples.

        Args:
            total_output_length (int): Number of output samples to generate.

        Returns:
            Formatted messages
        """
        if self.enable:

            output = np.zeros(total_output_length, dtype=np.int32)

            # Set output values
            for sample in range(total_output_length):
                output[sample] = self.count
                next_count = np.int32(self.count + self.step_size)
                if self.step_size >= 0:
                    if self.count >= self.stop_amplitude:
                        self.count = self.start_amplitude
                    else:
                        self.count = next_count
                else:
                    if self.count <= self.stop_amplitude:
                        self.count = self.start_amplitude
                    else:
                        self.count = next_count

            # Split output data into message sizes
            number_of_messages = math.ceil(len(output) / self.message_length)
            message_data = [[]] * number_of_messages
            for index in range(number_of_messages):
                message_data[index] = output[index * self.message_length:
                                             (index + 1) * self.message_length]

            return self.output_formatter([
                {"opcode": "sample", "data": data} for data in message_data])

        else:
            return self.output_formatter([])
