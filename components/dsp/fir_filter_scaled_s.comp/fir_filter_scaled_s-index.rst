.. fir_filter_scaled_s documentation

.. This file is protected by Copyright. Please refer to the COPYRIGHT file
   distributed with this source distribution.

   This file is part of OpenCPI <http://www.opencpi.org>

   OpenCPI is free software: you can redistribute it and/or modify it under the
   terms of the GNU Lesser General Public License as published by the Free
   Software Foundation, either version 3 of the License, or (at your option) any
   later version.

   OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>.


.. _fir_filter_scaled_s:


Finite Impulse Response (FIR) Filter (scaled) (``fir_filter_scaled_s``)
=======================================================================
Applies finite impulse response (FIR) filter to a stream of values, with a output scale factor to prevent overflows.

Design
------
Implements an FIR filter and then right shifts the output by a set amount. This right shift allows the gain of the filter to be compensated for by any power-of-2. A half-up rounder is used to minimise the DC bias due to truncation.

The mathematical representation of the implementation is given in :eq:`fir_filter_scaled_s-equation`.

.. math::
   :label: fir_filter_scaled_s-equation

   y[n] = \text{round}\left (\frac {\sum_{i=0}^{T-1} b_i * x[n-i]}{2^{N}} \right )

In :eq:`fir_filter_scaled_s-equation`:

 * :math:`x[n]` is the input values.

 * :math:`y[n]` is the output values.

 * :math:`b_i` is the taps values, where there are :math:`T` taps index from :math:`0` to :math:`T - 1`.

 * :math:`N` is the value of the output scale factor.

A block diagram representation of the implementation is given in :numref:`fir_filter_scaled_s-diagram`.

.. _fir_filter_scaled_s-diagram:

.. figure:: fir_filter_scaled_s.svg
   :alt: Block diagram outlining finite impulse response (fir) filter (scaled) implementation.
   :align: center

   FIR filter implementation. :math:`b_i` is the taps values. :math:`x[n]` is the input data. :math:`y[n]` is the output data.

Interface
---------
.. literalinclude:: ../specs/fir_filter_scaled_s-spec.xml
   :language: xml
   :lines: 1,19-

Ports
~~~~~
.. ocpi_documentation_ports::

   input: Primary input samples port.
   output: Primary output samples port.

Opcode Handling
~~~~~~~~~~~~~~~
The FIR filter is applied to values in sample opcode messages.

When a flush opcode message is received then the filter buffer is flushed. Here the flush behaviour is to give an input of zeros of the length set in the ``flush_length`` property. After the flush has been completed and the resulting sample opcode message sent, the flush message is then forwarded on. When ``flush_length`` is set to zero no sample opcode message will be outputted and the flush message will be passed through without effect.

All other opcodes are passed through this component.

Properties
~~~~~~~~~~
.. ocpi_documentation_properties::

   scale_factor: Setting to :math:`\left \lceil log_{2}\left ( \sum_{k=0}^{k=\texttt{number_of_taps} - 1} \texttt{taps}[k] \right ) \right \rceil` will achieve a DC gain through the FIR filter of between 0.5 and 1. This property must be set within 0 and :math:`16 + \lceil log_{2}\left(\texttt{number_of_taps} \right) \rceil`.

   flush_length: When set to zero no zeros are fed into the filter and flush opcode messages are passed through.

Implementations
---------------
.. ocpi_documentation_implementations:: ../fir_filter_scaled_s.hdl ../fir_filter_scaled_s.rcc

Example Application
-------------------
.. literalinclude:: example_app.xml
   :language: xml
   :lines: 1,19-

Dependencies
------------
The dependencies to other elements in OpenCPI are:

 * :ref:`FIR filter primitive <fir_filter-primitive>`

 * :ref:`Halfup rounding primitive <rounding_halfup-primitive>`

 * :ref:`Protocol interface delay primitive v2 <protocol_interface_delay_v2-primitive>`

 * :ref:`Flush inserter primitive v2 <flush_inserter_v2-primitive>`

There is also a dependency on:

 * ``ieee.std_logic_1164``

 * ``ieee.numeric_std``

 * ``ieee.math_real``

Limitations
-----------
Limitations of ``fir_filter_scaled_s`` are:

 * The HDL implementation does not take advantage of the resource savings possible when the FIR taps are symmetrical.

 * The HDL implementation uses a pipelined adder tree when possible, however if the number of serial adders becomes large other FIR filter implementations are more suitable as the adder tree will use significant FPGA resources.

Testing
-------
.. ocpi_documentation_test_platforms::

.. ocpi_documentation_test_result_summary::
