-- Scaled FIR Filter
--
-- This file is protected by Copyright. Please refer to the COPYRIGHT file
-- distributed with this source distribution.
--
-- This file is part of OpenCPI <http://www.opencpi.org>
--
-- OpenCPI is free software: you can redistribute it and/or modify it under the
-- terms of the GNU Lesser General Public License as published by the Free
-- Software Foundation, either version 3 of the License, or (at your option) any
-- later version.
--
-- OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
-- WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
-- A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
-- more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;
library ocpi;
use ocpi.types.all;
library sdr_dsp;
use sdr_dsp.fir_signed_array_pkg.all;
use sdr_dsp.sdr_dsp.fir_filter;
use sdr_dsp.sdr_dsp.rounding_halfup;

architecture rtl of worker is

  -- Component constants
  constant num_taps_c             : natural := to_integer(number_of_taps);
  constant num_multipliers_c      : natural := to_integer(number_of_multipliers);
  constant data_in_width_c        : integer := input_in.data'length;
  constant fir_out_width_c        : integer := data_in_width_c + pkg_coefficient_width + integer(ceil(log2(real(num_taps_c))));
  constant data_out_width_c       : integer := output_out.data'length;
  -- Delay constants
  constant fir_delay_c            : integer := integer(ceil(real(num_taps_c) / real(num_multipliers_c)));
  constant mac_delay_c            : integer := 2;
  constant num_serial_adders_c    : integer := integer(ceil(real(num_multipliers_c) / real(fir_delay_c)));
  constant serial_adder_delay_c   : integer := integer(ceil(real(num_multipliers_c) / real(num_serial_adders_c)));
  constant adder_tree_delay_c     : integer := integer(ceil(log2(real(num_serial_adders_c))));
  constant rounder_delay_c        : integer := 1;
  constant delay_c                : integer := fir_delay_c + mac_delay_c + serial_adder_delay_c + adder_tree_delay_c + rounder_delay_c;
  -- Input interface signals
  signal take                     : std_logic;
  signal data_in                  : signed(data_in_width_c - 1 downto 0);
  signal fir_ready                : std_logic;
  signal input_valid              : std_logic;
  signal input_interface          : worker_input_in_t;
  -- FIR signals
  signal fir_data_out             : signed(fir_out_width_c - 1 downto 0);
  signal rounded_fir_data_out     : signed(data_out_width_c - 1 downto 0);
  signal rounded_fir_data_out_slv : std_logic_vector(data_out_width_c - 1 downto 0);
  -- Scale factor signals
  signal scale_factor             : unsigned(integer(ceil(log2(real(fir_out_width_c - data_out_width_c)))) - 1 downto 0);
  signal scale_factor_int         : integer range 0 to (fir_out_width_c - data_out_width_c);

begin

  ------------------------------------------------------------------------------
  -- Flush inject
  ------------------------------------------------------------------------------
  -- Insert flush directly into input data stream.
  -- input_interface is used instead of input_in in rest of component.
  -- Take signal is from when input_interface.ready = '1' and output port ready.
  flush_insert_i : entity work.short_flush_injector
    generic map (
      data_in_width_g      => input_in.data'length,
      max_message_length_g => to_integer(ocpi_max_bytes_output)/2
      )
    port map (
      clk             => ctl_in.clk,
      reset           => ctl_in.reset,
      take_in         => take,
      input_in        => input_in,
      flush_length    => props_in.flush_length,
      input_out       => input_out,
      input_interface => input_interface
      );

  ------------------------------------------------------------------------------
  -- Interface delay
  ------------------------------------------------------------------------------
  take        <= output_in.ready and fir_ready;
  input_valid <= '1' when input_interface.valid = '1' and input_interface.opcode = short_timed_sample_sample_op_e else '0';
  data_in     <= signed(input_interface.data(data_in_width_c - 1 downto 0));

  interface_delay_i : entity work.short_protocol_delay
    generic map (
      delay_g              => delay_c,
      data_in_width_g      => input_in.data'length,
      processed_data_mux_g => '1'
      )
    port map (
      clk                 => ctl_in.clk,
      reset               => ctl_in.reset,
      enable              => output_in.ready,
      take_in             => fir_ready,
      input_in            => input_interface,
      processed_stream_in => rounded_fir_data_out_slv,
      output_out          => output_out
      );

  ------------------------------------------------------------------------------
  -- FIR Primitive
  ------------------------------------------------------------------------------
  fir_filter_real_i : fir_filter
    generic map(
      data_in_width_g   => data_in_width_c,
      num_taps_g        => num_taps_c,
      num_multipliers_g => num_multipliers_c
      )
    port map(
      clk            => ctl_in.clk,
      rst            => ctl_in.reset,
      enable         => output_in.ready,
      data_in_valid  => input_valid,
      take           => fir_ready,
      taps_in        => fir_signed_array(props_in.taps(0 to num_taps_c - 1)),
      data_in        => data_in,
      data_out       => fir_data_out,
      data_valid_out => open
      );

  ------------------------------------------------------------------------------
  -- Output scaling
  ------------------------------------------------------------------------------
  scale_factor     <= props_in.scale_factor(scale_factor'length - 1 downto 0);
  scale_factor_int <= to_integer(scale_factor);

  halfup_rounder_i : rounding_halfup
    generic map (
      input_width_g   => fir_out_width_c,
      output_width_g  => data_out_width_c,
      saturation_en_g => false
      )
    port map(
      clk            => ctl_in.clk,
      reset          => ctl_in.reset,
      clk_en         => output_in.ready,
      data_in        => fir_data_out,
      data_valid_in  => '0',
      binary_point   => scale_factor_int,
      data_out       => rounded_fir_data_out,
      data_valid_out => open
      );

  rounded_fir_data_out_slv <= std_logic_vector(rounded_fir_data_out);

end rtl;
