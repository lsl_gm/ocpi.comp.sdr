<?xml version="1.0"?>
<!-- This file is protected by Copyright. Please refer to the COPYRIGHT file
     distributed with this source distribution.

     This file is part of OpenCPI <http://www.opencpi.org>

     OpenCPI is free software: you can redistribute it and/or modify it under
     the terms of the GNU Lesser General Public License as published by the Free
     Software Foundation, either version 3 of the License, or (at your option)
     any later version.

     OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
     WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
     FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
     more details.

     You should have received a copy of the GNU Lesser General Public License
     along with this program. If not, see <http://www.gnu.org/licenses/>. -->
<tests usehdlfileio="true">
  <property test="true" name="subcase" type="string"/>
  <property test="true" name="taps_shape" type="string"/>
  <!-- Case 00: Run typical test case-->
  <case>
    <input port="input" script="generate.py --case typical" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="decimation_factor" value="8"/>
    <property name="taps_per_branch" value="8"/>
    <property name="tap_array_length" value="64"/>
    <property name="rounding_type" value="half_even"/>
    <property name="group_delay_seconds" value="8"/>
    <property name="group_delay_fractional" value="0x1f9add3739000000"/>
    <property name="taps" generate="generate_taps.py short 15"/>
    <property name="polyphase_decimator_xs.hdl.max_decimation" value="8"/>
    <property name="polyphase_decimator_xs.hdl.max_taps_per_branch" value="8"/>
    <property name="polyphase_decimator_xs.hdl.num_multipliers" value="1"/>
    <property name="taps_shape" value="kaiser"/>
  </case>
  <!-- Case 01: Property: Decimation factor (also max_decimation) -->
  <case>
    <input port="input" script="generate.py --case property" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="decimation_factor" values="1,30,255"/>
    <property name="taps_per_branch" value="8"/>
    <property name="tap_array_length" value="2040"/>
    <property name="rounding_type" value="half_even"/>
    <property name="group_delay_seconds" value="8"/>
    <property name="group_delay_fractional" value="0x1f9add3739000000"/>
    <property name="taps" generate="generate_taps.py short 15"/>
    <!-- Only *sim is required, otherwise RCC workers are not tested -->
    <property name="polyphase_decimator_xs.hdl.max_decimation" value="255" only="*sim"/>
    <property name="polyphase_decimator_xs.hdl.max_taps_per_branch" value="8"/>
    <property name="polyphase_decimator_xs.hdl.num_multipliers" value="1"/>
    <property name="taps_shape" value="kaiser"/>
  </case>
  <!-- Case 02: Property: Taps per branch (also max_taps_per_branch) -->
  <case>
    <input port="input" script="generate.py --case property" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="decimation_factor" value="8"/>
    <property name="taps_per_branch" values="1,3,30,255"/>
    <property name="tap_array_length" value="2040"/>
    <property name="rounding_type" value="half_even"/>
    <property name="group_delay_seconds" value="8"/>
    <property name="group_delay_fractional" value="0x1f9add3739000000"/>
    <property name="taps" generate="generate_taps.py short 15"/>
    <property name="polyphase_decimator_xs.hdl.max_decimation" value="8"/>
    <!-- Only *sim is required, otherwise RCC workers are not tested -->
    <property name="polyphase_decimator_xs.hdl.max_taps_per_branch" value="255" only="*sim"/>
    <property name="polyphase_decimator_xs.hdl.num_multipliers" value="1"/>
    <property name="taps_shape" value="kaiser"/>
  </case>
  <!-- Case 03: Property: Tap array length -->
  <case>
    <input port="input" script="generate.py --case property" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="decimation_factor" value="8"/>
    <property name="taps_per_branch" value="8"/>
    <property name="tap_array_length" values="64,2040,65535"/>
    <property name="rounding_type" value="half_even"/>
    <property name="group_delay_seconds" value="8"/>
    <property name="group_delay_fractional" value="0x1f9add3739000000"/>
    <property name="taps" generate="generate_taps.py short 15"/>
    <property name="polyphase_decimator_xs.hdl.max_decimation" value="8"/>
    <property name="polyphase_decimator_xs.hdl.max_taps_per_branch" value="8"/>
    <property name="polyphase_decimator_xs.hdl.num_multipliers" value="1"/>
    <property name="taps_shape" value="kaiser"/>
  </case>
  <!-- Case 04: Property: Rounding Type -->
  <case>
    <input port="input" script="generate.py --case property" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="decimation_factor" value="8"/>
    <property name="taps_per_branch" value="8"/>
    <property name="tap_array_length" value="64"/>
    <property name="rounding_type" values="truncate,half_even,half_up"/>
    <property name="group_delay_seconds" value="8"/>
    <property name="group_delay_fractional" value="0x1f9add3739000000"/>
    <property name="taps" generate="generate_taps.py short 15"/>
    <property name="polyphase_decimator_xs.hdl.max_decimation" value="8"/>
    <property name="polyphase_decimator_xs.hdl.max_taps_per_branch" value="8"/>
    <property name="polyphase_decimator_xs.hdl.num_multipliers" value="1"/>
    <property name="taps_shape" value="kaiser"/>
  </case>
  <!-- Case 05: Property: Group delay -->
  <case>
    <input port="input" script="generate.py --case property" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="decimation_factor" value="8"/>
    <property name="taps_per_branch" value="8"/>
    <property name="tap_array_length" value="64"/>
    <property name="rounding_type" value="half_even"/>
    <property name="group_delay_seconds" values="0,1,0xffffffff"/>
    <property name="group_delay_fractional" values="0,0x123456789,0xffffffffffffffff"/>
    <property name="taps" generate="generate_taps.py short 15"/>
    <property name="polyphase_decimator_xs.hdl.max_decimation" value="8"/>
    <property name="polyphase_decimator_xs.hdl.max_taps_per_branch" value="8"/>
    <property name="polyphase_decimator_xs.hdl.num_multipliers" value="1"/>
    <property name="taps_shape" value="kaiser"/>
    <property name="subcase" value="group_delay"/>
  </case>
  <!-- Case 06: Property: Taps -->
  <case>
    <input port="input" script="generate.py --case property" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="decimation_factor" value="8"/>
    <property name="taps_per_branch" value="8"/>
    <property name="tap_array_length" value="64"/>
    <property name="rounding_type" value="half_even"/>
    <property name="group_delay_seconds" value="8"/>
    <property name="group_delay_fractional" value="0x1f9add3739000000"/>
    <property name="taps" generate="generate_taps.py short 15"/>
    <property name="polyphase_decimator_xs.hdl.max_decimation" value="8"/>
    <property name="polyphase_decimator_xs.hdl.max_taps_per_branch" value="8"/>
    <property name="polyphase_decimator_xs.hdl.num_multipliers" value="1"/>
    <property name="taps_shape" values="all_ones,random"/>
  </case>
  <!-- Case 07: Property: num_multipliers -->
  <case>
    <input port="input" script="generate.py --case property" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="decimation_factor" value="8"/>
    <property name="taps_per_branch" value="8"/>
    <property name="tap_array_length" value="64"/>
    <property name="rounding_type" value="half_even"/>
    <property name="group_delay_seconds" values="0,1,0xffffffff"/>
    <property name="group_delay_fractional" values="0,0x123456789,0xffffffffffffffff"/>
    <property name="taps" generate="generate_taps.py short 15"/>
    <property name="polyphase_decimator_xs.hdl.max_decimation" value="8"/>
    <property name="polyphase_decimator_xs.hdl.max_taps_per_branch" value="8"/>
    <property name="polyphase_decimator_xs.hdl.num_multipliers" value="4"/>
    <property name="taps_shape" value="kaiser"/>
  </case>
  <!-- Case 08: Extreme sample values -->
  <case>
    <input port="input" script="generate.py --case sample" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="decimation_factor" value="8"/>
    <property name="taps_per_branch" value="8"/>
    <property name="tap_array_length" value="64"/>
    <property name="rounding_type" value="half_even"/>
    <property name="group_delay_seconds" value="8"/>
    <property name="group_delay_fractional" value="0x1f9add3739000000"/>
    <property name="taps" generate="generate_taps.py short 15"/>
    <property name="polyphase_decimator_xs.hdl.max_decimation" value="8"/>
    <property name="polyphase_decimator_xs.hdl.max_taps_per_branch" value="8"/>
    <property name="polyphase_decimator_xs.hdl.num_multipliers" value="1"/>
    <property name="taps_shape" value="kaiser"/>
    <property name="subcase"
      values="all_zero,all_maximum,all_minimum,real_zero,imaginary_zero,large_positive,large_negative,near_zero"/>
  </case>
  <!-- Case 09: Stress input port -->
  <case>
    <input port="input" script="generate.py --case input_stressing" messagesinfile="true" messagesize="16384" stressormode="full"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="decimation_factor" value="8"/>
    <property name="taps_per_branch" value="8"/>
    <property name="tap_array_length" value="64"/>
    <property name="rounding_type" value="half_even"/>
    <property name="group_delay_seconds" value="8"/>
    <property name="group_delay_fractional" value="0x1f9add3739000000"/>
    <property name="taps" generate="generate_taps.py short 15"/>
    <property name="polyphase_decimator_xs.hdl.max_decimation" value="8"/>
    <property name="polyphase_decimator_xs.hdl.max_taps_per_branch" value="8"/>
    <property name="polyphase_decimator_xs.hdl.num_multipliers" value="1"/>
    <property name="taps_shape" value="kaiser"/>
  </case>
  <!-- Case 10: Message lengths -->
  <case>
    <input port="input" script="generate.py --case message_size" messagesinfile="true" messagesize="16384" stressormode="full"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="decimation_factor" value="8"/>
    <property name="taps_per_branch" value="8"/>
    <property name="tap_array_length" value="64"/>
    <property name="rounding_type" value="half_even"/>
    <property name="group_delay_seconds" value="8"/>
    <property name="group_delay_fractional" value="0x1f9add3739000000"/>
    <property name="taps" generate="generate_taps.py short 15"/>
    <property name="polyphase_decimator_xs.hdl.max_decimation" value="8"/>
    <property name="polyphase_decimator_xs.hdl.max_taps_per_branch" value="8"/>
    <property name="polyphase_decimator_xs.hdl.num_multipliers" value="1"/>
    <property name="taps_shape" value="kaiser"/>
    <property name="subcase" values="shortest,longest,different_sizes"/>
  </case>
  <!-- Case 11: Time -->
  <case>
    <input port="input" script="generate.py --case time" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="decimation_factor" value="8"/>
    <property name="taps_per_branch" value="8"/>
    <property name="tap_array_length" value="64"/>
    <property name="rounding_type" value="half_even"/>
    <property name="group_delay_seconds" value="8"/>
    <property name="group_delay_fractional" value="0x1f9add3739000000"/>
    <property name="taps" generate="generate_taps.py short 15"/>
    <property name="polyphase_decimator_xs.hdl.max_decimation" value="8"/>
    <property name="polyphase_decimator_xs.hdl.max_taps_per_branch" value="8"/>
    <property name="polyphase_decimator_xs.hdl.num_multipliers" value="1"/>
    <property name="taps_shape" value="kaiser"/>
    <property name="subcase" values="zero,positive,maximum,consecutive,intra_consecutive,intra_multiple,eof"/>
  </case>
  <!-- Case 12: Sample interval -->
  <case>
    <input port="input" script="generate.py --case sample_interval" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="decimation_factor" value="8"/>
    <property name="taps_per_branch" value="8"/>
    <property name="tap_array_length" value="64"/>
    <property name="rounding_type" value="half_even"/>
    <property name="group_delay_seconds" value="8"/>
    <property name="group_delay_fractional" value="0x1f9add3739000000"/>
    <property name="taps" generate="generate_taps.py short 15"/>
    <property name="polyphase_decimator_xs.hdl.max_decimation" value="8"/>
    <property name="polyphase_decimator_xs.hdl.max_taps_per_branch" value="8"/>
    <property name="polyphase_decimator_xs.hdl.num_multipliers" value="1"/>
    <property name="taps_shape" value="kaiser"/>
    <property name="subcase" values="zero,positive,maximum,consecutive,intra_consecutive,intra_multiple,eof"/>
  </case>
  <!-- Case 13: Flush -->
  <case>
    <input port="input" script="generate.py --case flush" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="decimation_factor" value="8"/>
    <property name="taps_per_branch" value="8"/>
    <property name="tap_array_length" value="64"/>
    <property name="rounding_type" value="half_even"/>
    <property name="group_delay_seconds" value="8"/>
    <property name="group_delay_fractional" value="0x1f9add3739000000"/>
    <property name="taps" generate="generate_taps.py short 15"/>
    <property name="polyphase_decimator_xs.hdl.max_decimation" value="8"/>
    <property name="polyphase_decimator_xs.hdl.max_taps_per_branch" value="8"/>
    <property name="polyphase_decimator_xs.hdl.num_multipliers" value="1"/>
    <property name="taps_shape" value="kaiser"/>
    <property name="subcase" values="single,consecutive,intra_consecutive,intra_multiple,eof"/>
  </case>
  <!-- Case 14: Discontinuity -->
  <case>
    <input port="input" script="generate.py --case discontinuity" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="decimation_factor" value="8"/>
    <property name="taps_per_branch" value="8"/>
    <property name="tap_array_length" value="64"/>
    <property name="rounding_type" value="half_even"/>
    <property name="group_delay_seconds" value="8"/>
    <property name="group_delay_fractional" value="0x1f9add3739000000"/>
    <property name="taps" generate="generate_taps.py short 15"/>
    <property name="polyphase_decimator_xs.hdl.max_decimation" value="8"/>
    <property name="polyphase_decimator_xs.hdl.max_taps_per_branch" value="8"/>
    <property name="polyphase_decimator_xs.hdl.num_multipliers" value="1"/>
    <property name="taps_shape" value="kaiser"/>
    <property name="subcase" values="single,consecutive,intra_consecutive,intra_multiple,eof"/>
  </case>
  <!-- Case 15: Metadata -->
  <case>
    <input port="input" script="generate.py --case metadata" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="decimation_factor" value="8"/>
    <property name="taps_per_branch" value="8"/>
    <property name="tap_array_length" value="64"/>
    <property name="rounding_type" value="half_even"/>
    <property name="group_delay_seconds" value="8"/>
    <property name="group_delay_fractional" value="0x1f9add3739000000"/>
    <property name="taps" generate="generate_taps.py short 15"/>
    <property name="polyphase_decimator_xs.hdl.max_decimation" value="8"/>
    <property name="polyphase_decimator_xs.hdl.max_taps_per_branch" value="8"/>
    <property name="polyphase_decimator_xs.hdl.num_multipliers" value="1"/>
    <property name="taps_shape" value="kaiser"/>
    <property name="subcase" values="positive,consecutive,intra_consecutive,intra_multiple"/>
  </case>
  <!-- Case 16: Time Calculations-->
  <case>
    <input port="input" script="generate.py --case time_calculation" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="decimation_factor" value="8"/>
    <property name="taps_per_branch" value="8"/>
    <property name="tap_array_length" value="64"/>
    <property name="rounding_type" value="half_even"/>
    <property name="group_delay_seconds" value="8"/>
    <property name="group_delay_fractional" value="0x1f9add3739000000"/>
    <property name="taps" generate="generate_taps.py short 15"/>
    <property name="polyphase_decimator_xs.hdl.max_decimation" value="8"/>
    <property name="polyphase_decimator_xs.hdl.max_taps_per_branch" value="8"/>
    <property name="polyphase_decimator_xs.hdl.num_multipliers" value="1"/>
    <property name="taps_shape" value="kaiser"/>
    <property name="subcase" values="interval_change,time_change,minimum_increment,intra_consecutive,intra_multiple"/>
  </case>
  <!-- Case 17: Opcode interactions-->
  <case>
    <input port="input" script="generate.py --case opcode_interaction" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="decimation_factor" value="8"/>
    <property name="taps_per_branch" value="8"/>
    <property name="tap_array_length" value="64"/>
    <property name="rounding_type" value="half_even"/>
    <property name="group_delay_seconds" value="8"/>
    <property name="group_delay_fractional" value="0x1f9add3739000000"/>
    <property name="taps" generate="generate_taps.py short 15"/>
    <property name="polyphase_decimator_xs.hdl.max_decimation" value="8"/>
    <property name="polyphase_decimator_xs.hdl.max_taps_per_branch" value="8"/>
    <property name="polyphase_decimator_xs.hdl.num_multipliers" value="1"/>
    <property name="taps_shape" value="kaiser"/>
    <property name="subcase" values="time_flush,time_discontinuity,time_metadata"/>
  </case>
  <!-- Case 18: Soak test -->
  <case>
    <input port="input" script="generate.py --case soak" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="decimation_factor" values="2,30,255"/>
    <property name="taps_per_branch" values="3,7"/>
    <property name="tap_array_length" value="2040"/>
    <property name="rounding_type" values="truncate,half_even,half_up"/>
    <property name="group_delay_seconds" value="8"/>
    <property name="group_delay_fractional" value="0x1f9add3739000000"/>
    <property name="taps" generate="generate_taps.py short 15"/>
    <!-- Only *sim is required, otherwise RCC workers are not tested -->
    <property name="polyphase_decimator_xs.hdl.max_decimation" value="255" only="*sim"/>
    <property name="polyphase_decimator_xs.hdl.max_taps_per_branch" value="8"/>
    <property name="polyphase_decimator_xs.hdl.num_multipliers" values="1,4"/>
    <property name="taps_shape" value="kaiser"/>
    <property name="subcase" values="sample_only,all_opcodes"/>
  </case>
  <!-- EOF -->
</tests>
