#!/usr/bin/env python3

# Unit tests of the python implementation of Polyphase Decimator
#
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
# more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

"""Unit tests of the python implementation of Polyphase Decimator model."""

import decimal
import pytest
import random
import numpy as np
import scipy.signal as sig
import matplotlib.pyplot as plt

from polyphase_decimator import PolyphaseDecimator_XS

show_plots = False

decimal.getcontext().prec = 64


def split_group_delay(group_delay):
    """Function to split a Decimal into integer seconds and fractional parts.

    Args:
        group_delay (Decimal): Group delay to split.

    Returns:
        (int,int): Seconds and Fractional parts.
    """
    group_delay_seconds = int(group_delay)
    group_delay_fractional = int((group_delay -
                                  decimal.Decimal(group_delay_seconds)) *
                                 decimal.Decimal(2**64))
    return group_delay_seconds, group_delay_fractional


@pytest.fixture(params=[2, 10])
def decimation_factor(request):
    """Fixture to return decimation factors to be tested."""
    return request.param


@pytest.fixture(params=[0, 1, 2, 3])
def tap_offset(request):
    """Fixture to return tap offset used by tests."""
    return request.param


@pytest.fixture(params=[9, 30])
def taps(request, decimation_factor, tap_offset):
    """Fixture to return taps used by tests."""
    num_taps = request.param * decimation_factor
    t = sig.firwin(num_taps+tap_offset, 1.0/decimation_factor)
    return t


@pytest.fixture()
def input_signal():
    """Create some samples to interpolate then decimate in the tests."""
    sample_length = 1024
    vals = [0, 1, 0, 0, 0,
            complex(4, 1), 0, 0, 0, 0,
            complex(0, -1), 0, 0, complex(0, 2), 0]
    signal = np.fft.fft(vals, sample_length, axis=0)
    return signal / max(abs(signal))


@pytest.fixture(params=[20.0, "0.001", 2**12])
def start_time(request):
    """Fixture to return initial time used by tests."""
    return decimal.Decimal(request.param)


def check_samples(tap_length, decimation_factor, allowed_error, taps,
                  input_signal, interpolated_data, decimated_data,
                  decimated_data_compare=[]):
    """Helper function to check sample values are correct."""
    # tap_offset of 0 or 1, to correct for taps not being a
    # multiple of decimation rate.
    tap_offset = 1
    if (len(taps) % decimation_factor) != 0:
        tap_offset = 0

    if show_plots:
        plt.figure("1: {}, {}".format(tap_length, decimation_factor))
        plt.plot((tap_length) + np.arange(0, len(input_signal), 1),
                 np.real(input_signal), "b--")
        plt.plot((tap_length/2) + np.arange(0, len(interpolated_data))
                 / (decimation_factor), np.real(interpolated_data), "g-x")
        if len(decimated_data_compare) > 0:
            plt.plot(np.arange(0, len(decimated_data_compare)),
                     np.real(decimated_data_compare), "m-o")
        plt.plot(tap_offset + np.arange(0, len(decimated_data)),
                 np.real(decimated_data), "r-x")

    print("Input Samples: {}\nSample Interp: {}\nSample Decim:  {}"
          .format(input_signal[tap_length:tap_length+10],
                  interpolated_data[
              tap_length * decimation_factor:(tap_length
                                              * decimation_factor) + 10],
                  decimated_data[(2*tap_length)-1:2*tap_length+10]))

    # Skip over any startup filter transient (as we are comparing the original
    # signal to the interpolated -> decimated), there is will some start up,
    # and some filter delay present.
    # (offset tap_length, for transients,
    #  offset by another tap_length-tap_offset for filter delay).
    offset_dec_data = decimated_data[(
        2*tap_length)-tap_offset:len(input_signal)]
    error = [np.abs(a-b) for a, b in zip(
             offset_dec_data, input_signal[tap_length:len(input_signal)])]

    if show_plots:
        plt.figure("2: {}, {}".format(tap_length, decimation_factor))
        plt.plot(error)
        plt.show()

    for n, e in enumerate(error):
        if e >= allowed_error:
            print("error at index {}".format(n))
        assert(e <= allowed_error)


def test_pd_init():
    """Test that the fixtures accept the base parameters."""
    pd = PolyphaseDecimator_XS(decimation_factor=2, taps=[0, 1, 2, 3])
    assert(pd._decimation_factor == 2)


def test_time_without_sample_interval(input_signal):
    """Test a time message is forwarded unchanged."""
    pd = PolyphaseDecimator_XS(
        decimation_factor=4,
        taps=[n/10.0 for n in range(8)])
    # Send a time when there is no data present.
    val = pd.time(decimal.Decimal("22.1234"))
    # Expect time to be sent through..
    assert(len(val) == 1 and len(val[0]) == 1)
    assert(val[0][0]["opcode"] == "time")
    assert(abs(val[0][0]["data"] - decimal.Decimal("22.1234")) < 0.000001)

    # send an incomplete down sample.
    val = pd.sample([0])
    assert(len(val) == 0 or len(val[0]) == 0)
    # send another time, which should not be forwarded.
    val = pd.time(decimal.Decimal("22.1244"))
    assert(len(val) == 0 or len(val[0]) == 0)
    # send enough data
    if isinstance(pd, PolyphaseDecimator_XS):
        val = pd.sample([int(2**14*i.real) for i in input_signal])
    else:
        val = pd.sample(input_signal)
    assert(len(val[0]) >= 3)
    # Expect the first output sample, then time, the  remaining samples
    assert(val[0][0]["opcode"] == "sample")
    assert(len(val[0][0]["data"]) == 1)
    assert(val[0][1]["opcode"] == "time")
    assert(abs(val[0][1]["data"] - decimal.Decimal("22.1244")) < 0.000001)
    assert(val[0][2]["opcode"] == "sample")


def test_time_with_sample_interval_part_buffer1(start_time, input_signal):
    """Test a time message is forwarded unchanged."""
    decimation_factor = 5
    tap_per_br = 8
    group_delay = decimal.Decimal("1.5")
    input_si = decimal.Decimal("0.1")

    group_delay_seconds, group_delay_fractional = split_group_delay(
        group_delay)
    pd = PolyphaseDecimator_XS(
        decimation_factor=decimation_factor,
        taps=[n/10.0 for n in range(decimation_factor*tap_per_br)],
        group_delay_seconds=group_delay_seconds,
        group_delay_fractional=group_delay_fractional)
    # Send a sample interval when there is no data present.
    val = pd.sample_interval(input_si)
    assert(len(val[0]) == 1)
    assert(val[0][0]["opcode"] == "sample_interval")
    assert(val[0][0]["data"] == input_si*decimation_factor)

    # Send part buffer
    val = pd.sample([1]*2)
    assert(len(val[0]) == 0)

    # Send time after part buffer
    val = pd.time(start_time)
    assert(len(val[0]) == 0)

    # Send full buffer, expect sample and then time
    val = pd.sample([1]*(decimation_factor))
    assert(len(val[0]) == 2)
    assert(val[0][0]["opcode"] == "sample")
    assert(len(val[0][0]["data"]) == 1)
    assert(val[0][1]["opcode"] == "time")
    expected_time = (start_time-group_delay+(3*input_si))
    if expected_time < 0:
        expected_time += ((decimal.Decimal(2)**96) / (decimal.Decimal(2)**64))
    assert(abs(val[0][1]["data"] - expected_time) < 0.000001)

    # Complete the part buffer, expect sample
    val = pd.sample([1]*(3))
    assert(len(val[0]) == 1)
    assert(val[0][0]["opcode"] == "sample")
    assert(len(val[0][0]["data"]) == 1)


def test_time_with_sample_interval_part_buffer2(input_signal):
    """Test a time message is forwarded unchanged."""
    decimation_factor = 5
    tap_per_br = 8
    group_delay = decimal.Decimal("2.3")
    input_si = decimal.Decimal("0.1")
    start_time = decimal.Decimal("20.0")

    group_delay_seconds, group_delay_fractional = split_group_delay(
        group_delay)
    pd = PolyphaseDecimator_XS(
        decimation_factor=decimation_factor,
        taps=[n/10.0 for n in range(decimation_factor*tap_per_br)],
        group_delay_seconds=group_delay_seconds,
        group_delay_fractional=group_delay_fractional)
    # Send a sample interval when there is no data present.
    val = pd.sample_interval(input_si)
    assert(len(val[0]) == 1)
    assert(val[0][0]["opcode"] == "sample_interval")
    assert(val[0][0]["data"] == input_si*decimation_factor)

    # Send part buffer
    val = pd.sample([1]*1)
    assert(len(val[0]) == 0)

    # Send time after part buffer
    val = pd.time(start_time)
    assert(len(val[0]) == 0)

    # Send several full buffers, expect sample, time, samples
    val = pd.sample([1]*(decimation_factor*4))
    assert(len(val[0]) == 3)
    assert(val[0][0]["opcode"] == "sample")
    assert(len(val[0][0]["data"]) == 1)
    assert(val[0][1]["opcode"] == "time")
    assert(abs(val[0][1]["data"] - (start_time -
                                    group_delay+(4*input_si))) < 0.000001)
    assert(val[0][2]["opcode"] == "sample")
    assert(len(val[0][2]["data"]) == 3)


def test_time_with_sample_interval_part_buffer3(input_signal):
    """Test a time message is forwarded unchanged."""
    decimation_factor = 5
    tap_per_br = 8
    group_delay = decimal.Decimal((decimation_factor*tap_per_br)//2)
    input_si = decimal.Decimal("0.1")
    start_time = decimal.Decimal("20.0")

    group_delay_seconds, group_delay_fractional = split_group_delay(
        group_delay)
    pd = PolyphaseDecimator_XS(
        decimation_factor=decimation_factor,
        taps=[n/10.0 for n in range(decimation_factor*tap_per_br)],
        group_delay_seconds=group_delay_seconds,
        group_delay_fractional=group_delay_fractional)
    # Send a sample interval when there is no data present.
    val = pd.sample_interval(input_si)
    assert(len(val[0]) == 1)
    assert(val[0][0]["opcode"] == "sample_interval")
    assert(val[0][0]["data"] == input_si*decimation_factor)

    # Send a full buffer
    val = pd.sample([1]*decimation_factor)
    assert(len(val[0]) == 1)
    assert(val[0][0]["opcode"] == "sample")
    assert(len(val[0][0]["data"]) == 1)

    # Send a time, expect time to come out.
    val = pd.time(start_time)
    assert(len(val[0]) == 1)
    assert(val[0][0]["opcode"] == "time")
    assert(abs(val[0][0]["data"] - (start_time - group_delay)) < 0.000001)

    # Send 1 less than a full buffer and expect no output
    val = pd.sample([1]*(decimation_factor-1))
    assert(len(val[0]) == 0)

    # Send last sample, expect 1 sample only
    val = pd.sample([1]*1)
    assert(len(val[0]) == 1)
    assert(val[0][0]["opcode"] == "sample")
    assert(len(val[0][0]["data"]) == 1)


def test_time_with_overlapping_times(input_signal):
    """Test multiple times mid decimation result in only one output time."""
    decimation_factor = 5
    tap_per_br = 8
    group_delay = decimal.Decimal("0.8")
    input_si = decimal.Decimal("0.1")
    start_time = decimal.Decimal("20.0")
    second_time = decimal.Decimal("400.0")

    group_delay_seconds, group_delay_fractional = split_group_delay(
        group_delay)
    pd = PolyphaseDecimator_XS(
        decimation_factor=decimation_factor,
        taps=[n/10.0 for n in range(decimation_factor*tap_per_br)],
        group_delay_seconds=group_delay_seconds,
        group_delay_fractional=group_delay_fractional)
    # Send a SI when there is no data present.
    val = pd.sample_interval(input_si)
    assert(len(val[0]) == 1)
    assert(val[0][0]["opcode"] == "sample_interval")
    assert(val[0][0]["data"] == input_si*decimation_factor)

    # Send part buffer
    val = pd.sample([1]*2)
    assert(len(val[0]) == 0)

    # Send time, expect no output
    val = pd.time(start_time)
    assert(len(val[0]) == 0)

    # send part buffer, expect no output
    val = pd.sample([1]*2)
    assert(len(val[0]) == 0)

    # Send time, expect no output
    val = pd.time(second_time)
    assert(len(val[0]) == 0)

    # Send buffer, expect sample and 1st time
    val = pd.sample([1]*(decimation_factor))
    assert(val[0][0]["opcode"] == "sample")
    assert(len(val[0][0]["data"]) == 1)
    assert(val[0][1]["opcode"] == "time")
    assert(abs(val[0][1]["data"] - (second_time -
                                    group_delay+(input_si))) < 0.000001)
    assert(len(val[0]) == 2)

    # Send multiple buffer, expect no time
    val = pd.sample([1]*(12*decimation_factor))
    assert(len(val[0]) == 1)
    assert(val[0][0]["opcode"] == "sample")
    assert(len(val[0][0]["data"]) == 12)


def test_sample_interval(input_signal):
    """Test that the sample interval is altered to the output sample rate."""
    pd = PolyphaseDecimator_XS(
        decimation_factor=4,
        taps=[n/10.0 for n in range(8)])
    val = pd.sample_interval(decimal.Decimal("0.1"))
    assert(val[0][0]["opcode"] == "sample_interval")
    assert(abs(val[0][0]["data"] - decimal.Decimal("0.4")) < 0.000001)
    val = pd.sample([0])
    val = pd.sample_interval(decimal.Decimal("0.4"))
    if isinstance(pd, PolyphaseDecimator_XS):
        val = pd.sample([int(2**14*i.real) for i in input_signal])
    else:
        val = pd.sample(input_signal)
    assert(val[0][0]["opcode"] == "sample")
    assert(len(val) == 1 and len(val[0]) == 1)


def test_flush(input_signal):
    """Test that a flush leads to data being sent before if needed.

    If mid decimation, the flush will push out sample data before the
    flush itself is passed through.
    """
    decimation_factor = 4
    pd = PolyphaseDecimator_XS(
        decimation_factor=decimation_factor,
        taps=[n/10.0 for n in range(16)])
    val = pd.flush(0)
    assert(val[0][0]["opcode"] == "flush")

    input_reals = [int(2**14*i.real) for i in input_signal]
    if (len(input_reals) % decimation_factor) == 0:
        # Append to input signal to make it not a multiple of decimation factor
        input_reals.append(0)

    val = pd.sample(input_reals)
    assert(val[0][0]["opcode"] == "sample")
    assert(len(val[0][0]["data"]) == (len(input_reals)//decimation_factor))

    val = pd.flush(0)
    assert(len(val[0]) == 2)  # Expect sample and flush opcodes
    assert(val[0][0]["opcode"] == "sample")
    assert(val[0][1]["opcode"] == "flush")
    assert(len(val[0][0]["data"]) == decimation_factor)

    # Trim input to be an exact multiple of decimation factor
    input_reals = input_reals[:2 * decimation_factor]

    val = pd.sample(input_reals)
    assert(val[0][0]["opcode"] == "sample")
    assert(len(val[0][0]["data"]) == (len(input_reals)//decimation_factor))

    val = pd.flush(0)
    assert(len(val[0]) == 2)  # Expect sample and flush opcodes
    assert(val[0][0]["opcode"] == "sample")
    assert(val[0][1]["opcode"] == "flush")
    assert(len(val[0][0]["data"]) == decimation_factor)

    # Second flush with no data
    val = pd.flush(0)
    assert(len(val[0]) == 1)  # Expect no sample, only flush opcode
    assert(val[0][0]["opcode"] == "flush")
