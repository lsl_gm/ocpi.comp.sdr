#!/usr/bin/env python3

# Generates the input binary file for moving average filter testing
#
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
# more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

"""Generates the input binary file for moving average filter testing."""

import os
import random
import logging
import decimal

import opencpi.ocpi_protocols as ocpi_protocols
import opencpi.ocpi_testing as ocpi_testing
from ocpi_block_testing.generator.base_block_generator import BaseBlockGenerator
from ocpi_block_testing.generator.complex_short_block_generator import ComplexShortBlockGenerator
from ocpi_block_testing.get_generate_arguments import get_generate_arguments


log_level = int(os.environ.get("OCPI_LOG_LEVEL", 1))
if log_level > 9:
    verify_log_level = "DEBUG"
elif log_level > 5:
    verify_log_level = "INFO"
else:
    verify_log_level = "WARNING"
logging.basicConfig(level=verify_log_level)

arguments = get_generate_arguments(generator=BaseBlockGenerator)

taps_per_branch = int(os.environ["OCPI_TEST_taps_per_branch"])
decimation_factor = int(os.environ["OCPI_TEST_decimation_factor"])
taps = [int(tap) for tap in os.environ.get("OCPI_TEST_taps").split(",")]
subcase = os.environ["OCPI_TEST_subcase"]

group_delay_seconds = int(os.environ["OCPI_TEST_group_delay_seconds"])
group_delay_fractional = int(os.environ["OCPI_TEST_group_delay_fractional"])
decimal.getcontext().prec = 50
group_delay = (decimal.Decimal(group_delay_seconds)
               + (decimal.Decimal(group_delay_fractional)
                  / (decimal.Decimal(2) ** 64)))


seed = ocpi_testing.get_test_seed(arguments.case, subcase,
                                  arguments.case_number, arguments.subcase_number,
                                  taps_per_branch, decimation_factor, taps,
                                  group_delay_seconds, group_delay_fractional)

generator = ComplexShortBlockGenerator(block_length=decimation_factor)

# Sample Interval will be multiplied by decimation factor in component
# so reduce maximum value to prevent overflow.
generator.SAMPLE_INTERVAL_MAX //= decimation_factor

# Generate the test data messages
messages = generator.generate(seed, arguments.case, subcase,
                              arguments.case_number, arguments.subcase_number)
with ocpi_protocols.WriteMessagesFile(arguments.save_path, "complex_short_timed_sample") as file_id:
    file_id.write_dict_messages(messages)
