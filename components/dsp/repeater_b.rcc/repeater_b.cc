// Repeater RCC worker.
//
// This file is protected by Copyright. Please refer to the COPYRIGHT file
// distributed with this source distribution.
//
// This file is part of OpenCPI <http://www.opencpi.org>
//
// OpenCPI is free software: you can redistribute it and/or modify it under the
// terms of the GNU Lesser General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option) any
// later version.
//
// OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
// more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#include "repeater_b-worker.hh"
#include <vector>

using namespace OCPI::RCC;  // for easy access to RCC data types and constants
using namespace Repeater_bWorkerTypes;

class Repeater_bWorker : public Repeater_bWorkerBase {
  size_t index_start = 0;
  std::vector<bool> buf;
  uint8_t samples = 0;

  RCCResult samples_written() {
    samples = properties().samples;
    return RCC_OK;
  }

  RCCResult run(bool) {
    if (input.opCode() == Bool_timed_sampleSample_OPERATION) {
      size_t length = input.sample().data().size();
      const bool *inData =
          reinterpret_cast<const bool *>(input.sample().data().data());
      bool *outData = reinterpret_cast<bool *>(output.sample().data().data());
      output.setOpCode(Bool_timed_sampleSample_OPERATION);

      // Determine if new message
      if (index_start == 0) {
        size_t total_samples = length + samples * length;
        buf.resize(total_samples);
        size_t index = 0;
        while (index < total_samples) {
          buf[index++] = *inData;
          for (size_t j = 0; j < samples; j++) {
            buf[index + j] = *inData;
          }
          inData++;
          index += samples;
        }
      }

      // Limit the maximum message size
      uint16_t length_limited;
      size_t length_remaining = buf.size() - index_start;
      if (length_remaining >
          (REPEATER_B_OCPI_MAX_BYTES_OUTPUT / sizeof(bool))) {
        length_limited = REPEATER_B_OCPI_MAX_BYTES_OUTPUT / sizeof(bool);
      } else {
        length_limited = length_remaining;
      }

      // Copy data to the output
      size_t index_end = index_start + length_limited;
      std::copy(buf.begin() + index_start, buf.begin() + index_end, outData);
      index_start += length_limited;

      // Determine whether or not all the data is sent this time around
      output.sample().data().resize(length_limited);
      if (index_start != buf.size()) {
        output.advance(length_limited);
        return RCC_OK;
      } else {
        index_start = 0;
      }
      return RCC_ADVANCE;
    } else if (input.opCode() == Bool_timed_sampleTime_OPERATION) {
      // Pass through time opcode and time data
      output.setOpCode(Bool_timed_sampleTime_OPERATION);
      output.time().fraction() = input.time().fraction();
      output.time().seconds() = input.time().seconds();
      return RCC_ADVANCE;
    } else if (input.opCode() == Bool_timed_sampleSample_interval_OPERATION) {
      // Pass through sample interval opcode and sample interval data
      output.setOpCode(Bool_timed_sampleSample_interval_OPERATION);
      output.sample_interval().fraction() = input.sample_interval().fraction();
      output.sample_interval().seconds() = input.sample_interval().seconds();
      return RCC_ADVANCE;
    } else if (input.opCode() == Bool_timed_sampleFlush_OPERATION) {
      // Pass through flush opcode
      output.setOpCode(Bool_timed_sampleFlush_OPERATION);
      return RCC_ADVANCE;
    } else if (input.opCode() == Bool_timed_sampleDiscontinuity_OPERATION) {
      // Pass through discontinuity opcode
      output.setOpCode(Bool_timed_sampleDiscontinuity_OPERATION);
      return RCC_ADVANCE;
    } else if (input.opCode() == Bool_timed_sampleMetadata_OPERATION) {
      // Pass through metadata opcode, id, and data
      output.setOpCode(Bool_timed_sampleMetadata_OPERATION);
      output.metadata().id() = input.metadata().id();
      output.metadata().value() = input.metadata().value();
      return RCC_ADVANCE;
    } else {
      setError("Unknown OpCode Received");
      return RCC_FATAL;
    }
  }
};

REPEATER_B_START_INFO
// Insert any static info assignments here (memSize, memSizes, portInfo)
// e.g.: info.memSize = sizeof(MyMemoryStruct);
REPEATER_B_END_INFO
