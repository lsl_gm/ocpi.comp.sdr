-- Delay module for Complex short Protocol
--
-- This file is protected by Copyright. Please refer to the COPYRIGHT file
-- distributed with this source distribution.
--
-- This file is part of OpenCPI <http://www.opencpi.org>
--
-- OpenCPI is free software: you can redistribute it and/or modify it under the
-- terms of the GNU Lesser General Public License as published by the Free
-- Software Foundation, either version 3 of the License, or (at your option) any
-- later version.
--
-- OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
-- WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
-- A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
-- more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.

-- Creates a variable length delay pipeline for all interface signals in order
-- to align interface signals with a module that processes stream data.
-- delay_g should be set to the delay in clock cycles of the module that
-- processes stream data.

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;

library work;
use work.cic_interpolator_xs_worker_defs.all;

library sdr_interface;
use sdr_interface.sdr_interface.upsample_protocol_interface_delay_v2;

library sdr_math;
use sdr_math.sdr_math.non_restoring_divider;

entity complex_short_protocol_delay is
  generic (
    data_in_width_g      : integer := 32;
    stage1_delay_g       : integer := 1;
    stage2_delay_g       : integer := 1;
    max_message_length_g : integer := 4096
    );
  port (
    clk                  : in  std_logic;
    reset                : in  std_logic;
    enable               : in  std_logic;  -- high when output is ready
    upsample_enable      : in  std_logic;  -- high to progress upsample shift register
    sample_interval_hold : out std_logic;  -- high when data is ignored on input to protocol delay primitive
    pd_discontinuity     : out std_logic;
    discontinuity_hold   : out std_logic;
    input_in             : in  worker_input_in_t;  -- input streaming interface
    interpolation_factor : in  unsigned(15 downto 0);
    processed_stream_in  : in  std_logic_vector(data_in_width_g - 1 downto 0);
    processed_valid_in   : in  std_logic;
    processed_end_in     : in  std_logic;
    output_out           : out worker_output_out_t  -- output streaming interface
    );
end complex_short_protocol_delay;

architecture rtl of complex_short_protocol_delay is
  constant opcode_width_c : integer := integer(ceil(log2(real(
    complex_short_timed_sample_opcode_t'pos(complex_short_timed_sample_opcode_t'high)))));
  constant byte_enable_width_c : integer := integer(ceil(log2(real(input_in.byte_enable'length)))) + 1;

  function opcode_to_slv(inop : in complex_short_timed_sample_opcode_t) return std_logic_vector is
  begin
    return std_logic_vector(to_unsigned(complex_short_timed_sample_opcode_t'pos(inop), opcode_width_c));
  end function;

  function slv_to_opcode(inslv : in std_logic_vector(opcode_width_c - 1 downto 0)) return complex_short_timed_sample_opcode_t is
  begin
    return complex_short_timed_sample_opcode_t'val(to_integer(unsigned(inslv)));
  end function;

  signal pd_discontinuity_i    : std_logic;
  signal reset_i               : std_logic;
  signal discontinuity_disable : std_logic;

  signal input_opcode : std_logic_vector(opcode_width_c - 1 downto 0);

  signal sample_interval_hold_i : std_logic;
  signal divider_ready          : std_logic;
  signal input_si_valid         : std_logic;
  signal output_si_valid        : std_logic;
  signal divider_done           : std_logic;
  signal si_propagate           : std_logic;
  signal si_valid_shift         : std_logic;
  signal si_output              : std_logic;
  signal si_valid_pipe          : std_logic_vector(1 downto 0);

  -- Sample interval used to store the fractional data
  signal sample_interval_r        : std_logic_vector(63 downto 0);
  -- 96 bit + 1 signed bit
  signal sample_interval_input    : signed(96 downto 0);
  signal sample_interval_output   : signed(96 downto 0);
  signal sample_interval_output_r : std_logic_vector(95 downto 0);

  signal interpolation_factor_scaled : signed(96 downto 0);

  signal upsample_pd_out        : worker_input_in_t;
  signal upsample_pd_out_opcode : std_logic_vector(opcode_width_c-1 downto 0);

  signal processed_valid_in_i : std_logic;
  signal processed_end_in_i   : std_logic;
  signal eof_r                : std_logic;

  signal output_give   : std_logic;
  signal output_opcode : std_logic_vector(opcode_width_c - 1 downto 0);

begin

  input_opcode <= opcode_to_slv(input_in.opcode);
  reset_i      <= reset or pd_discontinuity_i;

  up_protocol_delay_i : upsample_protocol_interface_delay_v2
    generic map (
      stage1_delay_g          => stage1_delay_g,
      stage2_delay_g          => stage2_delay_g,
      data_width_g            => data_in_width_g,
      opcode_width_g          => opcode_width_c,
      byte_enable_width_g     => byte_enable_width_c,
      processed_data_mux_g    => '0',
      processed_data_opcode_g => opcode_to_slv(complex_short_timed_sample_sample_op_e)
      )
    port map (
      clk                 => clk,
      reset               => reset_i,
      enable              => enable,
      take_in             => upsample_enable,
      up_sample_factor    => interpolation_factor,
      processed_stream_in => (others => '0'),
      input_som           => input_in.som,
      input_eom           => input_in.eom,
      input_eof           => input_in.eof,
      input_valid         => input_in.valid,
      input_ready         => input_in.ready,
      input_byte_enable   => input_in.byte_enable,
      input_opcode        => input_opcode,
      input_data          => input_in.data,
      output_som          => upsample_pd_out.som,
      output_eom          => upsample_pd_out.eom,
      output_eof          => upsample_pd_out.eof,
      output_valid        => upsample_pd_out.valid,
      output_give         => upsample_pd_out.ready,
      output_byte_enable  => upsample_pd_out.byte_enable,
      output_opcode       => upsample_pd_out_opcode,
      output_data         => upsample_pd_out.data
      );

  -- gate processed_valid_in with enable
  processed_valid_in_i <= processed_valid_in and enable;
  processed_end_in_i   <= upsample_pd_out.eom;

  ------------------------------------------------------------------------------
  -- Sample Interval OpCode Capture
  --
  -- Incoming opcode, when valid, captured by shift reg.
  -- Sized to capture 64-bit fraction: seconds are
  -- captured directly by divider.
  -- Also used to clock the eof into a register
  ------------------------------------------------------------------------------
  sample_interval_in_p : process(clk)
  begin
    if rising_edge(clk) then
      if reset_i = '1' then
        eof_r <= '0';
      else
        if enable = '1' then
          eof_r <= upsample_pd_out.eof;
          if upsample_enable = '1' and input_in.valid = '1' and (sample_interval_hold_i = '0' or divider_done = '0')
            and input_in.opcode = complex_short_timed_sample_sample_interval_op_e then
            sample_interval_r <= input_in.data & sample_interval_r(sample_interval_r'high downto data_in_width_g);
          end if;
        end if;
      end if;
    end if;
  end process;


  ------------------------------------------------------------------------------
  -- Divider
  --
  -- Input validated by end of sample-interval message.
  -- Clock enabled by down-stream component.
  ------------------------------------------------------------------------------

  -- Input control
  input_si_valid <= '1' when ((input_in.opcode = complex_short_timed_sample_sample_interval_op_e)
                              and (input_in.valid = '1') and (input_in.eom = '1') and enable = '1' and (upsample_enable = '1'))
                    else '0';

  -- Port assignments
  interpolation_factor_scaled(interpolation_factor_scaled'high downto (interpolation_factor'high + 1)) <= (others => '0');
  interpolation_factor_scaled(interpolation_factor'range)                                              <= signed(std_logic_vector(interpolation_factor));
  sample_interval_input                                                                                <= '0' & signed(input_in.data) & signed(sample_interval_r);  -- Signed bit always set to '0'

  division_i : non_restoring_divider
    generic map (
      data_width_g => 97,
      method_g     => "floor"
      )
    port map(
      clk            => clk,
      reset          => reset_i,
      clk_en         => enable,
      data_in_valid  => input_si_valid,
      data_in_ready  => divider_ready,
      numerator      => sample_interval_input,
      denominator    => interpolation_factor_scaled,
      data_valid_out => output_si_valid,
      quotient       => sample_interval_output,
      remainder      => open
      );


  ------------------------------------------------------------------------------
  -- Flow Control
  --
  -- The main flow control (sample_interval_hold_i) halts/invalidates
  -- the input from the point that the divider is active, until a
  -- valid sample-interval op-code is output by the delay component.
  ------------------------------------------------------------------------------
  sample_interval_hold_i <= (not divider_ready) or output_si_valid;
  sample_interval_hold   <= sample_interval_hold_i;


  ------------------------------------------------------------------------------
  -- Sample Interval Message Validation
  --
  -- Valid output from the divider component is flagged (divider_done) until a
  -- valid sample-interval op-code is output by the delay component. This
  -- affects flow control via sample_interval_hold_i and sample_interval_hold
  -- externally from the delay component.
  ------------------------------------------------------------------------------
  sample_interval_out_p : process(clk)
  begin
    if rising_edge(clk) then
      if reset_i = '1' then
        divider_done   <= '0';
        si_valid_shift <= '0';
        si_propagate   <= '0';
      elsif enable = '1' then
        -- reset divider signal and check if finished shifting
        if si_propagate = '1' and si_valid_shift = '1' then
          divider_done <= '0';
          if to_integer(unsigned(si_valid_pipe)) = 0 and divider_done = '0' then
            si_propagate   <= '0';
            si_valid_shift <= '0';
          end if;
        end if;
        -- check if si has propagated through upsampler
        if upsample_pd_out.valid = '1' and upsample_pd_out.som = '1' and
          upsample_pd_out_opcode = opcode_to_slv(complex_short_timed_sample_sample_interval_op_e) then
          si_propagate <= '1';
          divider_done <= '0';
        end if;
        -- check if divider is done calculating
        if output_si_valid = '1' then
          divider_done             <= '1';  -- marks first value is valid
          si_valid_shift           <= '1';  -- marks that calculation complete and needs shifting
          sample_interval_output_r <= std_logic_vector(sample_interval_output(95 downto 0));
        end if;
      end if;
    end if;
  end process;

  -- Sample Interval Shift Register Process
  -- Shift the sample interval valid register through when the data is ready
  sample_interval_shift_p : process(clk)
  begin
    if rising_edge(clk) then
      if reset_i = '1' then
        si_valid_pipe <= (others => '0');
      elsif enable = '1' then
        -- Two conditions:
        -- SI already through upsampler and divider has previously or just ended
        -- Divider finished before and SI has just been output from upsampler
        if (si_propagate = '1' and (output_si_valid = '1' or si_valid_shift = '1')) or
          (divider_done = '1' and upsample_pd_out.valid = '1' and upsample_pd_out.som = '1' and
           upsample_pd_out_opcode = opcode_to_slv(complex_short_timed_sample_sample_interval_op_e)) then
          si_valid_pipe <= si_valid_pipe(si_valid_pipe'high-1 downto 0) & divider_done;
        end if;
      end if;
    end if;
  end process;

  si_output <= divider_done when (upsample_pd_out.valid = '1' and upsample_pd_out.som = '1' and
                                  upsample_pd_out_opcode = opcode_to_slv(complex_short_timed_sample_sample_interval_op_e))
               else divider_done and si_propagate and si_valid_shift;

  -- Discontinuity Process
  -- Tracks the progress of a discontinuity opcode through the delays
  -- and triggers a discontinuity reset once output.
  -- It also pauses input flow during the discontinuity opcode processing.
  discontinuity_p : process(clk)
  begin
    if rising_edge(clk) then
      if reset = '1' then
        discontinuity_disable <= '0';
        pd_discontinuity_i    <= '0';
        discontinuity_hold    <= '0';
      else
        if enable = '1' then
          if pd_discontinuity_i = '0' and discontinuity_disable = '0' and
            input_in.ready = '1' and input_in.opcode = complex_short_timed_sample_discontinuity_op_e then
            -- hold take until discontinuity has passed through
            discontinuity_hold <= '1';
          end if;
          if upsample_pd_out.ready = '1' and output_opcode = opcode_to_slv(complex_short_timed_sample_discontinuity_op_e) then
            -- discontinuity output triggers full reset
            pd_discontinuity_i <= '1';
          end if;
          if upsample_pd_out.ready = '1' then
            -- enable new discontinuity once new data has passed through
            discontinuity_disable <= '0';
          end if;
        end if;
        if pd_discontinuity_i = '1' then
          -- allow data flow
          discontinuity_hold    <= '0';
          -- stop reset
          pd_discontinuity_i    <= '0';
          -- disable extra discontinuity
          discontinuity_disable <= '1';
        end if;
      end if;
    end if;
  end process;

  ------------------------------------------------------------------------------
  -- Output Control
  --
  -- MUX for processed data.
  -- Masks sample interval data when passed through upsample delay.
  -- Constructs sample interval opcode with correct sample interval data.
  ------------------------------------------------------------------------------
  output_data_p : process(processed_stream_in, processed_valid_in_i, processed_end_in_i,
                          eof_r, upsample_pd_out_opcode, upsample_pd_out,
                          sample_interval_output_r, si_output, si_valid_pipe)
  begin
    output_opcode          <= upsample_pd_out_opcode;
    output_out.byte_enable <= upsample_pd_out.byte_enable;
    output_give            <= upsample_pd_out.ready;
    output_out.valid       <= upsample_pd_out.valid;
    output_out.data        <= upsample_pd_out.data;
    output_out.som         <= upsample_pd_out.som;
    output_out.eom         <= upsample_pd_out.eom;
    output_out.eof         <= upsample_pd_out.eof;

    if upsample_pd_out_opcode = opcode_to_slv(complex_short_timed_sample_sample_op_e) then
      output_out.data  <= processed_stream_in;
      output_out.valid <= processed_valid_in_i;
      output_out.eom   <= processed_end_in_i;
      output_out.eof   <= eof_r;
    elsif upsample_pd_out_opcode = opcode_to_slv(complex_short_timed_sample_sample_interval_op_e) then
      -- Mask SI when passed through upsample delay
      output_give      <= '0';
      output_out.valid <= '0';
    end if;

    -- Sample interval
    -- Fractional (31:0)
    if si_output = '1' then
      output_opcode    <= opcode_to_slv(complex_short_timed_sample_sample_interval_op_e);
      output_out.som   <= '1';
      output_give      <= '1';
      output_out.valid <= '1';
      output_out.data  <= sample_interval_output_r(31 downto 0);
      output_out.eom   <= '0';
    -- Fractional (63:32)
    elsif (si_valid_pipe(0) = '1') then
      output_opcode    <= opcode_to_slv(complex_short_timed_sample_sample_interval_op_e);
      output_out.som   <= '0';
      output_give      <= '1';
      output_out.valid <= '1';
      output_out.data  <= sample_interval_output_r(63 downto 32);
      output_out.eom   <= '0';
    -- Seconds
    elsif (si_valid_pipe(1) = '1') then
      output_opcode    <= opcode_to_slv(complex_short_timed_sample_sample_interval_op_e);
      output_out.som   <= '0';
      output_give      <= '1';
      output_out.valid <= '1';
      output_out.data  <= sample_interval_output_r(95 downto 64);
      output_out.eom   <= '1';
    end if;
  end process;

  pd_discontinuity  <= pd_discontinuity_i;
  output_out.give   <= output_give and not pd_discontinuity_i;
  output_out.opcode <= slv_to_opcode(output_opcode);

end rtl;
