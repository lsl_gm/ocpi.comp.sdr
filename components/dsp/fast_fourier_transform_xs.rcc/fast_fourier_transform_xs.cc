// RCC implementation of fast_fourier_transform_xs worker.
//
// This file is protected by Copyright. Please refer to the COPYRIGHT file
// distributed with this source distribution.
//
// This file is part of OpenCPI <http://www.opencpi.org>
//
// OpenCPI is free software: you can redistribute it and/or modify it under the
// terms of the GNU Lesser General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option) any
// later version.
//
// OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
// more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#include "fast_fourier_transform_xs-worker.hh"
#include <complex>
#include <cmath>
#include <algorithm>
#include <limits>
#include <queue>
#include <functional>
#include <utility>
#include "OcpiDebugApi.hh"
#include "../../math/common/time_utils.hh"

// LINT EXCEPTION: cpp_005: 1: Define use allowed in include guard
#define USING_KISS_FFT \
  FAST_FOURIER_TRANSFORM_XS_FFT_CORE_IMPLEMENTATION_KISS_FFT

#if USING_KISS_FFT
// LINT EXCEPTION: cpp_008: 1: Include allowed after include guard
#include "gen/kissfft/kiss_fft.h"
#else
// LINT EXCEPTION: cpp_008: 1: Include allowed after include guard
#include "../common/fft/fft_core.hh"
#endif

using namespace OCPI::RCC;  // for easy access to RCC data types and constants
using namespace Fast_fourier_transform_xsWorkerTypes;
using namespace std::placeholders;

class Fast_fourier_transform_xsWorker
    : public Fast_fourier_transform_xsWorkerBase {

#if USING_KISS_FFT
  kiss_fft_cfg kiss_cfg;
#else
  fft_core<RCCFloat> built_in_fft_core;
#endif

  std::vector<std::complex<RCCFloat>> frame_data_out;

  std::vector<std::complex<RCCFloat>> fft_frame;
  std::vector<std::complex<RCCFloat>>::iterator fft_frame_iterator;
  std::vector<Complex_short_timed_sampleSampleData> output_data_frame;
  std::vector<Complex_short_timed_sampleSampleData>::iterator
      output_data_iterator;

  // Internal State
  size_t sample_offset{0};          // The sample we are currently processing.
  size_t output_offset{0};          // The output that is being processed.
  RCCBoolean received_data{false};  // Have we received data into our buffers?
  // The number of samples required to create a fft frame
  size_t samples_until_complete_frame{0};
  // Flag to indicate if a processed fft frame is ready to output.
  RCCBoolean data_to_output{false};
  RCCBoolean send_metadata{false};
  RCCBoolean trigger_held_output_opcodes{false};
  RCCBoolean flush_held{false};
  RCCBoolean sample_interval_held{false};
  RCCBoolean time_held{false};
  RCCBoolean metadata_held{false};
  RCCBoolean discontinuity_held{false};
  RCCBoolean flush_in_progress{false};
  RCCBoolean advance_input{false};

  // Metadata related parameters
  struct metadata_queue_struct {
    uint32_t id;
    uint64_t value;
  };
  std::queue<metadata_queue_struct> metadata_queue;

  // Time related parameters
  RCCBoolean input_interval_valid{false};  // input interval valid flag
  uint32_t input_interval_seconds{0};      // Sample Interval on input port
  uint64_t input_interval_fraction{0};     // Sample Interval on input port
  uint32_t output_time_seconds{0};         // output time seconds part
  uint64_t output_time_fraction{0};        // output time fractional part

  // Properties
  RCCBoolean inverse_fft{false};
  RCCBoolean indicate_start_of_fft_block{false};
  RCCBoolean increment_fft_block_value{false};
  uint32_t start_of_fft_block_id{0};
  uint64_t start_of_fft_block_value{0};
  size_t fft_length{64};
  RCCFloat gain_factor{0.0};

  void reset() {
    this->received_data = false;
    this->sample_offset = 0;

    this->fft_frame_iterator = this->fft_frame.begin();
    this->samples_until_complete_frame = this->fft_length;

    this->output_data_iterator = this->output_data_frame.begin();
    this->data_to_output = false;

    this->output_offset = 0;
  }

  RCCResult increment_fft_block_value_written() {
    log(OCPI_LOG_DEBUG, "increment_fft_block_value_written() called");
    this->increment_fft_block_value = properties().increment_fft_block_value;
    return RCC_OK;
  }

  RCCResult start_of_fft_block_id_written() {
    log(OCPI_LOG_DEBUG, "start_of_fft_block_id_written() called");
    this->start_of_fft_block_id = properties().start_of_fft_block_id;
    return RCC_OK;
  }

  RCCResult start_of_fft_block_value_written() {
    log(OCPI_LOG_DEBUG, "start_of_fft_block_value_written() called");
    this->start_of_fft_block_value = properties().start_of_fft_block_value;
    return RCC_OK;
  }

  RCCResult gain_factor_written() {
    log(OCPI_LOG_DEBUG, "gain_factor_written() called");

    // Convert UQ8.8 gain factor to floating point by dividing by 2^8
    this->gain_factor =
        (static_cast<RCCFloat>(properties().gain_factor) / 256.0);
    return RCC_OK;
  }

  RCCResult start() {
    log(OCPI_LOG_DEBUG, "start() called");

    uint16_t result = static_cast<uint16_t>(std::log2(properties().fft_length));
    if (properties().fft_length != (1 << result)) {
      setError(
          "Invalid fft length supplied, expected a power of 2, "
          "but received (%lu)",
          properties().fft_length);
      return RCC_FATAL;
    }

    this->fft_length = properties().fft_length;
    // Initialise input frame
    this->samples_until_complete_frame = this->fft_length;
    this->fft_frame.resize(this->fft_length);
    this->fft_frame_iterator = this->fft_frame.begin();

    // Initialise output frame
    this->output_data_frame.resize(this->fft_length);
    this->data_to_output = false;

    this->inverse_fft = properties().inverse_fft;
    this->indicate_start_of_fft_block =
        properties().indicate_start_of_fft_block;

#if USING_KISS_FFT
    if (this->inverse_fft) {
      this->kiss_cfg = kiss_fft_alloc(this->fft_length, 1, 0, 0);
    } else {
      this->kiss_cfg = kiss_fft_alloc(this->fft_length, 0, 0, 0);
    }
    this->frame_data_out.resize(this->fft_length);
#endif
    return RCC_OK;
  }

  RCCResult stop() {
    log(OCPI_LOG_DEBUG, "stop() called");

#if USING_KISS_FFT
    kiss_fft_free(this->kiss_cfg);
#endif

    return RCC_OK;
  }

  RCCResult run(bool) {

    if (trigger_held_output_opcodes == true) {
      return send_held_messages_to_output();
    }

    if (input.eof()) {
      // Received EOF
      if (this->received_data) {
        // Process zeros to flush out the partial frame
        return handle_flush();
      }

      output.setEOF();
      return RCC_ADVANCE_FINISHED;
    }

    if (input.opCode() == Complex_short_timed_sampleSample_OPERATION) {
      const Complex_short_timed_sampleSampleData* samples =
          input.sample().data().data();
      size_t input_length = input.sample().data().size();

      return handle_samples(samples, input_length);
    } else if (input.opCode() == Complex_short_timed_sampleTime_OPERATION) {
      return handle_time();
    } else if (input.opCode() ==
               Complex_short_timed_sampleSample_interval_OPERATION) {
      return handle_sample_interval();
    } else if (input.opCode() == Complex_short_timed_sampleFlush_OPERATION) {
      // Flush number of fft elements with zeros,
      // and forward flush onto output
      return handle_flush();
    } else if (input.opCode() ==
               Complex_short_timed_sampleDiscontinuity_OPERATION) {
      return handle_discontinuity();
    } else if (input.opCode() == Complex_short_timed_sampleMetadata_OPERATION) {
      return handle_metadata();
    } else {
      setError("Unknown OpCode received");
      return RCC_FATAL;
    }
  }

  RCCResult send_held_messages_to_output() {

    if (this->flush_held) {
      this->flush_held = false;
      this->flush_in_progress = false;
      output.setOpCode(Complex_short_timed_sampleFlush_OPERATION);
      output.advance();
      return RCC_OK;
    }

    if (this->sample_interval_held) {
      this->sample_interval_held = false;
      output.setOpCode(Complex_short_timed_sampleSample_interval_OPERATION);
      output.sample_interval().seconds() = this->input_interval_seconds;
      output.sample_interval().fraction() = this->input_interval_fraction;
      output.advance();
      return RCC_OK;
    }

    if (this->time_held) {
      this->time_held = false;
      output.setOpCode(Complex_short_timed_sampleTime_OPERATION);
      output.time().seconds() = this->output_time_seconds;
      output.time().fraction() = this->output_time_fraction;
      output.advance();
      return RCC_OK;
    }

    if (this->metadata_held) {
      if (this->metadata_queue.empty()) {
        this->metadata_held = false;
      } else {
        output.setOpCode(Complex_short_timed_sampleMetadata_OPERATION);
        output.metadata().id() = this->metadata_queue.front().id;
        output.metadata().value() = this->metadata_queue.front().value;
        this->metadata_queue.pop();
        output.advance();
      }
      return RCC_OK;
    }

    if (this->discontinuity_held) {
      this->discontinuity_held = false;
      output.setOpCode(Complex_short_timed_sampleDiscontinuity_OPERATION);
      output.advance();
      return RCC_OK;
    }

    if (this->advance_input) {
      input.advance();
      this->advance_input = false;
    }

    // All opcodes have been sent, reset trigger flag & advance component.
    this->trigger_held_output_opcodes = false;
    return RCC_OK;
  }

  inline RCCBoolean partially_filled_fft_frame() {

    if (this->fft_frame_iterator > this->fft_frame.begin()) {
      return true;
    }
    return false;
  }

  RCCResult handle_discontinuity() {
    RCCResult result = RCC_OK;

    if (partially_filled_fft_frame()) {
      // Send on all other opcodes, before advancing this input opcode.
      this->trigger_held_output_opcodes = true;
      this->discontinuity_held = false;

      // This will be advanced when re-entered after other opcodes have been
      // sent.
      this->advance_input = false;
    } else {
      // Forward discontinuity to output
      output.setOpCode(Complex_short_timed_sampleDiscontinuity_OPERATION);
      result = RCC_ADVANCE;
    }

    // Reset input buffer and storage parameters
    reset();

    return result;
  }

  RCCResult handle_metadata() {

    if (partially_filled_fft_frame()) {
      // Check the size of the metadata queue, remove oldest
      // element if metadata_fifo_size has been reached.
      if (this->metadata_queue.size() == properties().metadata_fifo_size) {
        this->metadata_queue.pop();
      }

      // Store metadata message to be sent once the fft frame is complete
      metadata_queue_struct metadata;
      metadata.id = input.metadata().id();
      metadata.value = input.metadata().value();

      this->metadata_queue.push(metadata);
      this->metadata_held = true;
      // Metadata message has been stored ready to be
      // output after the FFT frame has been completed.
      // Advance the input only.
      input.advance();
      return RCC_OK;
    }

    // Forward to output port
    output.setOpCode(Complex_short_timed_sampleMetadata_OPERATION);
    output.metadata().id() = input.metadata().id();
    output.metadata().value() = input.metadata().value();
    return RCC_ADVANCE;
  }

  RCCResult handle_time() {
    this->output_time_seconds = input.time().seconds();
    this->output_time_fraction = input.time().fraction();

    if (partially_filled_fft_frame()) {
      this->time_held = true;
      // Time message has been held ready to be
      // output after the FFT frame has been
      // completed, advance input only.
      input.advance();
      return RCC_OK;
    } else {
      output.setOpCode(Complex_short_timed_sampleTime_OPERATION);
      output.time().seconds() = this->output_time_seconds;
      output.time().fraction() = this->output_time_fraction;
      return RCC_ADVANCE;
    }
  }

  void advance_time_by_sample_interval(const size_t advance_time_count) {
    // Calculate the time of relevant sample, by adding an input interval for
    // every input sample to be advanced past
    if (this->input_interval_valid) {
      for (size_t i = 0; i < advance_time_count; i++) {
        time_utils::add(&this->output_time_seconds, &this->output_time_fraction,
                        this->input_interval_seconds,
                        this->input_interval_fraction);
      }
    }
  }

  RCCResult handle_sample_interval() {
    this->input_interval_seconds = input.sample_interval().seconds();
    this->input_interval_fraction = input.sample_interval().fraction();
    this->input_interval_valid = true;  // Our interval is now initialised

    if (partially_filled_fft_frame() || this->sample_interval_held) {
      this->sample_interval_held = true;
      return handle_flush();
    } else {
      output.setOpCode(Complex_short_timed_sampleSample_interval_OPERATION);
      output.sample_interval().seconds() = this->input_interval_seconds;
      output.sample_interval().fraction() = this->input_interval_fraction;
      return RCC_ADVANCE;
    }
  }

  RCCResult handle_flush() {
    RCCResult result = RCC_OK;

    if (this->received_data) {
      // Is there any data currently in a fft_frame?
      if (partially_filled_fft_frame()) {
        this->flush_in_progress = true;
        this->flush_held = true;
        size_t flush_length =
            (this->fft_frame.end() - this->fft_frame_iterator);
        Complex_short_timed_sampleSampleData samples[flush_length];
        memset(samples, 0, sizeof(samples));

        result = handle_samples(samples, flush_length);
      } else {
        // is data already being sent to the output?
        if (this->data_to_output == true) {
          this->output_offset += send_fft_frame_to_output(
              (this->fft_length - this->output_offset));

          if (this->output_offset >= this->fft_length) {
            this->output_offset = 0;
            this->data_to_output = false;
            result = RCC_ADVANCE;
          } else {
            output.advance();
            result = RCC_OK;
          }
        } else {
          // No fft_frame data to process or output
          this->received_data = false;
        }
      }

      // Having flushed out samples, set received_data to false, advance
      // output only. Next run input will be the flush opcode again and we'll
      // forward it and advance.
      if (result == RCC_ADVANCE) {
        output.advance();
        this->received_data = false;
        return RCC_OK;
      }
      return result;
    }

    // Reset input buffer and storage parameters
    reset();

    // Forward the flush opcode to output if the flush_held flag is false,
    // otherwise set the trigger to send held opcodes to the output.
    if (this->flush_held == false) {
      output.setOpCode(Complex_short_timed_sampleFlush_OPERATION);
    } else {
      this->trigger_held_output_opcodes = true;
      // Advance the input upon completion of sending
      // the sample interval to the output port.
      this->advance_input = true;
      return RCC_OK;
    }

    return RCC_ADVANCE;
  }

  // Handles the sample modulation
  RCCResult handle_samples(const Complex_short_timed_sampleSampleData* samples,
                           const size_t input_length) {

    size_t length = input_length;

    // Check to see whether data has been received
    // if so set received_data flag
    if (length > 0) {
      this->received_data = true;
    }

    if (this->sample_offset > length) {
      // Sample offset should not be more than the input length
      setError("Internal error, sample_offset > length of samples");
      return RCC_FATAL;
    }

    if (this->sample_offset > 0) {
      // Already processed some samples, move past them to
      // process the next samples
      samples += this->sample_offset;
      length -= this->sample_offset;
    }

    // The FFT frame can be larger than a single output buffer therefore
    // only append samples and process the frame when there is currently
    // no data to output
    if (this->data_to_output == false) {
      if (this->time_held) {
        if (this->samples_until_complete_frame <= length) {
          // Only want to process some samples then stop to output time
          length = this->samples_until_complete_frame;
        } else {
          // Need more samples than is in the current message to finish the
          // current fft frame, so process all this message
        }

        // Only advance the time by the sample interval for
        // "real" samples and not for zero inserted samples.
        if (this->flush_in_progress == false) {
          advance_time_by_sample_interval(length);
        }
      }

      if (add_samples_to_current_frame(samples, length)) {
        process_fft_frame();
      }
    }

    // If data_to_output is true, then process fft has produced output data
    // otherwise not enough data available wait for more data to arrive
    if (this->data_to_output) {

      if (this->send_metadata == true) {
        return send_metadata_to_output();
      }

      this->output_offset +=
          send_fft_frame_to_output((this->fft_length - this->output_offset));

      if (this->output_offset >= this->fft_length) {
        this->output_offset = 0;
        this->data_to_output = false;

        // If a flush is in progress this trigger
        // will be set at the end of the flush.
        if (this->flush_in_progress == false) {
          this->trigger_held_output_opcodes = true;
        }
      } else {
        output.advance();
        return RCC_OK;
      }
    }

    return generate_rcc_result(input_length);
  }

  bool add_samples_to_current_frame(
      const Complex_short_timed_sampleSampleData* input_data,
      size_t input_length) {

    size_t samples_to_extract = 0;
    bool fft_frame_to_process = false;

    if (input_length < this->samples_until_complete_frame) {
      samples_to_extract = input_length;
    } else {
      samples_to_extract = this->samples_until_complete_frame;
    }

    auto functor = std::bind(&to_float_complex, _1, this->gain_factor);
    std::transform(input_data, (input_data + samples_to_extract),
                   this->fft_frame_iterator, functor);

    this->fft_frame_iterator += samples_to_extract;
    // If the fft frame vector has now got a full frame set the
    // fft_frame_to_process flag to true and set the frame iterator back to the
    // beginning of the fft_frame.
    if (this->fft_frame_iterator == this->fft_frame.end()) {
      fft_frame_to_process = true;
      this->fft_frame_iterator = this->fft_frame.begin();

      if (this->indicate_start_of_fft_block == true) {
        this->send_metadata = true;
      }
    } else {
      fft_frame_to_process = false;
    }

    // After extracting the input samples from the input buffer subtract that
    // number of samples from the samples_until_complete_frame parameter.
    this->samples_until_complete_frame -= samples_to_extract;
    // Increment the sample_offset so that next time data needs appending to a
    // frame the frame starts from the correct location in the input buffer.
    this->sample_offset += samples_to_extract;

    return fft_frame_to_process;
  }

  static std::complex<RCCFloat> to_float_complex(
      const Complex_short_timed_sampleSampleData data, RCCFloat gain_factor) {

    RCCFloat real = static_cast<RCCFloat>(data.real) * gain_factor;
    RCCFloat imag = static_cast<RCCFloat>(data.imaginary) * gain_factor;
    // Clip Values to be within range on input as well as output
    return std::complex<RCCFloat>(clip_to_int<int16_t>(real),
                                  clip_to_int<int16_t>(imag));
  }

  template <typename T>
  static T clip_to_int(const RCCFloat value) {
    T int_value{static_cast<T>(value)};
    static_assert(std::is_integral<T>::value, "Integral value required.");
    if (value <= std::numeric_limits<T>::min()) {
      int_value = std::numeric_limits<T>::min();
    } else if (value >= std::numeric_limits<T>::max()) {
      int_value = std::numeric_limits<T>::max();
    }
    return int_value;
  }

  // Scales input data using fft_length, performs output type (int16_t) limit
  // checks, before returning output data
  static Complex_short_timed_sampleSampleData to_output_data(
      const std::complex<RCCFloat> data_in, size_t fft_length,
      bool scale_output) {

    Complex_short_timed_sampleSampleData data_out;
    RCCFloat temp_real;
    RCCFloat temp_imag;
    if (scale_output) {
      temp_real = (data_in.real() / fft_length);
      temp_imag = (data_in.imag() / fft_length);
    } else {
      temp_real = (data_in.real());
      temp_imag = (data_in.imag());
    }

    data_out.real = clip_to_int<int16_t>(temp_real);
    data_out.imaginary = clip_to_int<int16_t>(temp_imag);

    return data_out;
  }

  void process_fft_frame() {

#if USING_KISS_FFT
    auto frame_data_in =
        reinterpret_cast<kiss_fft_cpx*>(this->fft_frame.data());

    kiss_fft(this->kiss_cfg, frame_data_in,
             reinterpret_cast<kiss_fft_cpx*>(this->frame_data_out.data()));

    auto functor = std::bind(&to_output_data, _1, this->fft_length, true);

    std::transform(this->frame_data_out.begin(), this->frame_data_out.end(),
                   this->output_data_frame.begin(), functor);
#else
    if (this->inverse_fft) {
      built_in_fft_core.ifftInterleaved(
          reinterpret_cast<RCCFloat*>(this->fft_frame.data()),
          this->fft_length);
    } else {
      built_in_fft_core.fftInterleaved(
          reinterpret_cast<RCCFloat*>(this->fft_frame.data()),
          this->fft_length);
    }

    // The built-in core has scaling built in on the output for the inverse
    // fft part. Therefore set the scale output flag to false otherwise leave
    // scale_output at its default value of true.
    bool scale_output = true;
    if (this->inverse_fft) {
      scale_output = false;
    }

    auto functor =
        std::bind(&to_output_data, _1, this->fft_length, scale_output);
    std::transform(this->fft_frame.begin(), this->fft_frame.end(),
                   this->output_data_frame.begin(), functor);
#endif
    this->output_data_iterator = this->output_data_frame.begin();
    this->data_to_output = true;
  }

  RCCResult send_metadata_to_output() {

    output.setOpCode(Complex_short_timed_sampleMetadata_OPERATION);
    output.metadata().id() = this->start_of_fft_block_id;
    output.metadata().value() = this->start_of_fft_block_value;

    if (this->increment_fft_block_value == true) {
      if (this->start_of_fft_block_value < UINT64_MAX) {
        this->start_of_fft_block_value++;
      } else {
        this->start_of_fft_block_value = 0;
      }
    }

    // only one message per fft frame is sent to the output.
    this->send_metadata = false;

    output.advance();
    return RCC_OK;
  }

  size_t send_fft_frame_to_output(size_t output_length) {

    Complex_short_timed_sampleSampleData* out_data =
        output.sample().data().data();

    const size_t max_output_samples =
        FAST_FOURIER_TRANSFORM_XS_OCPI_MAX_BYTES_OUTPUT / sizeof(*out_data);

    const size_t num_samples_to_process =
        std::min(max_output_samples, output_length);

    // Ensure output is sized correctly to hold output
    output.setOpCode(Complex_short_timed_sampleSample_OPERATION);
    output.sample().data().resize(num_samples_to_process);

    std::copy(this->output_data_iterator,
              (this->output_data_iterator + num_samples_to_process), out_data);

    this->output_data_iterator += num_samples_to_process;

    return num_samples_to_process;
  }

  RCCResult generate_rcc_result(const size_t input_length) {

    // If the sample buffer has been fully processed then advance the input
    // and output buffers, reset buffer offsets
    if ((this->sample_offset >= input_length) &&
        (0 == this->samples_until_complete_frame)) {
      this->sample_offset = 0;
      this->samples_until_complete_frame = this->fft_length;
      return RCC_ADVANCE;
    } else if ((this->sample_offset < input_length) &&
               (0 == this->samples_until_complete_frame)) {
      // Some samples have been processed and a complete FFT frame has been
      // created, reset samples_until_complete_frame and advance the output.
      this->samples_until_complete_frame = this->fft_length;
      output.advance();
      return RCC_OK;
    } else {
      // Not enough data has been received to populate a full FFT frame,
      // reset the sample offset and advance the input ready for more samples.
      this->sample_offset = 0;
      input.advance();
      return RCC_OK;
    }
  }
};

FAST_FOURIER_TRANSFORM_XS_START_INFO
// Insert any static info assignments here (memSize, memSizes, portInfo)
// e.g.: info.memSize = sizeof(MyMemoryStruct);
FAST_FOURIER_TRANSFORM_XS_END_INFO
