// Zero Padder RCC worker
//
// This file is protected by Copyright. Please refer to the COPYRIGHT file
// distributed with this source distribution.
//
// This file is part of OpenCPI <http://www.opencpi.org>
//
// OpenCPI is free software: you can redistribute it and/or modify it under the
// terms of the GNU Lesser General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option) any
// later version.
//
// OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
// more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#include "zero_padder_l-worker.hh"
#include <vector>

using namespace OCPI::RCC;  // for easy access to RCC data types and constants
using namespace Zero_padder_lWorkerTypes;

class Zero_padder_lWorker : public Zero_padder_lWorkerBase {
  size_t index_start = 0;
  std::vector<int32_t> buffer;
  uint8_t samples = 0;

  RCCResult samples_written() {
    this->samples = properties().samples;
    return RCC_OK;
  }

  RCCResult start() {
    this->index_start = 0;
    return RCC_OK;
  }

  RCCResult run(bool) {
    if (input.opCode() == Long_timed_sampleSample_OPERATION) {
      output.setOpCode(Long_timed_sampleSample_OPERATION);
      size_t length = input.sample().data().size();
      const int32_t *inData = input.sample().data().data();
      // Determine if new message
      if (this->index_start == 0) {
        // Buffer the whole dataset
        size_t total_samples = length + (this->samples * length);
        this->buffer.resize(total_samples);
        std::fill(this->buffer.begin(), this->buffer.end(), 0);
        for (size_t i = 0; i < length; i++) {
          this->buffer[i * (this->samples + 1)] = inData[i];
        }
      }
      // Limit the maximum message size
      size_t length_limited;
      size_t length_remaining = this->buffer.size() - this->index_start;
      if (length_remaining >
          (ZERO_PADDER_L_OCPI_MAX_BYTES_OUTPUT / sizeof(int32_t))) {
        length_limited = ZERO_PADDER_L_OCPI_MAX_BYTES_OUTPUT / sizeof(int32_t);
      } else {
        length_limited = length_remaining;
      }
      // Copy data to the output
      output.sample().data().resize(length_limited);
      int32_t *outData = output.sample().data().data();
      size_t index_end = this->index_start + length_limited;
      std::copy(this->buffer.begin() + this->index_start,
                this->buffer.begin() + index_end, outData);
      this->index_start += length_limited;
      // Determine whether or not all the data is sent this time around
      if (this->index_start != this->buffer.size()) {
        output.advance(length_limited);
        return RCC_OK;
      } else {
        this->index_start = 0;
      }
      return RCC_ADVANCE;
    } else if (input.opCode() == Long_timed_sampleTime_OPERATION) {
      // Pass through time opcode and time data
      output.setOpCode(Long_timed_sampleTime_OPERATION);
      output.time().fraction() = input.time().fraction();
      output.time().seconds() = input.time().seconds();
      return RCC_ADVANCE;
    } else if (input.opCode() == Long_timed_sampleSample_interval_OPERATION) {
      // Pass through sample interval opcode and sample interval data
      output.setOpCode(Long_timed_sampleSample_interval_OPERATION);
      output.sample_interval().fraction() = input.sample_interval().fraction();
      output.sample_interval().seconds() = input.sample_interval().seconds();
      return RCC_ADVANCE;
    } else if (input.opCode() == Long_timed_sampleFlush_OPERATION) {
      // Pass through flush opcode
      output.setOpCode(Long_timed_sampleFlush_OPERATION);
      return RCC_ADVANCE;
    } else if (input.opCode() == Long_timed_sampleDiscontinuity_OPERATION) {
      // Pass through discontinuity opcode
      output.setOpCode(Long_timed_sampleDiscontinuity_OPERATION);
      return RCC_ADVANCE;
    } else if (input.opCode() == Long_timed_sampleMetadata_OPERATION) {
      // Pass through metadata opcode, id, and data
      output.setOpCode(Long_timed_sampleMetadata_OPERATION);
      output.metadata().id() = input.metadata().id();
      output.metadata().value() = input.metadata().value();
      return RCC_ADVANCE;
    } else {
      setError("Unknown OpCode Received");
      return RCC_FATAL;
    }
  }
};

ZERO_PADDER_L_START_INFO
// Insert any static info assignments here (memSize, memSizes, portInfo)
// e.g.: info.memSize = sizeof(MyMemoryStruct);
ZERO_PADDER_L_END_INFO
