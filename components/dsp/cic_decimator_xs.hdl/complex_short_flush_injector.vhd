-- Flush injector for complex short protocol
--
-- This file is protected by Copyright. Please refer to the COPYRIGHT file
-- distributed with this source distribution.
--
-- This file is part of OpenCPI <http://www.opencpi.org>
--
-- OpenCPI is free software: you can redistribute it and/or modify it under the
-- terms of the GNU Lesser General Public License as published by the Free
-- Software Foundation, either version 3 of the License, or (at your option) any
-- later version.
--
-- OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
-- WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
-- A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
-- more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.

-- On receipt of a flush opcode, applies backpressure to the input port and
-- generates an input message of `flush_length` stream samples of value zero,
-- before releasing backpressure and sending on the flush opcode. All other
-- message types passthrough undelayed.

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;

library work;
use work.cic_decimator_xs_worker_defs.all;
library sdr_interface;
use sdr_interface.sdr_interface.flush_inserter_v2;

entity complex_short_flush_injector is
  generic (
    data_in_width_g         : integer          := 32;
    max_message_length_g    : integer          := 4096;
    processed_data_opcode_g : std_logic_vector := "000";
    flush_opcode_g          : std_logic_vector := "011"
    );
  port (
    clk               : in  std_logic;
    reset             : in  std_logic;
    enable            : in  std_logic;
    data_valid_in     : in  std_logic;
    take_in           : in  std_logic;
    input_in          : in  worker_input_in_t;
    flush_length      : in  unsigned;
    flush_change      : in  std_logic;
    decimation_factor : in  unsigned;
    flush_ready       : out std_logic;
    input_out         : out worker_input_out_t;
    input_interface   : out worker_input_in_t
    );
end complex_short_flush_injector;

architecture rtl of complex_short_flush_injector is

  constant opcode_width_c      : integer := integer(ceil(log2(real(complex_short_timed_sample_opcode_t'pos(complex_short_timed_sample_opcode_t'high)))));
  constant byte_enable_width_c : integer := input_in.byte_enable'length;

  function opcode_to_slv(inop : in complex_short_timed_sample_opcode_t) return std_logic_vector is
  begin
    return std_logic_vector(to_unsigned(complex_short_timed_sample_opcode_t'pos(inop), opcode_width_c));
  end function;

  function slv_to_opcode(inslv : in std_logic_vector(opcode_width_c - 1 downto 0)) return complex_short_timed_sample_opcode_t is
  begin
    return complex_short_timed_sample_opcode_t'val(to_integer(unsigned(inslv)));
  end function;

  signal input_opcode           : std_logic_vector(opcode_width_c - 1 downto 0);
  signal input_interface_opcode : std_logic_vector(opcode_width_c - 1 downto 0);
  signal decimator_count        : unsigned(decimation_factor'range);
  signal max_count              : unsigned(flush_length'length downto 0) := (others => '0');
  signal flush_length_mod_max   : unsigned(max_count'range);
  signal flush_length_mod_min   : unsigned(max_count'range);
  signal flush_length_diff      : unsigned(max_count'range);
  signal flush_length_calc      : unsigned(max_count'length downto 0);
  signal flush_length_total     : unsigned(flush_length_calc'range);
  signal data_seen              : std_logic;
  signal flush                  : std_logic;

begin

  flush <= '1' when input_in.ready = '1' and input_in.opcode = complex_short_timed_sample_flush_op_e else '0';

  input_opcode <= opcode_to_slv(input_in.opcode);

  -- Calculate samples to decimation boundary based on
  -- decimation_factor - ((flush_length + decimation_counter)
  --    MOD decimation_factor)
  flush_length_calc_p : process(clk)
  begin
    if rising_edge(clk) then
      flush_ready <= '1';
      -- calculate the modulation values
      if flush_change = '1' and max_count < flush_length then
        max_count   <= resize(max_count + decimation_factor, max_count'length);
        flush_ready <= '0';
      elsif flush_change = '0' then
        max_count   <= (others => '0');
        flush_ready <= '0';
      end if;
      flush_length_mod_max <= max_count;
      flush_length_mod_min <= max_count - decimation_factor;
      flush_length_diff    <= flush_length_mod_max - flush_length;
    end if;
  end process;

  -- track decimation progress and calculate the correct
  -- modulation based on the counter
  decimator_track_p : process(clk)
    variable flush_length_mod_v : unsigned(flush_length_mod_max'range);
    variable decimator_count_v  : unsigned(decimation_factor'range);
  begin
    if rising_edge(clk) then
      if reset = '1' then
        data_seen       <= '0';
        decimator_count <= (others => '0');
      else
        decimator_count_v := decimator_count;
        if input_interface_opcode = flush_opcode_g then
          data_seen <= '0';
        end if;
        if data_valid_in = '1' then
          data_seen <= '1';
          if decimator_count_v >= decimation_factor-1 then
            decimator_count_v := (others => '0');
          else
            decimator_count_v := decimator_count_v + 1;
          end if;
        end if;
        -- decide which modulus value to use
        if decimator_count_v > flush_length_diff then
          flush_length_mod_v := flush_length_mod_max;
        else
          flush_length_mod_v := flush_length_mod_min;
        end if;
        flush_length_calc <= resize(decimation_factor + flush_length_mod_v, flush_length_calc'length);
        decimator_count   <= decimator_count_v;
      end if;
    end if;
  end process;

  -- The below are the 3 options for flush_length_total:
  -- flush no zeroes when there has been no data
  -- use base flush length to avoid unnecessary extra zero
  -- use flush length to next decimation boundary
  flush_length_total <= (others => '0') when data_seen = '0'
                        else resize(flush_length, flush_length_total'length) when decimator_count = flush_length_mod_max or decimator_count = flush_length_mod_min
                        else resize(flush_length_calc - decimator_count, flush_length_total'length);

  flush_inserter_i : flush_inserter_v2
    generic map (
      data_width_g         => data_in_width_g,
      opcode_width_g       => opcode_width_c,
      byte_enable_width_g  => byte_enable_width_c,
      max_message_length_g => max_message_length_g,
      flush_opcode_g       => flush_opcode_g,
      data_opcode_g        => processed_data_opcode_g
      )
    port map (
      clk                => clk,
      reset              => reset,
      flush_length       => flush_length_total,
      take_in            => take_in,
      take_out           => input_out.take,
      input_som          => input_in.som,
      input_eom          => input_in.eom,
      input_eof          => input_in.eof,
      input_valid        => input_in.valid,
      input_byte_enable  => input_in.byte_enable,
      input_opcode       => input_opcode,
      input_data         => input_in.data,
      input_ready        => input_in.ready,
      output_som         => input_interface.som,
      output_eom         => input_interface.eom,
      output_eof         => input_interface.eof,
      output_valid       => input_interface.valid,
      output_byte_enable => input_interface.byte_enable,
      output_opcode      => input_interface_opcode,
      output_data        => input_interface.data,
      output_ready       => input_interface.ready
      );
  input_interface.opcode <= slv_to_opcode(input_interface_opcode);

end rtl;
