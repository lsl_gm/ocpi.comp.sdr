-- HDL Implementation of CIC decimator.
--
-- This file is protected by Copyright. Please refer to the COPYRIGHT file
-- distributed with this source distribution.
--
-- This file is part of OpenCPI <http://www.opencpi.org>
--
-- OpenCPI is free software: you can redistribute it and/or modify it under the
-- terms of the GNU Lesser General Public License as published by the Free
-- Software Foundation, either version 3 of the License, or (at your option) any
-- later version.
--
-- OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
-- WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
-- A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
-- more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;
library ocpi;
use ocpi.types.all;
library sdr_dsp;
use sdr_dsp.sdr_dsp.cic_dec;
use sdr_dsp.sdr_dsp.rounding_halfup;

architecture rtl of worker is

  -- Set input and output sizes
  constant i_word_index_c       : integer := input_in.data'length/2;
  constant q_word_index_c       : integer := input_in.data'length;
  constant data_word_size_c     : integer := input_in.data'length/2;
  -- Sets the size of the internal registers used to store the cic calculations
  constant cic_word_size_c      : integer := to_integer(cic_register_size);
  -- Sets number of comb and integrator stages
  constant comb_stages_c        : integer := to_integer(cic_order);
  constant int_stages_c         : integer := to_integer(cic_order);
  constant differential_delay_c : integer := to_integer(cic_differential_delay);
  -- Set delay through CIC and rounding primitive
  constant delay_c              : integer := comb_stages_c + int_stages_c + 2;
  constant opcode_width_c       : integer := integer(ceil(log2(real(complex_short_timed_sample_opcode_t'pos(complex_short_timed_sample_opcode_t'high)))));

  function opcode_to_slv(inop : in complex_short_timed_sample_opcode_t) return std_logic_vector is
  begin
    return std_logic_vector(to_unsigned(complex_short_timed_sample_opcode_t'pos(inop), opcode_width_c));
  end function;

  function slv_to_opcode(inslv : in std_logic_vector(opcode_width_c - 1 downto 0)) return complex_short_timed_sample_opcode_t is
  begin
    return complex_short_timed_sample_opcode_t'val(to_integer(unsigned(inslv)));
  end function;

  -- Interface signals
  signal enable           : std_logic;
  signal take             : std_logic;
  signal input_interface  : worker_input_in_t;
  signal input_hold       : std_logic;
  signal data_valid_in    : std_logic;
  signal data_eom_in      : std_logic;
  signal data_in_i        : signed(data_word_size_c - 1 downto 0);
  signal data_in_q        : signed(data_word_size_c - 1 downto 0);
  signal discontinuity    : std_logic;
  signal reset_hold       : std_logic := '0';
  signal reset_i          : std_logic;
  signal reset            : std_logic;
  signal reset_sr         : std_logic_vector(delay_c downto 0);
  signal injector_reset   : std_logic;
  signal comb_stage_flush : unsigned(props_in.down_sample_factor'length+integer(ceil(log2(real(differential_delay_c)))) downto 0);
  signal flush_length     : unsigned(comb_stage_flush'length+integer(ceil(log2(real(comb_stages_c)))) downto 0);
  signal flush_sr         : std_logic_vector(2 downto 0);
  signal flush_ready      : std_logic;

  -- CIC signals
  signal cic_data_valid     : std_logic;
  signal data_eom_out       : std_logic;
  signal data_out_i         : signed(cic_word_size_c - 1 downto 0);
  signal data_out_q         : signed(cic_word_size_c - 1 downto 0);
  signal rounded_data_out_i : signed(data_word_size_c - 1 downto 0);
  signal rounded_data_out_q : signed(data_word_size_c - 1 downto 0);
  signal scale_factor       : integer range 0 to cic_word_size_c - 1;
  signal round_data_valid   : std_logic;
  signal processed_data     : std_logic_vector(output_out.data'high downto 0);
  signal processed_end_in   : std_logic;

begin

  -- Process to calculate flush length values
  flush_length_p : process(ctl_in.clk)
  begin
    if rising_edge(ctl_in.clk) then
      -- this will only change when the property down_sample_factor changes
      comb_stage_flush <= resize(props_in.down_sample_factor * differential_delay_c, comb_stage_flush'length);
      flush_length     <= resize(comb_stages_c*(comb_stage_flush-1), flush_length'length);
    end if;
  end process;

  -- Process to track progress of flush length calculation
  flush_ready_p : process(ctl_in.clk)
  begin
    if rising_edge(ctl_in.clk) then
      if injector_reset = '1' then
        flush_sr <= (others => '0');
      else
        flush_sr <= flush_sr(flush_sr'high-1 downto 0) & '1';
      end if;
    end if;
  end process;

  ------------------------------------------------------------------------------
  -- Flush inject
  ------------------------------------------------------------------------------
  -- Insert flush directly into input data stream.
  -- input_interface is used instead of input_in in rest of component.
  -- Take signal is from when input_interface.ready = '1' and output port ready.
  flush_insert_i : entity work.complex_short_flush_injector
    generic map (
      data_in_width_g         => input_in.data'length,
      max_message_length_g    => to_integer(ocpi_max_bytes_output)/(input_in.data'length/8),
      processed_data_opcode_g => opcode_to_slv(complex_short_timed_sample_sample_op_e),
      flush_opcode_g          => opcode_to_slv(complex_short_timed_sample_flush_op_e)
      )
    port map (
      clk               => ctl_in.clk,
      reset             => injector_reset,
      enable            => enable,
      data_valid_in     => data_valid_in,
      take_in           => take,
      input_in          => input_in,
      flush_length      => flush_length,
      flush_change      => flush_sr(flush_sr'high),
      decimation_factor => props_in.down_sample_factor,
      flush_ready       => flush_ready,
      input_out         => input_out,
      input_interface   => input_interface
      );

  ------------------------------------------------------------------------------
  -- Data interface
  ------------------------------------------------------------------------------
  reset          <= ctl_in.reset or reset_i;
  -- stop flush injector doing anything when waiting for discontinuity
  injector_reset <= ctl_in.reset or reset_hold or discontinuity or props_in.down_sample_factor_written;
  -- CIC processing runs when data output is ready
  enable         <= output_in.ready and (not input_hold);
  -- Take input data whenever both the input and the output are ready
  take           <= flush_ready and input_interface.ready and enable and not reset_hold;
  discontinuity  <= '1' when take = '1' and input_interface.opcode = complex_short_timed_sample_discontinuity_op_e else '0';

  -- Reset Shift Register Process
  reset_p : process(ctl_in.clk)
  begin
    if rising_edge(ctl_in.clk) then
      if ctl_in.reset = '1' then
        reset_i    <= '0';
        reset_sr   <= (others => '0');
        reset_hold <= '0';
      elsif enable = '1' then
        reset_i  <= '0';
        reset_sr <= reset_sr(reset_sr'high-1 downto 0) & '0';
        if discontinuity = '1' or props_in.down_sample_factor_written = '1' then
          reset_sr(0) <= '1';
          reset_hold  <= '1';
        end if;
        if reset_sr(reset_sr'high-1) = '1' then
          reset_i <= '1';
        end if;
        if reset_sr(reset_sr'high) = '1' then
          reset_hold <= '0';
        end if;
      end if;
    end if;
  end process;

  data_valid_in <= '1' when input_interface.opcode = complex_short_timed_sample_sample_op_e
                   and input_interface.valid = '1' and take = '1' else '0';

  data_eom_in <= data_valid_in and input_interface.eom;

  -- Split I and Q data
  data_in_i <= signed(input_interface.data(i_word_index_c - 1 downto 0));
  data_in_q <= signed(input_interface.data(q_word_index_c - 1 downto i_word_index_c));

  -- Interface delay module
  -- Delays streaming interface signals to align with the delay introduced by
  -- the CIC calculation.
  interface_delay : entity work.complex_short_down_sampled_protocol_delay
    generic map (
      delay_g         => delay_c,
      data_in_width_g => input_in.data'length
      )
    port map (
      clk                 => ctl_in.clk,
      reset               => ctl_in.reset,
      enable              => output_in.ready,
      decimation_factor   => props_in.down_sample_factor,
      take_in             => take,
      input_in            => input_interface,
      processed_stream_in => processed_data,
      processed_mask_in   => round_data_valid,  -- 1 when CIC is outputting data
      processed_end_in    => processed_end_in,
      output_out          => output_out,
      input_hold_out      => input_hold
      );

  -- ---------------------------------------------------------------------------
  -- CIC Decimators
  -- ---------------------------------------------------------------------------
  -- Instantiate two CIC modules for I and Q streams
  cic_module_i : cic_dec
    generic map (
      int_stages_g         => int_stages_c,
      comb_stages_g        => comb_stages_c,
      differential_delay_g => differential_delay_c,
      input_word_size_g    => data_word_size_c,
      output_word_size_g   => cic_word_size_c
      )
    port map (
      clk                => ctl_in.clk,
      reset              => reset,
      clk_en             => enable,
      data_valid_in      => data_valid_in,
      data_eom_in        => data_eom_in,
      data_in            => data_in_i,
      down_sample_factor => props_in.down_sample_factor,
      data_valid_out     => cic_data_valid,
      data_eom_out       => data_eom_out,
      data_out           => data_out_i
      );

  cic_module_q : cic_dec
    generic map (
      int_stages_g         => int_stages_c,
      comb_stages_g        => comb_stages_c,
      differential_delay_g => differential_delay_c,
      input_word_size_g    => data_word_size_c,
      output_word_size_g   => cic_word_size_c
      )
    port map (
      clk                => ctl_in.clk,
      reset              => reset,
      clk_en             => enable,
      data_valid_in      => data_valid_in,
      data_eom_in        => '0',
      data_in            => data_in_q,
      down_sample_factor => props_in.down_sample_factor,
      data_valid_out     => open,
      data_eom_out       => open,
      data_out           => data_out_q
      );

  -- ---------------------------------------------------------------------------
  -- Output rounding and scaling
  -- ---------------------------------------------------------------------------
  -- Round output using half-up adder
  scale_factor <= to_integer(props_in.scale_output);

  halfup_rounder_i : rounding_halfup
    generic map (
      input_width_g   => cic_word_size_c,
      output_width_g  => data_word_size_c,
      saturation_en_g => false
      )
    port map(
      clk            => ctl_in.clk,
      reset          => ctl_in.reset,
      clk_en         => enable,
      data_in        => data_out_i,
      data_valid_in  => cic_data_valid,
      binary_point   => scale_factor,
      data_out       => rounded_data_out_i,
      data_valid_out => round_data_valid
      );

  halfup_rounder_q : rounding_halfup
    generic map (
      input_width_g   => cic_word_size_c,
      output_width_g  => data_word_size_c,
      saturation_en_g => false
      )
    port map(
      clk            => ctl_in.clk,
      reset          => ctl_in.reset,
      clk_en         => enable,
      data_in        => data_out_q,
      data_valid_in  => cic_data_valid,
      binary_point   => scale_factor,
      data_out       => rounded_data_out_q,
      data_valid_out => open
      );

  -- EOM Rounding Delay Process
  -- Clock delay to match EOM timing with rounding primitive
  eom_rounding_delay_p : process(ctl_in.clk)
  begin
    if rising_edge(ctl_in.clk) then
      if enable = '1' then
        processed_end_in <= data_eom_out;
      end if;
    end if;
  end process;

  processed_data <= std_logic_vector(rounded_data_out_q) & std_logic_vector(rounded_data_out_i);

end rtl;
