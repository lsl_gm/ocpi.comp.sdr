-- HDL Implementation of polyphase decimator xs.
--
-- This file is protected by Copyright. Please refer to the COPYRIGHT file
-- distributed with this source distribution.
--
-- This file is part of OpenCPI <http://www.opencpi.org>
--
-- OpenCPI is free software: you can redistribute it and/or modify it under the
-- terms of the GNU Lesser General Public License as published by the Free
-- Software Foundation, either version 3 of the License, or (at your option) any
-- later version.
--
-- OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
-- WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
-- A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
-- more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;
library ocpi;
use ocpi.types.all;

library sdr_dsp;
use sdr_dsp.sdr_dsp.polyphase_decimator;

architecture rtl of worker is
  function rounding_delay (r_type : rounding_type_t) return integer is
  begin
    case r_type is
      when truncate_e  => return 2;
      when half_up_e   => return 2;
      when half_even_e => return 2;
    end case;
  end;
  function to_string (r_type : rounding_type_t) return string is
  begin
    case r_type is
      when truncate_e  => return "truncate";
      when half_up_e   => return "half_up";
      when half_even_e => return "half_even";
    end case;
  end;
  constant rounding_mode_c : string := to_string(rounding_type);

  constant input_word_size_c       : integer := input_in.data'length/2;
  -- the maximum delay is all the branches with all the taps
  constant delay_c                 : integer := to_integer(max_taps_per_branch+1) * (to_integer(max_decimation)+1) + 8 + rounding_delay(rounding_type);
  constant taps_per_branch_width_c : integer := integer(ceil(log2(real(to_integer(max_taps_per_branch)+1))));
  constant decimation_width_c      : integer := integer(ceil(log2(real(to_integer(max_decimation)+1))));
  constant num_taps_c              : integer := (to_integer(max_taps_per_branch)+1) * to_integer(max_decimation);
  constant num_taps_width_c        : integer := integer(ceil(log2(real(num_taps_c))));
  constant num_multiplier_width_c  : integer := integer(ceil(log2(real(to_integer(num_multipliers)))));

  signal delay_length        : unsigned(integer(ceil(log2(real(delay_c))))-1 downto 0);
  signal discontinuity_delay : unsigned(integer(ceil(log2(real(delay_c))))-1 downto 0) := (others => '0');
  signal decimator_delay     : unsigned(integer(ceil(log2(real(delay_c))))-1 downto 0) := (others => '0');
  signal input_latency       : unsigned(integer(ceil(log2(real(delay_c))))-1 downto 0);
  signal decimation_factor   : unsigned (decimation_width_c-1 downto 0);
  signal decimation_valid    : std_logic;

  signal decimator_out_data  : signed(output_out.data'range);
  signal decimator_out_valid : std_logic;
  signal decimator_out_last  : std_logic;
  signal tap_valid           : std_logic;
  signal tap_data            : signed(15 downto 0);
  signal tap_addr            : unsigned(15 downto 0);

  signal flush_out : std_logic;
  signal no_data   : std_logic;

  signal ip_reset      : std_logic;
  signal discontinuity : std_logic;

  -- Interface signals
  signal enable             : std_logic;
  signal take_in            : std_logic;
  signal take_data          : std_logic;
  signal flush_wait         : std_logic;
  signal hold_discontinuity : std_logic;
  signal take_proto         : std_logic;
  signal input_interface    : worker_input_in_t;
  signal input_hold         : std_logic;
  signal data_valid_in      : std_logic;
  signal data_out_i         : signed(input_word_size_c - 1 downto 0);
  signal data_out_q         : signed(input_word_size_c - 1 downto 0);
  signal output_interface   : worker_output_out_t;
  signal input_last         : std_logic;

  signal input_counter : unsigned(decimation_width_c-1 downto 0);
  signal flush_length  : unsigned(num_taps_width_c-1 downto 0);

  signal taps_per_branch : unsigned(taps_per_branch_width_c-1 downto 0);
  signal num_taps_in     : unsigned(num_taps_width_c-1 downto 0);
begin

  -- assignments for polyphase_decimator_xs's volatile properties
  props_out.dummy     <= to_uchar(0);
  props_out.raw.done  <= '1';
  props_out.raw.error <= '0';
  props_out.raw.data  <= (others => '0');

  decimation_factor <= resize(props_in.decimation_factor, decimation_factor'length);

  taps_per_branch <= resize(
    resize(props_in.taps_per_branch, taps_per_branch'length + 1),
    taps_per_branch'length);

  -- Tap data is only 16 bits, therefore there are 2 tap messages possible
  -- on the raw interface
  tap_data <= signed(props_in.raw.data(15 downto 0)) when props_in.raw.byte_enable(0) = '1'
              else signed(props_in.raw.data(31 downto 16));

  -- Plus one offset as byte addressing is being converted to short
  -- (even though the data is bytes)
  tap_addr <= unsigned(props_in.raw.address(tap_addr'high+1 downto 1));

  tap_valid <= props_in.raw.is_write when tap_addr < (to_integer(max_taps_per_branch)) * (to_integer(max_decimation)+1) else '0';

  -- processing runs when data output is ready
  enable     <= output_in.ready and not input_hold;
  take_in    <= take_proto;
  take_proto <= take_data and not hold_discontinuity;

  ip_reset <= discontinuity or ctl_in.reset;

  hold_discontinuity <= '1' when (input_interface.opcode = complex_short_timed_sample_discontinuity_op_e
                                  and input_interface.ready = '1'
                                  and (decimator_delay /= 0 or discontinuity_delay /= 0 or flush_wait = '1'))
                        else '0';

  -- Delay discontinuity whilst decimation is completing
  decimate_pipeline_p : process(ctl_in.clk)
  begin
    if rising_edge(ctl_in.clk) then
      if ip_reset = '1' then
        decimator_delay     <= (others => '0');
        discontinuity_delay <= delay_length;
      else
        if enable = '1' then
          -- decimation delay
          if decimation_valid = '1' and input_interface.eom = '1' then
            -- if last at same time, + and - so nothing happens
            if decimator_out_last /= '1' then
              decimator_delay <= decimator_delay + 1;
            end if;
          elsif decimator_out_last = '1' and decimator_delay /= 0 then
            decimator_delay <= decimator_delay - 1;
          end if;
          -- minimum protocol delay
          discontinuity_delay <= delay_length;
          if input_interface.opcode = complex_short_timed_sample_discontinuity_op_e
            and input_interface.ready = '1' then
            discontinuity_delay <= discontinuity_delay - 1;
          end if;
        end if;
      end if;
    end if;
  end process;

  discontinuity <= '1' when (input_interface.opcode = complex_short_timed_sample_discontinuity_op_e
                             and input_interface.ready = '1'
                             and hold_discontinuity = '0'
                             and take_data = '1')
                   else '0';


  data_valid_in <= '1' when (input_interface.opcode = complex_short_timed_sample_sample_op_e
                             and input_interface.valid = '1'
                             and input_hold = '0')
                   else '0';

  flush_out <= '1' when (input_interface.opcode = complex_short_timed_sample_flush_op_e
                         and input_interface.ready = '1'
                         and take_data = '1')
               else '0';

  flush_inserter_i : entity work.complex_short_flush_injector
    generic map (
      data_in_width_g      => input_in.data'length,
      max_message_length_g => to_integer(ocpi_max_bytes_output/4)
      )
    port map (
      clk             => ctl_in.clk,
      reset           => ctl_in.reset,
      take_in         => take_in,
      input_in        => input_in,
      flush_length    => flush_length,
      input_out       => input_out,
      input_interface => input_interface
      );

  flush_input_count_p : process (ctl_in.clk)
  begin
    if rising_edge(ctl_in.clk) then
      if ip_reset = '1' then
        input_counter <= (others => '0');
        no_data       <= '1';
        flush_wait    <= '0';
      else
        if take_data = '1' and data_valid_in = '1' then
          no_data <= '0';
          if input_counter >= decimation_factor-1 then
            input_counter <= (others => '0');
          else
            input_counter <= input_counter + 1;
          end if;
        end if;

        if flush_out = '1' then
          no_data    <= '1';
          flush_wait <= '1';
        end if;
        if flush_wait = '1' and output_interface.opcode = complex_short_timed_sample_flush_op_e and output_interface.give = '1' then
          flush_wait <= '0';
        end if;
      end if;
    end if;
  end process;

  flush_length <= (others => '0') when no_data = '1'
                  else (num_taps_in) - input_counter;

  input_latency <= resize(decimation_factor, input_latency'length);

  process (decimation_factor, taps_per_branch)
  begin
    -- delay_length := 2 -- input / output registers of
    -- delay_length := 3 -- rounding method used
    -- delay_length := 6 -- MAC output_delay
    if decimation_factor < num_multipliers then
      delay_length <= 2 + resize(taps_per_branch, delay_length'length) + 6 + rounding_delay(rounding_type);
    else
      -- Ceiling of decimation factor / num_multipliers
      if decimation_factor(num_multiplier_width_c-1 downto 0) > 0 then
        delay_length <= 2 + resize((shift_right(decimation_factor, num_multiplier_width_c)+1)*taps_per_branch, delay_length'length) + 6 + rounding_delay(rounding_type);
      else
        delay_length <= 2 + resize((shift_right(decimation_factor, num_multiplier_width_c))*taps_per_branch, delay_length'length) + 6 + rounding_delay(rounding_type);
      end if;
    end if;
  end process;

  -- Interface delay module - This is a customised version of the downsample
  -- delay, needed due to the way timestamps and group delays are currently
  -- being applied to the opcodes.
  interface_delay_i : entity work.complex_short_down_sampled_protocol_delay
    generic map (
      delay_g               => delay_c,
      max_decimation_g      => to_integer(max_decimation),
      data_in_width_g       => input_in.data'length,
      ocpi_max_bytes_output => to_integer(ocpi_max_bytes_output)
      )
    port map (
      clk                    => ctl_in.clk,
      reset                  => ctl_in.reset,
      enable                 => output_in.ready,
      delay_length           => std_logic_vector(delay_length),
      take_in                => take_proto,
      take_out               => open,
      decimation_factor      => input_latency,
      group_delay_seconds    => unsigned(props_in.group_delay_seconds),
      group_delay_fractional => unsigned(props_in.group_delay_fractional),
      input_in               => input_interface,
      processed_stream_in    => std_logic_vector(decimator_out_data),
      processed_mask_in      => decimator_out_valid,  -- 1 when CIC is outputting data
      processed_end_in       => decimator_out_last,
      output_out             => output_interface,
      input_hold_out         => input_hold
      );
  output_out <= output_interface;

  input_last  <= input_interface.eom;
  num_taps_in <= resize(taps_per_branch * decimation_factor, num_taps_in'length);

  polyphase_decimator_i : polyphase_decimator
    generic map (
      data_width_g          => input_in.data'length/2,
      -- This has +1 to stop the values from being wrapped to zero if a
      -- decimation rate of 8 is requested
      max_decimation_g      => to_integer(max_decimation)+1,
      max_taps_per_branch_g => to_integer(max_taps_per_branch)+1,
      max_taps_g            => num_taps_c,
      num_multipliers_g     => to_integer(num_multipliers),
      data_complex_g        => true,
      rounding_mode_g       => rounding_mode_c
      )
    port map (
      clk                 => ctl_in.clk,
      reset               => ip_reset,
      enable              => enable,
      -- Control Signals
      -- decimation_in and num_taps_in are zero based numbers
      decimation_in       => decimation_factor,
      num_taps_in         => num_taps_in,
      num_taps_per_branch => props_in.taps_per_branch(taps_per_branch_width_c-1 downto 0),
      decimation_valid    => decimation_valid,
      -- Tap RAM signals
      tap_data_in         => tap_data,
      tap_addr_in         => tap_addr(num_taps_width_c-1 downto 0),
      tap_valid_in        => tap_valid,
      tap_ready_out       => open,
      -- Input Stream signals
      input_data_i        => signed(input_interface.data(15 downto 0)),
      input_data_q        => signed(input_interface.data(31 downto 16)),
      input_valid_in      => data_valid_in,
      input_last_in       => input_last,
      input_ready_out     => take_data,
      -- Output Stream signals
      output_data_i       => data_out_i,
      output_data_q       => data_out_q,
      output_valid_out    => decimator_out_valid,
      output_last_out     => decimator_out_last,
      output_ready_in     => '1'
      );
  decimator_out_data <= data_out_q & data_out_i;

end rtl;
