// Threshold RCC worker.
//
// This file is protected by Copyright. Please refer to the COPYRIGHT file
// distributed with this source distribution.
//
// This file is part of OpenCPI <http://www.opencpi.org>
//
// OpenCPI is free software: you can redistribute it and/or modify it under the
// terms of the GNU Lesser General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option) any
// later version.
//
// OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
// more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#include "threshold_s_b-worker.hh"

using namespace OCPI::RCC;  // for easy access to RCC data types and constants
using namespace Threshold_s_bWorkerTypes;

class Threshold_s_bWorker : public Threshold_s_bWorkerBase {
  int16_t threshold = 0;
  bool out_true = false;
  bool out_false = false;

  RCCResult threshold_written() {
    threshold = properties().threshold;
    return RCC_OK;
  }
  RCCResult invert_output_written() {
    if (properties().invert_output) {
      out_true = false;
      out_false = true;
    } else {
      out_true = true;
      out_false = false;
    }
    return RCC_OK;
  }

  RCCResult run(bool) {
    if (input.opCode() == Short_timed_sampleSample_OPERATION) {
      size_t length = input.sample().data().size();
      const int16_t *inData = input.sample().data().data();
      bool *outData = reinterpret_cast<bool *>(output.sample().data().data());

      output.setOpCode(Bool_timed_sampleSample_OPERATION);
      output.sample().data().resize(length);

      for (size_t i = 0; i < length; i++) {
        if (*inData >= threshold) {
          *outData = out_true;
        } else {
          *outData = out_false;
        }

        inData++;
        outData++;
      }

      return RCC_ADVANCE;
    } else if (input.opCode() == Short_timed_sampleTime_OPERATION) {
      // Pass through time opcode and data
      output.setOpCode(Bool_timed_sampleTime_OPERATION);
      output.time().fraction() = input.time().fraction();
      output.time().seconds() = input.time().seconds();
      return RCC_ADVANCE;
    } else if (input.opCode() == Short_timed_sampleSample_interval_OPERATION) {
      // Pass through sample interval opcode and sample interval data
      output.setOpCode(Bool_timed_sampleSample_interval_OPERATION);
      output.sample_interval().fraction() = input.sample_interval().fraction();
      output.sample_interval().seconds() = input.sample_interval().seconds();
      return RCC_ADVANCE;
    } else if (input.opCode() == Short_timed_sampleFlush_OPERATION) {
      // Pass through flush opcode
      output.setOpCode(Bool_timed_sampleFlush_OPERATION);
      return RCC_ADVANCE;
    } else if (input.opCode() == Short_timed_sampleDiscontinuity_OPERATION) {
      // Pass through discontinuity opcode
      output.setOpCode(Bool_timed_sampleDiscontinuity_OPERATION);
      return RCC_ADVANCE;
    } else if (input.opCode() == Short_timed_sampleMetadata_OPERATION) {
      // Pass through metadata opcode, id, and data
      output.setOpCode(Bool_timed_sampleMetadata_OPERATION);
      output.metadata().id() = input.metadata().id();
      output.metadata().value() = input.metadata().value();
      return RCC_ADVANCE;
    } else {
      setError("Unknown OpCode received");
      return RCC_FATAL;
    }
  }
};

THRESHOLD_S_B_START_INFO
// Insert any static info assignments here (memSize, memSizes, portInfo)
// e.g.: info.memSize = sizeof(MyMemoryStruct);
THRESHOLD_S_B_END_INFO
