.. polyphase_interpolator_xs HDL worker

.. This file is protected by Copyright. Please refer to the COPYRIGHT file
   distributed with this source distribution.

   This file is part of OpenCPI <http://www.opencpi.org>

   OpenCPI is free software: you can redistribute it and/or modify it under the
   terms of the GNU Lesser General Public License as published by the Free
   Software Foundation, either version 3 of the License, or (at your option) any
   later version.

   OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>.

:orphan:

.. _polyphase_interpolator_xs-HDL-worker:


``polyphase_interpolator_xs`` HDL Worker
========================================

Detail
------
The requested output sample rate must not be above that of the platform clock rate.

This worker uses the :ref:`Polyphase Interpolator primitive <polyphase_interpolator-primitive>` for the calculations where interleaved calculations are acceptable (i.e. not sample rate limited), which should be the general case as the output can only send one sample per clock cycle.

.. ocpi_documentation_worker::

Utilisation
-----------
.. ocpi_documentation_utilization::
