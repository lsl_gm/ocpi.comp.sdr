-- repeater_b HDL implementation.
--
-- This file is protected by Copyright. Please refer to the COPYRIGHT file
-- distributed with this source distribution.
--
-- This file is part of OpenCPI <http://www.opencpi.org>
--
-- OpenCPI is free software: you can redistribute it and/or modify it under the
-- terms of the GNU Lesser General Public License as published by the Free
-- Software Foundation, either version 3 of the License, or (at your option) any
-- later version.
--
-- OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
-- WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
-- A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
-- more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
library ocpi;
use ocpi.types.all;

architecture rtl of worker is

  signal valid_data     : std_logic;    -- true when input is valid
  signal get_next_value : std_logic;    -- true when ready for next input value
  signal samples        : unsigned(7 downto 0);  -- times to repeat sample
  signal data_d         : std_logic_vector(7 downto 0);  -- registered input
  signal eom_d          : std_logic;    -- registered end of message signal
  signal data_eom       : std_logic;    -- sets output port eom signal
  signal output         : worker_output_out_t;

begin

  -- Request input when pipeline is ready for next value
  input_out.take <= output_in.ready and get_next_value;

  -- Test for valid input
  valid_data <= '1' when input_in.valid = '1' and
                input_in.opcode = bool_timed_sample_sample_op_e else '0';

  -- Sample counter
  samples_count_p : process(ctl_in.clk)
  begin
    if rising_edge(ctl_in.clk) then
      if ctl_in.reset = '1' then
        samples <= to_unsigned(0, samples'length);
        eom_d   <= '0';
      elsif output_in.ready = '1' then
        if get_next_value = '1' and valid_data = '1' then
          samples <= props_in.samples;
          eom_d   <= input_in.eom;
        elsif samples > 0 then
          samples <= samples - 1;
        end if;
      end if;
      -- No need to reset data_d
      if output_in.ready = '1' then
        if get_next_value = '1' and valid_data = '1' then
          data_d <= input_in.data;
        end if;
      end if;
    end if;
  end process;

  -- Can get a new input word when finished counting
  get_next_value <= '1' when samples = 0 else '0';

  -- When the last input was the EOM, hold the EOM flag until the last repeat
  -- for this input sample. Or when input is EOM and we are not repeating
  -- output EOM flag straight away.
  data_eom <= '1' when (samples = 1 and eom_d = '1') or (props_in.samples = 0 and input_in.eom = '1') else '0';

  -- Streaming interface output
  output_out.give        <= input_in.ready       when get_next_value = '1' else '1';
  output_out.valid       <= input_in.valid       when get_next_value = '1' else '1';
  output_out.data        <= input_in.data        when get_next_value = '1' else data_d;
  output_out.byte_enable <= input_in.byte_enable when get_next_value = '1' else (others => '1');
  output_out.opcode      <= input_in.opcode      when get_next_value = '1' else bool_timed_sample_sample_op_e;

  -- Pass eom from input with non-sample messages.
  -- Use delayed eom with sample messages.
  output_out.eom <= input_in.eom when get_next_value = '1' and valid_data = '0' else data_eom;

end rtl;
