#!/usr/bin/env python3

# Python implementation of FIR filter for verification.
#
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
# more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

"""Python implementation of fir filter scaled."""

import numpy

import opencpi.ocpi_testing as ocpi_testing


class FirFilter(ocpi_testing.Implementation):
    """Python implementation of fir filter scaled."""

    def __init__(self, taps, flush_length):
        """Initialise the class.

        Args:
            taps (list of int): Tap values.
            flush_length (int): Number of zero samples that should be inserted into the FIR filter on receipt of a flush opcode.
        """
        taps = [numpy.float32(tap) for tap in taps]
        super().__init__(taps=taps, flush_length=flush_length)

        self._filter_buffer_real = [numpy.float32(0.0)] * len(self.taps)
        self._filter_buffer_imaginary = [numpy.float32(0.0)] * len(self.taps)

    def reset(self):
        """Reset the state to the same state as at initialisation."""
        self._filter_buffer_real = [numpy.float32(0.0)] * len(self.taps)
        self._filter_buffer_imaginary = [numpy.float32(0.0)] * len(self.taps)

    def sample(self, input_):
        """Handle an incoming sample opcode.

        Args:
            input_ (list of complex float): The sample values on the input_ port.

        Returns:
            Formatted messages.
        """
        data = self._filter(input_)

        return self.output_formatter([{"opcode": "sample", "data": data}])

    def flush(self, input_):
        """Process an incoming message with the flush opcode.

        Args:
            input_: ignored.

        Returns:
            Array of message dictionaries.
        """
        if input_ is True:
            if self.flush_length > 0:
                flush_data = self._filter(
                    [numpy.float32(0.0)] * self.flush_length)

                return self.output_formatter([
                    {"opcode": "sample", "data": flush_data},
                    {"opcode": "flush", "data": None}])

            else:
                return self.output_formatter([
                    {"opcode": "flush", "data": None}])

    def _filter(self, input_):
        data = [complex(0.0, 0.0)] * len(input_)

        for index, value in enumerate(input_):
            self._filter_buffer_real = numpy.insert(
                self._filter_buffer_real, 0, numpy.float32(value.real))
            self._filter_buffer_imaginary = numpy.insert(
                self._filter_buffer_imaginary, 0, numpy.float32(value.imag))
            self._filter_buffer_real = self._filter_buffer_real[0:-1]
            self._filter_buffer_imaginary = self._filter_buffer_imaginary[0:-1]

            output_real = numpy.float32(0.0)
            output_imaginary = numpy.float32(0.0)
            for tap, sample_real, sample_imaginary in zip(
                    self.taps, self._filter_buffer_real,
                    self._filter_buffer_imaginary):
                output_real = output_real + tap * sample_real
                output_imaginary = output_imaginary + tap * sample_imaginary

            data[index] = complex(numpy.float32(output_real),
                                  numpy.float32(output_imaginary))

        return data
