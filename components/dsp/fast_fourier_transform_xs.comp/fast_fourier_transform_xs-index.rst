.. fast_fourier_transform_xs documentation

.. This file is protected by Copyright. Please refer to the COPYRIGHT file
   distributed with this source distribution.

   This file is part of OpenCPI <http://www.opencpi.org>

   OpenCPI is free software: you can redistribute it and/or modify it under the
   terms of the GNU Lesser General Public License as published by the Free
   Software Foundation, either version 3 of the License, or (at your option) any
   later version.

   OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>.


.. _fast_fourier_transform_xs:


Fast Fourier Transform (``fast_fourier_transform_xs``)
======================================================
Converts a sequence of samples from time to frequency domain using a Fast Fourier Transform, or vice versa using an inverse Fast Fourier Transform.

Design
------

The intention of Fourier Analysis is to convert the samples which are in the time domain into the frequency domain. A Fast Fourier Transform (FFT) is an algorithm that calculates the Discrete Fourier Transform (DFT) of a sequence of samples in an extremely time efficient manner compared to a general DFT approach. However, a constraint of the FFT's efficiency is that the length of the computational frames is a power of 2. The Inverse FFT (IFFT) is the opposite of this transformation and converts from the frequency domain back into the time domain.

The FFT component is broken down into 3 key areas: Sample to FFT Frame Creator, FFT processing core and optional Metadata Inserter. See the image below for a pictorial representation of this break down.

.. _fast_fourier_transform_xs-design-diagram:

.. figure:: ./fft_design_diagram.svg
   :align: center

Sample to FFT Frame Creator
~~~~~~~~~~~~~~~~~~~~~~~~~~~

The Sample to FFT Frame Creator takes the incoming data stream (in the protocol defined by the worker) and converts that into the N number of FFT Frames which are then parsed into the FFT Core. The length of each FFT Frame is defined by the FFT Length property, ``fft_length``.

FFT Processing Core
~~~~~~~~~~~~~~~~~~~

The FFT Processing core can perform either a forward or inverse Fourier Transform (dubbed FFT and IFFT respectively), using a Radix-2 method. The different implementations are selected via the ``inverse_fft`` flag detailed in interface and properties sections.

Both the FFT and IFFT Processing Cores are made up of 3 main elements: a Bit Reversal, Butterfly algorithm and then a final Bit Reversal before the algorithm is complete. The sections below will go into further detail about each of the stages.

The following information details the FFT algorithm:

Bit Reversal Stages
^^^^^^^^^^^^^^^^^^^

The bit reversal stages are identical for both the FFT and the IFFT. The purpose of this stage is to reorder the input samples by performing a bit endianness swap of the indices before placing the sample in its newly defined position.

The table below shows an example of what the indices bit endianness swap looks like:

+-----+----+-----+
| 000 | -> | 000 |
+-----+----+-----+
| 001 | -> | 100 |
+-----+----+-----+
| 010 | -> | 010 |
+-----+----+-----+
| 011 | -> | 110 |
+-----+----+-----+
| 100 | -> | 001 |
+-----+----+-----+
| 101 | -> | 101 |
+-----+----+-----+
| 110 | -> | 011 |
+-----+----+-----+
| 111 | -> | 111 |
+-----+----+-----+

This process is performed on all samples before and after the butterfly process which is detailed in the next section.

Butterfly Stage
^^^^^^^^^^^^^^^

There are two slightly different variations of the butterfly stage, one for the FFT and another for the IFFT. The sections below will detail the differences between the two stages.

Fast Fourier Transform
^^^^^^^^^^^^^^^^^^^^^^

The FFT version of the radix-2 butterfly algorithm rearranges the sample data into two parts, one part is the even indexed samples denoted by the equation :math:`X_{2m}` and the other is the odd indexed samples which is denoted by :math:`X_{2m+1}`.
A common multiplier :math:`e^{{- {2\pi}i \over N}k}` called a Twiddle Factor is applied to the even and odd samples, where; :math:`i` denotes a unit complex number, the k element denotes the index, so for the even case :math:`(2m)k` is used and for the odd case :math:`(2m + 1)k` is used.
The complete equation that is computed during this stage is detailed below:

:math:`X_k = \sum_{m = 0}^{(N/2) - 1} X_{2m}e^{-{2\pi}i \over N}(2m)k + \sum_{m = 0}^{(N/2) - 1} X_{2m+1}e^{-{2\pi}i \over N}(2m+1)k`

Inverse Fast Fourier Transform
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The IFFT version of the radix-2 butterfly algorithm is almost identical to the FFT radix-2 butterfly algorithm. There are only two minor differences and that is the scaling of both the odd and even parts of the equation by :math:`1 \over N` and adjusting the Twiddle Factor to :math:`e^{{{2\pi}i \over N}k}`.
The complete equation that is computed during this stage is detailed below:

:math:`X_k = {1 \over N} * \sum_{m = 0}^{(N/2) - 1} X_{2m}e^{{2\pi}i \over N}(2m)k + {1 \over N} * \sum_{m = 0}^{(N/2) - 1} X_{2m+1}e^{{2\pi}i \over N}(2m+1)k`

Gain Factor
~~~~~~~~~~~

The gain factor allows for the scaling of input values. Due to the multiplication of twiddle factors, and summation of FFT terms, the output of the FFT has additional bits compared to the input. As such it is useful to be able to adjust the input gain to control for overflow/saturation, or detection of small signals.

The gain factor is stored as an unsigned UQ8.8 fixed point value, which can be converted to a floating point number by dividing by :math:`2^8`.

If the output of the gain factor multiplied by the input data is greater than can be stored within a complex short value (i.e. 16 bit real, 16 bit imaginary), then it is clipped to the limits of this range. This effect will lead to spurious values being outputted by the FFT, the severity of this depends on the occurrence of clipped values within the whole FFT frame.

Metadata Inserter
~~~~~~~~~~~~~~~~~
The Metadata Inserter provides a user modifiable metadata message that allows downstream modules to distinguish between each FFT frame. This metadata message is sent just before the corresponding FFT frame is sent to the output.

The user has the option to turn this feature on and off via the ``indicate_start_of_fft_block`` flag.

If the ``indicate_start_of_fft_block`` flag is set to true then the FFT module expects the ``start_of_fft_block_id`` and ``start_of_fft_block_value`` to be set to a valid value within the defined ranges detailed in the Properties section.

Note: By default the ``start_of_fft_block_id`` and ``start_of_fft_block_value`` properties are set to 0.

An additional option is available called ``increment_fft_block_value``. If this is set to true then before each FFT frame is processed the associated metadata message will have the ``start_of_fft_block_value`` incremented by 1. The 64 bit value will be treated as an unsigned, and so will wrap back to 0 on reaching the maximum value.

Internal Data Flow
~~~~~~~~~~~~~~~~~~
The internal data flow follows the states below:

**Wait for Data**

   * Wait until new input samples are received.

   * Once a sample message is received, add it to the input data buffer, (this buffer has a minimum size of a single FFT length).

   * Once input data buffer contains ``fft_length`` samples, proceed to **Processing Samples**. Otherwise wait for more data to arrive.

**Processing Samples**

   * Take ``fft_length`` samples from the input data buffer.

   * If ``indicate_start_of_fft_block`` is true move to **Metadata Inserter**, (continue with next line once completed).

   * Apply the ``gain_factor`` to the FFT Frame.


   * FFT Core will perform Bit Reversal and Butterfly Algorithm stages defined in the FFT Processing Core section.


   * Send FFT Frame to output port.

   * If the input data buffer still contains at least ``fft_length`` of samples run **Processing Samples** again, otherwise return to **Wait for Data**.

**Metadata Inserter**

   * Set Metadata ID field to ``start_of_fft_block_id``.

   * Set Metadata Value field to ``start_of_fft_block_value``.

   * Output Metadata.

   * If ``increment_fft_block_value`` is true

      * Increment ``start_of_fft_block_value`` by 1 ready for the next FFT process notification.

      * Once the ``start_of_fft_block_value`` has saturated wrap round to 0.

   * Return to **Processing Samples**.

Interface
---------
.. literalinclude:: ../specs/fast_fourier_transform_xs-spec.xml
   :language: xml
   :lines: 1,19-

Opcode Handling
~~~~~~~~~~~~~~~

.. _fast_fourier_transform_xs_Opcode_Handling_Time:

* Time

   * Resynchronises the internal input sample time.

   * This value is used, along with the incoming sample_interval to calculate the output sample time.

   * If the worker has partly populated a FFT frame with samples and a time opcode is received:

      * The current FFT frame is completed (through additional sample messages on the input port) and outputted.

      * The time is advanced by the sample_interval for each sample between the time being received, and the end of the FFT Frame. It is forwarded to the output in the order shown in the :ref:`Opcode Output Ordering Sequence <fast_fourier_transform_xs_Opcode_Output_Ordering_Sequence>`.

   * Otherwise:

      * Forward time to the output port.

   * **Notes:**

      * If a sample_interval has not been received, the time for the next outputted symbol cannot be calculated. In this case, the latest time received is not advanced, resulting in a slight inaccuracy in the downstream time.

* Sample Interval

   * Updates internally stored Sample Interval.

   * **Notes:**

      * This stored Sample Interval value gets applied to the time as specified in the :ref:`Opcode Handling Time Section <fast_fourier_transform_xs_Opcode_Handling_Time>` above.

   * If the worker has partly populated a FFT frame with samples and a sample interval opcode is received:

      * Completes any partially filled sample buffer by adding zero-valued samples, forwarding the complete FFT frame to the output.

      * Reset sample buffer.

      * Inserts a Flush opcode and forwards Sample Interval Opcode to the output in the order defined in: the :ref:`Opcode Output Ordering Sequence <fast_fourier_transform_xs_Opcode_Output_Ordering_Sequence>`.

   * Otherwise:

      * Forward Sample Interval to the output port.

* Flush

   * Completes any partially filled sample buffer by adding zero-valued samples, forwarding the complete FFT frame to the output.

   * Reset sample buffer.

   * Forward Flush to the output port in the order defined in the :ref:`Opcode Output Ordering Sequence <fast_fourier_transform_xs_Opcode_Output_Ordering_Sequence>`.

* Discontinuity

   * If the worker has partly populated a FFT frame with samples and a discontinuity opcode is received:

      * Forward any stored non-sample opcodes to the output in the order defined in the :ref:`Opcode Output Ordering Sequence <fast_fourier_transform_xs_Opcode_Output_Ordering_Sequence>`.

      * Reset input buffer.

      * Forwards the Discontinuity opcode.

   * Otherwise:

      * Reset input buffer.

      * Forward Discontinuity to the output port.

* Metadata (incoming message)

   * If the worker has partly populated an FFT frame with samples and a metadata opcode is received:

      * The metadata is pushed into the metadata FIFO buffer.

      * The input port continues processing other messages.

      * Once the FFT Frame processing has been completed, forward Metadata Opcodes to the output in the order defined in the :ref:`Opcode Output Ordering Sequence <fast_fourier_transform_xs_Opcode_Output_Ordering_Sequence>`.

   * Otherwise:

      * Forward metadata to the output port.

* Samples

   * Optionally output metadata message before each FFT frame, (if ``indicate_start_of_fft_block`` is true).

   * Apply FFT algorithm to these samples and send data to the output port.

   * Send on any other messages buffered during the FFT Frame, in the order defined in the :ref:`Opcode Output Ordering Sequence <fast_fourier_transform_xs_Opcode_Output_Ordering_Sequence>`.

**Note:** When an End-of-File (EOF) indication is received, if the worker has an FFT frame partly populated with samples, the worker completes it by adding zero-valued samples and forwards the complete FFT frame to the output.

.. _fast_fourier_transform_xs_Opcode_Output_Ordering_Sequence:

Opcode Output Ordering Sequence
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Upon completion of a FFT frame any opcodes that have been received or generated in response to received opcodes during the processing of the FFT frame will be sent to the output port in the order specified below:

   1. Flush Opcode (First)

   2. Sample Interval Opcode

   3. Time Opcode

   4. Metadata Opcode

   5. Discontinuity Opcode (Last)

**Notes:**

   * The property ``indicate_start_of_fft_block`` can be used to insert a metadata message before the FFT output is sent to the output port. This does not alter the ordering of any metadata messages received while the FFT frame is partially filled.

Properties
~~~~~~~~~~
.. ocpi_documentation_properties::

   fft_length: This value must be a power of 2.

   start_of_fft_block_value: This sets an internal counter, and reading this property will only return the value set and not the current internal value.

   indicate_start_of_fft_block: This metadata opcode will be output immediately before a Sample opcode and after any Time opcode.

   gain_factor: A UQ8.8 value to apply to the sample input. The default is 256 which is no gain (unity).

   metadata_fifo_size: The maximum number of metadata messages that will be held while a FFT frame is processed. If the maximum size is reached the oldest metadata message will be dropped.

Ports
~~~~~
.. ocpi_documentation_ports::

   input: Primary input samples port.
   output: Primary output samples port.

Implementations
---------------
.. ocpi_documentation_implementations:: ../fast_fourier_transform_xs.rcc ../fast_fourier_transform_xs.hdl

Example Application
-------------------
The example application can be used in combination with the following python code snippets to produce the output shown in :numref:`fast_fourier_transform_xs-example_app-diagram`.

.. spelling:word-list::
   py
   app
   xml

.. code-block:: python
   :name: input_gen.py
   :caption: input_gen.py - Generates `input.bin`

   # Generate input samples for use with the FFT Example application
   import numpy as np
   import ocpi_protocols

   amplitudes =  [2**10, 2**9, 2**8]   # Amplitudes to generate
   frequencies = [20/64, 10/64, 30/64] # Frequencies to generate
   time = np.arange(64)  # The sampling rate, therefore 1 FFT frame
   data = np.zeros(len(time), dtype=np.complex128)
   for a,f in zip(amplitudes, frequencies):
      data += a * np.exp(2j * np.pi * f * time)
   messages = [{"opcode" : "sample", "data" : data}]
   with ocpi_protocols.WriteMessagesFile("input.bin",
                                       "complex_short_timed_sample") as file_id:
      file_id.write_dict_messages(messages)

.. literalinclude:: example_app.xml
   :name: example_app.xml
   :caption: example_app.xml
   :language: xml
   :lines: 1,19-

.. code-block:: python
   :name: show_output.py
   :caption: show_output.py - Processes `output.bin`

   # Saves the output of the FFT Example app to example_app_output.svg
   import numpy as np
   import matplotlib.pyplot as plt
   import ocpi_protocols

   fs = 64
   with ocpi_protocols.ParseMessagesFile("output.bin",
                                       "complex_short_timed_sample") as opcodes:
      for f in opcodes.get_all_messages():
         if f["opcode"] == "sample":
               print("Saving magnitude waveform to example_app_output.svg")
               plt.plot(np.fft.fftfreq(fs, d=1/fs), np.absolute(f["data"]))
               plt.title("Magnitude plot of FFT")
               plt.savefig("example_app_output.svg")
         else:
               print(f)


.. _fast_fourier_transform_xs-example_app-diagram:

.. figure:: ./example_app_output.svg
   :alt: Output from Example application.
   :align: center

   Output from Example application.

Dependencies
------------
The dependencies to other elements in OpenCPI are:

 * :ref:`Metadata inserter primitive v2 <metadata_inserter_v2-primitive>`

 * :ref:`Protocol interface delay primitive v2 <protocol_interface_delay_v2-primitive>`

 * :ref:`Protocol interface FIFO primitive v2 <protocol_interface_fifo_v2-primitive>`

 * :ref:`Timestamp recovery primitive <timestamp_recovery-primitive>`

 * ``components/dsp/common/fft/fft_core.hh``

 * ``components/math/common/time_utils.hh``

There is also a dependency on:

 * ``ieee.std_logic_1164``

 * ``ieee.numeric_std``

 * ``ieee.math_real``

External Prerequisites
~~~~~~~~~~~~~~~~~~~~~~
Both the RCC and HDL implementations make use of external prerequisites, which are installed through the worker ``ocpidev build`` command, these in turn call the relevant scripts in the ``prerequisites/`` directory.

These scripts are designed to automatically pull the open-source repositories required. However if a local mirror is to be used instead this can be specified through the environmental variables: ``KISS_FFT_URL`` and ``ZIPCPU_FFT_URL`` for RCC and HDL respectively. Alternatively, the source code can be extracted to ``components/dsp/common/fft/kissfft`` and ``components/dsp/common/fft/dblclockfft`` respectively.


Limitations
-----------
Limitations of ``fast_fourier_transform_xs`` are:

 * The input to the FFT is clipped to signed 16 bit, therefore large values being calculated within the FFT core is likely to generate spurious signals on the output.

 * The minimum ``fft_length`` that has been tested and produces the desired output is 2.

 * Metadata messages are buffered in a FIFO if received during an FFT frame. The number of metadata messages that can be buffered at any one time is limited by the length of the FIFO determined by the property ``metadata_fifo_size``. When the FIFO becomes full the component will drop the oldest metadata message first.

 * The HDL variant makes use of a FIFO for the outgoing opcodes, the size of this FIFO can be set through the HDL only property ``opcode_fifo_depth``.

Testing
-------
.. ocpi_documentation_test_platforms::

.. ocpi_documentation_test_result_summary::
