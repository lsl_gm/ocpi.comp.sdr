#!/usr/bin/env python3

# Python implementation of moving average filter for testing
#
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
# more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

"""Python implementation of moving average filter."""

import opencpi.ocpi_testing as ocpi_testing


class MovingAverageFilter(ocpi_testing.Implementation):
    """Python implementation of moving average filter."""

    def __init__(self, moving_average_size, flush_length):
        """Initialise the class.

        Args:
            moving_average_size (ushort): Size of moving average window.
            flush_length (uchar): Number of zero samples that should be inserted into the moving average filter on receipt of a flush opcode.
        """
        super().__init__(moving_average_size=moving_average_size,
                         flush_length=flush_length)

        self._buffer = [0] * self.moving_average_size
        self._next_buffer_write_index = 0
        self._threshold = int(self.moving_average_size / 2)

    def reset(self):
        """Reset the state to the same state as at initialisation."""
        for i in range(self.moving_average_size):
            self._buffer[i] = 0
        self._next_buffer_write_index = 0

    def sample(self, data):
        """Handle an incoming sample opcode.

        Args:
            data (list of boolean): The sample values on the input port.

        Returns:
            Formatted messages.
        """
        output_values = [0] * len(data)
        for index, value in enumerate(data):
            self._buffer[self._next_buffer_write_index] = value
            self._next_buffer_write_index = (
                self._next_buffer_write_index + 1) % self.moving_average_size

            if sum(self._buffer) > self._threshold:
                output_values[index] = True
            else:
                output_values[index] = False

        return self.output_formatter(
            [{"opcode": "sample", "data": output_values}])

    def flush(self, *inputs):
        """Process an incoming message with the flush opcode.

        Args:
            inputs (list): ignored

        Returns:
            Array of message dictionaries
        """
        if self.flush_length > 0:
            flush_data = [0] * self.flush_length

            # As stream() will return a named tuple, with the names the output
            # port names - and moving average filter only has one output port
            # called "output" get the messages from that port. Then add the
            # flush message as this will be passed through after the flushed
            # data.
            resulting_messages = self.sample(flush_data).output
            resulting_messages.append({"opcode": "flush", "data": None})

            return self.output_formatter(resulting_messages)
        else:
            return self.output_formatter([{"opcode": "flush", "data": None}])
