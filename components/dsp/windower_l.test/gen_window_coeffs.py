#!/usr/bin/env python3

# Generates the coefficients for windower testing
#
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
# more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

"""Generates the window coefficients for windower_s testing."""

import os
import sys
import numpy as np
import math


def blackman_harris_value(N, n):
    """Returns Blackman-Harris window values.

    Args:
        N (int): window length
        n (int): current coefficient

    Returns:
        calculated coefficient
    """
    a0 = 0.35875
    a1 = 0.48829
    a2 = 0.14128
    a3 = 0.01168
    return a0 - (a1 * math.cos((2*math.pi*n)/N)) + \
        (a2 * math.cos((4*math.pi*n)/N)) - \
        (a3 * math.cos((6*math.pi*n)/N))


def hann_value(N, n):
    """Returns Hann window values.

    Args:
        N (int): window length
        n (int): current coefficient

    Returns:
        calculated coefficient
    """
    return (math.sin((math.pi * n) / N)) ** 2


def hamming_value(N, n):
    """Returns Hamming window values.

    Args:
        N (int): window length
        n (int): current coefficient

    Returns:
        calculated coefficient
    """
    a0 = 0.53836
    a1 = 1-a0
    return a0 - (a1 * math.cos((2*math.pi*n)/N))


def main():
    """Fetches the environment values and fetches the correct window."""
    # Validate and grab data from env and args
    if len(sys.argv) < 2:
        print("Output file argument missing")
        sys.exit(1)
    output_filename = sys.argv[1]
    window_length = int(os.environ.get("OCPI_TEST_window_length"))
    window_type = os.environ.get("OCPI_TEST_window_type").lower()
    if not window_type in ["hann", "blackman_harris", "hamming"]:
        print(f"Invalid window type specified {window_type}")
        sys.exit(1)
    # Calculate window
    if window_type == "hann":
        value_function = hann_value
    elif window_type == "hamming":
        value_function = hamming_value
    else:
        value_function = blackman_harris_value
    window = []
    max_value = np.iinfo(np.uint32).max
    for counter in range(0, window_length):
        new_value = int(max_value * value_function(window_length, counter))
        if new_value > max_value:
            new_value = max_value
        window.append(new_value)
    # Save window to output file
    output_file = open(output_filename, "w")
    for window_value in window:
        output_file.write("".join(str(window_value)+"\n"))
    output_file.close()
    sys.exit(0)


if __name__ == "__main__":
    main()
