// RCC implementation of cic_interpolator_xs worker.
//
// This file is protected by Copyright. Please refer to the COPYRIGHT file
// distributed with this source distribution.
//
// This file is part of OpenCPI <http://www.opencpi.org>
//
// OpenCPI is free software: you can redistribute it and/or modify it under the
// terms of the GNU Lesser General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option) any
// later version.
//
// OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
// more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#include <vector>
#include "../common/cic/cic_core.hh"
#include "cic_interpolator_xs-worker.hh"
#include "../../math/common/time_utils.hh"
#include "OcpiDebugApi.hh"

using namespace OCPI::RCC;  // for easy access to RCC data types and constants
using namespace Cic_interpolator_xsWorkerTypes;

static_assert(CIC_INTERPOLATOR_XS_CIC_REGISTER_SIZE <= 64,
              "cic_register_size must not be more than 64");

class Cic_interpolator_xsWorker : public Cic_interpolator_xsWorkerBase {
  uint8_t cic_order = CIC_INTERPOLATOR_XS_CIC_ORDER;
  uint8_t cic_differential_delay = CIC_INTERPOLATOR_XS_CIC_DIFFERENTIAL_DELAY;

  uint16_t up_sample_factor = 0;
  uint8_t scale_output = 0;
  uint16_t flush_length = (cic_order * cic_differential_delay) - 1;

  cic_core<Complex_short_timed_sampleSampleData> cic{cic_order,
                                                     cic_differential_delay};
  static const size_t output_buffer_size =
      CIC_INTERPOLATOR_XS_OCPI_MAX_BYTES_OUTPUT /
      sizeof(Complex_short_timed_sampleSampleData);

  // Zero filled buffer for flush opcode.
  std::vector<Complex_short_timed_sampleSampleData> zero_input;
  RCCBoolean output_flush_opcode = false;
  RCCBoolean data_received = false;

  void reset() {
    this->cic.reset();
    this->data_received = false;
    this->output_flush_opcode = false;
  }

  RCCResult stop() {
    this->cic.reset();
    return RCC_OK;
  }

  // Notification that up_sample_factor property has been written
  RCCResult up_sample_factor_written() {
    if (properties().up_sample_factor == 0) {
      setError("up sample factor must be greater than 0");
      return RCC_FATAL;
    }

    this->up_sample_factor = properties().up_sample_factor;
    this->cic.set_up_sample_factor(this->up_sample_factor);

    RCCDouble required_register_size =
        16 + log2(pow(this->up_sample_factor *
                          CIC_INTERPOLATOR_XS_CIC_DIFFERENTIAL_DELAY,
                      CIC_INTERPOLATOR_XS_CIC_ORDER) /
                  this->up_sample_factor);
    if (required_register_size > CIC_INTERPOLATOR_XS_CIC_REGISTER_SIZE) {
      log(OCPI_LOG_BAD,
          "Current cic_register_size is not large enough to support the "
          "current up_sample_factor, cic_differential_delay and cic_order");
    }

    return RCC_OK;
  }

  // Notification that scale_output property has been written
  RCCResult scale_output_written() {
    this->cic.set_scale_output(properties().scale_output);
    return RCC_OK;
  }

  RCCResult run(bool) {
    if (input.opCode() == Complex_short_timed_sampleSample_OPERATION) {
      size_t input_length = input.sample().data().size();
      const Complex_short_timed_sampleSampleData *inputData =
          input.sample().data().data();
      Complex_short_timed_sampleSampleData *outputData;
      output.setOpCode(Complex_short_timed_sampleSample_OPERATION);
      outputData = output.sample().data().data();
      size_t output_length = 0;
      this->data_received = true;

      if (this->cic.do_interpolator_work(inputData, input_length, outputData,
                                         &output_length, output_buffer_size)) {
        input.advance();
      }
      // Pass through processed output data
      output.sample().data().resize(output_length);
      output.advance();
      return RCC_OK;
    } else if (input.opCode() == Complex_short_timed_sampleTime_OPERATION) {
      // Pass through time opcode and time data
      output.setOpCode(Complex_short_timed_sampleTime_OPERATION);
      output.time().fraction() = input.time().fraction();
      output.time().seconds() = input.time().seconds();
      return RCC_ADVANCE;
    } else if (input.opCode() ==
               Complex_short_timed_sampleSample_interval_OPERATION) {
      // Calculate output sample interval as input interval / interpolation
      // factor
      uint32_t output_interval_seconds = input.sample_interval().seconds();
      uint64_t output_interval_fraction = input.sample_interval().fraction();

      time_utils::divide(&output_interval_seconds, &output_interval_fraction,
                         this->up_sample_factor);

      output.setOpCode(Complex_short_timed_sampleSample_interval_OPERATION);
      output.sample_interval().seconds() = output_interval_seconds;
      output.sample_interval().fraction() = output_interval_fraction;
      return RCC_ADVANCE;
    } else if (input.opCode() == Complex_short_timed_sampleFlush_OPERATION) {
      if (this->output_flush_opcode || this->flush_length == 0) {
        // Pass through flush opcode
        output.setOpCode(Complex_short_timed_sampleFlush_OPERATION);
        this->output_flush_opcode = false;
        return RCC_ADVANCE;
      }

      if (this->zero_input.size() != this->flush_length) {
        this->zero_input.resize(this->flush_length,
                                Complex_short_timed_sampleSampleData());
      }

      output.setOpCode(Complex_short_timed_sampleSample_OPERATION);
      Complex_short_timed_sampleSampleData *outputData =
          output.sample().data().data();
      size_t output_length = 0;
      if (this->cic.do_interpolator_work(zero_input.data(), this->flush_length,
                                         outputData, &output_length,
                                         output_buffer_size)) {
        this->output_flush_opcode = true;
      }
      output.sample().data().resize(output_length);
      output.advance();
      return RCC_OK;
    } else if (input.opCode() ==
               Complex_short_timed_sampleDiscontinuity_OPERATION) {
      // Reset cic core (reset history).
      this->reset();
      // Pass through discontinuity opcode
      output.setOpCode(Complex_short_timed_sampleDiscontinuity_OPERATION);
      return RCC_ADVANCE;
    } else if (input.opCode() == Complex_short_timed_sampleMetadata_OPERATION) {
      // Pass through metadata opcode, id, and data
      output.setOpCode(Complex_short_timed_sampleMetadata_OPERATION);
      output.metadata().id() = input.metadata().id();
      output.metadata().value() = input.metadata().value();
      return RCC_ADVANCE;
    } else {
      setError("Unknown OpCode Received");
      return RCC_FATAL;
    }
  }
};

CIC_INTERPOLATOR_XS_START_INFO
// Insert any static info assignments here (memSize, memSizes, portInfo)
// e.g.: info.memSize = sizeof(MyMemoryStruct);
CIC_INTERPOLATOR_XS_END_INFO
