#!/usr/bin/env python3

# Generates taps for fir_filter_scaled_s testing
#
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
# more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

"""Generates the taps for fir_filter_scaled_xs testing."""

import os
import argparse


def csv_split(string):
    """Split a csv format string.

    Args:
        string (string): Input csv string

    Returns:
        list of string: string split at comma
    """
    return string.split(",")


def parse_args():
    """Generate the arguments required.

    Returns:
        argument parser objects: taps and file path arguments
    """
    parser = argparse.ArgumentParser(description="Writes fir taps to a file")
    parser.add_argument("-t", "--taps", type=csv_split, required=True,
                        help="sets the taps explicitly, e.g., \"--taps=-1,0,1,2,...\"")
    parser.add_argument("file_path", type=str,
                        help="File path to save generated taps")
    arguments = parser.parse_args()
    return arguments.taps, arguments.file_path


def taps_to_file(taps, file_path):
    """Write the taps to a file.

    Args:
        taps (list of int): filter taps
        file_path (string): output file path
    """
    taps_file = open(file_path, "w")
    for val in taps:
        taps_file.write(val + "\n")
    taps_file.close()


def main():
    """Main code execution for generating taps."""
    taps, file_path = parse_args()
    taps_to_file(taps, file_path)


if __name__ == "__main__":
    main()
