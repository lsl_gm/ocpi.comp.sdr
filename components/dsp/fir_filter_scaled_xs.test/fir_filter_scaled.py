#!/usr/bin/env python3

# Python implementation of scaled FIR filter for testing
#
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
# more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

"""Python implementation of fir filter scaled."""

import opencpi.ocpi_testing as ocpi_testing
import numpy
import scipy.signal


class FirFilterScaled(ocpi_testing.Implementation):
    """Python implementation of fir filter scaled."""

    def __init__(self, taps, scale_factor, flush_length):
        """Initialise the class.

        Args:
            taps (list of int): Tap values.
            scale_factor (int): Divide the output of the FIR using this factor.
            flush_length (int): Number of zero samples that should be inserted into the FIR filter on receipt of a flush opcode message.
        """
        super().__init__(taps=taps, scale_factor=scale_factor,
                         flush_length=flush_length)

        self._filter_buffer_real = [0] * len(self.taps)
        self._filter_buffer_imag = [0] * len(self.taps)
        self._max_message_samples = 4096

    def reset(self):
        """Reset the state to the same state as at initialisation."""
        self._filter_buffer_real = [0] * len(self.taps)
        self._filter_buffer_imag = [0] * len(self.taps)

    def sample(self, input_):
        """Handle an incoming sample opcode.

        Args:
            input_ (list of complex): The sample values on the input_ port.

        Returns:
            Formatted messages.
        """
        input_data = numpy.array(input_)
        output_real = self._filter_real(numpy.real(input_data))
        output_imag = self._filter_imag(numpy.imag(input_data))
        output = list(
            numpy.array(output_real) + (numpy.array(output_imag) *
                                        complex(0, 1)))

        messages = [{
            "opcode": "sample",
            "data": output[index: index + self._max_message_samples]}
            for index in range(0, len(output),
                               self._max_message_samples)]
        return self.output_formatter(messages)

    def flush(self, input_):
        """Process an incoming message with the flush opcode.

        Args:
            input_: ignored.

        Returns:
            Array of message dictionaries.
        """
        if self.flush_length > 0:
            flush_data = [0] * self.flush_length
            messages = self.sample(flush_data).output
            messages.append({"opcode": "flush", "data": None})
            return self.output_formatter(messages)
        else:
            return self.output_formatter([
                {"opcode": "flush", "data": None}])

    def _filter_real(self, values):
        """Perform filtering on the real sample data.

        Args:
            values (list of int): Input sample messages.

        Returns:
            list of short: Filtered sample messages.
        """
        output = [0] * len(values)
        for index, value in enumerate(values):
            # Append value to end since scipy.signal.lfilter() does a "direct
            # II transpose" implementation, meaning tap "0" is multiplied by
            # value "N", tap "1" is multiplied by value "N-1", etc.
            self._filter_buffer_real = scipy.append(self._filter_buffer_real,
                                                    value)[1:]
            filter_result = int(scipy.signal.lfilter(
                self.taps, 1.0, self._filter_buffer_real)[-1])
            if self.scale_factor == 0:
                output[index] = numpy.int16(filter_result)
            else:
                shifted_result = numpy.right_shift(
                    filter_result, self.scale_factor - 1) + 1
                output[index] = numpy.int16(
                    numpy.right_shift(shifted_result, 1))
        return output

    def _filter_imag(self, values):
        """Perform filtering on the imaginary sample data.

        Args:
            values (list of int): Input sample messages.

        Returns:
            list of int: Filtered sample messages.
        """
        output = [0] * len(values)
        for index, value in enumerate(values):
            # Append value to end since scipy.signal.lfilter() does a "direct
            # II transpose" implementation, meaning tap "0" is multiplied by
            # value "N", tap "1" is multiplied by value "N-1", etc.
            self._filter_buffer_imag = scipy.append(self._filter_buffer_imag,
                                                    value)[1:]
            filter_result = int(scipy.signal.lfilter(
                self.taps, 1.0, self._filter_buffer_imag)[-1])
            if self.scale_factor == 0:
                output[index] = numpy.int16(filter_result)
            else:
                shifted_result = numpy.right_shift(
                    filter_result, self.scale_factor - 1) + 1
                output[index] = numpy.int16(
                    numpy.right_shift(shifted_result, 1))
        return output
