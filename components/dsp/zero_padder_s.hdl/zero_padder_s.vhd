-- zero_padder_s HDL implementation
--
-- This file is protected by Copyright. Please refer to the COPYRIGHT file
-- distributed with this source distribution.
--
-- This file is part of OpenCPI <http://www.opencpi.org>
--
-- OpenCPI is free software: you can redistribute it and/or modify it under the
-- terms of the GNU Lesser General Public License as published by the Free
-- Software Foundation, either version 3 of the License, or (at your option) any
-- later version.
--
-- OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
-- WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
-- A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
-- more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
library ocpi;
use ocpi.types.all;

architecture rtl of worker is

  signal valid_data     : std_logic;    -- true when input is valid
  signal get_next_value : std_logic;    -- true when ready for next input value
  signal samples        : unsigned(7 downto 0);  -- amount of zero's to pad
  signal eom_d          : std_logic;    -- registered end of message signal
  signal data_eom       : std_logic;    -- true when EOM should be inserted

begin

  -- Take input when both input and output ports are ready
  input_out.take <= output_in.ready and get_next_value;

  -- Test for valid input
  valid_data <= '1' when output_in.ready = '1' and get_next_value = '1' and input_in.valid = '1' and
                input_in.opcode = short_timed_sample_sample_op_e else '0';

  -- Sample counter
  samples_count_p : process(ctl_in.clk)
  begin
    if rising_edge(ctl_in.clk) then
      if ctl_in.reset = '1' then
        samples <= to_unsigned(0, samples'length);
        eom_d   <= '0';
      else
        if valid_data = '1' then
          samples <= props_in.samples;
          eom_d   <= input_in.eom;
        elsif output_in.ready = '1' and samples > to_unsigned(0, samples'length) then
          samples <= samples - 1;
        end if;
      end if;
    end if;
  end process;

  get_next_value <= '1' when samples = to_unsigned(0, samples'length) else '0';

  -- When the last input was the EOM, hold the EOM flag until the last repeat
  -- for this input sample. Or when input is EOM and we are not repeating
  -- output EOM flag straight away.
  data_eom <= '1' when (samples = 1 and eom_d = '1') or (props_in.samples = 0 and input_in.eom) else '0';

  -- Streaming interface output
  output_out.give        <= input_in.ready and get_next_value;
  output_out.valid       <= (get_next_value and input_in.valid) or not get_next_value;
  output_out.data        <= input_in.data        when get_next_value = '1' else (others => '0');
  output_out.byte_enable <= input_in.byte_enable when get_next_value = '1' else (others => '1');
  output_out.opcode      <= input_in.opcode      when get_next_value = '1' else short_timed_sample_sample_op_e;
  output_out.eom         <= input_in.eom         when get_next_value
                            and not (input_in.valid = '1' and input_in.opcode = short_timed_sample_sample_op_e) else data_eom;

end rtl;
