.. zero_padder_s documentation

.. This file is protected by Copyright. Please refer to the COPYRIGHT file
   distributed with this source distribution.

   This file is part of OpenCPI <http://www.opencpi.org>

   OpenCPI is free software: you can redistribute it and/or modify it under the
   terms of the GNU Lesser General Public License as published by the Free
   Software Foundation, either version 3 of the License, or (at your option) any
   later version.

   OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>.


.. _zero_padder_s:


Zero Padder (``zero_padder_s``)
===============================
Adds a settable number of zeros after each input data value.

Design
------
The mathematical representation of the implementation is given in :eq:`zero_padder_s-equation`.

.. math::
   :label: zero_padder_s-equation

   y[n] = \begin{cases}
            x[\frac{n}{R}] & \frac{n}{R} \in \mathbb{Z} \\
            0              & \text{otherwise}
          \end{cases}

In :eq:`zero_padder_s-equation`:

 * :math:`x[n]` is the input values.

 * :math:`y[n]` is the output values.

 * :math:`R` is the number of zeros added after each sample.

Interface
---------
.. literalinclude:: ../specs/zero_padder_s-spec.xml
   :language: xml
   :lines: 1,19-

Ports
~~~~~
.. ocpi_documentation_ports::

   input: Primary input samples port.
   output: Primary output samples port.

Opcode Handling
~~~~~~~~~~~~~~~
Zeros are only added after values in a sample opcode message. Message boundaries will be kept in the same relative position to the data, unless the output message would exceed the maximum message length for the protocol type, in which case the message is split.

All other opcodes pass through this component without any effect.

Properties
~~~~~~~~~~
.. ocpi_documentation_properties::

   samples: If set to zero the input data samples are passed through unmodified.

Implementations
---------------
.. ocpi_documentation_implementations:: ../zero_padder_s.hdl ../zero_padder_s.rcc

Example Application
-------------------
.. literalinclude:: example_app.xml
   :language: xml
   :lines: 1,19-

Dependencies
------------
The dependencies to other elements in OpenCPI are:

 * None

There is also a dependency on:

 * ``ieee.std_logic_1164``

 * ``ieee.numeric_std``

Limitations
-----------
Limitations of ``zero_padder_s`` are:

 * None.

Testing
-------
.. ocpi_documentation_test_platforms::

.. ocpi_documentation_test_result_summary::
