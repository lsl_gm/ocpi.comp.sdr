// RCC implementation of polyphase_interpolator_xs worker.
//
// This file is protected by Copyright. Please refer to the COPYRIGHT file
// distributed with this source distribution.
//
// This file is part of OpenCPI <http://www.opencpi.org>
//
// OpenCPI is free software: you can redistribute it and/or modify it under the
// terms of the GNU Lesser General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option) any
// later version.
//
// OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
// more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#include "polyphase_interpolator_xs-worker.hh"
#include "../common/polyphase_interpolator/polyphase_interpolator.hh"
#include "../../math/common/time_utils.hh"
#include "OcpiDebugApi.hh"  // OCPI_LOG_INFO

using namespace OCPI::RCC;  // for easy access to RCC data types and constants
using namespace Polyphase_interpolator_xsWorkerTypes;

class Polyphase_interpolator_xsWorker
    : public Polyphase_interpolator_xsWorkerBase {

  // Internal state
  dsp::polyphase_interpolator<int16_t, int64_t>* real_interpolator = nullptr;
  dsp::polyphase_interpolator<int16_t, int64_t>* imag_interpolator = nullptr;
  uint8_t interpolation_factor = properties().interpolation_factor;
  uint16_t taps_per_branch = 0;
  uint16_t number_of_taps = properties().number_of_taps;
  const size_t max_taps = POLYPHASE_INTERPOLATOR_XS_MAX_NUMBER_TAPS;
  uint16_t taps_length = max_taps;
  size_t sample_offset = 0;  // The sample currently being processed.
  uint8_t partial_sample = 0;
  uint32_t group_delay_seconds = 0;
  uint64_t group_delay_fractional = 0;
  bool received_data = false;  // Samples received.

  // Notification that group_delay_seconds property has been written
  RCCResult group_delay_seconds_written() {
    this->group_delay_seconds = properties().group_delay_seconds;
    return RCC_OK;
  }

  // Notification that group_delay_fractional property has been written
  RCCResult group_delay_fractional_written() {
    this->group_delay_fractional = properties().group_delay_fractional;
    return RCC_OK;
  }

  // Notification that taps property has been written
  RCCResult taps_written() { return check_and_set_taps(); }

  // Check taps properties and apply taps to the interpolators
  RCCResult check_and_set_taps() {
    if ((this->real_interpolator == nullptr) ||
        (this->imag_interpolator == nullptr)) {
      // Interpolator pointers have not been initialised - still doing
      // initialisation, and can't rely on all property values being set yet.
      // Do nothing
      return RCC_OK;
    }

    // Worker is initialised.  Check the taps properties.
    // No limits on range, so inputs not bounds checked
    // But will check the right number of taps are provided
    this->number_of_taps = properties().number_of_taps;
    const size_t taps_passed_in =
        sizeof(properties().taps) / sizeof(properties().taps[0]);

    if (taps_passed_in < this->number_of_taps) {
      setError(
          "Invalid number of taps supplied, expected %lu, "
          "but given %lu",
          this->number_of_taps, taps_passed_in);
      return RCC_FATAL;
    }

    // Check taps after required are all zeros, as buffer is max_size
    for (size_t index = this->number_of_taps; index < this->max_taps; index++) {
      if (properties().taps[index] != 0) {
        setError(
            "Invalid number of taps supplied, expected %lu, "
            "but non-zero value outside of this range",
            this->number_of_taps);
        return RCC_FATAL;
      }
    }

    // Actually set a subset of the number of taps used (which can be less due
    // to interpolation factor)
    const int16_t* taps = properties().taps;
    for (size_t index = 0; index < this->number_of_taps; index++) {
      int16_t tap_value = taps[index];
      this->real_interpolator->set_tap(index, tap_value);
      this->imag_interpolator->set_tap(index, tap_value);
    }

    return RCC_OK;
  }

  RCCResult start() {
    this->interpolation_factor = properties().interpolation_factor;
    this->number_of_taps = properties().number_of_taps;
    RCCFloat taps_per_branch_f =
        std::ceil(static_cast<RCCFloat>(properties().number_of_taps) /
                  static_cast<RCCFloat>(properties().interpolation_factor));
    if (this->interpolation_factor > this->number_of_taps) {
      setError(
          "interpolation_factor (%u) must not exceed the "
          "number of taps (%u).",
          this->interpolation_factor, this->number_of_taps);
      return RCC_FATAL;
    }
    if (taps_per_branch_f >
        std::numeric_limits<decltype(this->taps_per_branch)>::max()) {
      log(OCPI_LOG_BAD, "taps per branch is outside of the type range.");
    }
    this->taps_per_branch =
        static_cast<decltype(this->taps_per_branch)>(taps_per_branch_f);

    // Get the rounding type to use
    sdr_math::rounding_type rounding = sdr_math::rounding_type::MATH_HALF_EVEN;
    if (properties().rounding_type == ROUNDING_TYPE_HALF_EVEN) {
      rounding = sdr_math::rounding_type::MATH_HALF_EVEN;
    } else if (properties().rounding_type == ROUNDING_TYPE_HALF_UP) {
      rounding = sdr_math::rounding_type::MATH_HALF_UP;
    } else if (properties().rounding_type == ROUNDING_TYPE_TRUNCATE) {
      rounding = sdr_math::rounding_type::MATH_TRUNCATE;
    } else {
      setError("Unexpected rounding_type (%d)", properties().rounding_type);
      return RCC_FATAL;
    }

    sdr_math::overflow_type const overflow_type =
        sdr_math::overflow_type::MATH_SATURATE;

    this->taps_length = taps_per_branch * interpolation_factor;
    if (this->taps_length > POLYPHASE_INTERPOLATOR_XS_MAX_NUMBER_TAPS) {
      setError("taps_per_branch * interpolation_factor > max_number_taps");
      return RCC_FATAL;
    }
    this->real_interpolator = new dsp::polyphase_interpolator<int16_t, int64_t>(
        this->interpolation_factor, this->taps_per_branch, rounding,
        overflow_type);
    this->imag_interpolator = new dsp::polyphase_interpolator<int16_t, int64_t>(
        this->interpolation_factor, this->taps_per_branch, rounding,
        overflow_type);

    return check_and_set_taps();
  }

  RCCResult run(bool) {
    if (input.opCode() == Complex_short_timed_sampleSample_OPERATION) {
      const Complex_short_timed_sampleSampleData* data =
          input.sample().data().data();
      size_t const input_length = input.sample().data().size();
      return handle_samples(data, input_length);
    }
    if (input.opCode() == Complex_short_timed_sampleTime_OPERATION) {
      // Calc time of next output sample
      // and forward to output
      return handle_time();
    }
    if (input.opCode() == Complex_short_timed_sampleSample_interval_OPERATION) {
      // Calculate output sample interval as
      // input interval * decimation_factor
      // and forward to output
      return handle_interval();
    }
    if (input.opCode() == Complex_short_timed_sampleFlush_OPERATION) {
      // Flush buffers with zeroes, if req'd
      // and forward flush onto ouput
      return handle_flush();
    }
    if (input.opCode() == Complex_short_timed_sampleDiscontinuity_OPERATION) {
      // Reset input buffer
      return handle_discontinuity();
    }
    if (input.opCode() == Complex_short_timed_sampleMetadata_OPERATION) {
      // Forward to output port
      output.setOpCode(Complex_short_timed_sampleMetadata_OPERATION);
      output.metadata().id() = input.metadata().id();
      output.metadata().value() = input.metadata().value();
      return RCC_ADVANCE;
    }

    setError("Unknown OpCode received");
    return RCC_FATAL;
  }

  RCCResult handle_samples(const Complex_short_timed_sampleSampleData* data,
                           size_t const input_length) {
    // Apply interpolation to samples
    size_t length = input_length;

    if (length > 0) {
      this->received_data = true;
    }

    // Expecting n input samples to become N=n*interpolation factor.
    // Need to deal with N > max samples per message, by splitting one input
    // sample message into multiple output messages.
    // Check if there are samples left from last input buffer.
    if (this->sample_offset > length) {
      // Sample offset should not be more than the input length
      setError("Internal error, sample_offset > length of samples");
      return RCC_FATAL;
    }
    if (this->sample_offset > 0) {
      // Already processed some samples, move past them to
      // process the next samples
      data += this->sample_offset;
      length -= this->sample_offset;
    }

    output.setOpCode(Complex_short_timed_sampleSample_OPERATION);
    sample_offset += process_samples_into_output(data, length);

    if (((this->sample_offset == 0) || (this->sample_offset >= input_length)) &&
        (this->partial_sample == 0)) {
      // Processed all the values, so start with an
      // offset of zero into the next sample opcode.
      this->sample_offset = 0;
      // Processed all samples, so advance input and output
      return RCC_ADVANCE;
    }

    // Processed only some of the samples, advance output
    // but not input, and process the remainder next time
    output.advance();
    return RCC_OK;
  }

  RCCResult handle_interval() {
    // Calculate output sample interval as
    // input interval * decimation_factor
    uint32_t output_interval_seconds = input.sample_interval().seconds();
    uint64_t output_interval_fraction = input.sample_interval().fraction();

    time_utils::divide(&output_interval_seconds, &output_interval_fraction,
                       this->interpolation_factor);

    output.setOpCode(Complex_short_timed_sampleSample_interval_OPERATION);
    output.sample_interval().seconds() = output_interval_seconds;
    output.sample_interval().fraction() = output_interval_fraction;
    return RCC_ADVANCE;
  }

  RCCResult handle_time() {
    uint32_t output_time_seconds = input.time().seconds();
    uint64_t output_time_fraction = input.time().fraction();

    // Subtract group delay from time, wrapping around from zero to max time if
    // required
    time_utils::subtract(&output_time_seconds, &output_time_fraction,
                         this->group_delay_seconds,
                         this->group_delay_fractional);

    // Time is time of next sample, so output time now
    output.setOpCode(Complex_short_timed_sampleTime_OPERATION);
    output.time().seconds() = output_time_seconds;
    output.time().fraction() = output_time_fraction;
    return RCC_ADVANCE;
  }

  RCCResult handle_flush() {
    // Flush internal registers by pushing zeroes to interpolator and output
    // any samples produced
    if (this->received_data) {
      Complex_short_timed_sampleSampleData empty_buffer[this->number_of_taps];
      std::memset(empty_buffer, 0, sizeof(empty_buffer));

      // Push in zeroes, and handle the one or more sample opcodes output
      RCCResult result = handle_samples(empty_buffer, this->number_of_taps);

      if (result == RCC_ADVANCE) {
        // Having flushed out all the samples, advance output only. Next run
        // input will be the flush opcode again and then the flush opcode
        // will be forwarded and then advance.
        this->sample_offset = 0;
        this->partial_sample = 0;
        this->received_data = false;
        output.advance();
        return RCC_OK;
      } else {
        return result;
      }
    }

    // Reset interpolator
    this->real_interpolator->reset();
    this->imag_interpolator->reset();

    // Forward flush
    output.setOpCode(Complex_short_timed_sampleFlush_OPERATION);
    return RCC_ADVANCE;
  }

  RCCResult handle_discontinuity() {
    // Reset input buffer
    this->real_interpolator->reset();
    this->imag_interpolator->reset();
    this->partial_sample = 0;
    this->received_data = false;
    // Forward discontinuity to output
    output.setOpCode(Complex_short_timed_sampleDiscontinuity_OPERATION);
    return RCC_ADVANCE;
  }

  size_t process_samples_into_output(
      const Complex_short_timed_sampleSampleData* samples,
      size_t input_length) {

    // Expecting n input samples to become N=n*interpolation factor.
    // Need to deal with N > max samples per message, by splitting one input
    // sample message into multiple output messages.
    // Check if length * interpolation_factor > max_per_message and only
    // process some if true

    Complex_short_timed_sampleSampleData* out_data =
        output.sample().data().data();
    const size_t max_output_samples =
        POLYPHASE_INTERPOLATOR_XS_OCPI_MAX_BYTES_OUTPUT / sizeof(*out_data);

    // Calculate the size of the output buffer, this should be the minimum of
    // either the incoming samples * M or the max message size.
    // The actual number might be less than this... but should not be more.
    const size_t max_interpolated_samples =
        (input_length * this->interpolation_factor) +
        ((this->partial_sample > 0)
             ? (this->interpolation_factor - this->partial_sample)
             : 0);

    const size_t num_samples_to_output =
        std::min(max_output_samples, max_interpolated_samples);

    // Ensure output is sized correctly to hold output
    output.setOpCode(Complex_short_timed_sampleSample_OPERATION);
    output.sample().data().resize(num_samples_to_output);

    size_t branch = this->partial_sample;
    size_t num_samples_output = 0;
    size_t num_samples_processed = 0;

    while (num_samples_output < num_samples_to_output) {
      this->real_interpolator->set_branch(branch);
      this->imag_interpolator->set_branch(branch);

      if (branch == 0) {
        out_data->real =
            this->real_interpolator->step(samples[num_samples_processed].real);
        out_data->imaginary = this->imag_interpolator->step(
            samples[num_samples_processed].imaginary);
        num_samples_processed++;
      } else {
        out_data->real = this->real_interpolator->_step();
        out_data->imaginary = this->imag_interpolator->_step();
      }

      out_data++;
      num_samples_output++;
      branch = (branch + 1) % this->interpolation_factor;
    }

    this->partial_sample = branch;
    return num_samples_processed;
  }
};

POLYPHASE_INTERPOLATOR_XS_START_INFO
// Insert any static info assignments here (memSize, memSizes, portInfo)
// e.g.: info.memSize = sizeof(MyMemoryStruct);
POLYPHASE_INTERPOLATOR_XS_END_INFO
