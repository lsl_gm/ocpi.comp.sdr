// Polyphase Decimator Implementation
//
// This file is protected by Copyright. Please refer to the COPYRIGHT file
// distributed with this source distribution.
//
// This file is part of OpenCPI <http://www.opencpi.org>
//
// OpenCPI is free software: you can redistribute it and/or modify it under the
// terms of the GNU Lesser General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option) any
// later version.
//
// OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
// more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

// LINT EXCEPTION: cpp_005: 2: Define use allowed in include guard
#ifndef COMPONENTS_DSP_COMMON_POLYPHASE_DECIMATOR_POLYPHASE_DECIMATOR_HH_
#define COMPONENTS_DSP_COMMON_POLYPHASE_DECIMATOR_POLYPHASE_DECIMATOR_HH_

// LINT EXCEPTION: cpp_008: 1: Include allowed after include guard
#include <type_traits>
// LINT EXCEPTION: cpp_008: 1: Include allowed after include guard
#include <cmath>
// LINT EXCEPTION: cpp_008: 1: Include allowed after include guard
#include <vector>
// LINT EXCEPTION: cpp_008: 1: Include allowed after include guard
#include "OcpiDebugApi.hh"
// LINT EXCEPTION: cpp_008: 1: Include allowed after include guard
#include "../../../math/common/scale_and_round.hh"

namespace dsp {
// LINT EXCEPTION: cpp_011: 3: New class definition allowed in common
// implementation
template <class input_t, class coeff_t, class accum_t>
class polyphase_decimator {
  static_assert(std::is_arithmetic<input_t>::value,
                "input_t must be a numeric type");
  static_assert(std::is_arithmetic<coeff_t>::value,
                "coeff_t must be a numeric type");
  static_assert(std::is_arithmetic<accum_t>::value,
                "accum_t must be a numeric type");

 private:
  uint8_t downsample = 2;
  uint8_t taps_per_branch = 3;
  size_t taps_length = 0;
  sdr_math::rounding_type rounding = sdr_math::rounding_type::MATH_HALF_EVEN;
  coeff_t* taps = nullptr;
  input_t* in_data_history = nullptr;

  uint8_t branch = 0;
  accum_t accumulation = 0;
  const OCPI::RCC::RCCChar* log_prefix = nullptr;

 public:
  polyphase_decimator(uint8_t _downsample, uint8_t _taps_per_branch,
                      sdr_math::rounding_type _rounding,
                      const OCPI::RCC::RCCChar* _log_prefix)
      : downsample(_downsample),
        taps_per_branch(_taps_per_branch),
        taps_length(_downsample * _taps_per_branch),
        rounding(_rounding),
        taps(nullptr),
        in_data_history(nullptr),
        branch(0),
        accumulation(0),
        log_prefix(_log_prefix) {

    if (this->downsample == 0) {
      OCPI::OS::Log::print(OCPI_LOG_BAD,
                         "%s: Cannot set downsample to zero. Setting to 1.",
                         this->log_prefix);
      this->downsample = 1;
      this->taps_length = this->downsample * this->taps_per_branch;
    }
    if (this->taps_per_branch == 0) {
      OCPI::OS::Log::print(
          OCPI_LOG_BAD, "%s: Cannot set taps_per_branch to zero. Setting to 1.",
          this->log_prefix);
      this->taps_per_branch = 1;
      this->taps_length = this->downsample * this->taps_per_branch;
    }

    reset_taps();
    reset_history();
  }

  ~polyphase_decimator() {
    if (this->in_data_history != nullptr) {
      delete[] this->in_data_history;
      this->in_data_history = nullptr;
    }
    if (this->taps != nullptr) {
      delete[] this->taps;
      this->taps = nullptr;
    }
  }

  // The number of samples to be decimated away before the next output sample.
  size_t get_samples_until_next_output() const {
    return this->downsample - this->branch;
  }

  void set_tap(size_t index, coeff_t value) {
    if (index > this->taps_length) {
      OCPI::OS::Log::print(OCPI_LOG_BAD,
                         "%s: Cannot set tap beyond bounds of array. index=%lu",
                         this->log_prefix, index);
      return;
    }
    this->taps[index] = static_cast<coeff_t>(value);
  }

  // Reset input history and branch
  void reset() {
    this->branch = 0;
    this->accumulation = 0;
    reset_history();
  }

  bool process_sample(input_t input_sample, input_t* output_sample) {
    add_input_to_history(input_sample);

    // Multiply the taps and sample data on the current filter bank
    for (size_t i = 0; i < this->taps_per_branch; ++i) {
      coeff_t tap = this->taps[branch + (this->downsample * i)];
      input_t sample = this->in_data_history
                           [this->taps_length - (this->downsample * (i + 1))];

      // Not doing a scale divide here, defer that until rounding for output.
      this->accumulation +=
          static_cast<accum_t>(sample) * static_cast<accum_t>(tap);
    }
    this->branch++;

    if (this->branch >= this->downsample) {
      // Calc'd all branches, set output sample and reset accumulator
      *output_sample = _set_output(this->accumulation);
      this->branch = 0;
      this->accumulation = 0;
      return true;
    }
    return false;
  }

 private:
  // Resize history array and zero them all
  void reset_history() {
    if (this->in_data_history != nullptr) {
      delete[] this->in_data_history;
    }
    this->in_data_history = new input_t[this->taps_length];
    for (size_t index = 0; index < this->taps_length; ++index) {
      this->in_data_history[index] = static_cast<input_t>(0);
    }
  }

  // Resize tap array and zero them all
  void reset_taps() {
    if (this->taps != nullptr) {
      delete[] this->taps;
    }
    this->taps = new coeff_t[this->taps_length];
    for (size_t index = 0; index < this->taps_length; ++index) {
      this->taps[index] = static_cast<coeff_t>(0);
    }
  }

  // Insert new data at the start of the input history, remove the last entry
  void add_input_to_history(input_t sample) {
    // Note: taps_length guaranteed greater than zero by constructor.
    for (size_t index = this->taps_length - 1; index > 0; --index) {
      this->in_data_history[index] = this->in_data_history[index - 1];
    }
    this->in_data_history[0] = sample;
  }

  /// Internal function to scale, round and saturate an accumulation
  /// into an output value.
  /// @param value    The accumulated value to scale and round
  template <typename T = accum_t,
            typename std::enable_if<std::numeric_limits<T>::is_integer,
                                    T>::type* = nullptr>
  input_t _set_output(T value) const {
    input_t output = sdr_math::scale_and_round<T, input_t>(
        value, this->rounding, std::numeric_limits<input_t>::digits,
        sdr_math::overflow_type::MATH_SATURATE);
    return output;
  }

  /// Internal function to scale a floating point value into an output value.
  /// @param value    The accumulated value to scale and round
  template <typename T = accum_t,
            typename std::enable_if<std::numeric_limits<T>::is_integer == false,
                                    T>::type* = nullptr>
  input_t static _set_output(T value) {
    static_assert(std::numeric_limits<T>::is_integer,
                  "Non-integer _set_output() not implemented.");
    return value;
  }
};

}  // namespace dsp

#endif  // COMPONENTS_DSP_COMMON_POLYPHASE_DECIMATOR_POLYPHASE_DECIMATOR_HH_
