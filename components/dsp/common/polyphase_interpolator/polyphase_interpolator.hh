// Polyphase Interpolator Implementation
//
// This file is protected by Copyright. Please refer to the COPYRIGHT file
// distributed with this source distribution.
//
// This file is part of OpenCPI <http://www.opencpi.org>
//
// OpenCPI is free software: you can redistribute it and/or modify it under the
// terms of the GNU Lesser General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option) any
// later version.
//
// OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
// more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

// LINT EXCEPTION: cpp_005: 2: Define use allowed in include guard
#ifndef COMPONENTS_DSP_COMMON_POLYPHASE_INTERPOLATOR_POLYPHASE_INTERPOLATOR_HH_
#define COMPONENTS_DSP_COMMON_POLYPHASE_INTERPOLATOR_POLYPHASE_INTERPOLATOR_HH_

// LINT EXCEPTION: cpp_008: 1: Include allowed after include guard
#include <cstddef>
// LINT EXCEPTION: cpp_008: 1: Include allowed after include guard
#include <cstdint>
// LINT EXCEPTION: cpp_008: 1: Include allowed after include guard
#include <cmath>
// LINT EXCEPTION: cpp_008: 1: Include allowed after include guard
#include <cstring>
// LINT EXCEPTION: cpp_008: 1: Include allowed after include guard
#include <type_traits>
// LINT EXCEPTION: cpp_008: 1: Include allowed after include guard
#include <vector>
// LINT EXCEPTION: cpp_008: 1: Include allowed after include guard
#include "OcpiDebugApi.hh"
// LINT EXCEPTION: cpp_008: 1: Include allowed after include guard
#include "../../../math/common/scale_and_round.hh"

namespace dsp {

// LINT EXCEPTION: cpp_011: 4: Not a class definition, but template wrapped
// onto two lines
/// A common Polyphase Interpolator implementation
template <class input_t = OCPI::RCC::RCCFloat,
          class coefficient_t = OCPI::RCC::RCCFloat>
// LINT EXCEPTION: cpp_011: 2: New class definition allowed in common
// implementation
class polyphase_interpolator {
  static_assert(std::numeric_limits<input_t>::is_exact ||
                    std::numeric_limits<input_t>::is_iec559,
                "input_t must be a numeric type");
  static_assert(std::numeric_limits<input_t>::is_exact ||
                    std::numeric_limits<input_t>::is_iec559,
                "coefficient_t must be a numeric type");

 private:
  const uint8_t max_upsample = 0;
  const uint16_t max_taps_per_branch;  ///< Also the length of our history
  const uint16_t max_number_taps = 1;  ///< RAM size maximum
  uint16_t circular_index = 0;  ///< Current index into circular history buffer
  uint8_t upsample = 0;
  uint16_t taps_per_branch = 0;
  std::vector<coefficient_t> circular_data_history;
  std::vector<coefficient_t> taps;
  uint8_t branch = 0;
  sdr_math::rounding_type rounding = sdr_math::rounding_type::MATH_TRUNCATE;
  const sdr_math::overflow_type overflow = sdr_math::overflow_type::MATH_WRAP;

  /// Reset the history buffer to the initial, zeroed, state.
  void reset_history() {
    std::fill_n(this->circular_data_history.begin(), this->max_taps_per_branch,
                0);
  }

  /// Update circular index and add sample into circular history buffer.
  /// @param sample The sample value to add to the history.
  void update_input_history(input_t sample) {
    // Note: This will error if max taps is < 1.
    this->circular_index =
        (this->circular_index + this->max_taps_per_branch - 1) %
        this->max_taps_per_branch;
    this->circular_data_history[this->circular_index] =
        static_cast<coefficient_t>(sample);
  }

 public:
  /// Construct a polyphase interpolator
  /// @param _max_upsample        Maximum upsampling factor to be supported.
  /// @param _max_taps_per_branch The largest number of taps to support per
  ///                              polyphase branch.
  /// @param _rounding            The rounding type to use.
  /// @param _overflow            How to handle accumulation overflow.
  polyphase_interpolator(const uint8_t _max_upsample,
                         const uint16_t _max_taps_per_branch,
                         const sdr_math::rounding_type _rounding,
                         const sdr_math::overflow_type _overflow)
      : max_upsample(_max_upsample),
        max_taps_per_branch(_max_taps_per_branch),
        max_number_taps(_max_taps_per_branch * _max_upsample),
        upsample(_max_upsample),
        taps_per_branch(_max_taps_per_branch),
        rounding(_rounding),
        overflow(_overflow) {
    OCPI::OS::Log::print(OCPI_LOG_DEBUG,
                       "Polyphase Interpolator (taps_per_br: %iu, "
                       "max_upsample: %i, max_taps: %iu)",
                       this->taps_per_branch, this->upsample,
                       this->max_number_taps);
    this->circular_data_history.resize(this->max_taps_per_branch);
    this->taps.resize(this->max_number_taps);
    this->upsample = max_upsample;
    this->taps_per_branch = max_taps_per_branch;
    std::fill_n(this->taps.begin(), this->max_number_taps, 0);
    reset();
  }

  /// Reset to the initial state, with history zeroed.
  void reset() {
    this->branch = 0;
    reset_history();
  }

  /// Set the current polyphase filter branch.
  /// @param value The branch to use.
  void set_branch(const uint8_t value) {
    this->branch = value % this->upsample;
  }

  /// Get the current polyphase filter branch.
  uint8_t get_branch() const { return this->branch; }

  /// Set the current interpolation/upsample factor.
  /// @param value The upsample factor to use.
  void set_upsample(const uint8_t value) {
    this->upsample = std::min(value, this->max_upsample);
  }

  /// Set the current number of taps to use per interpolation branch.
  /// @param value The number of taps to use.
  void set_taps_per_branch(const uint16_t value) {
    this->taps_per_branch = std::min(value, this->max_taps_per_branch);
  }

  /// Set the value of a specific tap.
  /// @param index The index of the tap in the tap array.
  /// @param value The value to set for the tap.
  void set_tap(const size_t index, const input_t value) {
    if (index >= this->taps.size()) {
      OCPI::OS::Log::print(OCPI_LOG_BAD,
                         "tap index %lu, outside of array limits %lu", index,
                         this->taps.size());
    } else {
      this->taps.at(index) = static_cast<coefficient_t>(value);
    }
  }

  /// Set the current rounding type to use.
  /// @param round The rounding type to use.
  void set_rounding_type(sdr_math::rounding_type round) {
    this->rounding = round;
  }

  /// Get the current rounding type in use.
  sdr_math::rounding_type get_rounding_type() const { return this->rounding; }

  /// Set the current overflow type to use.
  /// @param overflow The overflow type to use.
  void set_overflow_type(sdr_math::overflow_type overflow) {
    this->overflow = overflow;
  }

  /// Get the current overflow/saturation option in use.
  sdr_math::overflow_type get_overflow_type() const { return this->overflow; }

  /// Internal function to scale, round and optionally saturate an accumulation
  /// into an output value.
  /// @param value    The accumulated value to scale and round
  template <typename T = coefficient_t,
            typename std::enable_if<std::numeric_limits<T>::is_integer,
                                    T>::type* = nullptr>
  inline input_t _set_output(T value) const {
    input_t output = sdr_math::scale_and_round<T, input_t>(
        value, this->rounding, std::numeric_limits<input_t>::digits,
        this->overflow);
    return output;
  }

  /// Internal function to scale a floating point value into an output value.
  /// @param value    The accumulated value to scale and round
  template <typename T = coefficient_t,
            typename std::enable_if<std::numeric_limits<T>::is_integer == false,
                                    T>::type* = nullptr>
  inline input_t static _set_output(T value) {
    static_assert(std::numeric_limits<T>::is_integer,
                  "Non-integer _set_output() not implemented.");
    return value;
  }

  /// Accumulate and return the interpolated value for the current branch.
  /// Does so without adding a sample value to the input history first.
  input_t _step() const {
    // Tracks the current tap for the selected branch
    size_t tap_index = this->branch;

    // Accumulates the interpolated value for the current branch
    coefficient_t accumulation = coefficient_t(0);
    for (size_t i = 0; i < this->taps_per_branch; i++) {
      size_t history_index =
          (this->circular_index + i) % this->max_taps_per_branch;
      assert(tap_index < this->taps.size());
      accumulation +=
          this->taps[tap_index] * this->circular_data_history[history_index];
      tap_index += this->upsample;
    }
    return _set_output(accumulation);
  }

  /// Accumulate and return the interpolated value for the current branch.
  /// Add the passed sample value to the input history first.
  /// @param sample The sample value to add to the input history.
  input_t step(input_t sample) {
    update_input_history(sample);
    return _step();
  }
};

}  // namespace dsp

#endif  // COMPONENTS_DSP_COMMON_POLYPHASE_INTERPOLATOR_POLYPHASE_INTERPOLATOR_HH_
