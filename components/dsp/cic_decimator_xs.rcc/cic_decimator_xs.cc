// RCC implementation of cic_decimator_xs worker.
//
// This file is protected by Copyright. Please refer to the COPYRIGHT file
// distributed with this source distribution.
//
// This file is part of OpenCPI <http://www.opencpi.org>
//
// OpenCPI is free software: you can redistribute it and/or modify it under the
// terms of the GNU Lesser General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option) any
// later version.
//
// OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
// more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#include <algorithm>
#include "../common/cic/cic_core.hh"
#include "cic_decimator_xs-worker.hh"
#include "../../math/common/time_utils.hh"
#include "OcpiDebugApi.hh"

using namespace OCPI::RCC;  // for easy access to RCC data types and constants
using namespace Cic_decimator_xsWorkerTypes;

static_assert(CIC_DECIMATOR_XS_CIC_REGISTER_SIZE <= 64,
              "cic_register_size must not be more than 64");

class Cic_decimator_xsWorker : public Cic_decimator_xsWorkerBase {
  uint8_t cic_order = CIC_DECIMATOR_XS_CIC_ORDER;
  uint8_t cic_differential_delay = CIC_DECIMATOR_XS_CIC_DIFFERENTIAL_DELAY;

  uint16_t down_sample_factor = 2;
  uint8_t scale_output = 0;

  cic_core<Complex_short_timed_sampleSampleData> cic{cic_order,
                                                     cic_differential_delay};

  // Buffer for decimated values - the output buffer must not be accessed until
  // there is actual output, so use this buffer for output of cic_core.
  static const uint32_t output_buffer_size =
      CIC_DECIMATOR_XS_OCPI_MAX_BYTES_OUTPUT /
      sizeof(Complex_short_timed_sampleSampleData);
  Complex_short_timed_sampleSampleData outputBuffer[output_buffer_size];
  size_t output_length = 0;
  size_t output_count = 0;

  // Delayed timestamp handling
  RCCBoolean timestamp_received = false;
  uint64_t current_sample_time_fraction = 0;
  uint32_t current_sample_time_seconds = 0;
  uint64_t sample_interval_fraction = 0;
  uint32_t sample_interval_seconds = 0;
  uint16_t sample_count = 0;

  // Tracks how much data has been flushed when active_flush_length is too long
  // to be contained within a single message
  uint32_t flushed_data_length = 0;
  // Adjusted from flush_length for any part completed decimations.
  uint32_t active_flush_length = 0;
  // Flags to control flush processing.
  RCCBoolean samples_received = false;
  RCCBoolean processing_flush = false;

  // Reset to start state.
  void reset() {
    this->cic.reset();
    this->timestamp_received = false;
    this->samples_received = false;
    this->sample_count = 0;
  }

  RCCResult start() {
    if (properties().down_sample_factor == 0) {
      setError("down sample factor must be greater than 0");
      return RCC_FATAL;
    }

    this->cic.set_down_sample_factor(properties().down_sample_factor);
    this->down_sample_factor = properties().down_sample_factor;

    this->sample_count = 0;
    return RCC_OK;
  }

  RCCResult stop() {
    this->reset();
    return RCC_OK;
  }

  // Notification that scale_output property has been written
  RCCResult scale_output_written() {
    this->cic.set_scale_output(properties().scale_output);
    return RCC_OK;
  }

  // Notification that down_sample_factor property has been written
  RCCResult down_sample_factor_written() {
    if (properties().down_sample_factor == 0) {
      setError("down sample factor must be greater than 0");
      return RCC_FATAL;
    }
    this->cic.set_down_sample_factor(properties().down_sample_factor);
    this->down_sample_factor = properties().down_sample_factor;

    RCCDouble required_register_size =
        16 + CIC_DECIMATOR_XS_CIC_ORDER *
                 log2(this->down_sample_factor *
                      CIC_DECIMATOR_XS_CIC_DIFFERENTIAL_DELAY);
    if (required_register_size > CIC_DECIMATOR_XS_CIC_REGISTER_SIZE) {
      log(OCPI_LOG_BAD,
          "Current cic_register_size is not large enough to support the "
          "current down_sample_factor, cic_differential_delay and cic_order");
    }

    return RCC_OK;
  }

  RCCResult process_samples(
      const Complex_short_timed_sampleSampleData *inputData,
      size_t input_length) {
    // If this is a new set of samples, then process.
    if (this->output_length == 0) {
      this->output_length = this->cic.do_decimator_work(inputData, input_length,
                                                        this->outputBuffer);
      this->output_count = 0;

      // If there is a pending timestamp then increment it up to the next output
      // sample or end of input samples, whichever comes first.
      if (this->timestamp_received) {
        uint16_t down_sample_count = this->sample_count;
        for (size_t i = 0; (i < input_length) &&
                               (down_sample_count < this->down_sample_factor);
             ++i, ++down_sample_count) {
          time_utils::add(&this->current_sample_time_seconds,
                          &this->current_sample_time_fraction,
                          this->sample_interval_seconds,
                          this->sample_interval_fraction);
        }
      }
      // Calculate the new position in the decimation.
      this->sample_count =
          (this->sample_count + input_length) % this->down_sample_factor;
    }

    // If no output, wait for more input.
    if (this->output_length == 0) {
      return RCC_ADVANCE;
    }

    if (this->timestamp_received) {
      if (this->output_count == 0) {
        // Output next sample before timestamp.
        output.setOpCode(Complex_short_timed_sampleSample_OPERATION);
        output.sample().data().resize(1);
        Complex_short_timed_sampleSampleData *outputData =
            output.sample().data().data();
        outputData[0] = this->outputBuffer[0];
        ++this->output_count;
        output.advance();
        return RCC_OK;
      }
      // Now the held timestamp.
      this->timestamp_received = false;
      output.setOpCode(Complex_short_timed_sampleTime_OPERATION);
      output.time().fraction() = this->current_sample_time_fraction;
      output.time().seconds() = this->current_sample_time_seconds;
      output.advance();
      if (this->output_length > 1) {
        // More output samples - just send timestamp and re-enter.
        return RCC_OK;
      }
      // Done with this input sample opcode.
      this->output_length = 0;
      return RCC_ADVANCE;
    }
    // Pass through the rest of the processed samples.
    output.setOpCode(Complex_short_timed_sampleSample_OPERATION);
    output.sample().data().resize(this->output_length - this->output_count);
    Complex_short_timed_sampleSampleData *outputData =
        output.sample().data().data();
    std::copy_n(this->outputBuffer + this->output_count,
                this->output_length - this->output_count, outputData);
    output.advance();
    this->output_length = 0;
    return RCC_ADVANCE;
  }

  RCCResult run(bool) {
    if (input.eof()) {
      // Output any held timestamp at eof.
      if (this->timestamp_received) {
        this->timestamp_received = false;
        output.setOpCode(Complex_short_timed_sampleTime_OPERATION);
        output.time().fraction() = this->current_sample_time_fraction;
        output.time().seconds() = this->current_sample_time_seconds;
        output.advance();
        return RCC_OK;
      }
      output.setEOF();
      return RCC_ADVANCE_FINISHED;
    }
    if (input.opCode() == Complex_short_timed_sampleSample_OPERATION) {
      this->samples_received = true;
      RCCResult result = this->process_samples(input.sample().data().data(),
                                               input.sample().data().size());
      if (result == RCC_ADVANCE) {
        input.advance();
        result = RCC_OK;
      }
      return result;
    } else if (input.opCode() == Complex_short_timed_sampleTime_OPERATION) {
      if (this->sample_count == 0) {
        // Not part way through decimation - forward immediately.
        output.setOpCode(Complex_short_timed_sampleTime_OPERATION);
        output.time().fraction() = input.time().fraction();
        output.time().seconds() = input.time().seconds();
        return RCC_ADVANCE;
      }
      // Part through a decimation, hold time until after next output sample.
      this->current_sample_time_fraction = input.time().fraction();
      this->current_sample_time_seconds = input.time().seconds();
      this->timestamp_received = true;
      input.advance();
      return RCC_OK;
    } else if (input.opCode() ==
               Complex_short_timed_sampleSample_interval_OPERATION) {
      // Save the sample interval value for potential timestamp adjustment
      this->sample_interval_fraction = input.sample_interval().fraction();
      this->sample_interval_seconds = input.sample_interval().seconds();
      // Multiply by decimation factor
      uint64_t output_interval_fraction = input.sample_interval().fraction();
      uint32_t output_interval_seconds = input.sample_interval().seconds();
      time_utils::multiply(&output_interval_seconds, &output_interval_fraction,
                           this->down_sample_factor);
      // Output sample interval opcode and sample interval data
      output.setOpCode(Complex_short_timed_sampleSample_interval_OPERATION);
      output.sample_interval().fraction() = output_interval_fraction;
      output.sample_interval().seconds() = output_interval_seconds;
      return RCC_ADVANCE;
    } else if (input.opCode() == Complex_short_timed_sampleFlush_OPERATION) {
      if (!this->processing_flush) {
        // First time through adjust for any partly processed decimation
        this->active_flush_length =
            this->cic_order *
            ((this->down_sample_factor * this->cic_differential_delay) - 1);
        // Ensure flush ends on a decimation boundary
        uint32_t samples_to_decimation_boundary =
            this->down_sample_factor -
            ((this->active_flush_length + this->sample_count) %
             this->down_sample_factor);
        // but only if the active_flush_length didn't end at the previous
        // boundary already
        if (samples_to_decimation_boundary != this->down_sample_factor) {
          this->active_flush_length += samples_to_decimation_boundary;
        }

        this->processing_flush = true;
      }
      if ((this->flushed_data_length < this->active_flush_length) &&
          this->samples_received) {
        // When a flush is requested input the set number of zeros into the cic
        // worker
        const Complex_short_timed_sampleSampleData zeroed_input
            [CIC_DECIMATOR_XS_OCPI_MAX_BYTES_OUTPUT /
             sizeof(Complex_short_timed_sampleSampleData)] = {{0, 0}};
        size_t input_length = 0;

        // Cast the result of the unsigned integer subtraction to uint32_t to
        // prevent integral promotion.
        if (static_cast<uint32_t>(this->active_flush_length -
                                  this->flushed_data_length) <=
            CIC_DECIMATOR_XS_OCPI_MAX_BYTES_OUTPUT /
                sizeof(Complex_short_timed_sampleSampleData)) {
          input_length = this->active_flush_length - this->flushed_data_length;
        } else {
          // Output maximum amount of data that will fit into a single message
          // OpenCPI maximum number of samples in a message is given by
          // CIC_DECIMATOR_XS_OCPI_MAX_BYTES_OUTPUT / sizeof(this protocol)
          input_length = CIC_DECIMATOR_XS_OCPI_MAX_BYTES_OUTPUT /
                         sizeof(Complex_short_timed_sampleSampleData);
        }

        RCCResult result = this->process_samples(zeroed_input, input_length);
        if (result == RCC_ADVANCE) {
          // Note that input_length zeros have been flushed and return OK
          // to continue processing the flush opcode on the next run call.
          this->flushed_data_length += input_length;
          result = RCC_OK;
        }
        return result;
      }
      // Pass through flush opcode
      output.setOpCode(Complex_short_timed_sampleFlush_OPERATION);
      this->flushed_data_length = 0;
      this->samples_received = false;
      this->processing_flush = false;
      return RCC_ADVANCE;
    } else if (input.opCode() ==
               Complex_short_timed_sampleDiscontinuity_OPERATION) {
      // Output any held timestamp before discontinuity.
      if (this->timestamp_received) {
        this->timestamp_received = false;
        output.setOpCode(Complex_short_timed_sampleTime_OPERATION);
        output.time().fraction() = this->current_sample_time_fraction;
        output.time().seconds() = this->current_sample_time_seconds;
        output.advance();
        return RCC_OK;
      }
      this->reset();
      // Pass through discontinuity opcode
      output.setOpCode(Complex_short_timed_sampleDiscontinuity_OPERATION);
      return RCC_ADVANCE;
    } else if (input.opCode() == Complex_short_timed_sampleMetadata_OPERATION) {
      // Pass through metadata opcode, id, and data
      output.setOpCode(Complex_short_timed_sampleMetadata_OPERATION);
      output.metadata().id() = input.metadata().id();
      output.metadata().value() = input.metadata().value();
      return RCC_ADVANCE;
    } else {
      setError("Unknown OpCode Received");
      return RCC_FATAL;
    }
  }
};

CIC_DECIMATOR_XS_START_INFO
// Insert any static info assignments here (memSize, memSizes, portInfo)
// e.g.: info.memSize = sizeof(MyMemoryStruct);
CIC_DECIMATOR_XS_END_INFO
