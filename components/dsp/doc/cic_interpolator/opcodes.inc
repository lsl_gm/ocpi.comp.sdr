.. cic_interpolator_opcodes

.. This file is protected by Copyright. Please refer to the COPYRIGHT file
   distributed with this source distribution.

   This file is part of OpenCPI <http://www.opencpi.org>

   OpenCPI is free software: you can redistribute it and/or modify it under the
   terms of the GNU Lesser General Public License as published by the Free
   Software Foundation, either version 3 of the License, or (at your option) any
   later version.

   OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>.

Sample opcode messages are up sampled by the CIC interpolator. Output sample opcode messages over ``ocpi_max_bytes_output``/``sample_size_bytes`` bytes are split into multiple sample opcode messages. All other message boundaries are preserved.

Flush opcode messages trigger the component to insert a calculated number of zero samples into the input of the CIC in order to flush out any historical data. On receipt of a flush opcode message backpressure is applied to the input port while the samples are flushed. This will result in the output of a sample opcode message that is :math:`\texttt{((cic_order*cic_differential_delay)-1)} * \texttt{up_sample_factor}` samples long. The flush opcode message is then passed from the input to the output.

Discontinuity opcode messages reset the CIC filter buffers to zero. The discontinuity opcode message is then passed from the input to the output.

Sample interval opcode messages adjusted for the interpolation by dividing the interval value by the up sample factor. The sample interval opcode message with the new value is sent to the output.

Time opcode messages are forwarded to the output.

Metadata opcode messages are forwarded to the output.
