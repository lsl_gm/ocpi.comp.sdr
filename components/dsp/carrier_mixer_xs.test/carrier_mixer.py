#!/usr/bin/env python3

# Python implementation of carrier mixer for testing
#
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
# more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

"""Python implementation of carrier mixer for testing."""

import numpy

import opencpi.ocpi_protocols as ocpi_protocols
import opencpi.ocpi_testing as ocpi_testing


class CarrierMixer(ocpi_testing.Implementation):
    """Python implementation of carrier mixer for testing."""

    def __init__(self, carrier_amplitude, instep_opcode_passthrough):
        """Initialise the class.

        Args:
            carrier_amplitude (ushort): Amplitude of the carrier mixed with the input signal.
            instep_opcode_passthrough (bool): When true opcodes from the instep port are passed to the output, when false opcodes from the input port are passed to the output.
        """
        super().__init__(carrier_amplitude=carrier_amplitude,
                         instep_opcode_passthrough=instep_opcode_passthrough)

        self.input_ports = ["input", "instep"]
        if instep_opcode_passthrough:
            self.non_sample_opcode_port_select = 1

        self._input_data = []
        self._instep_data = []

        self._output_message_lengths = []

        # Scaling applied to the carrier amplitude
        self._carrier_scaling = 1.646760258
        self._amplitude = self._carrier_scaling * self.carrier_amplitude

        self._phase_accumulator = 0

        self._max_output_length = ocpi_protocols.PROTOCOLS[
            "complex_short_timed_sample"].max_sample_length

    def reset(self):
        """Reset the state to the same state as at initialisation."""
        self._input_data = []
        self._instep_data = []

        self._phase_accumulator = 0

    def select_input(self, input_, instep):
        """Determine which input port should be advanced.

        Args:
            input_ (str): The next opcode on the input_ port.
            instep (str): The next opcode on the instep port.

        Returns:
            An integer of the input port to be advanced, indexed from zero.
        """
        # Since the only one port is used to pass through non sample opcode and
        # set message lengths, read all of the data to come in on the other
        # port first so when sample data is needed it is available
        if self.instep_opcode_passthrough:
            if input_ is not None:
                return 0
            else:
                return 1
        else:
            if instep is not None:
                return 1
            else:
                return 0

    def sample(self, input_, instep):
        """Handle an incoming sample opcode.

        Args:
            input_ (list of int): The sample values on the input_ port.
            instep (list of int): The sample values on the instep port.

        Returns:
            Formatted messages.
        """
        if input_ is not None:
            self._input_data = self._input_data + list(input_)
            if self.instep_opcode_passthrough is False:
                self._output_message_lengths.append(len(input_))
        if instep is not None:
            self._instep_data = self._instep_data + list(instep)
            if self.instep_opcode_passthrough is True:
                self._output_message_lengths.append(len(instep))

        messages = []
        while len(self._output_message_lengths) > 0:
            # Check there is enough data on both inputs for output
            if len(self._input_data) >= self._output_message_lengths[0] and \
                    len(self._instep_data) >= self._output_message_lengths[0]:
                output_length = self._output_message_lengths.pop(0)

                while output_length > self._max_output_length:
                    samples = self._get_samples(self._max_output_length)
                    messages.append({"opcode": "sample", "data": samples})
                    output_length = output_length - self._max_output_length
                    # Remove the data from the input port buffers that has been
                    # processed
                    self._input_data = self._input_data[
                        self._max_output_length:]
                    self._instep_data = self._instep_data[
                        self._max_output_length:]

                samples = self._get_samples(output_length)
                messages.append({"opcode": "sample", "data": samples})
                # Remove the data from the input port buffers that has been
                # processed
                self._input_data = self._input_data[output_length:]
                self._instep_data = self._instep_data[output_length:]

            else:
                break

        return self.output_formatter(messages)

    def _get_samples(self, output_length):
        """Create output data.

        Args:
            output_length (int): Number of samples to process.

        Returns:
            List: Resultant complex short output values.
        """
        output = [complex(0, 0)] * output_length

        for index, (data_value, instep_delta) in enumerate(zip(
                self._input_data[0:output_length],
                self._instep_data[0:output_length])):
            complex_carrier = numpy.exp(
                complex(0, (2 * numpy.pi * self._phase_accumulator) / (2**32)))
            mixed_input = self._amplitude * complex_carrier * data_value
            output[index] = complex(self._scale_output(mixed_input.real),
                                    self._scale_output(mixed_input.imag))

            self._phase_accumulator = self._phase_accumulator + instep_delta

        return output

    def _scale_output(self, value):
        """Scale output before returning.

        Shift by the correct number of bits and take a value within the
        allowed range of a short.

        Args:
            value (int): The to-be-scaled value.

        Returns:
            int: The scaled value.
        """
        scale_factor = 15

        # Shift value and limit to range supported by shorts
        value = numpy.right_shift(int(value), scale_factor)
        return int(numpy.int16(value))
