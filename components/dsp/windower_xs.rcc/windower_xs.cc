// windower_xs RCC worker.
//
// This file is protected by Copyright. Please refer to the COPYRIGHT file
// distributed with this source distribution.
//
// This file is part of OpenCPI <http://www.opencpi.org>
//
// OpenCPI is free software: you can redistribute it and/or modify it under the
// terms of the GNU Lesser General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option) any
// later version.
//
// OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
// more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#include <cmath>
#include "windower_xs-worker.hh"

using namespace OCPI::RCC;  // for easy access to RCC data types and constants
using namespace Windower_xsWorkerTypes;

class Windower_xsWorker : public Windower_xsWorkerBase {
  // Incidates the next coefficient that will be used when windowing.
  size_t coefficient_index = 0;
  // Local copy of coefficient property
  uint32_t *coefficients = NULL;

  RCCResult start() {
    this->coefficient_index = 0;
    coefficients_written();
    return RCC_OK;
  }

  RCCResult stop() {
    if (this->coefficients != NULL) {
      delete[] this->coefficients;
    }
    this->coefficients = NULL;

    return RCC_OK;
  }

  RCCResult coefficients_written() {
    if (this->coefficients != NULL) {
      delete[] this->coefficients;
    }
    this->coefficients = new uint32_t[properties().window_length];
    for (size_t index = 0; index < properties().window_length; index++) {
      this->coefficients[index] = properties().coefficients[index];
    }
    return RCC_OK;
  }

  int16_t apply_coefficient(int16_t input_value, uint32_t coefficient) {
    // double_t is used in the following calculation in preference to RCCFloat
    // in order to match the accuracy in the HDL implementation of this
    // worker
    double_t coeff_f = static_cast<double_t>(coefficient) / 0x100000000;
    double_t input_f = static_cast<double_t>(input_value);
    return static_cast<int16_t>(round(input_f * coeff_f));
  }

  void window_data(const Complex_short_timed_sampleSampleData *in_data,
                   Complex_short_timed_sampleSampleData *out_data,
                   size_t in_data_length) {
    for (size_t i = 0; i < in_data_length; i++) {
      out_data[i].real = apply_coefficient(
          in_data[i].real, this->coefficients[this->coefficient_index]);
      out_data[i].imaginary = apply_coefficient(
          in_data[i].imaginary, this->coefficients[this->coefficient_index]);
      this->coefficient_index =
          (this->coefficient_index + 1) % properties().window_length;
    }
  }

  RCCResult run(bool) {
    if (input.opCode() == Complex_short_timed_sampleSample_OPERATION) {
      size_t length = input.sample().data().size();
      const Complex_short_timed_sampleSampleData *inData =
          input.sample().data().data();
      Complex_short_timed_sampleSampleData *outData =
          output.sample().data().data();
      output.setOpCode(Complex_short_timed_sampleSample_OPERATION);
      output.sample().data().resize(length);
      window_data(inData, outData, length);
      return RCC_ADVANCE;
    } else if (input.opCode() == Complex_short_timed_sampleTime_OPERATION) {
      // Pass through time opcode and time data
      output.setOpCode(Complex_short_timed_sampleTime_OPERATION);
      output.time().fraction() = input.time().fraction();
      output.time().seconds() = input.time().seconds();
      return RCC_ADVANCE;
    } else if (input.opCode() ==
               Complex_short_timed_sampleSample_interval_OPERATION) {
      // Pass through sample interval opcode and sample interval data
      output.setOpCode(Complex_short_timed_sampleSample_interval_OPERATION);
      output.sample_interval().fraction() = input.sample_interval().fraction();
      output.sample_interval().seconds() = input.sample_interval().seconds();
      return RCC_ADVANCE;
    } else if (input.opCode() == Complex_short_timed_sampleFlush_OPERATION) {
      // Reset coefficient index
      this->coefficient_index = 0;
      // Pass through flush opcode
      output.setOpCode(Complex_short_timed_sampleFlush_OPERATION);
      return RCC_ADVANCE;
    } else if (input.opCode() ==
               Complex_short_timed_sampleDiscontinuity_OPERATION) {
      // Reset coefficient index
      this->coefficient_index = 0;
      // Pass through discontinuity opcode
      output.setOpCode(Complex_short_timed_sampleDiscontinuity_OPERATION);
      return RCC_ADVANCE;
    } else if (input.opCode() == Complex_short_timed_sampleMetadata_OPERATION) {
      // Pass through metadata opcode, id, and data
      output.setOpCode(Complex_short_timed_sampleMetadata_OPERATION);
      output.metadata().id() = input.metadata().id();
      output.metadata().value() = input.metadata().value();
      return RCC_ADVANCE;
    } else {
      setError("Unknown OpCode received");
      return RCC_FATAL;
    }
  }
};

WINDOWER_XS_START_INFO
// Insert any static info assignments here (memSize, memSizes, portInfo)
// e.g.: info.memSize = sizeof(MyMemoryStruct);
WINDOWER_XS_END_INFO
