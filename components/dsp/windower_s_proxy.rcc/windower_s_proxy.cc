// windower_s_proxy.
//
// This file is protected by Copyright. Please refer to the COPYRIGHT file
// distributed with this source distribution.
//
// This file is part of OpenCPI <http://www.opencpi.org>
//
// OpenCPI is free software: you can redistribute it and/or modify it under the
// terms of the GNU Lesser General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option) any
// later version.
//
// OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
// more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#include <cmath>
#include "windower_s_proxy-worker.hh"

using namespace OCPI::RCC;  // for easy access to RCC data types and constants
using namespace Windower_s_proxyWorkerTypes;

class Windower_s_proxyWorker : public Windower_s_proxyWorkerBase {
  const RunCondition m_aRunCondition;

 public:
  Windower_s_proxyWorker() : m_aRunCondition(RCC_NO_PORTS) {
    // Run function should never be called
    setRunCondition(&m_aRunCondition);
  }

 private:
  uint32_t *coefficients = NULL;
  static const uint32_t MAX_INT_VALUE = UINT32_MAX;

  void calculate_hann(uint16_t window_length) {
    for (size_t index = 0; index < window_length; index++) {
      double_t new_coefficient =
          MAX_INT_VALUE * pow(sin((M_PI * index) / window_length), 2);
      if (new_coefficient > MAX_INT_VALUE) {
        new_coefficient = MAX_INT_VALUE;
      }
      coefficients[index] = static_cast<uint32_t>(new_coefficient);
    }
  }

  void calculate_blackman_harris(uint16_t window_length) {
    static const double_t a0 = 0.35875;
    static const double_t a1 = 0.48829;
    static const double_t a2 = 0.14128;
    static const double_t a3 = 0.01168;
    for (size_t index = 0; index < window_length; index++) {
      double_t new_coefficient =
          MAX_INT_VALUE * (a0 - (a1 * cos((2 * M_PI * index) / window_length)) +
                           (a2 * cos((4 * M_PI * index) / window_length)) -
                           (a3 * cos((6 * M_PI * index) / window_length)));
      if (new_coefficient > MAX_INT_VALUE) {
        new_coefficient = MAX_INT_VALUE;
      }
      coefficients[index] = static_cast<uint32_t>(new_coefficient);
    }
  }

  void calculate_hamming(uint16_t window_length) {
    for (size_t index = 0; index < window_length; index++) {
      double_t new_coefficient =
          MAX_INT_VALUE *
          (0.54 - (0.46 * cos((2 * M_PI * index) / window_length)));
      if (new_coefficient > MAX_INT_VALUE) {
        new_coefficient = MAX_INT_VALUE;
      }
      coefficients[index] = static_cast<uint32_t>(new_coefficient);
    }
  }

  RCCResult window_type_written() {
    // Calculate coefficients
    coefficients = new uint32_t[properties().window_length];
    if (m_properties.window_type == WINDOW_TYPE_HANN) {
      calculate_hann(properties().window_length);
    } else if (m_properties.window_type == WINDOW_TYPE_BLACKMAN_HARRIS) {
      calculate_blackman_harris(properties().window_length);
    } else if (m_properties.window_type == WINDOW_TYPE_HAMMING) {
      calculate_hamming(properties().window_length);
    } else {
      setError("unknown window_type specified");
      return RCC_FATAL;
    }
    // Configure slave coefficients
    for (size_t index = 0; index < properties().window_length; index++) {
      slave.set_coefficients(index, coefficients[index]);
    }
    // Tidy up
    delete[] coefficients;
    // Done
    return RCC_OK;
  }

  RCCResult start() {
    // Set window length of slave
    slave.set_window_length(properties().window_length);
    return window_type_written();
  }

  RCCResult run(bool) { return RCC_DONE; }
};

WINDOWER_S_PROXY_START_INFO
// Insert any static info assignments here (memSize, memSizes, portInfo)
// e.g.: info.memSize = sizeof(MyMemoryStruct);
WINDOWER_S_PROXY_END_INFO
