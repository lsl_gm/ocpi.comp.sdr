#!/usr/bin/env python3

# Runs checks for cic_decimator_s testing
#
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
# more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

"""Cascaded Integrator-Comb Decimator short verifier."""

import os
import sys
import logging

import opencpi.ocpi_testing as ocpi_testing

from cic_decimator import CicDecimatorShort

order_property = int(os.environ.get("OCPI_TEST_cic_order"))
delay_factor_property = int(os.environ.get("OCPI_TEST_cic_differential_delay"))
down_sample_factor_property = int(
    os.environ.get("OCPI_TEST_down_sample_factor"))
output_scale_factor_property = int(os.environ.get("OCPI_TEST_scale_output"))
cic_register_size_property = int(os.environ.get("OCPI_TEST_cic_register_size"))

input_file_path = str(sys.argv[-1])
output_file_path = str(sys.argv[-2])

test_id = ocpi_testing.get_test_case(input_file_path)
log_level = int(os.environ.get("OCPI_LOG_LEVEL", 1))

if log_level > 9:
    verify_log_level = "DEBUG"
elif log_level > 5:
    verify_log_level = "INFO"
else:
    verify_log_level = "WARNING"

logfile = os.path.dirname(output_file_path)
logfile = os.path.join(logfile, f"{test_id}.py.log")
logging.basicConfig(level=verify_log_level, filename=logfile, filemode="w")

cic_decimator_implementation = CicDecimatorShort(
    order_property,
    delay_factor_property,
    down_sample_factor_property,
    output_scale_factor_property,
    cic_register_size_property)

verifier = ocpi_testing.Verifier(cic_decimator_implementation)
verifier.set_port_types(["short_timed_sample"],
                        ["short_timed_sample"], ["equal"])

if not verifier.verify(test_id, [input_file_path], [output_file_path]):
    sys.exit(1)
