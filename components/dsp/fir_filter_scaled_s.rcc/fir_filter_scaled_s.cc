// RCC implementation of fir_filter_scaled_s worker.
//
// This file is protected by Copyright. Please refer to the COPYRIGHT file
// distributed with this source distribution.
//
// This file is part of OpenCPI <http://www.opencpi.org>
//
// OpenCPI is free software: you can redistribute it and/or modify it under the
// terms of the GNU Lesser General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option) any
// later version.
//
// OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
// more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#include "../common/fir/fir_core.hh"
#include "fir_filter_scaled_s-worker.hh"

using namespace OCPI::RCC;  // for easy access to RCC data types and constants
using namespace Fir_filter_scaled_sWorkerTypes;

class Fir_filter_scaled_sWorker : public Fir_filter_scaled_sWorkerBase {
  const uint8_t number_of_taps = FIR_FILTER_SCALED_S_NUMBER_OF_TAPS;

  fir_core<int64_t> *m_fir = NULL;
  uint16_t flush_length = 0;
  uint8_t scale_factor = 0;

  // Fir_core functions require taps and output parameters to be of the same
  // type. Therefore this container is used to hold each 16 bit tap value within
  // 64 bits of memory. 64 bits are used instead of 32 to avoid overflow when
  // the fir taps and input are both large.
  int64_t taps_64[UINT8_MAX];

  // Tracks how much data has been flushed when flush_length is too long to be
  // contained within a single message
  uint16_t flushed_data_length = 0;

  RCCResult start() {
    // Save 16 bit taps container into 64 bit container
    std::copy(&properties().taps[0], &properties().taps[this->number_of_taps],
              this->taps_64);
    this->m_fir = new fir_core<int64_t>(this->number_of_taps, this->taps_64);
    return RCC_OK;
  }

  RCCResult stop() {
    delete this->m_fir;
    return RCC_OK;
  }

  // Notification that scale_factor property has been written
  RCCResult scale_factor_written() {
    this->scale_factor = properties().scale_factor;
    return RCC_OK;
  }

  // Notification that taps property has been written
  RCCResult taps_written() {
    std::copy(&properties().taps[0], &properties().taps[this->number_of_taps],
              this->taps_64);
    if (this->m_fir != NULL) {
      this->m_fir->set_taps(this->number_of_taps, this->taps_64);
    }
    return RCC_OK;
  }

  // Notification that flush_length property has been written
  RCCResult flush_length_written() {
    this->flush_length = properties().flush_length;
    return RCC_OK;
  }

  // Scales the output data by an amount determined by the scale_factor property
  // and saves into the given scaled output container
  void scale_output(int64_t *outputData, size_t output_length,
                    int16_t *scaled_outputData) {
    if (this->scale_factor) {
      const auto scale_factor_minus_one = this->scale_factor - 1;
      for (size_t i = 0; i < output_length; i++) {
        // Scale output
        int32_t outputData_shifted = *outputData++ >> scale_factor_minus_one;
        // Add 1 before performing final shift to ensure half up rounding
        *scaled_outputData++ = (outputData_shifted + 1) >> 1;
      }
    } else {
      for (size_t i = 0; i < output_length; i++) {
        *scaled_outputData++ = *outputData++ & 0xFFFF;
      }
    }
  }

  RCCResult run(bool) {
    if (input.opCode() == Short_timed_sampleSample_OPERATION) {
      size_t length = input.sample().data().size();
      const int16_t *inputData = input.sample().data().data();
      int16_t *outputData = output.sample().data().data();

      // Create a 64 bit container to hold preshifted output data values
      int64_t outputData_64
          [FIR_FILTER_SCALED_S_OCPI_MAX_BYTES_OUTPUT / sizeof(int16_t)];
      int64_t *outputData_64_pointer = outputData_64;

      // Fir_core functions require input and output to be of the same type.
      // Therefore this container is used to hold each 16 bit input value within
      // 64 bits of memory.
      int64_t inputData_64
          [FIR_FILTER_SCALED_S_OCPI_MAX_BYTES_OUTPUT / sizeof(int16_t)];
      // Save 16 bit input container into 64 bit container
      std::copy(inputData, inputData + length, inputData_64);

      output.setOpCode(Short_timed_sampleSample_OPERATION);
      output.sample().data().resize(length);
      if (length > 0) {
        this->m_fir->do_work(inputData_64, length, outputData_64);
      }

      scale_output(outputData_64_pointer, length, outputData);

      return RCC_ADVANCE;
    } else if (input.opCode() == Short_timed_sampleTime_OPERATION) {
      // Pass through time opcode and time data
      output.setOpCode(Short_timed_sampleTime_OPERATION);
      output.time().fraction() = input.time().fraction();
      output.time().seconds() = input.time().seconds();
      return RCC_ADVANCE;
    } else if (input.opCode() == Short_timed_sampleSample_interval_OPERATION) {
      // Pass through sample interval opcode and sample interval data
      output.setOpCode(Short_timed_sampleSample_interval_OPERATION);
      output.sample_interval().fraction() = input.sample_interval().fraction();
      output.sample_interval().seconds() = input.sample_interval().seconds();
      return RCC_ADVANCE;
    } else if (input.opCode() == Short_timed_sampleFlush_OPERATION) {
      if (this->flushed_data_length < this->flush_length) {
        // When a flush is requested input the set number of zeros
        const int64_t zero_input_data
            [FIR_FILTER_SCALED_S_OCPI_MAX_BYTES_OUTPUT / sizeof(int16_t)] = {0};
        const int64_t *inputData = zero_input_data;
        int16_t *outputData = output.sample().data().data();

        // Fir_core functions require input and output to be of the same type.
        // Therefore this container is used to hold each 16 bit output value
        // within 64 bits of memory.
        int64_t outputData_64
            [FIR_FILTER_SCALED_S_OCPI_MAX_BYTES_OUTPUT / sizeof(int16_t)];
        int64_t *outputData_64_pointer = outputData_64;

        uint16_t length = 0;
        // Cast the result of the unsigned integer subtraction to uint16_t to
        // prevent integral promotion.
        if (static_cast<uint16_t>(this->flush_length -
                                  this->flushed_data_length) <=
            (FIR_FILTER_SCALED_S_OCPI_MAX_BYTES_OUTPUT / sizeof(int16_t))) {
          length = this->flush_length - this->flushed_data_length;
        } else {
          // Output maximum amount of data that will fit into a single message
          // (ocpi.comp.sdr limit for short samples in message is given by
          // FIR_FILTER_SCALED_S_OCPI_MAX_BYTES_OUTPUT/sizeof(int16_t))
          length = FIR_FILTER_SCALED_S_OCPI_MAX_BYTES_OUTPUT / sizeof(int16_t);
        }

        output.setOpCode(Short_timed_sampleSample_OPERATION);
        output.sample().data().resize(length);
        this->m_fir->do_work(inputData, length, outputData_64_pointer);

        scale_output(outputData_64_pointer, length, outputData);

        this->flushed_data_length += length;
        output.advance();
        return RCC_OK;
      } else {
        // Pass through flush opcode
        output.setOpCode(Short_timed_sampleFlush_OPERATION);
        this->flushed_data_length = 0;
        return RCC_ADVANCE;
      }
    } else if (input.opCode() == Short_timed_sampleDiscontinuity_OPERATION) {
      // Pass through discontinuity opcode
      output.setOpCode(Short_timed_sampleDiscontinuity_OPERATION);
      return RCC_ADVANCE;
    } else if (input.opCode() == Short_timed_sampleMetadata_OPERATION) {
      // Pass through metadata opcode, id, and data
      output.setOpCode(Short_timed_sampleMetadata_OPERATION);
      output.metadata().id() = input.metadata().id();
      output.metadata().value() = input.metadata().value();
      return RCC_ADVANCE;
    } else {
      setError("Unknown OpCode Received");
      return RCC_FATAL;
    }
  }
};

FIR_FILTER_SCALED_S_START_INFO
// Insert any static info assignments here (memSize, memSizes, portInfo)
// e.g.: info.memSize = sizeof(MyMemoryStruct);
FIR_FILTER_SCALED_S_END_INFO
