-- HDL Implementation of ander component.
--
-- This file is protected by Copyright. Please refer to the COPYRIGHT file
-- distributed with this source distribution.
--
-- This file is part of OpenCPI <http://www.opencpi.org>
--
-- OpenCPI is free software: you can redistribute it and/or modify it under the
-- terms of the GNU Lesser General Public License as published by the Free
-- Software Foundation, either version 3 of the License, or (at your option) any
-- later version.
--
-- OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
-- WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
-- A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
-- more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
library ocpi;
use ocpi.types.all;

architecture rtl of worker is

  signal input_1_take : std_logic;
  signal input_2_take : std_logic;
  signal valid_data   : std_logic;
  signal ander_out    : std_logic_vector(output_out.data'length-1 downto 0);

begin

  input_1_out.take <= input_1_take;
  input_2_out.take <= input_2_take;

  valid_data <= '1' when input_1_in.valid = '1' and input_2_in.valid = '1' and
                input_1_in.opcode = short_timed_sample_sample_op_e and
                input_2_in.opcode = short_timed_sample_sample_op_e else '0';

  -- Always take non-sample opcodes from both inputs.
  -- Only take sample opcodes if both ports have a sample opcode.
  input_take_logic_p : process (output_in.ready, valid_data,
                                input_1_in.ready, input_1_in.opcode,
                                input_2_in.ready, input_2_in.opcode)
  begin
    input_1_take <= '0';
    input_2_take <= '0';
    -- If inputs and output are ready
    if output_in.ready = '1' then
      -- Always take when a non-sample opcode
      if input_1_in.opcode /= short_timed_sample_sample_op_e then
        input_1_take <= '1';
      end if;
      -- Always take when a non-sample opcode, but this will be discarded
      if input_2_in.opcode /= short_timed_sample_sample_op_e then
        input_2_take <= '1';
      end if;
      -- Only take a sample opcode if both inputs are valid,
      if valid_data = '1' then
        input_1_take <= '1';
        input_2_take <= '1';
      end if;
    end if;
  end process;

  -- Perform and operation
  ander_out <= input_1_in.data and input_2_in.data;

  output_out.data <= std_logic_vector(ander_out) when input_1_in.opcode = short_timed_sample_sample_op_e
                     else input_1_in.data;
  output_out.valid       <= input_1_in.valid and input_1_take;
  output_out.give        <= input_1_in.ready and input_1_take;
  output_out.opcode      <= input_1_in.opcode;
  output_out.byte_enable <= input_1_in.byte_enable;
  output_out.eom         <= input_1_in.eom;

end rtl;
