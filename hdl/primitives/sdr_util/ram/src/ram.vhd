-- HDL Implementation of a ram buffer
--
-- This file is protected by Copyright. Please refer to the COPYRIGHT file
-- distributed with this source distribution.
--
-- This file is part of OpenCPI <http://www.opencpi.org>
--
-- OpenCPI is free software: you can redistribute it and/or modify it under the
-- terms of the GNU Lesser General Public License as published by the Free
-- Software Foundation, either version 3 of the License, or (at your option) any
-- later version.
--
-- OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
-- WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
-- A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
-- more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;

entity ram is
  generic (
    data_width_g    : integer := 8;
    buffer_depth_g  : integer := 16;
    dual_port_g     : boolean := false;
    rd_pipeline_g   : integer := 2;
    write_first_g   : boolean := false;
    initalization_g : std_logic_vector
    );
  port (
    clk       : in std_logic;
    enable    : in std_logic;
    -- On a write enable the ram is shifted
    wr_enable : in std_logic;
    wr_addr   : in std_logic_vector(integer(ceil(log2(real(buffer_depth_g))))-1 downto 0);
    wr_data   : in std_logic_vector(data_width_g-1 downto 0);

    rd_addr : in  std_logic_vector(integer(ceil(log2(real(buffer_depth_g))))-1 downto 0);
    rd_data : out std_logic_vector(data_width_g-1 downto 0)
    );
end ram;

architecture rtl of ram is
  type ram_t is array(0 to buffer_depth_g-1) of std_logic_vector(data_width_g-1 downto 0);

  function to_ram_format(data : std_logic_vector(data_width_g*buffer_depth_g-1 downto 0)) return ram_t is
    variable ret : ram_t;
  begin
    for entry in 0 to buffer_depth_g-1 loop
      ret(entry) := data((entry+1)*data_width_g-1 downto entry*data_width_g);
    end loop;
    return ret;
  end function;

  signal ram : ram_t := to_ram_format(initalization_g);

  signal rd_addr_i : integer range 0 to buffer_depth_g-1;

begin

  ram_wr_p : process(clk)
  begin
    if rising_edge(clk) then
      if enable = '1' and wr_enable = '1' then
        ram(to_integer(unsigned(wr_addr))) <= wr_data;
      end if;
    end if;
  end process;


  rd_addr_i <= to_integer(unsigned(rd_addr)) when dual_port_g else to_integer(unsigned(wr_addr));

  pipe_0_g : if rd_pipeline_g = 0 generate
    rd_data <= ram(rd_addr_i);
  end generate;

  pipe_1_g : if rd_pipeline_g = 1 generate
    signal reg_rd : std_logic_vector(rd_data'range);
    signal reg_tl : std_logic_vector(rd_data'range);
  begin
    ram_p : process(clk)
    begin
      if rising_edge(clk) then
        if enable = '1' then
          if write_first_g and wr_enable = '1' then
            reg_rd <= wr_data;
          else
            reg_rd <= ram(rd_addr_i);
          end if;
        end if;
      end if;
    end process;
    rd_data <= reg_rd;
  end generate;

  pipe_2_g : if rd_pipeline_g > 1 generate
    type reg_t is array(0 to rd_pipeline_g-1) of std_logic_vector(rd_data'range);
    signal reg_rd : reg_t;
  begin
    ram_p : process(clk)
    begin
      if rising_edge(clk) then
        if enable = '1' then
          for n in 1 to rd_pipeline_g-1 loop
            reg_rd(n) <= reg_rd(n-1);
          end loop;

          if write_first_g and wr_enable = '1' then
            reg_rd(0) <= wr_data;
          else
            reg_rd(0) <= ram(rd_addr_i);
          end if;
        end if;
      end if;
    end process;
    rd_data <= reg_rd(reg_rd'high);
  end generate;

end rtl;
