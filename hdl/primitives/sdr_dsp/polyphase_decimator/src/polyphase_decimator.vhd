-- HDL Implementation of a Polyphase Decimator.
--
-- This file is protected by Copyright. Please refer to the COPYRIGHT file
-- distributed with this source distribution.
--
-- This file is part of OpenCPI <http://www.opencpi.org>
--
-- OpenCPI is free software: you can redistribute it and/or modify it under the
-- terms of the GNU Lesser General Public License as published by the Free
-- Software Foundation, either version 3 of the License, or (at your option) any
-- later version.
--
-- OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
-- WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
-- A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
-- more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.

--------------------------------------------------------------------------------
-- Polyphase Decimator
--------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;

library sdr_dsp;
use sdr_dsp.sdr_dsp.rounding_halfup;
use sdr_dsp.sdr_dsp.rounding_halfeven;
use sdr_dsp.sdr_dsp.rounding_truncate;

entity polyphase_decimator is
  generic (
    data_width_g          : positive := 16;
    max_decimation_g      : positive := 255;
    max_taps_per_branch_g : positive := 256;
    max_taps_g            : positive := 256;
    num_multipliers_g     : positive := 1;
    data_complex_g        : boolean  := true;
    rounding_mode_g       : string   := "half_even"
    );
  port (
    clk                 : in  std_logic;
    reset               : in  std_logic;
    enable              : in  std_logic;
    -- Control Signals
    -- decimation_in and num_taps_in are one based numbers
    decimation_in       : in  unsigned(integer(ceil(log2(real(max_decimation_g))))-1 downto 0);
    num_taps_in         : in  unsigned(integer(ceil(log2(real(max_taps_g))))-1 downto 0);
    num_taps_per_branch : in  unsigned(integer(ceil(log2(real(max_taps_per_branch_g))))-1 downto 0);
    -- High when a decimation run occurs
    decimation_valid    : out std_logic;

    -- Tap RAM signals
    tap_data_in      : in  signed(data_width_g-1 downto 0);
    tap_addr_in      : in  unsigned(integer(ceil(log2(real(max_taps_g))))-1 downto 0);
    tap_valid_in     : in  std_logic;
    tap_ready_out    : out std_logic;
    -- Input Stream signals
    input_data_i     : in  signed(data_width_g-1 downto 0);
    input_data_q     : in  signed(data_width_g-1 downto 0);
    input_valid_in   : in  std_logic;
    input_last_in    : in  std_logic;
    input_ready_out  : out std_logic;
    -- Output Stream signals
    output_data_i    : out signed(data_width_g-1 downto 0);
    output_data_q    : out signed(data_width_g-1 downto 0);
    output_valid_out : out std_logic;
    output_last_out  : out std_logic;
    output_ready_in  : in  std_logic
    );
end polyphase_decimator;

architecture rtl of polyphase_decimator is

  function is_pow_2(number : integer) return boolean is
    variable num_minus_1 : unsigned(31 downto 0) := to_unsigned(number - 1, 32);
    variable num         : unsigned(31 downto 0) := to_unsigned(number, 32);
  begin
    for n in 0 to 31 loop
      if (num_minus_1(n) and num(n)) = '1' then
        return false;
      end if;
    end loop;
    return true;
  end function;

  function data_length(data_width : integer; is_data_complex : boolean) return integer is
  begin
    if is_data_complex then
      return data_width * 2;
    else
      return data_width;
    end if;
  end function;

  function log_data_width(data_width : integer) return integer is
    variable ret : integer;
  begin
    ret := integer(ceil(log2(real(data_width))));
    if ret < 1 then
      return 1;
    else
      return ret;
    end if;
  end function;

  constant decimation_width_c   : natural := log_data_width(max_decimation_g);
  constant multiplier_width_c   : natural := log_data_width(num_multipliers_g);
  constant multiplier_log2_c    : natural := integer(log2(real(num_multipliers_g)));
  constant ram_depth_per_mult_c : natural := integer(ceil(real(max_taps_g)));
  constant ram_addr_width_c     : natural := log_data_width(ram_depth_per_mult_c);
  constant ram_read_pipeline_c  : natural := 2;
  constant mac_pipeline_c       : natural := ram_read_pipeline_c + 5;
  constant mac_width_c          : natural := 2*data_width_g + log_data_width(max_taps_per_branch_g * max_decimation_g);

  type tap_ram_t is array(0 to (ram_depth_per_mult_c)-1) of signed(data_width_g-1 downto 0);
  type branch_ram_t is array(0 to num_multipliers_g-1) of signed(mac_width_c-1 downto 0);
  type input_ram_t is array(0 to num_multipliers_g-1) of std_logic_vector(data_length(data_width_g, data_complex_g)-1 downto 0);

  signal reset_r           : std_logic;
  signal clear_mem         : std_logic;
  signal input_ready       : std_logic;
  signal input_valid       : std_logic;
  signal input_last        : std_logic;
  signal input_last_r      : std_logic;
  signal input_data        : std_logic_vector(data_length(data_width_g, data_complex_g)-1 downto 0);
  signal input_sample_we   : std_logic;
  signal input_data_mult   : input_ram_t := (others => (others => '0'));
  signal input_num_samples : unsigned(decimation_width_c-1 downto 0);

  signal mult_sample_idx : unsigned(multiplier_width_c-1 downto 0);

  -- Setting the taps is controlled globally
  signal tap_ready       : std_logic;
  signal tap_valid       : std_logic;
  signal tap_data_in_r   : signed(data_width_g-1 downto 0);
  signal tap_mult_addr   : unsigned(multiplier_width_c-1 downto 0);
  signal input_tap_count : unsigned(decimation_width_c-1 downto 0);
  signal tap_idx_addr    : unsigned(ram_addr_width_c-1 downto 0);

  -- global decimation signals (others are within the generate block)
  signal run_decimation    : std_logic;
  signal decim_sample_left : unsigned(decimation_width_c-1 downto 0);
  signal decim_sample_idx  : unsigned(num_taps_in'range);
  signal decim_tap_idx     : unsigned(ram_addr_width_c-1 downto 0);

  -- Output signals / rounding
  signal mult_out_data_i     : branch_ram_t := (others => (others => '0'));
  signal mult_out_data_q     : branch_ram_t := (others => (others => '0'));
  signal mult_out_valid      : std_logic_vector(num_multipliers_g-1 downto 0);
  signal last_msg            : std_logic_vector(num_multipliers_g-1 downto 0);
  signal round_last_msg_pipe : std_logic_vector(1 downto 0);

  signal mac_saturate_i     : signed(mac_width_c-1 downto 0);
  signal mac_saturate_q     : signed(mac_width_c-1 downto 0);
  signal mac_saturate_valid : std_logic;

  signal round_data_i   : signed(2*data_width_g-1 downto 0);
  signal round_data_q   : signed(2*data_width_g-1 downto 0);
  signal round_valid_in : std_logic;

  -- Note: Not putting in "use sdr_util.sdr_util.circular_buffer" as that
  -- would enforce a build order onto the hdl primitives, whereas this component
  -- declaration avoids that, as it is blackboxed until the worker level.
  component circular_buffer is
    generic (
      data_width_g         : integer;
      buffer_depth_g       : integer;
      relative_read_addr_g : boolean;
      rd_pipeline_g        : integer
      );
    port (
      clk               : in  std_logic;
      reset             : in  std_logic;
      enable            : in  std_logic;
      -- Definable wrapping depth, if less than buffer_depth_g is desired.
      buffer_wrap_depth : in  unsigned(integer(ceil(log2(real(buffer_depth_g))))-1 downto 0);
      -- On a write enable the RAM is shifted
      wr_enable         : in  std_logic;
      wr_data           : in  std_logic_vector(data_width_g-1 downto 0);
      rd_addr           : in  std_logic_vector(integer(ceil(log2(real(buffer_depth_g))))-1 downto 0);
      rd_data           : out std_logic_vector(data_width_g-1 downto 0);
      tail_addr         : out std_logic_vector(integer(ceil(log2(real(buffer_depth_g))))-1 downto 0);
      tail_data         : out std_logic_vector(data_width_g-1 downto 0)
      );
  end component;

begin

  assert (rounding_mode_g = "truncate") or (rounding_mode_g = "half_up") or (rounding_mode_g = "half_even")
    report "rounding type unknown!" severity failure;
  assert num_multipliers_g = 1 or is_pow_2(num_multipliers_g) report "Multiplier count must be a power of 2"
    severity failure;

  clk_rst_p : process(clk)
  begin
    if rising_edge(clk) then
      reset_r <= reset;
    end if;
  end process;

  input_valid     <= '1' when input_valid_in = '1' and input_ready = '1' else '0';
  input_ready_out <= input_ready;

  input_ready <= '0' when (decim_tap_idx >= decimation_in
                           or reset_r = '1' or clear_mem = '1')
                 else enable;

  -- If the input data is complex, then we double the data width on the incoming
  -- data buffers
  input_data_complex_gen : if data_complex_g generate
    input_data <= std_logic_vector(input_data_i) & std_logic_vector(input_data_q);
  end generate;
  input_data_real_gen : if not data_complex_g generate
    input_data <= std_logic_vector(input_data_i);
  end generate;

  -- Schedule a decimation, when there are enough samples (including the
  -- sample currently on the input line).
  run_decimation <= '1' when (input_valid = '1' and
                              input_num_samples >= (decimation_in-1))
                    else '0';
  decimation_valid <= run_decimation;
  input_last       <= (input_last_r or (input_last_in and input_valid)) and run_decimation;

  input_sample_counter_p : process (clk) is
  begin
    if rising_edge(clk) then

      if reset = '1' then
        clear_mem         <= '1';
        input_num_samples <= (others => '0');
        mult_sample_idx   <= (others => '0');
        input_last_r      <= '0';

      elsif enable = '1' then
        input_sample_we <= '0';
        if clear_mem = '1' then
          for n in 0 to num_multipliers_g-1 loop
            input_data_mult(n) <= (others => '0');
          end loop;
          input_sample_we <= '1';
          if decim_sample_idx = (decim_sample_idx'range => '1') then
            clear_mem       <= '0';
            input_sample_we <= '1';
          end if;
        end if;

        if input_valid = '1' then
          input_data_mult(to_integer(mult_sample_idx)) <= input_data;
          mult_sample_idx                              <= mult_sample_idx + 1;
          input_num_samples                            <= input_num_samples + 1;

          if input_last_in = '1' then
            input_last_r <= '1';
          end if;

          if input_num_samples >= (decimation_in-1) then
            input_num_samples <= (others => '0');
            input_last_r      <= '0';
          end if;

          if input_num_samples = (decimation_in-1)
            or mult_sample_idx = num_multipliers_g-1
            or num_multipliers_g = 1 then
            mult_sample_idx <= (others => '0');
            input_sample_we <= '1';
          end if;
        end if;
      end if;
    end if;
  end process;

  tap_ready     <= '1';
  tap_ready_out <= tap_ready;

  -- We make an assumption that the whole tap RAM is always loaded together
  -- and will *always* be loaded after the decimation factor has been set.
  -- (The factor is an initial setting, so this should be a fair assumption)
  tap_ram_load_p : process (clk) is
  begin
    if rising_edge(clk) then
      tap_valid <= '0';

      if reset = '1' then
        input_tap_count <= (others => '0');
        tap_idx_addr    <= (others => '0');
        tap_mult_addr   <= (others => '0');
      elsif tap_valid_in = '1' and tap_ready = '1' then
        tap_valid       <= '1';
        tap_data_in_r   <= tap_data_in;
        input_tap_count <= input_tap_count + 1;
        tap_mult_addr   <= tap_mult_addr + 1;

        if input_tap_count >= (decimation_in-1) then
          input_tap_count <= (others => '0');
        end if;

        if input_tap_count >= (decimation_in-1)
          or num_multipliers_g = 1
          or tap_mult_addr = num_multipliers_g-1
        then
          tap_mult_addr <= (others => '0');
          tap_idx_addr  <= tap_idx_addr + 1;
        end if;

        if tap_addr_in = 0 then
          input_tap_count <= (others => '0');
          tap_idx_addr    <= (others => '0');
          tap_mult_addr   <= (others => '0');
        end if;
      end if;
    end if;
  end process;

  -- If the decimation rate is less than the number of multipliers
  -- then we need to cap to only use that many multipliers, if we
  -- didn't do this capping then we would have issues with needing
  -- to pass the same sample to multiple multipliers at once.
  -- Whereas if the decimation rate is greater than the number of
  -- multipliers then we can stack the decimation products in the
  -- following order (Example with 2 mults, decimation=5,
  -- taps_per_branch=2):
  -- A0, C0, E0, A1, C1, E1
  -- B0, D0,  0, B1, D1,  0
  decimation_sample_ctrl_p : process (clk) is
  begin
    if rising_edge(clk) then
      if reset = '1' then
        decim_sample_idx <= (others => '0');
      end if;

      if enable = '1' then

        if decim_sample_idx < num_taps_in then
          if decim_sample_left > num_multipliers_g then
            decim_sample_idx  <= decim_sample_idx + num_multipliers_g;
            decim_sample_left <= decim_sample_left - num_multipliers_g;
          else
            decim_sample_idx  <= decim_sample_idx + decim_sample_left;
            decim_sample_left <= decimation_in;
          end if;
        end if;

        if clear_mem = '1' then
          decim_sample_idx <= decim_sample_idx + 1;
        end if;

        if decim_tap_idx > 0 then
          decim_tap_idx <= decim_tap_idx - 1;
        end if;

        if run_decimation = '1' then
          decim_sample_idx  <= (others => '0');
          decim_sample_left <= decimation_in;

          decim_tap_idx <=
            resize(shift_right(to_unsigned(to_integer(decimation_in) + num_multipliers_g - 1, decim_tap_idx'length), multiplier_log2_c)
                   * num_taps_per_branch, decim_tap_idx'length);
        end if;

      end if;
    end if;
  end process;


  mac_architecture_gen : for mult_idx in 0 to num_multipliers_g-1 generate
    -- reset them all to zero, this should be a flush command
    signal run_decimation_r : std_logic;
    signal sample_we        : std_logic;
    signal last_msg_r       : std_logic;
    signal sample_rd_data   : std_logic_vector(input_data'range);

    signal tap_ram  : tap_ram_t := (others => (others => '0'));
    signal tap_data : signed(data_width_g-1 downto 0);

    signal sample_rd_addr : unsigned(decim_tap_idx'range);
    signal tap_rd_addr    : unsigned(decim_tap_idx'range);
    signal tail_addr      : std_logic_vector(decim_tap_idx'range);
    signal tail_addr_i    : std_logic_vector(decim_tap_idx'range);
    signal tail_addr_r    : std_logic_vector(decim_tap_idx'range);

    -- multiplier output
    signal tap_x_sample_i   : signed(2*data_width_g-1 downto 0);
    signal tap_x_sample_q   : signed(2*data_width_g-1 downto 0);
    signal tap_x_sample_i_r : signed(2*data_width_g-1 downto 0);
    signal tap_x_sample_q_r : signed(2*data_width_g-1 downto 0);
    -- accumulator testing
    signal mac_value_i      : signed(mac_width_c-1 downto 0);
    signal mac_value_q      : signed(mac_width_c-1 downto 0);

    signal fir_pipe          : std_logic_vector(mac_pipeline_c-1 downto 0);
    signal last_sample_pipe  : std_logic_vector(mac_pipeline_c-1 downto 0);
    signal mac_last_msg_pipe : std_logic_vector(mac_pipeline_c-1 downto 0);
  begin

    input_data_buffer_i : circular_buffer
      generic map (
        data_width_g         => data_length(data_width_g, data_complex_g),
        buffer_depth_g       => ram_depth_per_mult_c,
        relative_read_addr_g => false,
        rd_pipeline_g        => ram_read_pipeline_c
        )
      port map (
        clk               => clk,
        reset             => reset,
        enable            => enable,
        buffer_wrap_depth => (others => '1'),  -- coerced to the max depth
        wr_enable         => input_sample_we,
        wr_data           => input_data_mult(mult_idx),
        rd_addr           => std_logic_vector(sample_rd_addr),
        rd_data           => sample_rd_data,
        tail_addr         => tail_addr,
        tail_data         => open
        );

    tail_addr_i <= tail_addr when run_decimation_r = '1' else tail_addr_r;
    process(tail_addr_i, decim_tap_idx) is
      variable sample_rd_addr_v : signed(sample_rd_addr'length downto 0);
    begin
      sample_rd_addr_v := signed('0' & tail_addr_i) - signed('0' & decim_tap_idx);
      if sample_rd_addr_v < 0 then
        sample_rd_addr_v := sample_rd_addr_v + ram_depth_per_mult_c;
      end if;
      sample_rd_addr <= unsigned(sample_rd_addr_v(sample_rd_addr'range));
    end process;

    tail_addr_r_p : process (clk) is
    begin
      if rising_edge(clk) then
        if reset = '1' then
          last_msg_r <= '0';
        elsif enable = '1' then
          run_decimation_r <= run_decimation;

          if decim_tap_idx = 1 then
            last_msg_r <= '0';
          end if;
          if input_last = '1' then
            last_msg_r <= '1';
          end if;

          if fir_pipe(0) = '1' then
            tap_rd_addr <= tap_rd_addr + 1;
          end if;
          if run_decimation_r = '1' then
            tail_addr_r <= tail_addr;

            tap_rd_addr <= (others => '0');
          end if;
        end if;
      end if;
    end process;

    -- This block relates to the tap RAM accesses.
    -- This includes the output and the input (and the delay pipeline)
    tap_ram_b : block
      signal tap_data_i       : signed(data_width_g-1 downto 0);
      signal tap_data_1r      : signed(data_width_g-1 downto 0);
      signal tap_data_2r      : signed(data_width_g-1 downto 0);
      signal tap_output_zeros : std_logic_vector(3 downto 0);
    begin
      tap_ram_p : process (clk) is
      begin
        if rising_edge(clk) then
          if enable = '1' then
            tap_data_i  <= tap_ram(to_integer(tap_rd_addr));
            tap_data_1r <= tap_data_i;
            tap_data_2r <= tap_data_1r;

            for n in 1 to tap_output_zeros'high loop
              tap_output_zeros(n) <= tap_output_zeros(n-1);
            end loop;

            if (decim_sample_idx + mult_idx) > num_taps_in then
              tap_output_zeros(0) <= '1';
            else
              tap_output_zeros(0) <= '0';
            end if;
          end if;

          if tap_valid = '1' and tap_mult_addr = mult_idx then
            tap_ram(to_integer(tap_idx_addr)) <= tap_data_in_r;
          end if;
        end if;
      end process;
      tap_data <= tap_data_2r when tap_output_zeros(tap_output_zeros'high) = '0'
                  else (others => '0');
    end block;

    multiplier_p : process (clk) is
    begin
      if rising_edge(clk) then
        if enable = '1' then
          tap_x_sample_i <= signed(tap_data) * signed(sample_rd_data((2*data_width_g)-1 downto data_width_g));
          tap_x_sample_q <= signed(tap_data) * signed(sample_rd_data(data_width_g-1 downto 0));

          tap_x_sample_i_r <= tap_x_sample_i;
          tap_x_sample_q_r <= tap_x_sample_q;
        end if;
      end if;
    end process;

    accumulate_p : process (clk) is
    begin
      if rising_edge(clk) then
        if reset = '1' then
          mac_value_i              <= (others => '0');
          mac_value_q              <= (others => '0');
          fir_pipe                 <= (others => '0');
          mult_out_valid(mult_idx) <= '0';

        elsif enable = '1' then
          -- pipeline active shift-registers.
          for n in 1 to fir_pipe'high loop
            fir_pipe(n) <= fir_pipe(n-1);
          end loop;
          for n in 1 to last_sample_pipe'high loop
            last_sample_pipe(n) <= last_sample_pipe(n-1);
          end loop;
          for n in 1 to mac_last_msg_pipe'high loop
            mac_last_msg_pipe(n) <= mac_last_msg_pipe(n-1);
          end loop;

          -- Default values for the pipeline shift-registers
          fir_pipe(0)              <= '0';
          last_sample_pipe(0)      <= '0';
          mac_last_msg_pipe(0)     <= '0';
          mult_out_valid(mult_idx) <= '0';
          last_msg(mult_idx)       <= mac_last_msg_pipe(mac_last_msg_pipe'high);

          if decim_tap_idx > 0 then
            fir_pipe(0) <= '1';
          end if;
          if decim_tap_idx = 1 then
            last_sample_pipe(0)  <= '1';
            mac_last_msg_pipe(0) <= last_msg_r;
          end if;

          -- Accumulate stage
          if fir_pipe(fir_pipe'high-1) = '1' then
            mac_value_i <= mac_value_i + tap_x_sample_i_r;
            mac_value_q <= mac_value_q + tap_x_sample_q_r;
          end if;

          -- Output stage
          if last_sample_pipe(last_sample_pipe'high) = '1' then
            mult_out_data_i(mult_idx) <= mac_value_i;
            mult_out_data_q(mult_idx) <= mac_value_q;
            mult_out_valid(mult_idx)  <= '1';
            mac_value_i               <= (others => '0');
            mac_value_q               <= (others => '0');
          end if;
        end if;
      end if;
    end process;
  end generate;

  -- This is currently a parallel adder, but the timing should be
  -- such that a cascade adder should also work.
  output_sample_p : process (clk) is
    variable mac_value_i_v : signed(mac_width_c-1 downto 0);
    variable mac_value_q_v : signed(mac_width_c-1 downto 0);
  begin
    if rising_edge(clk) then
      if reset = '1' then
        round_valid_in     <= '0';
        mac_saturate_valid <= '0';

      elsif enable = '1' then
        mac_saturate_valid <= mult_out_valid(0);
        round_valid_in     <= mac_saturate_valid;

        -- Get the values across the multipliers all added together
        if mult_out_valid(0) = '1' then
          mac_value_i_v := (others => '0');
          mac_value_q_v := (others => '0');
          for n in 0 to num_multipliers_g-1 loop
            mac_value_i_v := mac_value_i_v + mult_out_data_i(n);
            mac_value_q_v := mac_value_q_v + mult_out_data_q(n);
          end loop;
          mac_saturate_i <= mac_value_i_v;
          mac_saturate_q <= mac_value_q_v;
        end if;

        if mac_saturate_valid = '1' then
          -- test for saturation within the MAC
          if mac_saturate_i(mac_width_c-1) = '0' then
            if mac_saturate_i(mac_width_c-2 downto (2*data_width_g)-1) /= (mac_width_c-2 downto (2*data_width_g)-1 => '0') then
              round_data_i(round_data_i'length-1)                       <= '0';
              round_data_i(round_data_i'length-2 downto data_width_g-1) <= (others => '1');
              round_data_i(data_width_g-2)                              <= '0';
              round_data_i(data_width_g-3 downto 0)                     <= (others => '1');
            else
              round_data_i <= mac_saturate_i(round_data_i'length-1 downto 0);
            end if;
          else
            if mac_saturate_i(mac_width_c-2 downto (2*data_width_g)-1) /= (mac_width_c-2 downto (2*data_width_g)-1 => '1') then
              round_data_i(round_data_i'length-1)                       <= '1';
              round_data_i(round_data_i'length-2 downto data_width_g-1) <= (others => '0');
              round_data_i(data_width_g-2)                              <= '1';
              round_data_i(data_width_g-3 downto 0)                     <= (others => '0');
            else
              round_data_i <= mac_saturate_i(round_data_i'length-1 downto 0);
            end if;
          end if;

          if mac_saturate_q(mac_width_c-1) = '0' then
            if mac_saturate_q(mac_width_c-2 downto (2*data_width_g)-1) /= (mac_width_c-2 downto (2*data_width_g)-1 => '0') then
              round_data_q(round_data_i'length-1)                       <= '0';
              round_data_q(round_data_i'length-2 downto data_width_g-1) <= (others => '1');
              round_data_q(data_width_g-2)                              <= '0';
              round_data_q(data_width_g-3 downto 0)                     <= (others => '1');
            else
              round_data_q <= mac_saturate_q(round_data_q'length-1 downto 0);
            end if;
          else
            if mac_saturate_q(mac_width_c-2 downto (2*data_width_g)-1) /= (mac_width_c-2 downto (2*data_width_g)-1 => '1') then
              round_data_q(round_data_i'length-1)                       <= '1';
              round_data_q(round_data_i'length-2 downto data_width_g-1) <= (others => '0');
              round_data_q(data_width_g-2)                              <= '1';
              round_data_q(data_width_g-3 downto 0)                     <= (others => '0');
            else
              round_data_q <= mac_saturate_q(round_data_q'length-1 downto 0);
            end if;
          end if;
        end if;
      end if;
    end if;
  end process;

  --------------------------------------------------
  -- Selectable Rounding
  --------------------------------------------------
  truncate_gen : if rounding_mode_g = "truncate" generate
  begin

    truncate_real_i : rounding_truncate
      generic map (
        input_width_g   => 2*data_width_g,
        output_width_g  => data_width_g,
        saturation_en_g => true
        )
      port map (
        clk            => clk,
        reset          => reset,
        clk_en         => enable,
        data_in        => round_data_i,
        data_valid_in  => round_valid_in,
        binary_point   => data_width_g - 1,
        data_out       => output_data_i,
        data_valid_out => output_valid_out
        );

    truncate_imag_i : rounding_truncate
      generic map (
        input_width_g   => 2*data_width_g,
        output_width_g  => data_width_g,
        saturation_en_g => true
        )
      port map (
        clk            => clk,
        reset          => reset,
        clk_en         => enable,
        data_in        => round_data_q,
        data_valid_in  => round_valid_in,
        binary_point   => data_width_g - 1,
        data_out       => output_data_q,
        data_valid_out => output_valid_out
        );

  end generate;


  half_up_gen : if rounding_mode_g = "half_up" generate
  begin

    half_up_real_i : rounding_halfup
      generic map (
        input_width_g   => 2*data_width_g,
        output_width_g  => data_width_g,
        saturation_en_g => true
        )
      port map (
        clk            => clk,
        reset          => reset,
        clk_en         => enable,
        data_in        => round_data_i,
        data_valid_in  => round_valid_in,
        binary_point   => data_width_g - 1,
        data_out       => output_data_i,
        data_valid_out => output_valid_out
        );

    half_up_imag_i : rounding_halfup
      generic map (
        input_width_g   => 2*data_width_g,
        output_width_g  => data_width_g,
        saturation_en_g => true
        )
      port map (
        clk            => clk,
        reset          => reset,
        clk_en         => enable,
        data_in        => round_data_q,
        data_valid_in  => round_valid_in,
        binary_point   => data_width_g - 1,
        data_out       => output_data_q,
        data_valid_out => open
        );
  end generate;

  half_even_gen : if rounding_mode_g = "half_even" generate
  begin

    half_even_real_i : rounding_halfeven
      generic map (
        input_width_g   => 2*data_width_g,
        output_width_g  => data_width_g,
        saturation_en_g => true
        )
      port map (
        clk            => clk,
        reset          => reset,
        clk_en         => enable,
        data_in        => round_data_i,
        data_valid_in  => round_valid_in,
        binary_point   => data_width_g - 1,
        data_out       => output_data_i,
        data_valid_out => output_valid_out
        );

    half_even_imag_i : rounding_halfeven
      generic map (
        input_width_g   => 2*data_width_g,
        output_width_g  => data_width_g,
        saturation_en_g => true
        )
      port map (
        clk            => clk,
        reset          => reset,
        clk_en         => enable,
        data_in        => round_data_q,
        data_valid_in  => round_valid_in,
        binary_point   => data_width_g - 1,
        data_out       => output_data_q,
        data_valid_out => open
        );
  end generate;

  rounding_last_pipeline_p : process (clk)
  begin
    if rising_edge(clk) then
      if enable = '1' then
        for n in 1 to round_last_msg_pipe'high loop
          round_last_msg_pipe(n) <= round_last_msg_pipe(n-1);
        end loop;
        round_last_msg_pipe(0) <= last_msg(0);
        output_last_out        <= round_last_msg_pipe(round_last_msg_pipe'high);
      end if;
    end if;
  end process;

end rtl;
