#!/usr/bin/env python3

# Generates the files from the ZipCPU dblclk core needed for the FFT primitives
#
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
# more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

"""Generates the files from the ZipCPU dblclk core needed for the FFT primitives."""

import subprocess
import os
import math
import re
import sys
import xml.etree.ElementTree as ET
from pathlib import Path

import hex_to_vh
import gen_fft_map


def get_fft_lengths(build_file):
    """Get the fft lengths from the build xml.

    Args:
        build_file (Path-like): path to the build file.

    Returns:
        Set containing the FFT lengths needed.
    """
    tree = ET.parse(build_file)
    return set([int(t.attrib["value"])
                for t in tree.iter("parameter")
                if t.attrib["name"] == "fft_length"])


def fftstage_changes(filename):
    """Replace the call to readmemh with actual array look-ups.

    Args:
        filename (Path-like): Path of the fftstage file to edit.
    """
    newfile = []
    for l in open(filename):
        if "readmemh" in l:
            newfile += """genvar n;
                generate
                    for (n=(1<<LGSPAN); n > 0; n = n-1) begin
                        initial cmem[(1<<LGSPAN)-n] = COEFFILE[((n)*2*CWIDTH)-1: (n-1)*2*CWIDTH];
                    end
                endgenerate
                """
        else:
            newfile += l
    with open(filename, "w") as f:
        for l in newfile:
            f.write(l)


def fftmain_changes(filename, module_name, coeff_file):
    """Replace parts of the fftmain module.

    Args:
        filename (Path-like): Path to the `fftmain.v` file.
        module_name (str): Name of the module (generally fftmain or ifftmain).
        coeff_file (Path-like): Path to the relevant coefficients header file.
    """
    newfile = []
    print(
        f"Insert cmem as a ROM table (changing from 1D to 2D shape) into: {module_name}, ({filename})")
    for i, l in enumerate(open(filename)):
        if f"module {module_name}(" in l:
            newfile += re.sub(r"(.*)fftmain(.*)",
                              fr"\1fftmain_{fft_length}\2", l)
        elif ".COEFFILE(" in l:
            newfile += re.sub(r"\"(.*).hex\"", r"\1", l)
        # Jump into the file to the start of the internals of the module
        # (actual location is flexible)
        elif i == 86:
            newfile += l
            for l2 in open(coeff_file):
                newfile += l2
        else:
            newfile += l
    with open(filename, "w") as f:
        for l in newfile:
            f.write(l)


def get_fftgen_path():
    """Locate the fftgen tool within the prerequisites-build directory.

    Returns:
        Path of fftgen executable or None
    """
    try:
        dblclock_dir = Path(os.environ["OCPI_CDK_DIR"]).joinpath(
            "../prerequisites-build/dblclockfft/")
    except:
        print(f"Error: OCPI_CDK_DIR has not been set.", file=sys.stderr)
        return None

    for f in dblclock_dir.glob("dblclockfft-*/sw/fftgen"):
        if f.exists():
            return f

    print(f"Error: Failed to find dblclock fftgen tool.", file=sys.stderr)
    return None


if __name__ == "__main__":
    input_width = 17
    proj_dir = Path(__file__).parent.joinpath("../../../..").resolve()

    # Create the gen directories
    Path("gen/fft").mkdir(parents=True, exist_ok=True)

    # Get fftgen tool
    fftgen_path = get_fftgen_path()

    if fftgen_path is None:
        # Need to download and/or install the FFT build tools
        prereq_script = proj_dir.joinpath(
            "prerequisites/dblclockfft/install-dblclockfft.sh")
        if not prereq_script.exists():
            print(f"Failed to find build prerequisites script: {prereq_script}",
                  file=sys.stderr)
            sys.exit(1)

        print("Downloading dblclockfft fftgen tool", file=sys.stderr)
        subprocess.run(str(prereq_script), shell=True)

        fftgen_path = get_fftgen_path()

    if not fftgen_path or not fftgen_path.exists():
        print(f"Failed to find or download fftgen tool", file=sys.stderr)
        sys.exit(1)

    component_build_file = proj_dir.joinpath(
        "components/dsp/fast_fourier_transform_xs.hdl/fast_fourier_transform_xs-build.xml")

    if not component_build_file.exists():
        print(f"Failed to find all -build.xml components using this module: {component_build_file}",
              file=sys.stderr)
        sys.exit(1)

    fft_lengths = list(get_fft_lengths(component_build_file))

    updated_files = False
    for fft_length in fft_lengths:
        fft_mults = 2*(math.log2(fft_length) + 2)
        dir_path = Path(f"gen/fft/{fft_length}")
        dir_path.mkdir(parents=True, exist_ok=True)

        if (dir_path / f"fftmain_{fft_length}.v").exists():
            continue
        else:
            updated_files = True
            print(
                f"Generating FFT Core with FFT Length: {fft_length}, and {fft_mults} multipliers, (at: {dir_path})")

            # Run tool
            subprocess.run(
                f"{fftgen_path} -k 1 -d {dir_path} -f {fft_length} -n {input_width} -S -p {int(fft_mults)}", shell=True)
            print("Convert the following hex files: {}".format(
                ", ".join([str(p.name) for p in dir_path.glob("*.hex")])))
            hex_to_vh.parse_files(dir_path.glob("cmem*.hex"),
                                  Path(dir_path / "coeff.vh"))

            # Remove autogenerated readmemh commands (these do not work with OCPI)
            # Alter COEFFILE lines within the fftstage.v
            fftstage_changes(dir_path / "fftstage.v")
            fftmain_changes(dir_path / "fftmain.v",
                            "fftmain", dir_path / "coeff.vh")
            os.replace(dir_path / "fftmain.v", dir_path /
                       f"fftmain_{fft_length}.v")

            # Run the tool a second time to generate the inverse version of the files
            subprocess.run(
                f"{fftgen_path} -k 1 -d {dir_path} -i -f {fft_length} -n {input_width} -S -p {int(fft_mults)}", shell=True)
            hex_to_vh.parse_files(dir_path.glob("icmem*.hex"),
                                  Path(dir_path / "icoeff.vh"))
            fftstage_changes(dir_path / "ifftstage.v")
            fftmain_changes(dir_path / "ifftmain.v",
                            "ifftmain", dir_path / "icoeff.vh")
            os.replace(dir_path / "ifftmain.v", dir_path /
                       f"ifftmain_{fft_length}.v")
    if updated_files:
        with open("gen/fast_fourier_transform_map.vhd", "w") as o:
            gen_fft_map.add_entity(17, o)
            gen_fft_map.add_architecture(17, list(fft_lengths), o)
