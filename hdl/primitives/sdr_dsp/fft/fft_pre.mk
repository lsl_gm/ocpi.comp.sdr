# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public License along
# with this program. If not, see <http://www.gnu.org/licenses/>.

FFT_CORE_FILES := \
    bimpy.v \
    bitreverse.v \
    butterfly.v \
    convround.v \
    hwbfly.v \
    laststage.v \
    longbimpy.v \
    qtrstage.v \
    fftstage.v \

# It looks like the autogeneration from OCPI only works for xsim, as
# synthesised targets are handled through variable manipulation as opposed to
# rules, therefore call out to python for generation of files.
mkfile_dir := $(dir $(abspath $(lastword $(MAKEFILE_LIST))))

# Python function to look at -build.xml of FFT HDL worker, extract the FFT lengths specified
# and create the list of gen/fft/<length>/<filename> source files needed.
define python_generate_fft_files
from pathlib import Path
import xml.etree.ElementTree as ET;
tree=ET.parse("$(mkfile_dir)../../../../components/dsp/fast_fourier_transform_xs.hdl/fast_fourier_transform_xs-build.xml");
fft_lengths = list(set([t.attrib["value"] for t in tree.iter("parameter") if t.attrib["name"] == "fft_length"]))
for fft_length in fft_lengths:
  print("gen/fft/{0}/fftmain_{0}.v".format(fft_length))
  print("gen/fft/{0}/ifftmain_{0}.v".format(fft_length))
for file in "$(FFT_CORE_FILES)".split():
  f = Path(file)
  basename = str(f)[:-len(f.suffix)]
  print("gen/fft/{}/{}.v".format(fft_lengths[0], basename))
endef

ifneq ($(MAKECMDGOALS),clean)
# Run generate.py to create the files in gen/fft required, and set
# the FFT_LENGTH_CORE_FILES to the list of files that should have been generated
OUT := $(shell python3 fft/generate.py)
FFT_LENGTH_CORE_FILES := $(shell python3 -c '$(python_generate_fft_files)')
endif

SourceFiles += $(FFT_LENGTH_CORE_FILES)
SourceFiles += gen/fast_fourier_transform_map.vhd

