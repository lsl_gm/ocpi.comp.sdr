.. rounding_truncate documentation

.. This file is protected by Copyright. Please refer to the COPYRIGHT file
   distributed with this source distribution.

   This file is part of OpenCPI <http://www.opencpi.org>

   OpenCPI is free software: you can redistribute it and/or modify it under the
   terms of the GNU Lesser General Public License as published by the Free
   Software Foundation, either version 3 of the License, or (at your option) any
   later version.

   OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>.


.. _rounding_truncate-primitive:


Truncating Rounder (``rounding_truncate``)
==========================================
Truncation, or Floor rounding for a static width signed input.

Design
------
The input stream is arithmetically right-shifted by ``binary_point``, truncating the integer and discarding all fractional bits, then truncated from above (losing the most-significant bits of the input data), to the output width.

In addition it is possible to enable saturation, which clips the output if the incoming value has a larger fixed point magnitude, than that which can be expressed within the outputs number of bits.

Implementation
--------------
The primitive is designed such that a new sample can be inserted into the primitive on every rising edge of ``clk``. The ``data_valid_in`` signal allows each input sample to be marked as valid or not. The pipeline advances every rising edge of ``clk`` regardless of if valid data is passed to the primitive or not. The ``data_valid_out`` signal is a delayed version of ``data_valid_in`` that indicates which outputs were calculated based on valid inputs.

The ``clk_en`` signal provides a clock enable for the primitive. When set low no data will enter or leave the primitive and all calculations are stopped. When set high the module will operate normally.

Interface
---------

Generics
~~~~~~~~

 * ``input_width_g`` (``integer``): Sets width of ``data_in`` signal.

 * ``output_width_g`` (``integer``): Sets width of ``data_out`` signal.

 * ``saturation_en_g`` (``boolean``): Enables saturation on the data when an overflow has occurred as a result of the resolution changing from input to output.


Ports
~~~~~

 * ``clk`` (``std_logic``), in: Clock. Inputs and outputs registered on rising edge.

 * ``reset`` (``std_logic``), in: Reset. Active high, synchronous with rising edge of clock.

 * ``clk_en`` (``std_logic``), in: Clock Enable. If low the module will not operate.

 * ``data_in`` (``signed``, ``input_width_g`` bits), in: Input value for primitive.

 * ``data_valid_in`` (``std_logic``), in: ``data_in`` is valid when ``data_valid_in`` is high.

 * ``binary_point`` (``integer``), in: Number of fractional bits in the least significant bits of the ``data_in`` input.

 * ``data_out`` (``signed``, ``output_width_g`` bits), out: Output value for primitive.

 * ``data_valid_out`` (``std_logic``), out: ``data_out`` is valid when ``data_valid_out`` is high.

Dependencies
------------
The dependencies to other elements in OpenCPI are:

 * None.

There is also a dependency on:

 * ``ieee.std_logic_1164``

 * ``ieee.numeric_std``

Limitations
-----------
Limitations of ``rounding_truncate`` are:

 * A small negative bias will be introduced when using this primitive.

 * If this causes an issue convergent rounding may be used instead using the :ref:`Half Even Rounder Primitive <rounding_halfeven-primitive>`.
