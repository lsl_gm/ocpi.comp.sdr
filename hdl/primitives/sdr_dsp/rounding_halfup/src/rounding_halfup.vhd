-- HDL Implementation of a half-up rounder.
--
-- This file is protected by Copyright. Please refer to the COPYRIGHT file
-- distributed with this source distribution.
--
-- This file is part of OpenCPI <http://www.opencpi.org>
--
-- OpenCPI is free software: you can redistribute it and/or modify it under the
-- terms of the GNU Lesser General Public License as published by the Free
-- Software Foundation, either version 3 of the License, or (at your option) any
-- later version.
--
-- OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
-- WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
-- A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
-- more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity rounding_halfup is
  generic (
    input_width_g   : integer := 32;
    output_width_g  : integer := 16;
    saturation_en_g : boolean := false
    );
  port (
    clk            : in  std_logic;
    reset          : in  std_logic;
    clk_en         : in  std_logic;
    data_in        : in  signed(input_width_g - 1 downto 0);
    data_valid_in  : in  std_logic;
    binary_point   : in  integer range 0 to input_width_g - 1;
    data_out       : out signed(output_width_g - 1 downto 0);
    data_valid_out : out std_logic
    );
end rounding_halfup;

architecture rtl of rounding_halfup is
begin

  assert binary_point < input_width_g report "binary point must be less than input_width_g" severity failure;
  assert output_width_g <= input_width_g report "output_width_g must be less or equal to input_width_g" severity failure;

  halfup_rounder_p : process(clk)
    variable halfup_v   : signed (input_width_g-1 downto 0);
    variable data_out_v : signed (output_width_g-1 downto 0);
  begin
    if rising_edge(clk) then
      if clk_en = '1' then
        halfup_v := data_in;
        if binary_point = 0 then
          -- Output data_out'length least significant bits
          data_out_v := halfup_v(output_width_g-1 downto 0);
        else
          halfup_v   := SHIFT_RIGHT(halfup_v, binary_point-1);
          halfup_v   := halfup_v + 1;   -- Add 0.5 (effectively)
          halfup_v   := SHIFT_RIGHT(halfup_v, 1);
          -- Truncate data to desired output width
          data_out_v := halfup_v(output_width_g-1 downto 0);
        end if;

        -- Saturation check
        if (saturation_en_g = true) and (input_width_g > output_width_g + binary_point) then

          -- If +ve, all remaining bits are '0'
          if halfup_v(input_width_g-1 downto output_width_g-1) > 0 then
            data_out_v(data_out_v'high)            := '0';
            data_out_v(data_out_v'high-1 downto 0) := (others => '1');

          -- If -ve, all remaining bits are '1'
          elsif halfup_v(input_width_g-1 downto output_width_g-1) < -1 then
            data_out_v(data_out_v'high)            := '1';
            data_out_v(data_out_v'high-1 downto 0) := (others => '0');
          end if;
        end if;
        data_out <= data_out_v(output_width_g-1 downto 0);
      end if;
    end if;
  end process;

  round_valid_p : process(clk)
  begin
    if rising_edge(clk) then
      if reset = '1' then
        data_valid_out <= '0';
      elsif clk_en = '1' then
        data_valid_out <= data_valid_in;
      end if;
    end if;
  end process;

end rtl;
