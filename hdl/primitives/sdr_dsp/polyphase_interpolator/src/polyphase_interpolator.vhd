-- HDL Implementation of a Polyphase Interpolator.
--
-- This file is protected by Copyright. Please refer to the COPYRIGHT file
-- distributed with this source distribution.
--
-- This file is part of OpenCPI <http://www.opencpi.org>
--
-- OpenCPI is free software: you can redistribute it and/or modify it under the
-- terms of the GNU Lesser General Public License as published by the Free
-- Software Foundation, either version 3 of the License, or (at your option) any
-- later version.
--
-- OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
-- WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
-- A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
-- more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;

library sdr_util;
use sdr_util.sdr_util.ram;

library sdr_dsp;
use sdr_dsp.sdr_dsp.rounding_halfup;
use sdr_dsp.sdr_dsp.rounding_halfeven;

entity polyphase_interpolator is
  generic (
    complex_data_g  : boolean  := false;
    single_branch_g : boolean  := false;
    data_width_g    : positive := 16;
    max_num_taps_g  : positive := 32;
    rounding_mode_g : string   := ""
    );
  port (
    clk                  : in  std_logic;
    reset                : in  std_logic                                                      := '0';
    data_reset           : in  std_logic                                                      := '0';
    enable               : in  std_logic                                                      := '1';
    -- Control Signals
    num_taps_in          : in  unsigned(integer(ceil(log2(real(max_num_taps_g))))-1 downto 0) := to_unsigned(max_num_taps_g-1, integer(ceil(log2(real(max_num_taps_g)))));
    interpolation_factor : in  unsigned(integer(ceil(log2(real(max_num_taps_g))))-1 downto 0);
    run_single_in        : in  std_logic                                                      := '0';
    branch_in            : in  unsigned(integer(ceil(log2(real(max_num_taps_g))))-1 downto 0) := (others => '0');
    -- Tap RAM signals
    tap_data_in          : in  signed(data_width_g-1 downto 0);
    tap_addr_in          : in  unsigned(integer(ceil(log2(real(max_num_taps_g))))-1 downto 0);
    tap_valid_in         : in  std_logic;
    tap_ready_out        : out std_logic;
    -- Input Stream signals
    input_data_i_in      : in  signed(data_width_g-1 downto 0);
    input_data_q_in      : in  signed(data_width_g-1 downto 0);
    input_valid_in       : in  std_logic;
    input_last_in        : in  std_logic                                                      := '0';
    input_ready_out      : out std_logic;
    -- Output Stream signals
    output_last_out      : out std_logic;
    output_data_i_out    : out signed(data_width_g-1 downto 0);
    output_data_q_out    : out signed(data_width_g-1 downto 0);
    output_valid_out     : out std_logic;
    output_ready_in      : in  std_logic
    );
end polyphase_interpolator;

architecture rtl of polyphase_interpolator is

  constant accumulator_growth_c : integer := integer(ceil(log2(real(max_num_taps_g))))/2;

  -- state machine enum signal
  type fsm_t is (idle, accumulate);
  signal state : fsm_t;

  signal data_resetting : std_logic := '0';
  signal reset_ip       : std_logic := '0';
  -- Note: mac_valid is 1 shorter than the last_tap, as the last tap resets the
  -- accumulator_i after the last calculation.
  signal mac_valid      : std_logic_vector(4 downto 0);
  signal last_tap       : std_logic_vector(5 downto 0);
  signal last_out       : std_logic_vector(5 downto 0);
  signal last_branch    : std_logic;

  -- accumulator for MAC
  signal accumulator_i : signed(((2*data_width_g)+accumulator_growth_c)-1 downto 0);
  signal accumulator_q : signed(((2*data_width_g)+accumulator_growth_c)-1 downto 0);
  signal multiply_i    : signed(2*data_width_g-1 downto 0);
  signal multiply_q    : signed(2*data_width_g-1 downto 0);

  -- sample and tap signals
  signal input_data_i : signed(data_width_g-1 downto 0);
  signal input_data_q : signed(data_width_g-1 downto 0);
  signal sample_i     : std_logic_vector(data_width_g-1 downto 0);
  signal sample_q     : std_logic_vector(data_width_g-1 downto 0);
  signal tap          : std_logic_vector(data_width_g-1 downto 0);

  -- counter signals
  constant tap_width_c  : integer := integer(ceil(log2(real(max_num_taps_g))));
  signal branch_counter : unsigned(tap_width_c-1 downto 0);

  signal sample_ram_addr_mux : unsigned(tap_width_c-1 downto 0);
  signal sample_ram_rd_addr  : unsigned(tap_width_c-1 downto 0) := (others => '0');
  signal sample_ram_wr_addr  : unsigned(tap_width_c-1 downto 0) := (others => '0');

  -- tap ram signals
  signal tap_valid     : std_logic;
  signal tap_index     : unsigned(tap_width_c-1 downto 0);
  signal tap_data_in_r : signed(data_width_g-1 downto 0);

  -- rounded/truncated data
  signal rounded_output_data_i : signed(data_width_g-1 downto 0);
  signal rounded_output_data_q : signed(data_width_g-1 downto 0);
  signal rounded_output_valid  : std_logic;

  -- control signals
  signal input_ready       : std_logic;
  signal tap_ready         : std_logic;
  signal accumulator_valid : std_logic;

  signal sample_ram_we  : std_logic;
  signal tap_ram_enable : std_logic;
  signal last_msg       : std_logic;

begin

  -- check valid rounding type
  assert (rounding_mode_g = "truncate") or (rounding_mode_g = "half_up") or (rounding_mode_g = "half_even")
    report "rounding type unknown!" severity failure;

  assert (input_valid_in /= '1' or
          num_taps_in = 0 or
          interpolation_factor = 0 or
          (enable = '1' and input_valid_in = '1'
           and num_taps_in >= interpolation_factor))
    report "Number of branches should be less than the number of taps."
    severity error;

  reset_ip        <= reset or data_reset;
  input_ready_out <= input_ready;
  sample_ram_we   <= (input_ready and input_valid_in) or data_resetting;

  ------------------------------------------------------------------------------
  -- Sample RAM Primitive
  -- Sample ram for I (real) and Q if complex data, these are stored in 2
  -- separate ram blocks
  sample_ram_addr_mux <= sample_ram_wr_addr when state = idle         else sample_ram_rd_addr;
  input_data_i        <= input_data_i_in    when data_resetting = '0' else (others => '0');

  sample_i_ram_i : ram
    generic map (
      data_width_g    => data_width_g,
      buffer_depth_g  => max_num_taps_g,
      dual_port_g     => false,
      rd_pipeline_g   => 3,
      write_first_g   => true,
      initalization_g => (data_width_g*max_num_taps_g-1 downto 0 => '0')
      )
    port map (
      clk       => clk,
      enable    => enable,
      wr_enable => sample_ram_we,
      wr_addr   => std_logic_vector(sample_ram_addr_mux),
      wr_data   => std_logic_vector(input_data_i),
      rd_addr   => (others => '-'),
      rd_data   => sample_i
      );


  q_ram_gen : if complex_data_g generate
    input_data_q <= input_data_q_in when data_resetting = '0' else (others => '0');
    sample_q_ram_i : ram
      generic map (
        data_width_g    => data_width_g,
        buffer_depth_g  => max_num_taps_g,
        dual_port_g     => false,
        rd_pipeline_g   => 3,
        write_first_g   => true,
        initalization_g => (data_width_g*max_num_taps_g-1 downto 0 => '0')
        )
      port map (
        clk       => clk,
        enable    => enable,
        wr_enable => sample_ram_we,
        wr_addr   => std_logic_vector(sample_ram_addr_mux),
        wr_data   => std_logic_vector(input_data_q),
        rd_addr   => (others => '-'),
        rd_data   => sample_q
        );
  end generate;

  --------
  -- sample addresses
  sample_ram_index_p : process (clk)
  begin
    if rising_edge(clk) then
      if enable = '1' then
        if state = idle and (input_valid_in = '1' or data_resetting = '1' or data_reset = '1') then
          if sample_ram_wr_addr < max_num_taps_g-1 and data_reset = '0' then
            sample_ram_wr_addr <= sample_ram_wr_addr + 1;
          else
            sample_ram_wr_addr <= (others => '0');
            data_resetting     <= '0';
            -- Start a flush of data through the data_resetting flag.
            if data_reset = '1' then
              data_resetting <= '1';
            end if;
          end if;
          sample_ram_rd_addr <= sample_ram_wr_addr;
        end if;

        if state = accumulate then
          if last_tap(0) = '1' then
            sample_ram_rd_addr <= sample_ram_wr_addr-1;
          else
            if sample_ram_rd_addr = 0 then
              -- note if num_taps_in equals the full max_taps_g and this is a
              -- power of 2, then num_taps_in will actually be 0, therefore the
              -- -1 will still wrap around to the maximum value permissable.
              sample_ram_rd_addr <= to_unsigned(max_num_taps_g - 1, sample_ram_rd_addr'length);
            else
              sample_ram_rd_addr <= sample_ram_rd_addr - 1;
            end if;
          end if;
        end if;
      end if;
    end if;
  end process;

  ------------------------------------------------------------------------------
  -- Tap RAM Primitive
  ------------------------------------------------------------------------------
  tap_ram_enable <= enable or tap_valid;
  tap_ready_out  <= tap_ready;
  tap_ready      <= '1' when state = idle else '0';

  tap_ram_i : ram
    generic map (
      data_width_g    => data_width_g,
      buffer_depth_g  => max_num_taps_g,
      dual_port_g     => false,
      rd_pipeline_g   => 3,
      write_first_g   => true,
      initalization_g => (data_width_g*max_num_taps_g-1 downto 0 => '0')
      )
    port map (
      clk       => clk,
      enable    => tap_ram_enable,
      wr_enable => tap_valid,
      wr_addr   => std_logic_vector(tap_index),
      wr_data   => std_logic_vector(tap_data_in_r),
      rd_addr   => (others => '-'),
      rd_data   => tap
      );

  tap_ram_index_p : process (clk)
  begin
    if rising_edge(clk) then
      if state = idle then
        tap_valid <= '0';
        if single_branch_g then
          tap_index <= branch_in;
        else
          tap_index <= (others => '0');
        end if;
        if tap_ready = '1' and tap_valid_in = '1' then
          tap_index     <= tap_addr_in;
          tap_valid     <= '1';
          tap_data_in_r <= tap_data_in;
        end if;
      elsif enable = '1' and state = accumulate then
        tap_index <= tap_index + interpolation_factor;
        if last_tap(0) = '1' then
          if last_branch = '1' then
            if single_branch_g then
              tap_index <= branch_in;
            else
              tap_index <= (others => '0');
            end if;
          else
            tap_index <= branch_counter+1;
          end if;
        end if;
      end if;
    end if;
  end process;

  --------
  -- MAC Process
  -- Process that multiplies and accumulates samples and taps together
  mac_p : process (clk)
  begin
    if rising_edge(clk) then
      if enable = '1' then
        multiply_i <= (signed(sample_i) * signed(tap));
      end if;

      -- reset of data bus should be acceptable as this will be incorporated
      -- into dsp slice
      if reset = '1' then
        accumulator_i <= (others => '0');
      elsif enable = '1' then
        if last_tap(last_tap'high) = '1' then
          if mac_valid(mac_valid'high) = '1' then
            accumulator_i <= resize(multiply_i, accumulator_i'length);
          else
            accumulator_i <= (others => '0');
          end if;
        elsif mac_valid(mac_valid'high) = '1' then
          accumulator_i <= multiply_i + accumulator_i;
        end if;
      end if;
    end if;
  end process mac_p;

  q_mac_gen : if complex_data_g generate
    mac_q_p : process(clk)
    begin
      if rising_edge(clk) then
        if enable = '1' then
          multiply_q <= (signed(sample_q) * signed(tap));
        end if;

        if reset = '1' then
          accumulator_q <= (others => '0');
        elsif enable = '1' then
          if last_tap(last_tap'high) = '1' then
            if mac_valid(mac_valid'high) = '1' then
              accumulator_q <= resize(multiply_q, accumulator_q'length);
            else
              accumulator_q <= (others => '0');
            end if;
          elsif mac_valid(mac_valid'high) = '1' then
            accumulator_q <= multiply_q + accumulator_q;
          end if;
        end if;
      end if;
    end process;
  end generate;

  accumulator_valid <= last_tap(last_tap'high);
  output_last_out   <= last_out(last_out'high);

  last_branch <= '1' when single_branch_g or branch_counter >= (interpolation_factor-1) else '0';
  last_tap(0) <= '1' when (tap_index >= (num_taps_in - interpolation_factor)
                           and mac_valid(0) = '1')
                 else '0';
  mac_valid(0) <= '1' when state = accumulate else '0';

  ------------------------------------------------------------------------------
  -- Main FSM process
  main_fsm_p : process (clk)
  begin
    if rising_edge(clk) then
      if reset = '1' then
        state       <= idle;
        input_ready <= '0';
      elsif enable = '1' then
        last_out(last_out'high downto 1)   <= last_out(last_out'high-1 downto 0);
        last_tap(last_tap'high downto 1)   <= last_tap(last_tap'high-1 downto 0);
        mac_valid(mac_valid'high downto 1) <= mac_valid(mac_valid'high-1 downto 0);

        input_ready <= '0';
        last_out(0) <= '0';
        case state is
          when idle =>
            input_ready <= '1';
            if single_branch_g then
              branch_counter <= branch_in;
            else
              branch_counter <= (others => '0');
            end if;

            if data_reset = '1' or data_resetting = '1' then
              input_ready <= '0';
            elsif sample_ram_we = '1' or run_single_in = '1' then
              input_ready <= '0';
              last_msg    <= input_last_in;
              state       <= accumulate;
            end if;

          when accumulate =>
            if last_tap(0) = '1' then
              if not single_branch_g then
                branch_counter <= branch_counter + 1;
              end if;

              if last_branch = '1' then
                input_ready <= '1';
                last_out(0) <= last_msg;
                state       <= idle;
              end if;
            end if;

          when others =>
            report "Bad state found" severity error;
            state <= idle;
        end case;
      end if;
    end if;
  end process main_fsm_p;


  ------------------------------------------------------------------------------
  -- Selectable Rounding
  ------------------------------------------------------------------------------
  truncation_gen : if rounding_mode_g = "truncate" generate
    component rounding_truncate
      generic (
        input_width_g   : natural := 32;
        output_width_g  : natural := 16;
        saturation_en_g : boolean := false
        );
      port (
        clk            : in  std_logic;
        reset          : in  std_logic;
        clk_en         : in  std_logic;
        data_in        : in  signed(input_width_g - 1 downto 0);
        data_valid_in  : in  std_logic;
        binary_point   : in  integer range 0 to input_width_g - 1;
        data_out       : out signed(output_width_g - 1 downto 0);
        data_valid_out : out std_logic
        );
    end component;
  begin
    rounding_truncate_real_i : rounding_truncate
      generic map (
        input_width_g   => accumulator_i'length,
        output_width_g  => data_width_g,
        saturation_en_g => true
        )
      port map (
        clk            => clk,
        reset          => reset,
        clk_en         => enable,
        data_in        => accumulator_i,
        data_valid_in  => accumulator_valid,
        binary_point   => data_width_g - 1,
        data_out       => rounded_output_data_i,
        data_valid_out => rounded_output_valid
        );
    rounding_q_gen : if complex_data_g generate
      rounding_truncate_imag_i : rounding_truncate
        generic map (
          input_width_g   => accumulator_i'length,
          output_width_g  => data_width_g,
          saturation_en_g => true
          )
        port map (
          clk            => clk,
          reset          => reset,
          clk_en         => enable,
          data_in        => accumulator_q,
          data_valid_in  => accumulator_valid,
          binary_point   => data_width_g - 1,
          data_out       => rounded_output_data_q,
          data_valid_out => open
          );
    end generate;
  end generate truncation_gen;

  half_up_gen : if rounding_mode_g = "half_up" generate
    component rounding_halfup
      generic (
        input_width_g   : integer := 32;
        output_width_g  : integer := 16;
        saturation_en_g : boolean := false
        );
      port (
        clk            : in  std_logic;
        reset          : in  std_logic;
        clk_en         : in  std_logic;
        data_in        : in  signed(input_width_g - 1 downto 0);
        data_valid_in  : in  std_logic;
        binary_point   : in  integer range 0 to input_width_g - 1;
        data_out       : out signed(output_width_g - 1 downto 0);
        data_valid_out : out std_logic
        );
    end component;
  begin
    round_half_up_real_i : rounding_halfup
      generic map (
        input_width_g   => accumulator_i'length,
        output_width_g  => data_width_g,
        saturation_en_g => true
        )
      port map (
        clk            => clk,
        reset          => reset,
        clk_en         => enable,
        data_in        => accumulator_i,
        data_valid_in  => accumulator_valid,
        binary_point   => data_width_g - 1,
        data_out       => rounded_output_data_i,
        data_valid_out => rounded_output_valid
        );
    rounding_q_gen : if complex_data_g generate
      round_half_up_imag_i : rounding_halfup
        generic map (
          input_width_g   => accumulator_i'length,
          output_width_g  => data_width_g,
          saturation_en_g => true
          )
        port map (
          clk            => clk,
          reset          => reset,
          clk_en         => enable,
          data_in        => accumulator_q,
          data_valid_in  => accumulator_valid,
          binary_point   => data_width_g - 1,
          data_out       => rounded_output_data_q,
          data_valid_out => open
          );
    end generate;
  end generate half_up_gen;

  half_even_gen : if rounding_mode_g = "half_even" generate
    component rounding_halfeven
      generic (
        input_width_g   : integer := 32;
        output_width_g  : integer := 16;
        saturation_en_g : boolean := false
        );
      port (
        clk            : in  std_logic;
        reset          : in  std_logic;
        clk_en         : in  std_logic;
        data_in        : in  signed(input_width_g - 1 downto 0);
        data_valid_in  : in  std_logic;
        binary_point   : in  integer range 0 to input_width_g - 1;
        data_out       : out signed(output_width_g - 1 downto 0);
        data_valid_out : out std_logic
        );
    end component;
  begin
    round_half_even_real_i : rounding_halfeven
      generic map (
        input_width_g   => accumulator_i'length,
        output_width_g  => data_width_g,
        saturation_en_g => true
        )
      port map (
        clk            => clk,
        reset          => reset,
        clk_en         => enable,
        data_in        => accumulator_i,
        data_valid_in  => accumulator_valid,
        binary_point   => data_width_g - 1,
        data_out       => rounded_output_data_i,
        data_valid_out => rounded_output_valid
        );
    rounding_q_gen : if complex_data_g generate
      round_half_even_imag_i : rounding_halfeven
        generic map (
          input_width_g   => accumulator_i'length,
          output_width_g  => data_width_g,
          saturation_en_g => true
          )
        port map (
          clk            => clk,
          reset          => reset,
          clk_en         => enable,
          data_in        => accumulator_q,
          data_valid_in  => accumulator_valid,
          binary_point   => data_width_g - 1,
          data_out       => rounded_output_data_q,
          data_valid_out => open
          );
    end generate;
  end generate half_even_gen;

  ------------------------------------------------------------------------------
  -- Output Buffer Process
  ------------------------------------------------------------------------------
  output_data_i_out <= rounded_output_data_i;
  output_data_q_out <= rounded_output_data_q;
  output_valid_out  <= rounded_output_valid;


end rtl;
