.. downsample_protocol_interface_delay_v2 documentation

.. This file is protected by Copyright. Please refer to the COPYRIGHT file
   distributed with this source distribution.

   This file is part of OpenCPI <http://www.opencpi.org>

   OpenCPI is free software: you can redistribute it and/or modify it under the
   terms of the GNU Lesser General Public License as published by the Free
   Software Foundation, either version 3 of the License, or (at your option) any
   later version.

   OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>.


.. _downsample_protocol_interface_delay_v2-primitive:


Protocol Interface Delay (Down-sample) for V2 HDL Interface (``downsample_protocol_interface_delay_v2``)
========================================================================================================
Delays OpenCPI interface signals to align them with a processed data signal. Handles timestamp correction to maintain timestamp alignment with down-sampled data. Upgraded to OpenCPI version 2 standard.

Design
------
When data processing is performed on a stream of input data it typically results in a valid output one or more clock cycles after the input. This primitive delays OpenCPI interface signals in order to align them with a processed data stream. When the input opcode is equal to ``PROCESSED_DATA_OPCODE_G`` the processed data stream is output instead of the delayed input data.

This primitive differs from the :ref:`protocol interface delay primitive v2 <protocol_interface_delay_v2-primitive>` as it also handles realignment of timestamp messages when the sample the timestamp is referring to is removed due to a down-sampling operation.

The delay can be variable at run time to allow for run-time downsample rates which have a variable latency, and different responses to flush and discontinuity opcodes.

The primitive is also capable of subtracting a group delay onto outgoing timestamps.

Implementation
--------------
A shift register of length ``DELAY_G`` is constructed for each of the interface signals (``EOM`` (end of message), ``valid``, ``byte_enable``, ``opcode`` and ``data``). A shift register of length ``DELAY_G`` is also constructed for the interface ``take`` signal to keep track of when data was actually taken from the input interface. If the ``dynamic_delay_g`` generic is set to false then this shift register is static, however if set to true then these signals are put into a dynamically sized shift register, with the actual pull out length being defined by ``delay_length``.

The ``DELAY_G`` value is set to the same length as the delay present between the input signal and the processed data. As such, at the end of the shift registers, the interface signals are correctly aligned with the processed data. A multiplexer is used to select either the processed data signal or the delayed input signal. Processed data is selected when the delayed ``opcode`` signal is equal to ``PROCESSED_DATA_OPCODE_G`` and the delayed ``valid`` signal is ``1``. The output ``give`` signal is driven whenever the delayed version of the interface ``take`` signal is ``1`` and the output interface is ready to accept data.

If the output interface is not ready to accept data then the delay shift registers are not advanced. This means for every input signal an output signal is generated after a delay of ``DELAY_G`` clock cycles where the output is ready to accept data.

Timestamp messages are suppressed until the next data sample so that it can be determined if the timestamp related to a sample that is removed due to the down-sampling operation. If the sample is not removed, the timestamp is passed on unmodified. If the sample is removed the timestamp is incremented by the sample interval for each new data sample into the module (that is not output due to the down-sampling operation). When a data sample is output from the module the adjusted timestamp is inserted just before the data sample. The relative location of the timestamp versus the incoming sample can be specified through the ``sample_before_timestamp_g`` generic. This allows setting whether the data sample received after a timestamp message should be output before or after that timestamp message, (for example if the timestamp applied to the in flight data buffer, or to the next processed buffer).

It is also possible to set timestamp messages to be forwarded without delaying until the next data sample is received in situations where there is no data currently being processed. This mode can be set to occur if either (or both) ``last_is_data_reset_g`` or ``discontinuity_is_data_reset_g`` are set to be true. In these scenarios, ``processed_end_in`` being high or a discontinuity opcode will be assumed to return the processed_data pipeline to its initial state, and as such allow timestamps to be sent without delaying until an incoming sample has occurred.

Interface
---------

Generics
~~~~~~~~

 * ``dynamic_delay_g`` (``boolean``): Defines whether a dynamic or static shift register should be used.

 * ``sample_before_timestamp_g`` (``boolean``): Defines whether the next sample on ``processed_data`` should be output before or after any received timestamps.

 * ``last_is_data_reset_g`` (``boolean``): Defines whether an end of message signal passed through the data path should be interpreted as the processed_data pipeline being empty (therefore removing any timestamp opcode reordering).

 * ``discontinuity_is_data_reset_g`` (``boolean``): Defines whether a discontinuity message should be interpreted as the processed_data pipeline has been reset (therefore removing any timestamp opcode reordering).

 * ``use_processed_signals_g`` (``boolean``): Defines whether the ``processed_end_in`` and ``processed_mask_in`` signals are used for the output data or whether the output data uses the delayed input signals.

 * ``delay_g`` (``positive``): Sets the length of the interface delay (in clock cycles) to be added.

 * ``data_width_g`` (``positive``): Sets the width of the interface data signal.

 * ``opcode_width_g`` (``positive``): Sets the width of the interface opcode signal.

 * ``byte_enable_width_g`` (``positive``): Sets the width of the interface byte enable signal.

 * ``processed_data_opcode_g`` (``std_logic_vector``): Sets the value of the opcode for which the ``processed_stream_in`` signal should be used for the output data signal instead of the delayed input data signal.

 * ``time_opcode_g`` (``std_logic_vector``): Sets the value of the opcode which indicates that the message contains a timestamp.

 * ``sample_interval_opcode_g`` (``std_logic_vector``): Sets the value of the opcode which indicates that the message contains the sample interval.

 * ``discontinuity_opcode_g`` (``std_logic_vector``): Sets the value of the opcode which indicates that the message contains a discontinuity.

Ports
~~~~~

 * ``clk`` (``std_logic``), in: Clock. Inputs and outputs registered on rising edge.

 * ``reset`` (``std_logic``), in: Reset. Active high, synchronous with rising edge of clock.

 * ``enable`` (``std_logic``), in: Enable. When high, the delay pipeline is advanced. Therefore, data is absorbed on ``input`` and ``output`` changes when ``enable`` is high.

 * ``take_in`` (``std_logic``), in: Qualifies ``input_valid`` and ``input_ready``. Optional - can be left unconnected or driven high when not used. This only needs to be used when ``enable`` can be high while the input is not changing. There is no need to feed-back ``input_hold_out`` to ``take_in`` - that is handled internally.

 * ``delay_length`` (``std_logic_vector``, ``delay_g`` bits), in: The current delay length to use (only applies when ``dynamic_delay_g`` is true).

 * ``group_delay_seconds`` (``std_logic_vector``, 32 bits), in: The seconds of any group delay desired (default to not being 0), this is intended to be connected to a worker property if a group delay is desired.

 * ``group_delay_fractional`` (``std_logic_vector``, 64 bits), in: The fractional section of any group delay desired (default to not being 0), this is intended to be connected to a worker property if a group delay is desired.

 * ``input_hold_out`` (``std_logic``), out: When high data processing should be stopped and data should not be taken from the input interface. This indicates that the primitive is generating data, i.e. an adjusted timestamp.

 * ``processed_stream_in`` (``std_logic_vector``, ``DATA_WIDTH_G`` bits), in: The processed data stream. i.e. the data this primitive is aligning the interface signals with.

 * ``processed_mask_in`` (``std_logic``), in: Set to '1' when the processed data stream is valid and not discarded from the down-sampling operation.

 * ``processed_end_in`` (``std_logic``), in: Set to '1' when the processed data stream has reached the end of message.

 * ``input_som`` (``std_logic``), in: SOM signal from OpenCPI interface. Optional - can be left unconnected if ``output_eof`` is not required.

 * ``input_eom`` (``std_logic``), in: EOM signal from OpenCPI interface.

 * ``input_eof`` (``std_logic``), in: EOF signal from OpenCPI interface. Optional - can be left unconnected if ``output_eof`` is not required.

 * ``input_valid`` (``std_logic``), in: Valid signal from OpenCPI interface.

 * ``input_ready`` (``std_logic``), in: Ready signal from OpenCPI interface.

 * ``input_byte_enable`` (``std_logic_vector``, ``BYTE_ENABLE_WIDTH_G`` bits), in: Byte enable signal from OpenCPI interface.

 * ``input_opcode`` (``std_logic_vector``, ``OPCODE_WIDTH_G`` bits), in: Opcode signal from OpenCPI interface.

 * ``input_data`` (``std_logic_vector``, ``DATA_WIDTH_G`` bits), in: Data signal from OpenCPI interface.

 * ``output_som`` (``std_logic``), out: SOM signal on output OpenCPI interface.

 * ``output_eom`` (``std_logic``), out: EOM signal on output OpenCPI interface.

 * ``output_eof`` (``std_logic``), out: EOF signal on output OpenCPI interface.

 * ``output_valid`` (``std_logic``), out: Valid signal on output OpenCPI interface.

 * ``output_give`` (``std_logic``), out: Give signal on output OpenCPI interface.

 * ``output_byte_enable`` (``std_logic_vector``, ``BYTE_ENABLE_WIDTH_G`` bits), out: Byte enable signal on output OpenCPI interface.

 * ``output_opcode`` (``std_logic_vector``, ``OPCODE_WIDTH_G`` bits), out: Opcode signal on output OpenCPI interface.

 * ``output_data`` (``std_logic_vector``, ``DATA_WIDTH_G`` bits), out: Data signal on output OpenCPI interface.

Dependencies
------------
The dependencies to other elements in OpenCPI are:

 * :ref:`Timestamp recovery primitive <timestamp_recovery-primitive>`

There is also a dependency on:

 * ``ieee.std_logic_1164``

 * ``ieee.numeric_std``

 * ``ieee.math_real``

Limitations
-----------
Limitations of ``downsample_protocol_interface_delay`` are:

 * None.
