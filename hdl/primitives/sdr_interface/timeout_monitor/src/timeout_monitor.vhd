-- Timeout primitive.
--
-- This file is protected by Copyright. Please refer to the COPYRIGHT file
-- distributed with this source distribution.
--
-- This file is part of OpenCPI <http://www.opencpi.org>
--
-- OpenCPI is free software: you can redistribute it and/or modify it under the
-- terms of the GNU Lesser General Public License as published by the Free
-- Software Foundation, either version 3 of the License, or (at your option) any
-- later version.
--
-- OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
-- WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
-- A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
-- more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity timeout_monitor is
  generic (
    sample_opcode_g     : std_logic_vector;
    data_width_g        : positive;
    time_width_g        : positive;
    opcode_width_g      : positive;
    byte_enable_width_g : positive
    );
  port (
    clk                : in  std_logic;
    reset              : in  std_logic;
    enable             : in  std_logic;  -- only enable when next data is available
    take_in            : in  std_logic;
    timeout_in         : in  unsigned(time_width_g - 1 downto 0);
    time_valid         : in  std_logic;
    time_fraction      : in  unsigned(time_width_g - 1 downto 0);
    input_som          : in  std_logic;
    input_eom          : in  std_logic;
    input_eof          : in  std_logic;
    input_valid        : in  std_logic;
    input_ready        : in  std_logic;
    input_byte_enable  : in  std_logic_vector(byte_enable_width_g - 1 downto 0);
    input_opcode       : in  std_logic_vector(opcode_width_g - 1 downto 0);
    input_data         : in  std_logic_vector(data_width_g - 1 downto 0);
    timeout_out        : out std_logic;
    -- Output interface signals
    output_som         : out std_logic;
    output_eom         : out std_logic;
    output_eof         : out std_logic;
    output_valid       : out std_logic;
    output_give        : out std_logic;
    output_byte_enable : out std_logic_vector(byte_enable_width_g - 1 downto 0);
    output_opcode      : out std_logic_vector(opcode_width_g - 1 downto 0);
    output_data        : out std_logic_vector(data_width_g - 1 downto 0)
    );
end timeout_monitor;

architecture rtl of timeout_monitor is
  type byte_enable_array_t is array (1 downto 0) of std_logic_vector(byte_enable_width_g - 1 downto 0);
  type opcode_array_t is array (1 downto 0) of std_logic_vector(opcode_width_g - 1 downto 0);
  type data_array_t is array (1 downto 0) of std_logic_vector(data_width_g - 1 downto 0);

  -- Interface delay registers
  signal input_register_ready       : std_logic_vector(1 downto 0);
  signal input_register_som         : std_logic_vector(1 downto 0);
  signal input_register_eom         : std_logic_vector(1 downto 0);
  signal input_register_eof         : std_logic_vector(1 downto 0);
  signal input_register_valid       : std_logic_vector(1 downto 0);
  signal input_register_byte_enable : byte_enable_array_t;
  signal input_register_opcode      : opcode_array_t;
  signal input_register_data        : data_array_t;

  signal give                : std_logic;
  signal message_in_progress : std_logic;
  signal timeout             : std_logic;
  signal timeout_r           : std_logic;
  signal time_current        : unsigned(time_width_g - 1 downto 0) := (others => '0');
  signal time_previous       : unsigned(time_width_g - 1 downto 0) := (others => '0');
begin

  time_current_p : process(clk)
  begin
    if rising_edge(clk) then
      if time_valid = '1' then
        time_current <= time_fraction;
      end if;
    end if;
  end process;

  -- Keep track of time
  time_p : process(clk)
  begin
    if rising_edge(clk) then
      if (input_valid = '1' and input_opcode = sample_opcode_g) or timeout_r = '1' then
        time_previous       <= time_current;
        message_in_progress <= '1';
        if input_register_eom(1) = '1' or timeout_r = '1' then
          message_in_progress <= '0';
        end if;
      end if;
      if reset = '1' then
        message_in_progress <= '0';
      end if;
    end if;
  end process;

  -- confirm timeout is not disabled
  timeout <= '1' when timeout_in /= 0 and message_in_progress = '1'
             and time_current - time_previous >= timeout_in else '0';

  timeout_out <= timeout;

  interface_delay_pipeline_p : process(clk)
  begin
    if rising_edge(clk) then
      if enable = '1' then
        give <= '0';
        if take_in = '1' or input_eof = '1' or timeout_r = '1' then  -- eof currently prevents timeout to be tested by framework
          give                       <= '1';
          input_register_ready       <= input_register_ready(0) & input_ready;
          input_register_valid       <= input_register_valid(0) & input_valid;
          input_register_som         <= input_register_som(0) & input_som;
          input_register_eom         <= input_register_eom(0) & (input_eom or timeout_r);
          input_register_eof         <= input_register_eof(0) & input_eof;
          input_register_byte_enable <= input_register_byte_enable(0) & input_byte_enable;
          input_register_opcode      <= input_register_opcode(0) & input_opcode;
          input_register_data        <= input_register_data(0) & input_data;
        end if;
      end if;
      if reset = '1' then
        input_register_ready <= (others => '0');
        input_register_valid <= (others => '0');
        input_register_eof   <= (others => '0');
      end if;
    end if;
  end process;

  timeout_p : process(clk)
  begin
    if rising_edge(clk) then
      if enable = '1' and take_in = '1' then
        timeout_r <= '0';               -- stop forcing through
      elsif timeout = '1' then
        timeout_r <= '1';
      end if;
    end if;
  end process;

  output_data <= input_register_data(1);

  -- Streaming interface output
  output_give        <= input_register_ready(1) and give;
  output_valid       <= input_register_valid(1) and give;
  output_som         <= input_register_som(1);
  output_eom         <= input_register_eom(1);
  output_eof         <= input_register_eof(1);
  output_byte_enable <= input_register_byte_enable(1);
  output_opcode      <= input_register_opcode(1);

end rtl;
