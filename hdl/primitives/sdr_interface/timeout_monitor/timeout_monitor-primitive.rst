.. timeout_monitor documentation

.. This file is protected by Copyright. Please refer to the COPYRIGHT file
   distributed with this source distribution.

   This file is part of OpenCPI <http://www.opencpi.org>

   OpenCPI is free software: you can redistribute it and/or modify it under the
   terms of the GNU Lesser General Public License as published by the Free
   Software Foundation, either version 3 of the License, or (at your option) any
   later version.

   OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>.


.. _timeout_monitor-primitive:


Timeout Monitor (``timeout_monitor``)
=====================================
This primitive will insert an end of message into the interface stream if the difference between the current time and the time recorded at the last seen valid input sample opcode exceeds the timeout value.


Design
------
To avoid a trailing end of message in output sample messages, where ``output_eom`` is ``1`` whilst ``output_valid`` is ``0``, the interface messages are stored inside registers. This allows the timeout end of message to be output with the final sample of the message.


Implementation
--------------
Stores the interface signals in internal registers when ``take_in`` is ``1``. If new data does not arrive before the difference in time exceeds the ``timeout_in`` value, the newest data held will be output with the end of message asserted.

Internal registers are constructed for each of the interface signals (``som`` (start of message), ``eom`` (end of message), ``ready``, ``valid``, ``byte_enable``, ``opcode``, and ``data``).

The previous time value is updated when the ``input_valid`` signal is ``1`` and the ``input_opcode`` signal is equal to ``sample_opcode_g``. The output ``give`` signal is driven high for a single clock cycle when the ``take_in`` signal is high, the delayed version of the interface ``take`` signal is ``1``, and the output interface is ready to accept data.

Interface
---------

Generics
~~~~~~~~

 * ``sample_opcode_g`` (``std_logic_vector``): Sets the value of the opcode that will trigger the latest time value to be used as the old time to compare against.

 * ``data_width_g`` (``positive``): Sets the width of the interface data signal.

 * ``time_width_g`` (``positive``): Sets the width of the timeout data signals. Should be set to be the length of the fractional part of the time interface.

 * ``opcode_width_g`` (``positive``): Sets the width of the interface opcode signal.

 * ``byte_enable_width_g`` (``positive``): Sets the width of the interface byte enable signal.

Ports
~~~~~

 * ``clk`` (``std_logic``), in: Clock. Inputs and outputs registered on rising edge.

 * ``reset`` (``std_logic``), in: Reset. Active high, synchronous with rising edge of clock.

 * ``enable`` (``std_logic``), in: Enable. When high, the delay pipeline can be advanced by the other control signals.

 * ``take_in`` (``std_logic``), in: Triggers the storing of interface data in the internal shift registers. Shifts the shift registers. Qualifies ``input_ready`` and ``input_valid``.

 * ``timeout_in`` (``unsigned``, ``time_width_g`` bits), in: The timeout value used to measure against the change in the current ``time_fraction`` and the previous ``time_fraction`` values. Timeout functionality is disabled when this is set to 0.

 * ``time_valid`` (``std_logic``), in: Valid signal from OpenCPI time interface.

 * ``time_fraction`` (``unsigned``, ``time_width_g`` bits), in: Data signal from OpenCPI time interface.

 * ``input_som`` (``std_logic``), in: SOM signal from OpenCPI interface.

 * ``input_eom`` (``std_logic``), in: EOM signal from OpenCPI interface.

 * ``input_eof`` (``std_logic``), in: EOF signal from OpenCPI interface. Secondary trigger that will shift the internal shift registers.

 * ``input_valid`` (``std_logic``), in: Valid signal from OpenCPI interface.

 * ``input_ready`` (``std_logic``), in: Ready signal from OpenCPI interface.

 * ``input_byte_enable`` (``std_logic_vector``, ``byte_enable_width_g`` bits), in: Byte enable signal from OpenCPI interface.

 * ``input_opcode`` (``std_logic_vector``, ``opcode_width_g`` bits), in: Opcode signal from OpenCPI interface.

 * ``input_data`` (``std_logic_vector``, ``data_width_g`` bits), in: Data signal from OpenCPI interface.

 * ``timeout_out`` (``std_logic``), out: Timeout signal to indicate when a timeout has occurred.

 * ``output_som`` (``std_logic``), out: SOM signal on output OpenCPI interface.

 * ``output_eom`` (``std_logic``), out: EOM signal on output OpenCPI interface.

 * ``output_eof`` (``std_logic``), out: EOF signal on output OpenCPI interface.

 * ``output_valid`` (``std_logic``), out: Valid signal on output OpenCPI interface.

 * ``output_give`` (``std_logic``), out: Give signal on output OpenCPI interface.

 * ``output_byte_enable`` (``std_logic_vector``, ``byte_enable_width_g`` bits), out: Byte enable signal on output OpenCPI interface.

 * ``output_opcode`` (``std_logic_vector``, ``opcode_width_g`` bits), out: Opcode signal on output OpenCPI interface.

 * ``output_data`` (``std_logic_vector``, ``data_width_g`` bits), out: Data signal on output OpenCPI interface.

Dependencies
------------
The dependencies to other elements in OpenCPI are:

 * None.

There is also a dependency on:

 * ``ieee.std_logic_1164``

 * ``ieee.numeric_std``

.. timeout_monitor-primitive_Limitations:

Limitations
-----------
Limitations of ``timeout_monitor`` are:

 * Timeout triggering at the right time has not been fully tested.
