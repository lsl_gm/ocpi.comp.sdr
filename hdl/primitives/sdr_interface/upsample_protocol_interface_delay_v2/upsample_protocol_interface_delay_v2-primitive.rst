.. upsample_protocol_interface_delay_v2 documentation

.. This file is protected by Copyright. Please refer to the COPYRIGHT file
   distributed with this source distribution.

   This file is part of OpenCPI <http://www.opencpi.org>

   OpenCPI is free software: you can redistribute it and/or modify it under the
   terms of the GNU Lesser General Public License as published by the Free
   Software Foundation, either version 3 of the License, or (at your option) any
   later version.

   OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>.


.. _upsample_protocol_interface_delay_v2-primitive:


Upsample Protocol Interface Delay Version 2 (``upsample_protocol_interface_delay_v2``)
======================================================================================
Delays OpenCPI interface signals to align them with a processed data signal. Used when the data is being up-sampled and extra samples need to be added into the pipeline.

Design
------
When data processing is performed on a stream of input data it typically results in a valid output one or more clock cycles after the input. This primitive delays OpenCPI interface signals in order to align them with a processed data stream. When the input opcode is equal to ``PROCESSED_DATA_OPCODE_G`` the processed data stream is output instead of the delayed input data.

Implementation
--------------
Two shift registers of length ``STAGE1_DELAY_G`` and ``STAGE2_DELAY_G`` are constructed for each of the interface signals (``SOM`` (start of message), ``EOM`` (end of message), ``take_in``, ``valid``, ``byte_enable``, ``opcode``, and ``data``).

The ``STAGE1_DELAY_G`` value is set to the same length as the delay present between the input signal and the interpolated data. The ``STAGE2_DELAY_G`` value is set to the same length as the delay present between the interpolated data and the processed data.As such, at the end of the shift registers, the interface signals are correctly aligned with the processed data. A multiplexer is used to select either the processed data signal or the delayed input signal. Processed data is selected when the delayed ``opcode`` signal is equal to ``PROCESSED_DATA_OPCODE_G`` and the delayed ``valid`` signal is ``1``. The output ``give`` signal is driven whenever the delayed version of the interface ``take`` signal is ``1`` and the output interface is ready to accept data.

If the output interface is not ready to accept data then the delay shift registers are not advanced. This means for every input signal an output signal is generated after a delay of ``DELAY_G`` clock cycles where the output is ready to accept data.

Interface
---------

Generics
~~~~~~~~

 * ``STAGE1_DELAY_G`` (``positive``): Sets the length of the interface delay (in clock cycles) before interpolation.

 * ``STAGE2_DELAY_G`` (``positive``): Sets the length of the interface delay (in clock cycles) after interpolation.

 * ``DATA_WIDTH_G`` (``positive``): Sets the width of the interface data signal.

 * ``OPCODE_WIDTH_G`` (``positive``): Sets the width of the interface opcode signal.

 * ``BYTE_ENABLE_WIDTH_G`` (``positive``): Sets the width of the interface byte enable signal.

 * ``PROCESSED_DATA_MUX_G`` (``std_logic``): When ``1`` and the delayed interface ``opcode`` signal is equal to ``PROCESSED_DATA_OPCODE_G``, then data from the ``processed_stream_in`` signal should be used for the output data signal instead of the delayed input data signal. When ``0`` the delayed data signal is always used.

 * ``PROCESSED_DATA_OPCODE_G`` (``std_logic_vector``): Sets the value of the opcode for which the ``processed_stream_in`` signal should be used for the output data signal instead of the delayed input data signal.

Ports
~~~~~

 * ``clk`` (``std_logic``), in: Clock. Inputs and outputs registered on rising edge.

 * ``reset`` (``std_logic``), in: Reset. Active high, synchronous with rising edge of clock.

 * ``enable`` (``std_logic``), in: Enable. When high, the delay pipeline is advanced. Therefore, data is absorbed on input and output changes when enable is high.

 * ``take_in`` (``std_logic``), in: Qualifies input_valid and input_ready. Optional - can be left unconnected or driven high when not used. This only needs to be used when enable can be high while the input is not changing.

 * ``upsample_factor`` (``unsigned``), in: The number of extra samples that will be added per sample into the data stream due to interpolation

 * ``processed_stream_in`` (``std_logic_vector``, ``DATA_WIDTH_G`` bits), in: The processed data stream. I.e. the data this primitive is aligning the interface signals with.

 * ``input_som`` (``std_logic``), in: SOM signal from OpenCPI interface.

 * ``input_eom`` (``std_logic``), in: EOM signal from OpenCPI interface.

 * ``input_eof`` (``std_logic``), in: EOF signal from OpenCPI interface.

 * ``input_valid`` (``std_logic``), in: Valid signal from OpenCPI interface.

 * ``input_ready`` (``std_logic``), in: Valid signal from OpenCPI interface.

 * ``input_byte_enable`` (``std_logic_vector``, ``BYTE_ENABLE_WIDTH_G`` bits), in: Byte enable signal from OpenCPI interface.

 * ``input_opcode`` (``std_logic_vector``, ``OPCODE_WIDTH_G`` bits), in: Opcode signal from OpenCPI interface.

 * ``input_data`` (``std_logic_vector``, ``DATA_WIDTH_G`` bits), in: Data signal from OpenCPI interface.

 * ``output_som`` (``std_logic``), out: SOM signal on output OpenCPI interface.

 * ``output_eom`` (``std_logic``), out: EOM signal on output OpenCPI interface.

 * ``output_eof`` (``std_logic``), out: EOF signal on output OpenCPI interface.

 * ``output_valid`` (``std_logic``), out: Valid signal on output OpenCPI interface.

 * ``output_give`` (``std_logic``), out: Give signal on output OpenCPI interface.

 * ``output_byte_enable`` (``std_logic_vector``, ``BYTE_ENABLE_WIDTH_G`` bits), out: Byte enable signal on output OpenCPI interface.

 * ``output_opcode`` (``std_logic_vector``, ``OPCODE_WIDTH_G`` bits), out: Opcode signal on output OpenCPI interface.

 * ``output_data`` (``std_logic_vector``, ``DATA_WIDTH_G`` bits), out: Data signal on output OpenCPI interface.

Dependencies
------------
The dependencies to other elements in OpenCPI are:

 * None.

There is also a dependency on:

 * ``ieee.std_logic_1164``

 * ``ieee.numeric_std``

Limitations
-----------
Limitations of ``protocol_interface_delay`` are:

 * None
