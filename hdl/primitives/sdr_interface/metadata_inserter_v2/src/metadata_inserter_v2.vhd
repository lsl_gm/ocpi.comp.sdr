-- Metadata Inserter primitive
--
-- This file is protected by Copyright. Please refer to the COPYRIGHT file
-- distributed with this source distribution.
--
-- This file is part of OpenCPI <http://www.opencpi.org>
--
-- OpenCPI is free software: you can redistribute it and/or modify it under the
-- terms of the GNU Lesser General Public License as published by the Free
-- Software Foundation, either version 3 of the License, or (at your option) any
-- later version.
--
-- OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
-- WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
-- A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
-- more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity metadata_inserter_v2 is
  generic (
    data_width_g        : positive;
    opcode_width_g      : positive;
    byte_enable_width_g : positive;
    metadata_opcode_g   : std_logic_vector;
    data_opcode_g       : std_logic_vector
    );
  port (
    clk                : in  std_logic;
    reset              : in  std_logic;
    metadata_id        : in  std_logic_vector(31 downto 0);  -- Id of the metadata message that should be inserted.
    metadata_value     : in  std_logic_vector(63 downto 0);  -- Value of the metadata message that should be inserted.
    trigger            : in  std_logic;  -- High when a metadata message should be outputted.
    message_complete   : out std_logic;  -- Set for 1 clk when the metadata message has been completed
    take_in            : in  std_logic;
    take_out           : out std_logic;
    input_som          : in  std_logic;
    input_eom          : in  std_logic;
    input_eof          : in  std_logic;
    input_valid        : in  std_logic;
    input_byte_enable  : in  std_logic_vector(byte_enable_width_g - 1 downto 0);
    input_opcode       : in  std_logic_vector(opcode_width_g - 1 downto 0);
    input_data         : in  std_logic_vector(data_width_g - 1 downto 0);
    input_ready        : in  std_logic;
    output_som         : out std_logic;
    output_eom         : out std_logic;
    output_eof         : out std_logic;
    output_valid       : out std_logic;
    output_byte_enable : out std_logic_vector(byte_enable_width_g - 1 downto 0);
    output_opcode      : out std_logic_vector(opcode_width_g - 1 downto 0);
    output_data        : out std_logic_vector(data_width_g - 1 downto 0);
    output_give        : out std_logic
    );
end metadata_inserter_v2;

architecture rtl of metadata_inserter_v2 is

  function rotate_byte_order(data : std_logic_vector; rotate_width : natural := 8) return std_logic_vector is
    variable ret : std_logic_vector(data'range);
  begin
    for idx in 0 to (data'length-1)/rotate_width loop
      ret((ret'length-1) - idx*rotate_width downto (ret'length-1) - (idx*rotate_width + (rotate_width-1))) := data(((idx+1)*rotate_width)-1 downto idx*rotate_width);
    end loop;
    return ret;
  end function;

  constant num_transfers_c : natural := (128 / data_width_g);
  signal insert            : std_logic;

  signal trigger_i          : std_logic;
  signal trigger_r          : std_logic;
  signal take_out_i         : std_logic;
  signal som_seen           : std_logic;
  signal message_complete_i : std_logic;
  signal message_complete_r : std_logic;

  signal triggered : std_logic;

  signal meta_som      : std_logic;
  signal meta_eom      : std_logic;
  signal meta_word_i   : std_logic_vector(127 downto 0);
  signal meta_word     : std_logic_vector(127 downto 0);
  signal meta_word_pos : std_logic_vector(num_transfers_c-1 downto 0);
  signal meta_data     : std_logic_vector(data_width_g - 1 downto 0);

  signal input_som_r         : std_logic;
  signal input_eom_r         : std_logic;
  signal input_eof_r         : std_logic;
  signal input_valid_r       : std_logic;
  signal input_byte_enable_r : std_logic_vector(byte_enable_width_g - 1 downto 0);
  signal input_opcode_r      : std_logic_vector(opcode_width_g - 1 downto 0);
  signal input_data_r        : std_logic_vector(data_width_g - 1 downto 0);
  signal input_ready_r       : std_logic;

begin

  trigger_i <= '1' when take_in = '1' and trigger = '1' and input_opcode = data_opcode_g
               else '0';

  trigger_reg_p : process(clk)
  begin
    if rising_edge(clk) then
      if reset = '1' or triggered = '1' then
        trigger_r <= '0';
      elsif trigger = '1' and input_opcode /= data_opcode_g and take_in = '1' then
        trigger_r <= '1';
      end if;
    end if;
  end process;

  trigger_catch_p : process(clk)
  begin
    if rising_edge(clk) then
      if reset = '1' or message_complete_i = '1' then
        triggered  <= '0';
        take_out_i <= '1';
        insert     <= '0';
      elsif trigger_i = '1' and take_in = '1' then
        triggered  <= '1';
        take_out_i <= '0';
        insert     <= '1';
      elsif take_in = '1' and trigger_r = '1' and input_ready_r = '1' and input_eom_r = '1' then
        triggered  <= '1';
        take_out_i <= '0';
        insert     <= '1';
      end if;

      if insert = '0' then
        meta_word     <= meta_word_i;
        meta_word_pos <= std_logic_vector(to_unsigned(1, meta_word_pos'length));
      else
        if take_in = '1' then
          meta_word <= meta_word(meta_word'high - (data_width_g) downto 0)
                       & meta_word(meta_word'high downto meta_word'high - (data_width_g-1));

          meta_word_pos <= meta_word_pos(meta_word_pos'high-1 downto 0) & meta_word_pos(meta_word_pos'high);
        end if;
      end if;

    end if;
  end process;

  small_data_width_gen : if data_width_g <= 32 generate
    meta_word_i <= rotate_byte_order(metadata_id, data_width_g) & std_logic_vector(to_unsigned(0, 32)) & rotate_byte_order(metadata_value, data_width_g);
  end generate;
  data_width_64_gen : if data_width_g > 32 and data_width_g <= 64 generate
    meta_word_i <= std_logic_vector(to_unsigned(0, 32)) & metadata_id & rotate_byte_order(metadata_value, data_width_g);
  end generate;
  data_width_128_gen : if data_width_g > 64 and data_width_g <= 128 generate
    meta_word_i <= std_logic_vector(to_unsigned(0, 32)) & metadata_id & metadata_value;
  end generate;

  take_out <= take_out_i and take_in;

  meta_data          <= meta_word(meta_word'high downto meta_word'high - (data_width_g-1));
  meta_som           <= meta_word_pos(meta_word_pos'low);
  meta_eom           <= meta_word_pos(meta_word_pos'high);
  message_complete_i <= meta_eom and take_in;

  message_complete <= message_complete_i;

  input_register_p : process (clk)
  begin
    if rising_edge(clk) then
      if take_in = '1' then
        message_complete_r <= message_complete_i;
      end if;

      if take_in = '1' and take_out_i = '1' then
        input_som_r         <= input_som;
        input_eom_r         <= input_eom;
        input_eof_r         <= input_eof;
        input_ready_r       <= input_ready;
        input_valid_r       <= input_valid;
        input_opcode_r      <= input_opcode;
        input_data_r        <= input_data;
        input_byte_enable_r <= input_byte_enable;

        if reset = '1' or (input_ready = '1' and input_eom = '1') or trigger_i = '1' then
          som_seen <= '0';
        elsif (input_ready = '1' and input_som = '1') or message_complete_r = '1' then
          som_seen <= '1';
        end if;
      end if;
    end if;
  end process;

  -----------------------------------------------------------------------------
  -- Multiplex the output data stream
  -----------------------------------------------------------------------------
  output_som         <= input_som_r or message_complete_r         when insert = '0' else meta_som;
  output_eom         <= input_eom_r or trigger_i                  when insert = '0' else meta_eom;
  output_data        <= input_data_r                              when insert = '0' else meta_data;
  output_eof         <= input_eof_r                               when insert = '0' else '0';
  output_give        <= input_ready_r or (trigger_i and som_seen) when insert = '0' else '1';
  output_valid       <= input_valid_r                             when insert = '0' else '1';
  output_opcode      <= input_opcode_r                            when insert = '0' else metadata_opcode_g;
  output_byte_enable <= input_byte_enable_r                       when insert = '0' else (others => '1');

end rtl;
