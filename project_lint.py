#!/usr/bin/env python3

# Project specific code checkers
#
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
# more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

"""Custom lint rules for the ocpi.comp.sdr project."""

import xml.etree.ElementTree as ET

from ocpi_linter import BaseCodeChecker
from ocpi_linter import XmlCodeChecker
from ocpi_linter import BaseCodeCheckerDefaults, CppCodeCheckerDefaults


# Code checker classes have settings to allow project changes to be enacted.
# The available options are located in the <Lang>_code_checker.py in a default
# value class. This is then set as the static variable `checker_settings`
# As these are static settings, the *most recent* value specified will be used.
XmlCodeChecker.checker_settings.comment_maximum_line_length = 132
XmlCodeChecker.checker_settings.maximum_line_length = 132

# Allow inline suppression within the source code in ocpi.comp.sdr
if "--inline-suppr" not in CppCodeCheckerDefaults.cppcheck_other_options:
    CppCodeCheckerDefaults.cppcheck_other_options.append("--inline-suppr")


class TestXMLCodeChecker(BaseCodeChecker):
    """A checker for Test XML files (*-test.xml).

    This is a new code checker, therefore it derives straight from the
    `BaseCodeChecker`. That base class provides the `lint()` function,
    which in turn calls all the included `test_*` functions.
    """

    @staticmethod
    def get_supported_file_extensions():
        """Return a list of file extension this code checker supports.

        Returns:
            List of file extensions
        """
        return [".xml"]

    def test_testxml_001(self):
        """Test that the -test.xml contains all the expected test cases.

        The list of expected test cases are mentioned in:
        components/doc/component_testing_guidelines/component_testing_guidelines.rst.

        Returns:
            tuple: Tuple of test name, and dictionary of issues.
        """
        # Only run this rule against test XML files
        allowed_cases = [
            "typical",
            "property",
            "sample",
            "sample_other_port",
            "input_stressing",
            "input_stressing_other_port",
            "message_size",
            "time",
            "time_other_port",
            "sample_interval",
            "sample_interval_other_port",
            "flush",
            "flush_other_port",
            "discontinuity",
            "discontinuity_other_port",
            "metadata",
            "metadata_other_port",
            "custom",
            "soak",
            # Additional block based codes.
            "time_calculation",
            "opcode_interaction",
            # Allowed "custom" cases
            "empty",
        ]

        test_name = "Test XML generator cases"
        issues = []
        if "-test.xml" in str(self.path):
            for case in ET.fromstring("".join(self._code)).findall("case"):
                for input_element in case.findall("input"):
                    input_script = input_element.attrib.get("script", None)
                    if not input_script:
                        continue
                    if not ("--case" in input_script or "-c" in input_script):
                        issues.append({
                            "line": 0,
                            "message": "Input property doesn't have \"case\" within script."})
                        continue

                    if "--case" in input_script:
                        case = input_script.split("--case")[1].split()[0]
                    else:  # "-c"
                        case = input_script.split("-c")[1].split()[0]
                    if case not in allowed_cases:
                        # ElementTree doesn't give access to the source line, therefore reopen file, and find the
                        # script line.
                        case_gen = [lineno+1 for lineno,
                                    line in enumerate(self._code) if input_script in line]
                        issues.append({
                            "line": case_gen[0],
                            "message": f"Case \"{case}\" is not an allowed generator case."})
        return test_name, issues

    def test_testxml_002(self):
        """Test that the -test.xml has ordered elements within cases.

        The list of expected case ordering is mentioned in:
        components/doc/component_testing_guidelines/component_testing_guidelines.rst.

        Returns:
            tuple: Tuple of test name, and dictionary of issues.
        """
        test_name = "Test XML tag ordering"
        issues = []
        if "-test.xml" in str(self.path):
            case_number = 0
            for case in ET.fromstring("".join(self._code)).findall("case"):
                output_seen = False
                prop_seen = False
                for tag in case.getchildren():
                    if tag.tag == "input":
                        if output_seen:
                            issues.append({
                                "line": 0,
                                "message": f"Input element seen after Output element, in case {case_number}."})
                        elif prop_seen:
                            issues.append({
                                "line": 0,
                                "message": f"Input element seen after Property element, in case {case_number}."})
                        else:
                            input_seen = True
                    elif tag.tag == "output":
                        if prop_seen:
                            issues.append({
                                "line": 0,
                                "message": f"Output element seen after Property element, in case {case_number}."})
                        else:
                            output_seen = True
                    elif tag.tag == "property":
                        prop_seen = True
                    else:
                        issues.append({
                            "line": 0,
                            "message": f"Unrecognised element: \"{tag.tag}\", in case {case_number}."})
                case_number += 1
        return test_name, issues

    def test_testxml_003(self):
        """Test that "values" attribute isn't used when "value" is meant.

        Returns:
            tuple: Tuple of test name, and dictionary of issues.
        """
        test_name = "Test XML singular values attribute"
        issues = []
        if "-test.xml" in str(self.path):
            case_number = 0
            for case in ET.fromstring("".join(self._code)).findall("case"):
                for prop in case.findall("property"):
                    values_text = prop.attrib.get("values", ",")
                    if values_text.find(",") == -1:
                        name = prop.attrib.get("name", "")
                        msg = ("Use of \"values\" attribute without list for " +
                               "property \"{}\", in case {}. " +
                               "Use \"value\" attribute instead.")
                        issues.append({"line": 0,
                                       "message": msg.format(name, case_number)})
                case_number += 1
        return test_name, issues


class MakefileCodeChecker(BaseCodeChecker):
    """Makefile checker (required while OCPI is not make-less)."""

    checker_settings = BaseCodeCheckerDefaults
    checker_settings.license_notice = """# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public License along
# with this program. If not, see <http://www.gnu.org/licenses/>."""

    @staticmethod
    def get_supported_file_extensions():
        """Return a list of file extension this code checker supports.

        Returns:
            List of file extensions
        """
        return ["Makefile", ".mk"]

    def test_makefile_000(self):
        """Check the first lines in file are in the correct format.

        **Test name:** Correct opening lines

        Returns:
            A tuple containing a string with the test name and a list of the
            identified test issues.
        """
        test_name = "Correct opening lines"

        issues = []

        if len(self._code) < self.minimum_number_of_lines:
            issues = [{"line": None,
                       "message": "File is not large enough to include license notice."}]
            return test_name, issues

        # License notice
        line_number = 0
        if (len(self._code) - line_number) < self.checker_settings.license_notice.count("\n"):
            issues.append({
                "line": None,
                "message": "File does not contain the expected license notice."})
            return test_name, issues

        for license_line in self.checker_settings.license_notice.splitlines():
            if self._code[line_number] != license_line:
                issues.append({
                    "line": line_number + 1,
                    "message": "License notice is incorrect, should be \""
                    + license_line + "\"."})
            line_number = line_number + 1

        # Blank line
        if self._code[line_number] != "":
            issues.append({
                "line": line_number + 1,
                "message": "Blank line does not follow license notice."})

        return test_name, issues
