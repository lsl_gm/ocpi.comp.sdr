#!/bin/bash
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public License along
# with this program. If not, see <http://www.gnu.org/licenses/>.

[ -z "$OCPI_CDK_DIR" ] && echo 'Environment variable OCPI_CDK_DIR not set' && exit 1

target_platform="$1"
name="dblclockfft"
description='dblclockfft FFT Core source'
# version=master
# github_dl_url="https://github.com/ZipCPU/dblclockfft/archive/refs/heads/"
# Note: ZipCPU does not have tags and "releases" for dblclock, so this script was
# using the "master" branch instead of a tag as the code hadn't changed for >2 years.
# However, that branch has changed recently, and still has no tags, so switching to
# use a commit sha for a known-good version of master (16/01/2024).
version="1d75a992efd0528edea128a903aafdabe133cb08"
github_dl_url="https://github.com/ZipCPU/dblclockfft/archive/"
dl_url="${ZIPCPU_FFT_URL:-${github_dl_url}}${version}.tar.gz"
cross_build=1

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

destination="${name}-${version}"

# Check if tarball already downloaded and extracted
cache_dir="$OCPI_CDK_DIR/../prerequisites-build"
fft_parent_dir="${cache_dir}/${name}/"
fft_extracted_dir="${cache_dir}/${name}/${destination}"

if [[ -d "${fft_extracted_dir}" ]]; then
  echo "${description} prerequisite already downloaded and extracted."

elif [[ -f "${cache_dir}/${destination}.tar.gz" ]]; then
  echo "${description} prerequisite already downloaded, extracting it."
  mkdir -p "${fft_parent_dir}"
  tar -C "${fft_parent_dir}" -xzf "${cache_dir}/${destination}.tar.gz"

else
  echo "Downloading and extracting ${description} prerequisite."

  # Download and extract source
  if ! (
    source "$OCPI_CDK_DIR/scripts/setup-prerequisite.sh" \
           "$target_platform" \
           "$name" \
           "$description" \
           "${dl_url%/*}" \
           "${dl_url##*/}" \
           "$destination" \
           "$cross_build"
  )
  then
    echo >&2 "WARNING: Failed to obtain ${description} prerequisite."
    echo >&2 "         This may need obtaining later to build the FFT"
    echo >&2 "         component - but can be ignored if not actually"
    echo >&2 "         building the FFT component."
    exit 0
  fi

  echo "Ready to copy extracted ${description} prerequisite code."
fi

if [[ ! -f "${fft_extracted_dir}/sw/fftgen" ]]; then
  echo >&2 "Building fftgen - please ignore ctags warning."
  VERILATOR_ROOT=/dev/null make -C "${fft_extracted_dir}/sw"
fi

# The part of the dblclockfft FFT core that is being used within the
# FFT HDL worker generates the files it needs, therefore no further
# copying of files is needed.
