#!/usr/bin/env python3

# Python implementation of polyphase clock synchroniser for verification
#
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
# more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

"""Polyphase Clock Synchroniser Sample Processing."""

import numpy as np
import logging
import scipy.signal as sig
import decimal
import opencpi.ocpi_testing as ocpi_testing
from sdr_interface.sample_time import SampleTime
from sdr_communication._polyphase_clock_synchroniser.signal_manipulation import to_twos
from sdr_dsp.polyphase_interpolator import PolyphaseInterpolator, PolyphaseInterpolator_Int
from sdr_communication._polyphase_clock_synchroniser.gardner_loop import GardnerLoop, GardnerLoopInt


class PolyphaseClockSynchroniser(ocpi_testing.Implementation):
    """A Polyphase Clock Synchroniser with floating point inputs and outputs."""

    def __init__(self, samples_per_symbol=2,
                 max_interpolation=32,
                 filter_taps=None, loop_coefficients=[1.5, 0.05, 0.5],
                 logging_level="WARNING", case="casexx", rounding_type="truncate"):
        """Initialise a clock synchroniser that has complex integer values.

        Args:
            samples_per_symbol (int, optional): The integer number of samples received per symbol. Defaults to 2.
            max_interpolation (int, optional): The number of branches that can be used for interpolation. Defaults to 32.
            filter_taps (list, optional): The polyphase interpolation tap coefficients. Defaults to None.
            loop_coefficients (list, optional): Coefficients for the loop filter, and NCO, structured as:[Kp, Ki, Knco]. Defaults to [512, 2047, 1024].
            logging_level (str, optional): Controls the amount of log output produced. Defaults to "WARNING".
            case (str, optional): Case description, used as log file name. Defaults to "casexx".
            rounding_type (str, optional): Selects the method used to convert floats to integers. Defaults to "truncate".
        """
        assert isinstance(samples_per_symbol, int), (
            "samples_per_symbol passed to polyphase_clock_synchroniser "
            "must be an integer, not a {}.".format(type(samples_per_symbol)))
        assert samples_per_symbol % 2 == 0, (
            "samples_per_symbol must be an even value, not {}".format(
                samples_per_symbol))
        assert samples_per_symbol > 1, (
            "samples_per_symbol must be greater than 1. Current value is {}"
            .format(samples_per_symbol))

        assert max_interpolation > 2, (
            "upsampling rate must be greater than 2, Current value is {}"
            .format(max_interpolation))

        assert len(loop_coefficients) == 3, (
            "The loop filter requires exactly 3 coefficients, but {} provided"
            .format(len(loop_coefficients)))

        super().__init__(_samples_per_symbol=samples_per_symbol,
                         _max_interpolation=max_interpolation)

        logging.basicConfig(level=logging_level,
                            filename=case+".polyphase_clock_synchroniser.py.log",
                            filemode="w")

        self._rounding_type = rounding_type
        decimal.getcontext().prec = 50

        if filter_taps is not None:
            self._filter_taps_per_branch = int(len(filter_taps)
                                               / self._max_interpolation)
            self._filter_taps = filter_taps
            logging.info("Taps Provided as: {}x{} \n {}"
                         .format(self._max_interpolation,
                                 self._filter_taps_per_branch,
                                 self._filter_taps))
        else:
            self._filter_taps_per_branch = 5
            self._filter_taps = (max_interpolation * sig.firwin(
                max_interpolation * self._filter_taps_per_branch,
                1/(max_interpolation),
                window=("kaiser", 5.0)))

        self._in_data_history = [0] * 4

        self._prop = np.float64(loop_coefficients[0])
        self._ki = np.float64(loop_coefficients[1])
        self._knco = np.float64(loop_coefficients[2])
        logging.info("Coefficients Kp: {}, Ki: {}, Knco: {}"
                     .format(self._prop, self._ki, self._knco))

        self._pif = PolyphaseInterpolator(
            interpolation_factor=max_interpolation,
            taps=self._filter_taps,
            max_taps_per_branch=self._filter_taps_per_branch,
            max_interpolation=max_interpolation,
            rounding_type="truncate",
            logger_level=logging_level)
        self._pif.branch = 0
        self._ted = GardnerLoop(mask_none=False)

        self._skip = False
        self._stuff = False
        self._output_idx = 0
        self._sample_offset = 0
        self._err_i = np.float64(0.0)
        self._err_val = np.float64(0.0)
        self._ted_val = np.float64(0.0)
        self._nco_branch = np.float64(0.0)
        self._reduced_branch = 0
        self._pif_val = 0
        self._sample_time = SampleTime(
            interval_down_sampling_factor=samples_per_symbol)

    @property
    def samples_per_symbol(self):
        """Input samples for a single output symbols.

        Returns:
            Int: Current Samples per Symbol.
        """
        return self._samples_per_symbol

    @samples_per_symbol.setter
    def samples_per_symbol(self, val):
        self._samples_per_symbol = val

    @property
    def phi(self):
        """Current phase offset.

        Returns:
            float: Current value of the phase offset.
        """
        return self._phi

    @phi.setter
    def phi(self, val):
        self._phi = ((val + np.pi) % (2*np.pi)) - np.pi

    @property
    def filter_taps(self):
        """Taps set within interpolator.

        Returns:
            list: Taps of interpolator.
        """
        return self._filter_taps

    @filter_taps.setter
    def filter_taps(self, val):
        self._filter_taps = val

    def _step(self, val):
        """Step through a single sample of the synchroniser loop.

        Args:
            val: The new sample to process.

        Returns:
            List of interpolant values.
        """
        # First test whether there is a need to stuff or skip samples.
        # In actuality you always update the data within the interpolator.
        # - if skipping:
        # Search for the peak value (depending upon direction of change this
        # might be the last, or first interpolant)
        # - if stuffing:
        # Insert the value, but don't pay attention to the output (set to zero)
        if self._stuff:
            self._stuff = False
            interpolant1 = self._pif.step(val)
            self._pif.branch = self._nco_branch
            interpolant2 = self._pif.step_without_history_update()
            if np.abs(interpolant1) > np.abs(interpolant2):
                self._pif_val = interpolant1
            else:
                self._pif_val = interpolant2

            # If there is only 2 SPS then there is no purpose to rotating 1
            # symbol backwards and instead need to ADD 2 additional samples
            # (ie. one peak, and one zero)
            if self._samples_per_symbol > 2:
                self._sample_offset -= 1
            else:
                return [interpolant1, interpolant2]

        if self._skip:
            self._skip = False
            self._pif_val = None
            self._pif.step(val)
            return []

        # Update the PIF to the branch we care about, then calculate the new
        # interpolant
        self._pif.branch = np.round(self._nco_branch)
        self._pif_val = self._pif.step(val)

        self._sample_offset += 1
        # Skip if this is not at the SPS rate
        if self._sample_offset % int(np.floor(self._samples_per_symbol
                                              / 2.0)) != 0:
            return []

        # Timing Error, (gardner loop)
        self._ted_val = self._ted.step(self._pif_val)

        if self._ted_val is None:
            return [self._pif_val]

        # Loop Filter (PI)
        self._err_i = np.float64((np.float64(self._ted_val) * self._ki)
                                 + self._err_i)
        self._err_val = np.float64(self._err_i
                                   + (np.float64(self._ted_val) * self._prop))

        # Error Accumulator, used to select the active branch
        self._nco_branch += np.float64(self._err_val * self._knco)
        if not np.isfinite(self._nco_branch):
            raise RuntimeError("Nan or Inf detected in accumulator")

        # Handle wrap around events at the edge of the interpolation range. In
        # doing so, an extra symbol is added (stuffed) or removed (skipped) and
        # the branch number wrapped to the other end of the range.
        if np.round(self._nco_branch) >= self._max_interpolation:
            self._nco_branch -= self._max_interpolation - 0.5
            self._skip = True

        elif np.round(self._nco_branch) <= -1.0:
            self._nco_branch += self._max_interpolation - 0.5
            self._stuff = True

        # Return the interpolant, this is potentially the end symbol but
        # which of the two possible values is unknown within a single step.
        return [self._pif_val]

    def _flush(self):
        """Flush all runtime values from the symbol sync."""
        self._skip = False
        self._stuff = False
        self._output_idx = 0
        self._sample_offset = 0
        self._err_i = np.float64(0.0)
        self._err_val = np.float64(0.0)
        self._ted_val = np.float64(0.0)
        self._nco_branch = np.float64(0.0)
        self._pif_val = 0
        self._ted.flush()
        self._pif.reset()
        self._pif.branch = 0

    def _round_sample(self, value):
        """Round the passed sample value to an integer.

        The method used for rounding is defined through `_rounding_type`.

        Args:
            value - The non-integer sample value to round.

        Returns:
            The resultant integer value.
        """
        if self._rounding_type == "half_up":
            return int(np.trunc(value + np.copysign(0.5, value)))
        elif self._rounding_type == "half_even":
            return int(np.around(value))
        else:
            # Default to truncate:
            return int(np.trunc(value))

    def _sample(self, values):
        """Process the sample values through the polyphase synchroniser.

        Args:
            values - The sample values to process.

        Returns:
            The resultant sample values.
        """
        output_values = []
        for v in values:
            s = self.step(np.float64(int(v)/(2**15)))
            logging.debug("sample: {}, pif: {}({}), ted: {}, nco: {}"
                          .format(v/(2**15), self._pif_val, s, self._ted_val,
                                  self._nco_branch))
            if s is not None:
                # Apply a crude clamp
                if s > 0.999:
                    s = 0.999
                    logging.warning("Clipping positive values, on float to "
                                    "integer conversion")
                elif s < -0.999:
                    s = -0.999
                    logging.warning("Clipping negative values, on float to "
                                    "integer conversion")
                # Scale and round using current rounding mode
                scaled_int = self._round_sample(s*(2**15))
                output_values.append(scaled_int)
        logging.info("Sample Downsampled from: {}, to: {}, (output idx: {})"
                     .format(len(values), len(output_values),
                             self._output_idx))
        return output_values

    def step(self, value):
        """Step through a single sample of the synchroniser loop.

        Args:
            value: The next sample value to feed into the loop.

        Returns:
            The next synchronised value, or None.
        """
        ret = None
        samp = self._step(value)
        for f in samp:
            if self._output_idx % self._samples_per_symbol == 1:
                ret = f
            self._output_idx = (self._output_idx +
                                1) % self._samples_per_symbol
        return ret

    def process(self, data):
        """Flush then process incoming data.

        Args:
            data: The incoming sample values.

        Returns:
            List of symbols generated from the input data.
        """
        symbol = []
        symbol_idx = []
        s = 0
        n = 0
        self._flush()
        for idx, d in enumerate(data):
            samp = self._step(d)
            for f in samp:
                s = f
                if (n) % 2 == 1:
                    symbol.append(s)
                    symbol_idx.append(float(idx))
                n += 1
        return symbol

    def reset(self):
        """Put the implementation into the same state as at initialisation."""
        self._flush()

    def sample(self, values):
        """Process the receipt of a sample message and return resulting messages.

        Args:
            values (list): The sample values within the sample message.

        Returns:
            A list of dictionaries which are the output messages for the output
            port.
        """
        ret = []

        ret.append({"opcode": "sample", "data": self._sample(values)})

        return self.output_formatter(ret)

    def sample_interval(self, value):
        """Process the receipt of a sample_interval message and return resulting messages.

        Args:
            value: The Sample interval value for the input port.

        Returns:
            A list of dictionaries which are the output messages for the output
            port.  Contains an output sample interval of input*SPS.
        """
        self._sample_time.interval = value

        return self.output_formatter(
            [{"opcode": "sample_interval", "data": self._sample_time.opcode_interval()}])

    def time(self, value):
        """Process the receipt of a time message and return resulting messages.

        Args:
            value: Time value for the input port.

        Returns:
            An empty list - no message are generated immediately on receipt of a
            time message.
        """
        ret = []
        self._sample_time.time = value
        ret.append(
            {"opcode": "time", "data": self._sample_time.opcode_time()})
        self._sample_time.time = None
        return self.output_formatter(ret)

    def flush(self, *inputs):
        """Process the receipt of a flush message and return resulting messages.

        Args:
            inputs* (bools): Indicate if a flush message has been received on
                an input port (True) or not received (False). There must be as
                many inputs arguments as there are input ports. The order of
                ports and inputs arguments matches that defined in
                self.input_ports.

        Returns:
            A list of dictionaries which are the output messages for the port.
        """
        ret = []
        if self._sample_offset > 0:
            # We need to pad zeros into the input
            inp = [0] * (self._filter_taps_per_branch)
            samples = self._sample(inp)
            ret.append({"opcode": "sample", "data": samples})
        ret.append({"opcode": "flush", "data": None})
        self._ted.flush()
        self._pif.reset()
        self._output_idx = 0
        self._sample_offset = 0
        return self.output_formatter(ret)

    def discontinuity(self, *inputs):
        """Process the receipt of a discontinuity message and return resulting messages.

        Args:
            inputs* (bools): Indicate if a flush message has been received on
                an input port (True) or not received (False). There must be as
                many inputs arguments as there are input ports. The order of
                ports and inputs arguments matches that defined in
                self.input_ports.

        Returns:
            A list of dictionaries which are the output messages for the port.
        """
        self._flush()
        self._pif.branch = 0
        self._reduced_branch = 0
        return self.output_formatter(
            [{"opcode": "discontinuity", "data": None}])


class PolyphaseClockSynchroniserInt(PolyphaseClockSynchroniser):
    """A Polyphase Clock Synchroniser with integer inputs and outputs."""

    def __init__(self, samples_per_symbol=2,
                 max_interpolation=32,
                 filter_taps=None, loop_coefficients=[512, 2047, 1024],
                 logging_level="WARNING", case="casexx", rounding_type="truncate",
                 output_depth=16, input_depth=16, internal_depth=32):
        """Initialise a clock synchroniser that has complex integer values.

        Args:
            samples_per_symbol (int, optional): The integer number of samples received per symbol. Defaults to 2.
            max_interpolation (int, optional): The number of branches that can be used for interpolation. Defaults to 32.
            filter_taps (list, optional): The polyphase interpolation tap coefficients. Defaults to None.
            loop_coefficients (list, optional): Coefficients for the loop filter, and NCO, structured as:[Kp, Ki, Knco]. Defaults to [512, 2047, 1024].
            logging_level (str, optional): Controls the amount of log output produced. Defaults to "WARNING".
            case (str, optional): Case description, used as log file name. Defaults to "casexx".
            rounding_type (str, optional): Selects the method used to convert floats to integers. Defaults to "truncate".
            output_depth (int, optional): Number of bits used for output values. Defaults to 16.
            input_depth (int, optional): Number of bits used for input values. Defaults to 16.
            internal_depth (int, optional): Number of bits used during internal calculations. Defaults to 32.
        """
        # Set up the general Polyphase clock class (this includes the
        # non-int specific input parameters, and the OCPI_Implementation class)
        super().__init__(samples_per_symbol, max_interpolation, filter_taps,
                         loop_coefficients, logging_level, case, rounding_type)

        if filter_taps is not None:
            self._filter_taps_per_branch = int(len(filter_taps)
                                               / self._max_interpolation)
            self._filter_taps = np.trunc(filter_taps)
            logging.info("Taps Provided as: {}x{} \n {}"
                         .format(self._max_interpolation,
                                 self._filter_taps_per_branch,
                                 self._filter_taps))
        else:
            self._filter_taps = (2**(input_depth-1)) * (max_interpolation * sig.firwin(
                max_interpolation * self._filter_taps_per_branch,
                1/(max_interpolation),
                window=("kaiser", 5.0)))

        self._prop = int(loop_coefficients[0])
        self._ki = int(loop_coefficients[1])
        self._knco = int(loop_coefficients[2])
        logging.info("Coefficients Kp: {}, Ki: {}, Knco: {}"
                     .format(self._prop, self._ki, self._knco))

        self._pif = PolyphaseInterpolator_Int(
            interpolation_factor=max_interpolation,
            taps=self._filter_taps,
            bit_depth=input_depth,
            rounding_type=rounding_type,
            max_interpolation=max_interpolation,
            max_taps_per_branch=self._filter_taps_per_branch,
            logger_level=logging_level)

        self._pif.branch = 0
        self._ted = GardnerLoopInt(
            mask_none=False, input_depth=input_depth, output_depth=output_depth)

        self._err_i = int(0)
        self._err_val = int(0)
        self._ted_val = int(0)
        self._nco_branch = int(0)

        self._input_depth = input_depth
        self._input_max = (2**(input_depth-1))-1
        self._input_min = -(2**(input_depth-1))

        self._output_depth = output_depth
        self._output_max = (2**(output_depth-1))-1
        self._output_min = -(2**(output_depth-1))

        self._internal_depth = internal_depth
        self._internal_max = (2**(internal_depth-1))-1
        self._internal_min = -(2**(internal_depth-1))

        self._branch_depth = int(np.ceil(np.log2(max_interpolation)))
        self._reduced_branch = 0

    def _sample(self, values):
        """Process the sample through the polyphase synchroniser.

        Args:
            values - The sample values to process.

        Returns:
            The resultant sample values.
        """
        output_values = []
        for v in values:
            s = self.step(int(v))
            logging.debug("sample: {}, pif: {}({}), ted: {}, nco: {} : {} err: {} ({}), sk{} st{}"
                          .format(v, self._pif_val, s, self._ted_val,
                                  self._nco_branch, self._pif.branch,
                                  self._err_val, self._err_i,
                                  self._skip, self._stuff))
            if s is not None:
                output_values.append(s)
        logging.info("Sample Downsampled from: {}, to: {}, (output idx: {})"
                     .format(len(values), len(output_values),
                             self._output_idx))
        return output_values

    def _round_branch(self):
        """Round the non-integral value for the branch to an integer.

        Returns:
            Branch value rounded to an integer.
        """
        rounded_branch = (int(self._nco_branch) >> ((2*self._input_depth) -
                                                    (self._branch_depth))) & ((2**self._branch_depth)-1)
        logging.debug(
            f"rounding TED branch down to {rounded_branch} (from: br_depth:{self._branch_depth}")
        return rounded_branch

    def _step(self, val):
        """Step through a single sample of the synchroniser loop.

        Args:
            val: The new sample to process.

        Returns:
            List of interpolant values.
        """
        # First test whether there is a need to stuff or skip samples.
        # In actuality you always update the data within the interpolator,
        # - if skipping:
        # Search for the peak value (depending upon direction of change this
        # might be the last, or first interpolant)
        # - if stuffing:
        # insert the value, but don't pay attention to the output (set to zero)

        if self._stuff:
            self._stuff = False
            interpolant1 = self._pif.step(val)
            self._pif.branch = self._round_branch()
            interpolant2 = self._pif.step_without_history_update()
            if np.abs(interpolant1) > np.abs(interpolant2):
                self._pif_val = interpolant1
            else:
                self._pif_val = interpolant2

            # If there is only 2 SPS then there is no purpose to rotating 1
            # symbol backwards and instead need to ADD 2 additional samples
            # (ie. one peak, and one zero)
            if self._samples_per_symbol > 2:
                self._sample_offset -= 1
            else:
                return [interpolant1, interpolant2]

        if self._skip:
            self._skip = False
            self._pif_val = None
            self._pif.step(val)
            return []

        # Update the PIF to the branch we care about, then calculate the new
        # interpolant
        self._pif.branch = self._round_branch()
        self._pif_val = self._pif.step(val)

        self._sample_offset += 1
        # Skip if this is not at the SPS rate
        if self._sample_offset % int(np.floor(self._samples_per_symbol
                                              / 2.0)) != 0:
            return []

        # Timing Error, (gardner loop)
        self._ted_val = self._ted.step(self._pif_val)

        if self._ted_val is None:
            return [self._pif_val]

        # Loop Filter (PI) (coefficients are 5 bits in)
        self._err_i += (int(self._ted_val) * int(self._ki))
        self._err_i = to_twos(int(self._err_i) & (
            (2**(2*self._input_depth))-1), self._input_depth*2)

        self._err_val = self._err_i + \
            (int(self._ted_val) * int(self._prop))

        # Error Accumulator, used to select the active branch
        self._nco_branch += int((self._err_val >> self._input_depth) *
                                int(self._knco))
        self._nco_branch = to_twos(int(self._nco_branch) & (
            (2**(2*self._input_depth))-1), self._internal_depth)
        if not np.isfinite(self._nco_branch):
            raise RuntimeError("Nan or Inf detected in accumulator")

        if self._err_i > self._internal_max or self._err_i < self._internal_min:
            raise ValueError("Error: Value err_i {} is outside of integer input range [{}-{}]"
                             .format(self._err_i, self._internal_min, self._internal_max))
        if self._err_val > self._internal_max or self._err_val < self._internal_min:
            raise ValueError("Error: Value err_val {} is outside of integer input range [{}-{}]"
                             .format(self._err_val, self._internal_min, self._internal_max))
        if self._nco_branch > self._internal_max or self._nco_branch < self._internal_min:
            raise ValueError("Error: Value nco_branch {} is outside of integer input range [{}-{}]"
                             .format(self._nco_branch, self._internal_min, self._internal_max))

        # Drift - Handling wrap around events at the edge of the interpolation
        # range... Mask the glitch in the error corrector through skip and
        # stuffing wrap the branch, with a slight margin, to handle rounding
        # error
        branch = self._round_branch()
        logging.debug("nco:{}, br: {}, reduced: {} input: {}, br_depth: {}, max_interp: {}"
                      .format(self._nco_branch, branch, self._reduced_branch,
                              self._input_depth,
                              self._branch_depth, (self._max_interpolation-1)))
        if branch == 0 and self._reduced_branch == (self._max_interpolation-1):
            self._skip = True
            logging.debug("skip")

        elif branch == (self._max_interpolation-1) and self._reduced_branch == 0:
            self._stuff = True
            logging.debug("stuff")

        branch = self._round_branch()
        self._reduced_branch = branch
        if self._reduced_branch >= self._max_interpolation or self._reduced_branch < 0:
            raise ValueError("Error: Value nco_branch {} is outside of integer input range [{}-{}]"
                             .format(self._reduced_branch, 0, self._max_interpolation))

        # Return the interpolant, this is potentially the end symbol. but
        # which of the two possible values is unknown within a single step.
        return [self._pif_val]


class PolyphaseClockSynchroniserComplexInt(PolyphaseClockSynchroniserInt):
    """A Polyphase Clock Synchroniser with complex integer inputs and outputs."""

    def __init__(self, samples_per_symbol=2,
                 max_interpolation=32,
                 filter_taps=None, loop_coefficients=[512, 2047, 1024],
                 logging_level="WARNING", case="casexx", rounding_type="truncate",
                 output_depth=16, input_depth=16, internal_depth=32):
        """Initialise a clock synchroniser that has complex integer values.

        Args:
            samples_per_symbol (int, optional): The integer number of samples received per symbol. Defaults to 2.
            max_interpolation (int, optional): The number of branches that can be used for interpolation. Defaults to 32.
            filter_taps (list, optional): The polyphase interpolation tap coefficients. Defaults to None.
            loop_coefficients (list, optional): Coefficients for the loop filter, and NCO, structured as:[Kp, Ki, Knco]. Defaults to [512, 2047, 1024].
            logging_level (str, optional): Controls the amount of log output produced. Defaults to "WARNING".
            case (str, optional): Case description, used as log file name. Defaults to "casexx".
            rounding_type (str, optional): Selects the method used to convert floats to integers. Defaults to "truncate".
            output_depth (int, optional): Number of bits used for output values. Defaults to 16.
            input_depth (int, optional): Number of bits used for input values. Defaults to 16.
            internal_depth (int, optional): Number of bits used during internal calculations. Defaults to 32.
        """
        # Set up the general Polyphase clock class (this includes the
        # non-int specific input parameters, and the OCPI_Implementation class)
        super().__init__(samples_per_symbol, max_interpolation,
                         filter_taps, loop_coefficients,
                         logging_level, case, rounding_type,
                         output_depth, input_depth, internal_depth)

        del self._pif
        self._real_pif = PolyphaseInterpolator_Int(
            interpolation_factor=max_interpolation,
            taps=self._filter_taps,
            bit_depth=input_depth,
            rounding_type=rounding_type,
            max_interpolation=max_interpolation,
            max_taps_per_branch=self._filter_taps_per_branch,
            logger_name="PolyIntImag",
            logger_level=logging_level)

        self._imag_pif = PolyphaseInterpolator_Int(
            interpolation_factor=max_interpolation,
            taps=self._filter_taps,
            bit_depth=input_depth,
            rounding_type=rounding_type,
            max_interpolation=max_interpolation,
            max_taps_per_branch=self._filter_taps_per_branch,
            logger_name="PolyIntImag",
            logger_level=logging_level)

        self._real_pif.branch = 0
        self._imag_pif.branch = 0

        self._real_ted = GardnerLoopInt(
            mask_none=False, input_depth=input_depth, output_depth=output_depth,
            logger_name="GardnerI")
        self._imag_ted = GardnerLoopInt(
            mask_none=False, input_depth=input_depth, output_depth=output_depth,
            logger_name="GardnerQ")

    def _sample(self, values):
        """Process the passed sample values through the polyphase synchroniser.

        Args:
            values - The sample values to process.

        Returns:
            The resultant sample values.
        """
        output_values = []
        for v in values:
            s = self.step(complex(int(v.real), int(v.imag)))
            logging.debug("sample: {}, pif: {}({}), ted: {}, nco: {} : {} err: {} ({}), sk{} st{}"
                          .format(v, self._pif_val, s, self._ted_val,
                                  self._nco_branch, self._real_pif.branch,
                                  self._err_val, self._err_i,
                                  self._skip, self._stuff))
            if s is not None:
                output_values.append(s)
        logging.info("Sample Downsampled from: {}, to: {}, (output idx: {})"
                     .format(len(values), len(output_values),
                             self._output_idx))
        return output_values

    def _flush(self):
        """Flush all runtime values from the symbol sync."""
        self._skip = False
        self._stuff = False
        self._output_idx = 0
        self._sample_offset = 0
        self._err_i = np.float64(0.0)
        self._err_val = np.float64(0.0)
        self._ted_val = np.float64(0.0)
        self._nco_branch = np.float64(0.0)
        self._pif_val = 0
        self._real_ted.flush()
        self._imag_ted.flush()
        self._real_pif.reset()
        self._imag_pif.reset()
        self._real_pif.branch = 0
        self._imag_pif.branch = 0

    def _step(self, val):
        """Step through a single sample of the synchroniser loop.

        Args:
            val: The new sample to process.

        Returns:
            List of interpolant values.
        """
        # First test whether there is a need to stuff or skip samples.
        # In actuality you always update the data within the interpolator,
        # - if skipping:
        # Search for the peak value (depending upon direction of change this
        # might be the last, or first interpolant)
        # - if stuffing:
        # insert the value, but don't pay attention to the output (set to zero)

        if self._stuff:
            self._stuff = False
            interpolant1 = complex(self._real_pif.step(val.real),
                                   self._imag_pif.step(val.imag))
            self._real_pif.branch = self._round_branch()
            self._imag_pif.branch = self._round_branch()
            interpolant2 = complex(self._real_pif.step_without_history_update(),
                                   self._imag_pif.step_without_history_update())
            if np.abs(interpolant1) > np.abs(interpolant2):
                self._pif_val = interpolant1
            else:
                self._pif_val = interpolant2

            # If there is only 2 SPS then there is no purpose to rotating 1
            # symbol backwards and instead need to ADD 2 additional samples
            # (ie. one peak, and one zero)
            if self._samples_per_symbol > 2:
                self._sample_offset -= 1
            else:
                return [interpolant1, interpolant2]

        if self._skip:
            self._skip = False
            self._pif_val = None
            self._real_pif.step(val.real)
            self._imag_pif.step(val.imag)
            return []

        # Update the PIF to the branch we care about, then calculate the new
        # interpolant
        self._real_pif.branch = self._round_branch()
        self._imag_pif.branch = self._round_branch()
        self._pif_val = complex(self._real_pif.step(val.real),
                                self._imag_pif.step(val.imag))

        self._sample_offset += 1
        # Skip if this is not at the SPS rate
        if self._sample_offset % int(np.floor(self._samples_per_symbol
                                              / 2.0)) != 0:
            return []

        # Timing Error, (gardner loop)
        self._real_ted_val = self._real_ted.step(self._pif_val.real)
        self._imag_ted_val = self._imag_ted.step(self._pif_val.imag)

        if self._real_ted_val is None or self._imag_ted_val is None:
            return [self._pif_val]
        self._ted_val = (self._real_ted_val + self._imag_ted_val) >> 1
        # Loop Filter (PI) (coefficients are 5 bits in)
        self._err_i += ((self._ted_val) * int(self._ki))

        self._err_i = to_twos(int(self._err_i) & (
            (2**(2*self._input_depth))-1), self._input_depth*2)

        self._err_val = self._err_i + (self._ted_val * int(self._prop))

        # Error Accumulator, used to select the active branch
        self._nco_branch += int((self._err_val >> self._input_depth) *
                                int(self._knco))
        self._nco_branch = to_twos(int(self._nco_branch) & (
            (2**(2*self._input_depth))-1), self._internal_depth)
        if not np.isfinite(self._nco_branch):
            raise RuntimeError("Nan or Inf detected in accumulator")

        if self._err_i > self._internal_max or self._err_i < self._internal_min:
            raise ValueError("Error: Value err_i {} is outside of integer input range [{}-{}]"
                             .format(self._err_i, self._internal_min, self._internal_max))
        if self._err_val > self._internal_max or self._err_val < self._internal_min:
            raise ValueError("Error: Value err_val {} is outside of integer input range [{}-{}]"
                             .format(self._err_val, self._internal_min, self._internal_max))
        if self._nco_branch > self._internal_max or self._nco_branch < self._internal_min:
            raise ValueError("Error: Value nco_branch {} is outside of integer input range [{}-{}]"
                             .format(self._nco_branch, self._internal_min, self._internal_max))

        # Drift - Handling wrap around events at the edge of the interpolation
        # range... Mask the glitch in the error corrector through skip and
        # stuffing wrap the branch, with a slight margin, to handle rounding
        # error
        branch = self._round_branch()
        logging.debug("nco:{}, reduced: {} input: {}, br_depth: {}"
                      .format(self._nco_branch, branch, self._input_depth,
                              self._branch_depth))
        if branch == 0 and self._reduced_branch == (self._max_interpolation-1):
            self._skip = True
            logging.debug("skip")

        elif branch == (self._max_interpolation-1) and self._reduced_branch == 0:
            self._stuff = True
            logging.debug("stuff")

        branch = self._round_branch()
        self._reduced_branch = branch
        if self._reduced_branch >= self._max_interpolation or self._reduced_branch < 0:
            raise ValueError("Error: Value nco_branch {} is outside of integer input range [{}-{}]"
                             .format(self._reduced_branch, 0, self._max_interpolation))

        # Return the interpolant, this is potentially the end symbol. but
        # which of the two possible values is unknown within a single step.
        return [self._pif_val]

    def flush(self, *inputs):
        """Process the receipt of a flush message and return resulting messages.

        Args:
            inputs* (bools): Indicate if a flush message has been received on
                an input port (True) or not received (False). There must be as
                many inputs arguments as there are input ports. The order of
                ports and inputs arguments matches that defined in
                self.input_ports.

        Returns:
            A list of dictionaries which are the output messages for the port.
        """
        ret = []
        if self._sample_offset > 0:
            # We need to pad zeros into the input
            inp = [0] * (self._filter_taps_per_branch)
            samples = self._sample(inp)
            ret.append({"opcode": "sample", "data": samples})
        ret.append({"opcode": "flush", "data": None})
        self._real_ted.flush()
        self._imag_ted.flush()
        self._real_pif.reset()
        self._imag_pif.reset()
        self._output_idx = 0
        self._sample_offset = 0
        return self.output_formatter(ret)

    def discontinuity(self, *inputs):
        """Process the receipt of a discontinuity message and return resulting messages.

        Args:
            inputs* (bools): Indicate if a flush message has been received on
                an input port (True) or not received (False). There must be as
                many inputs arguments as there are input ports. The order of
                ports and inputs arguments matches that defined in
                self.input_ports.

        Returns:
            A list of dictionaries which are the output messages for the port.
        """
        self._flush()
        self._reduced_branch = 0
        return self.output_formatter(
            [{"opcode": "discontinuity", "data": None}])
