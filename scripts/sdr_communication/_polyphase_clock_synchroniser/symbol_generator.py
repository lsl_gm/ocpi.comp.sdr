#!/usr/bin/env python3

# Python implementation of a symbol generator block for verification
#
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
# more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

"""Symbol Generator for Polyphase Clock Synchroniser verification."""

import numpy as np
import random
import scipy.signal as sig


class SymbolGenerator:
    """A Symbol Generator to produce streams of data."""

    def __init__(self, n_symbols=10, samples_per_symbol=2,
                 awgn_level=-127, phase_offset=0,
                 seed=100, filter_taps=None,
                 symbols=[-1, 1], mode="random", frequency_offset=0):
        """Symbol Generation to produce streams of data.

        Args:
          n_symbols (int): Number of symbols to generate output stream for
          samples_per_symbol (int): samples per symbol
          awgn_level (int): Additive white gaussian noise level in dB
          phase_offset (decimal): phase offset in radians
          seed (int): Random Generation seed value
          filter_taps (list): Taps to use for the interpolation filter
          symbols (list): symbol set to use
          mode (string): random or preamble sequences
          frequency_offset (decimal): clock error to generate in stream
        """
        self._seed = seed
        random.seed(seed)
        self._n_symbols = n_symbols
        self._symbols = symbols
        self._samples_per_symbol = samples_per_symbol
        self._filter_taps = filter_taps
        self._awgn_level = awgn_level
        self._linear_awgn = 0.0
        self._symbol_data = None
        self._samples = None
        self._noise_data = None
        self._sample_idx = 0
        self._phase_offset = phase_offset
        self._mode = mode
        self._allowed_modes = ["random", "preamble"]
        self._frequency_offset = frequency_offset

        self.awgn_level = awgn_level

    @property
    def samples_per_symbol(self):
        """Get samples per symbols."""
        return self._samples_per_symbol

    @samples_per_symbol.setter
    def samples_per_symbol(self, val):
        """Set samples per symbol."""
        self._samples_per_symbol = val

    @property
    def n_symbols(self):
        """Get number of symbols."""
        return self._n_symbols

    @n_symbols.setter
    def n_symbols(self, val):
        """Set number of symbols."""
        self._n_symbols = val

    @property
    def symbols(self):
        """Return the current symbols."""
        return self._symbols

    @property
    def phase_offset(self):
        """Return the phase offset."""
        return self._phase_offset

    @phase_offset.setter
    def phase_offset(self, val):
        """Set phase offset."""
        self._phase_offset = ((val + np.pi) % (2*np.pi)) - np.pi

    @property
    def seed(self):
        """Return the seed value."""
        return self._seed

    @seed.setter
    def seed(self, val):
        """Set seed."""
        self._seed = val
        random.seed(self._seed)

    @property
    def filter_taps(self):
        """Return the list of filter tap values."""
        return self._filter_taps

    @filter_taps.setter
    def filter_taps(self, val):
        """Set filter taps."""
        self._filter_taps = val

    @property
    def awgn_level(self):
        """Return the additive white gaussian noise level in dB."""
        return self._awgn_level

    @awgn_level.setter
    def awgn_level(self, val):
        """Set the additive white gaussian noise level.

        Args:
           val (numeric/integral): awgn level in negative decibels.
        """
        if val > 0:
            raise ValueError("Noise value should be in negative decibels.")
        self._awgn_level = val
        self._linear_awgn = 10**(self._awgn_level/10)

    @property
    def mode(self):
        """Return the mode, "random" or "preamble" sequences."""
        return self._mode

    @mode.setter
    def mode(self, val):
        """Set mode."""
        if val.lower() in self._allowed_modes:
            self._mode = val.lower()
        else:
            ValueError("Only modes: {} are allowed".format(
                self._allowed_modes))

    @property
    def frequency_offset(self):
        """Get frequency offset."""
        return self._frequency_offset

    @frequency_offset.setter
    def frequency_offset(self, val):
        """Set frequency offset."""
        self._frequency_offset = val

    @property
    def symbol_data(self):
        """Get symbol data."""
        return self._symbol_data

    @property
    def sample_data(self):
        """Get the sample data."""
        return self._samples

    def _generate(self):
        """Generate symbol data, storing it in self._samples."""
        if self._mode == "preamble":
            self._symbol_data = [
                ((d % 2)*2)-1 for d in range(int(self._n_symbols))]
        else:
            self._symbol_data = [random.choice(
                self._symbols) for _ in range(int(self._n_symbols))]

        if self._samples_per_symbol > 1:
            self._samples = sig.lfilter(
                sig.firwin(self._samples_per_symbol, 1 /
                           self._samples_per_symbol), 1,
                np.repeat(self.symbol_data, self._samples_per_symbol))
        else:
            self._samples = np.array(self._symbol_data)

    def _rotate_data_phase(self):
        """Alter the data phases within self._samples."""
        x_values = (np.arange(self._n_symbols,
                              step=1.0/self._samples_per_symbol)
                    + (-1.0 + 1.0/self._samples_per_symbol)
                    + (self._phase_offset/np.pi))
        xp = (np.arange(self._n_symbols, step=1.0/self._samples_per_symbol)
              + (-1.0 + 1.0/self._samples_per_symbol))
        self._samples = np.interp(x_values, xp, self._samples)

    def _add_awgn(self):
        """Add some random Gaussian noise to the data in samples.

        Returns:
            Sample data with additive white Gaussian noise
        """
        self._noise_data = \
            [s + random.gauss(mu=0.0, sigma=self._linear_awgn)
             for s in self._samples]
        return self._noise_data

    def _add_clock_error(self, data):
        """Resample the passed data to generate data with a clock error.

        Args:
            data: Sample values to which a clock error should be added

        Returns:
            Sample data with an added clock error
        """
        num_samples = len(data)
        return sig.resample(data, round(num_samples +
                                        (num_samples*self._frequency_offset)))

    def generate(self):
        """Generate a stream of symbols, with clock error and noise.

        Returns:
            List of symbols incorporating a clock error and noise
        """
        # Reset the seed to ensure the same output is produced
        random.seed(self._seed)
        self._generate()
        self._rotate_data_phase()
        self._samples = self._add_clock_error(self._samples)
        return self._add_awgn()

    def step_output(self, num_samples=1):
        """Return the next few samples from the self._samples list.

        Args:
            num_samples: The number of samples to step through

        Returns:
            List of samples
        """
        if not self._samples:
            ValueError("No samples have been generated")
        ret = self._samples[self._sample_idx: self._sample_idx+num_samples]
        self._sample_idx += num_samples
        return ret
