#!/usr/bin/env python3

# Python implementation of gardner loop block for verification
#
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
# more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

"""Python implementation of gardner loop block for verification."""

import numpy as np
import logging

from .signal_manipulation import to_twos


class GardnerLoop:
    """A Gardner timing error detector."""

    def __init__(self, mask_none=False, logger_name="Gardner"):
        """Initialises a Gardner Loop class.

        Args:
            mask_none (bool): Whether to replace None results (for cycles which
                              do not generate an output) with 0's instead
            logger_name (str): Name of the logger instance.
        """
        self._mask_none = mask_none
        self._in_data_history = [0] * 4
        self._samples_per_symbol = 2
        self._sample_offset = 0
        self._ted_val = 0
        self.logger = logging.getLogger(logger_name)
        self.logger.info(f"Gardner started with mask_none:{mask_none}")

    @property
    def mask_none(self):
        """Whether to replace None results with zero instead.

        Cycles which do not generate an output can return None or 0. If this
        property is set, zeros will be used.
        """
        return self._mask_none

    @mask_none.setter
    def mask_none(self, val):
        self._mask_none = val

    def clear_sample_offset(self):
        """Resets the Sample offset to 0.

        This will cause a gardner output on the next sample.
        """
        self.logger.debug("Clear sample offset")
        self._sample_offset = 0

    def _update_input_history(self, data):
        """Insert the new sample into the history buffer.

        Args:
            data: The sample value.
        """
        # Insert new sample, and trim to length
        length = len(self._in_data_history)
        self._in_data_history.insert(0, data)
        del self._in_data_history[length:]

    def _generate_gardner(self):
        """Produce an error value based on the current input history.

        Returns:
            The error value, or None.
        """
        if (np.all(np.signbit(self._in_data_history[0:3]))
                or np.all([not s for s in np.signbit(self._in_data_history[0:3])])):
            # Test for runs of 1's or 0's, as do not want to include them
            # in an algorithm which is made to measure zero-crossing, Set to
            # 0 to prevent runs of false errors
            self.logger.debug("ted = 0 (no sign change)")
            self._ted_val = np.float32(0)
        elif self._sample_offset % 2 == 0:
            # Normal Gardner, runs every 2nd sample
            self._ted_val = np.float32(
                self._in_data_history[1]) * (self._in_data_history[0]
                                             - self._in_data_history[2])
            # Wrap values
            self.logger.debug(f"ted = {self._ted_val} (raw)")
            self._ted_val = ((self._ted_val + 1.0) % 2.0) - 1.0
            self.logger.debug(f"ted = {self._ted_val} (after wrapping)")

            # test whether centring on a peak of null if so resync
            if (np.abs(self._in_data_history[0]) > np.abs(self._in_data_history[1])
                    and np.abs(self._in_data_history[2]) > np.abs(self._in_data_history[3])):
                self._ted_val = ((self._ted_val + 0.8) % 2.0) - 1.0
                self.logger.debug(f"ted = {self._ted_val} (after resyncing)")

            if self._ted_val > 1.0 or self._ted_val < -1.0:
                raise ValueError("Error: Value {} is outside of integer input range [{}-{}]"
                                 .format(self._ted_val, -1, 1))

            return self._ted_val
        else:
            # On the alternating sample the value is held
            self._ted_val = self._ted_val
        return None

    def flush(self):
        """Clear out any internal runtime values."""
        self.logger.info("flush")
        self._in_data_history = [np.float32(0)] * 4
        self._sample_offset = 0
        self._ted_val = np.float32(0)

    def step(self, sample):
        """Step on by a single value.

        Args:
            sample: The next input value.

        Returns:
            The calculated error value.
        """
        err = None
        self._update_input_history(sample)
        err = self._generate_gardner()
        self._sample_offset += 1
        if self._mask_none and err is None:
            return 0

        return err


class GardnerLoopInt(GardnerLoop):
    """Integer version of the Gardner loop.

    This makes use of fixed point integers (i.e. integer arithmetic and shifts)
    and expects inputs/outputs to be integers
    """

    def __init__(self, mask_none=False,
                 output_depth=16, input_depth=16, logger_name="Gardner"):
        """Initialises a Gardner Loop class.

        Args:
            mask_none (bool): Whether to replace None results (for cycles which
                              do not generate an output) with 0's instead.
            input_depth (int): The number of bits in the input values.
            output_depth (int): The number of bits in the output values.
            logger_name (str): Name of the logger instance.
        """
        super().__init__(mask_none, logger_name)
        self._in_data_history = [0] * 4
        self._samples_per_symbol = 2
        self._sample_offset = 0
        self._ted_val = 0

        self._input_depth = input_depth
        self._input_max = (2**(input_depth-1))-1
        self._input_min = -(2**(input_depth-1))
        self._output_depth = output_depth

    def _update_input_history(self, data):
        """Insert the new sample into the history buffer.

        Args:
            data: The sample value.
        """
        # Just to ensure data is int
        val = int(np.trunc(data))
        if val > self._input_max or val < self._input_min:
            raise ValueError("Error: Value {} is outside of integer input range [{}-{}]"
                             .format(val, self._input_min, self._input_max))
        # Insert new sample, and trim to length
        length = len(self._in_data_history)
        self._in_data_history.insert(0, val)
        del self._in_data_history[length:]

    def _generate_gardner(self):
        """Produce an error value based on the current input history.

        Returns:
            The error value, or None.
        """
        self.logger.debug(f"History is: {self._in_data_history}")
        if (np.all(np.signbit(self._in_data_history[:3]))
                or np.all([not s for s in np.signbit(self._in_data_history[:3])])):
            # Test for runs of 1's or 0's, as do not want to include them
            # in an algorithm which is made to measure zero-crossing, Set to
            # 0 to prevent runs of false errors
            self.logger.debug("ted = 0 (no sign change)")
            self._ted_val = 0
        elif self._sample_offset % 2 == 0:
            # Normal Gardner, runs every 2nd sample
            self._ted_val = (int(self._in_data_history[1])
                             * int(self._in_data_history[0] - self._in_data_history[2])
                             // (2**((self._input_depth) + (self._input_depth - self._output_depth))))
            self.logger.debug(f"ted = {self._ted_val} (raw) - "
                              f"({int(self._in_data_history[1])} * {int(self._in_data_history[0] - self._in_data_history[2])}) / {(2**((self._input_depth) + (self._input_depth - self._output_depth)))}")
            # test whether centring on a peak of null if so resync
            if (np.abs(self._in_data_history[0]) > np.abs(self._in_data_history[1])
                    and np.abs(self._in_data_history[2]) > np.abs(self._in_data_history[3])):
                self._ted_val = self._input_max - self._ted_val
                self.logger.debug(f"ted = {self._ted_val} (resync)")

            self._ted_val = to_twos(
                (self._ted_val) & ((2**self._input_depth)-1), self._input_depth)
            self.logger.debug(f"ted = {self._ted_val} (wrapped)")
            if self._ted_val > self._input_max or self._ted_val < self._input_min:
                raise ValueError("Error: Value {} is outside of integer input range [{}-{}]"
                                 .format(self._ted_val, self._input_min, self._input_max))

            return self._ted_val
        else:
            # On the alternating sample the value is held
            self._ted_val = self._ted_val
        return None

    def flush(self):
        """Clear out any internal runtime values."""
        self.logger.info("Flush received")
        self._in_data_history = [int(0)] * 4
        self._sample_offset = 0
        self._ted_val = int(0)
