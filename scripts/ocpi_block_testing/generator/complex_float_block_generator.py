#!/usr/bin/env python3

# Class for generating complex float sample input data
#
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
# more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

"""Block based generator for Complex Float samples."""

import random
import math
from opencpi.ocpi_testing.ocpi_testing.generator.float_generator import FloatGeneratorDefaults
from opencpi.ocpi_testing.ocpi_testing.generator.complex_float_generator import ComplexFloatGeneratorDefaults
from .base_block_generator import BaseBlockGenerator


class ComplexFloatBlockGenerator(BaseBlockGenerator):
    """Complex float protocol test data generator."""

    def __init__(self, block_length, number_of_soak_blocks=25):
        """Initialise complex float block generator class.

        Defines the default values for the variables that control the values
        and size of messages that are generated.

        Returns:
            An initialised ComplexFloatGenerator instance.
        """
        super().__init__(block_length, number_of_soak_blocks)

        # Set variables as local as may be modified when set in the specific
        # generator. Keep the same variable names to ensure documentation
        # matches.
        self.MESSAGE_SIZE_LONGEST = ComplexFloatGeneratorDefaults.MESSAGE_SIZE_LONGEST
        self.FLOAT_MINIMUM = FloatGeneratorDefaults.FLOAT_MINIMUM
        self.FLOAT_MAXIMUM = FloatGeneratorDefaults.FLOAT_MAXIMUM
        self.FLOAT_SMALL = FloatGeneratorDefaults.FLOAT_SMALL
        self.TYPICAL_AMPLITUDE_MEAN = \
            FloatGeneratorDefaults.TYPICAL_AMPLITUDE_MEAN
        self.TYPICAL_AMPLITUDE_DISTRIBUTION_WIDTH = \
            FloatGeneratorDefaults.TYPICAL_AMPLITUDE_DISTRIBUTION_WIDTH
        self.TYPICAL_MAXIMUM_AMPLITUDE = \
            FloatGeneratorDefaults.TYPICAL_MAXIMUM_AMPLITUDE

    def sample(self, seed, subcase):
        """Generate sample messages to test different supported values.

        Usually called via ``self.generate()``.

        Args:
            seed (int): The seed value to use for random number generation.
            subcase (str): Name of the subcase messages are to be generated
                for.

        Returns:
            Messages for the stated subcase.
        """
        random.seed(seed)
        number_of_samples = max(self.SAMPLE_DATA_LENGTH, self._block_length)
        if subcase == "all_zero":
            return self._split_samples_into_opcodes([complex(0.0, 0.0)] * number_of_samples)

        elif subcase == "all_maximum":
            return self._split_samples_into_opcodes([complex(self.FLOAT_MAXIMUM, self.FLOAT_MAXIMUM)] * number_of_samples)

        elif subcase == "all_minimum":
            return self._split_samples_into_opcodes([complex(self.FLOAT_MINIMUM, self.FLOAT_MINIMUM)] * number_of_samples)

        elif subcase == "real_zero":
            data = [complex(0.0, 0.0)] * number_of_samples
            for index in range(number_of_samples):
                data[index] = complex(
                    0.0, random.uniform(self.FLOAT_MINIMUM, self.FLOAT_MAXIMUM))
            return self._split_samples_into_opcodes(data)

        elif subcase == "imaginary_zero":
            data = [complex(0.0, 0.0)] * number_of_samples
            for index in range(number_of_samples):
                data[index] = complex(
                    random.uniform(self.FLOAT_MINIMUM, self.FLOAT_MAXIMUM), 0.0)
            return self._split_samples_into_opcodes(data)

        elif subcase == "large_positive":
            data = [complex(0.0, 0.0)] * number_of_samples
            near_random_limit = self.FLOAT_MAXIMUM - self.SAMPLE_NEAR_RANGE
            for index in range(number_of_samples):
                data[index] = complex(
                    random.uniform(near_random_limit, self.FLOAT_MAXIMUM),
                    random.uniform(near_random_limit, self.FLOAT_MAXIMUM))
            return self._split_samples_into_opcodes(data)

        elif subcase == "large_negative":
            data = [complex(0.0, 0.0)] * number_of_samples
            near_random_limit = self.FLOAT_MINIMUM + self.SAMPLE_NEAR_RANGE
            for index in range(number_of_samples):
                data[index] = complex(
                    random.uniform(self.FLOAT_MINIMUM, near_random_limit),
                    random.uniform(self.FLOAT_MINIMUM, near_random_limit))
            return self._split_samples_into_opcodes(data)

        elif subcase == "near_zero":
            data = [complex(0.0, 0.0)] * number_of_samples
            for index in range(number_of_samples):
                data[index] = complex(
                    random.uniform(-self.FLOAT_SMALL,
                                   self.FLOAT_SMALL),
                    random.uniform(-self.FLOAT_SMALL,
                                   self.FLOAT_SMALL))
            return self._split_samples_into_opcodes(data)

        elif subcase == "positive_infinity":
            data = self._full_scale_random_sample_values(number_of_samples)
            for count in range(4):
                data[random.randint(0, number_of_samples)].real = float("inf")
                data[random.randint(0, number_of_samples)].imag = float("inf")
                data[random.randint(0, number_of_samples)] = complex(
                    float("inf"), float("inf"))
            return self._split_samples_into_opcodes(data)

        elif subcase == "negative_infinity":
            data = self._full_scale_random_sample_values(number_of_samples)
            for count in range(4):
                data[random.randint(0, number_of_samples)].real = -float("inf")
                data[random.randint(0, number_of_samples)].imag = -float("inf")
                data[random.randint(0, number_of_samples)
                     ] = complex(-float("inf"), -float("inf"))
            return self._split_samples_into_opcodes(data)

        elif subcase == "not_a_number":
            data = self._full_scale_random_sample_values(number_of_samples)
            for count in range(4):
                data[random.randint(0, number_of_samples)].real = float("nan")
                data[random.randint(0, number_of_samples)].imag = float("nan")
                data[random.randint(0, number_of_samples)] = complex(
                    float("nan"), float("nan"))
            return self._split_samples_into_opcodes(data)

        else:
            return super().sample(seed, subcase)

    def _generate_sine_wave_samples(self, number_of_samples, subcase):
        """Generate samples with sinusoidal waves.

        Combine a number of sinusoidal waves together, so that the output is
        the superposition of several sinusoidal waves.

        The _number_samples_sent retains the current sample number so that repeated
        calls continue the waveform. The generator can be reset by setting
        self._number_samples_sent to zero.

        Args:
            number_of_samples (int): Number of samples to generate.
            subcase (str): Subcase for which to generate waveforms.

        Returns:
            List of samples.
        """
        (frequencies, phases, amplitudes) = self._get_wave_parameters(subcase)
        data = [complex(0.0, 0.0)] * number_of_samples
        for index in range(len(data)):
            sample = self._number_samples_sent + index

            for frequency, phase, amplitude in zip(frequencies, phases, amplitudes):
                phase_term = (2 * math.pi * frequency * sample) + phase
                data[index] += complex(amplitude * math.cos(phase_term),
                                       amplitude * math.sin(phase_term))

        # Increment sample number for next call
        self._number_samples_sent += len(data)

        return data

    def _generate_random_samples(self, number_of_samples):
        """Generate samples with random values.

        The values generated are a subset of the whole supported range that
        floats can represent, this range size is set by
        self.LIMITED_SCALE_FACTOR.

        Args:
            number_of_samples (int): The number of samples to generate.

        Returns:
            List of the generated random values.
        """
        limited_range_min = self.LIMITED_SCALE_FACTOR * self.FLOAT_MINIMUM
        limited_range_max = self.LIMITED_SCALE_FACTOR * self.FLOAT_MAXIMUM

        data = [complex(0.0, 0.0)] * number_of_samples
        for index in range(number_of_samples):
            data[index] = complex(
                random.uniform(limited_range_min, limited_range_max),
                random.uniform(limited_range_min, limited_range_max))
        return data

    def _full_scale_random_sample_values(self, number_of_samples):
        """Generate samples with random values.

        Args:
            number_of_samples (int): The number of samples to generate.

        Returns:
            List of the generated random values.
        """
        data = [complex(0.0, 0.0)] * number_of_samples
        for index in range(number_of_samples):
            data[index] = complex(
                random.uniform(self.FLOAT_MINIMUM, self.FLOAT_MAXIMUM),
                random.uniform(self.FLOAT_MINIMUM, self.FLOAT_MAXIMUM))
        return data
