#!/usr/bin/env python3

# Python implementation of common sample time and interval calculations
#
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
# more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

"""Common sample time and interval calculations."""

import logging
import decimal


class SampleTime:
    """Sample time and interval container.

    Time wraps at minimum and maximum (UQ32.64 binary). Values are
    converted and held as python decimal.
    """

    Context = decimal.Context(prec=96)
    """decimal.Context to use to ensure a consistent precision"""

    # Time is unsigned UQ32.64. Range is [0,TIME_RANGE).
    TIME_RANGE = ((decimal.Decimal(2, context=Context)**96) /
                  (decimal.Decimal(2, context=Context)**64))

    def __init__(self,
                 group_delay_seconds=0,
                 group_delay_fractional=0,
                 interval_up_sampling_factor=1,
                 interval_down_sampling_factor=1):
        """Initialise the sample time container.

        _time (decimal): Time (UQ32.64) or None.
        _input_interval (decimal): Sample Interval for calculations (UQ32.64 or None).
        _output_interval (decimal):  Sample Interval scaled and truncated to 64 bits.
        _group_delay (UQ32.64): Amount to subtract from time.
        _interval_scaling_factor (decimal): Scaling factor due to up/down
                                            sampling to apply to output_interval.

        Args:
            group_delay_seconds (number or string representation):
                Integer seconds part of the UQ32.64 value
                for the group delay to apply to time.
            group_delay_fractional (number or string representation):
                Integer representing the fractional part of the UQ32.64 value
                for the group delay to apply to time.
            interval_up_sampling_factor (number or string representation):
                Interpolation factor used to scale the interval.
                Set to 1 for no scaling.
            interval_down_sampling_factor (number or string representation):
                Decimation factor used to scale the interval.
                Set to 1 for no scaling.

        Returns:
            Instance of the SampleTime class
        """
        self._time = None

        self._interval_scaling_factor = (
            decimal.Decimal(interval_down_sampling_factor,
                            context=self.Context) /
            decimal.Decimal(interval_up_sampling_factor,
                            context=self.Context)
        )

        self._input_interval = None    # type: decimal.Decimal
        self._output_interval = None   # type: decimal.Decimal

        # Convert group delay inputs to integers (in case they are strings)
        group_delay_seconds = int(group_delay_seconds)
        group_delay_fractional = int(group_delay_fractional)

        self._group_delay = ((decimal.Decimal(group_delay_fractional, context=self.Context)
                              / decimal.Decimal(2**64, context=self.Context))
                             + decimal.Decimal(group_delay_seconds, context=self.Context))

    @property
    def time(self):
        """Get sample time value.

        Returns:
            The time (Decimal)
        """
        return self._time

    @time.setter
    def time(self, time):
        """Set the time value.

        Args:
            time (number or string representation): Time value to use.
        """
        if time is None:
            self._time = None
        else:
            self._time = decimal.Decimal(
                time, context=self.Context) - self._group_delay
            self._normalise()

    @property
    def interval(self):
        """Get sample interval value.

        Returns:
            The sample interval (Decimal) for the incoming data.
        """
        return self._input_interval

    @interval.setter
    def interval(self, interval):
        """Set sample interval values.

        input_interval is set to given value and stored with full precision for
        use in calculations.

        output_interval is adjusted by up and down sampling factors and
        truncated to a 64 bit fractional part for use in output opcodes.

        Args:
            interval (number or string representation):
                Sample interval for the incoming data.
        """
        self._input_interval = decimal.Decimal(interval, context=self.Context)
        scaled_interval = self._input_interval * self._interval_scaling_factor

        if self._input_interval >= 2**32 or scaled_interval >= 2**32:
            raise ValueError("Sample Interval too large for 32bit seconds")

        self._output_interval = decimal.Decimal(
            int(scaled_interval * (2**64)), context=self.Context) / (2**64)
        if self._output_interval != scaled_interval:
            logging.info("Truncating output sample_interval from: {}, to: {}"
                         .format(scaled_interval, self._output_interval))

    def reset(self, time=None, interval=None):
        """Reset working values.

        Args:
            time (uint, optional): Current time reset value
            interval (uint, optional): Sample interval reset value
        """
        if time is None:
            self._time = None
        else:
            self._time = decimal.Decimal(time, context=self.Context)
            self._normalise()
        if interval is None:
            self._input_interval = None
            self._output_interval = None
        else:
            self.interval(interval)

    def opcode_time(self):
        """Return time for use in time opcode output.

        Truncates fractional part to 64 bits.
        Guarantees to return a valid time: Aborts if time is
        not valid (None).

        Returns:
            Sample time (Decimal)
        """
        if self._time is None:
            raise ValueError("Attempt to use time when not valid")
        output_time = decimal.Decimal(
            int(self._time * (2**64)), context=self.Context) / (2**64)
        if output_time != self._time:
            logging.info("Truncating opcode time from: {}, to: {}"
                         .format(self._time, output_time))

        return output_time

    def opcode_interval(self):
        """Return output interval for use in sample_interval opcode output.

        Guarantees to return a valid interval: Aborts if interval is
        not valid (None).

        Returns:
            Sample interval (Decimal)
        """
        if self._output_interval is None:
            raise ValueError("Attempt to use interval when not valid")

        return self._output_interval

    def _normalise(self):
        """Ensure time is within protocol limits.

        Accounts for increment beyond upper limit and decrement
        below lower limit (0). Supports multiple wraps that can
        occur from very large increment values in test data.
        """
        if self._time is None:
            raise ValueError(f"Cannot normalise time=None")
        elif self._time >= self.TIME_RANGE:
            self._time %= self.TIME_RANGE
            logging.debug(
                f"Time wrapped at {self.TIME_RANGE}: time now {self._time}")
        # decimal % and built-in % behave differently for -ve so loop and add instead.
        while self._time < 0:
            self._time += self.TIME_RANGE

    def advance_time_by(self, samples):
        """Advance time by sample_interval to reflect samples received/processed.

        Args:
            samples (int): Number of samples by which to advance

        Returns:
            New time (Decimal/None)
        """
        if self._time is not None and self._input_interval is not None:
            self._time += samples * self._input_interval
            self._normalise()
        return self._time
