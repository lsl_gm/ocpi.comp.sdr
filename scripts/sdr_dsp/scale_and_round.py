#!/usr/bin/env python3

# Python implementation of scaled FIR filter for testing
#
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
# more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

"""Library class to provide scale and round of sample values."""

import numpy
import logging


class ScaleAndRound:
    """A scale and round class."""

    def __init__(self, rounding_type, scale_factor, output_depth,
                 overflow_type, logger_name="ScaleAndRound",
                 logger_level: str = "WARN"):
        """Initialises the scale and round class.

        Args:
            scale_factor (int): Number of bits to be removed.
            rounding_type (string): How to round result. Allowed values are
                                    "half_up", "half_even" or "truncate"
            overflow_type (string): Output be saturated or wrapped. Allowed
                                    values are "saturate" or "wrap".
            output_depth (int): Number of bits in output (8,16,32,64).
            logger_name (str): Name of the logger instance.
            logger_level (str): Minimum level of log messages to be generated.
        """
        self.logger = logging.getLogger(logger_name)
        self.logger.setLevel(logger_level)

        self._scale_factor = scale_factor
        rounding_type = str(rounding_type).lower()
        if scale_factor == 0:
            self.scale_and_round = self.pass_through
        elif rounding_type == "half_up":
            self.scale_and_round = self.half_up
        elif rounding_type == "half_even":
            self.scale_and_round = self.half_even
        elif rounding_type == "truncate":
            self.scale_and_round = self.truncate
        else:
            raise RuntimeError(f"Unsupported rounding_type: {rounding_type}")

        overflow_type = str(overflow_type).lower()
        if overflow_type == "saturate":
            self.saturate_output = True
        elif overflow_type == "wrap":
            self.saturate_output = False
        else:
            raise RuntimeError(f"Unsupported overflow_type: {overflow_type}")

        self._output_depth = output_depth
        self._output_max = +int(2**(output_depth-1))-1
        self._output_min = -int(2**(output_depth-1))
        if output_depth == 8:
            self._output_type = numpy.int8
        elif output_depth == 16:
            self._output_type = numpy.int16
        elif output_depth == 32:
            self._output_type = numpy.int32
        elif output_depth == 64:
            self._output_type = numpy.int64
        else:
            raise RuntimeError(f"Unsupported output_depth: {output_depth}")

    def pass_through(self, value):
        """Pass value unchanged (scale = 0).

        If the scale_factor is zero, just constrain the value to the output size.

        Args:
            value: Value to pass through (with possible type conversion).
        """
        return self._output_type(value)

    def half_up(self, value):
        """Arithmetically rounds half to next higher value.

        Positive values round away from zero.
        Negative values round towards zero.

        Args:
            value (int): Value to round.

        Returns:
            int: Scaled and rounded value.
        """
        rounded = int(value)
        if value != 0:
            rounded >>= (self._scale_factor-1)
            rounded += 1
            rounded >>= 1
            if self.saturate_output:
                rounded = numpy.clip(
                    rounded, self._output_min, self._output_max)

        result = self._output_type(rounded)
        self.logger.debug(f"Rounded {value} to {result}")
        return result

    def half_even(self, value):
        """Arithmetically rounds half to nearest even.

        Args:
            value (int): Value to round.

        Returns:
            int: Scaled and rounded value.
        """
        integer_value = int(value)
        if value != 0:
            odd_bit = (1 << self._scale_factor)
            is_odd = integer_value & odd_bit

            half_mask = (odd_bit - 1)
            half_bit = (1 << (self._scale_factor - 1))
            is_exactly_half = ((integer_value & half_mask) == half_bit)

            # Remove fractional bits from the integer value
            integer_value >>= (self._scale_factor - 1)

            # Add 0.5 and floor divide by 2
            integer_value = ((integer_value + 1) // 2)

            if (is_exactly_half and (not is_odd)):
                integer_value += -1

            if self.saturate_output:
                integer_value = numpy.clip(
                    integer_value, self._output_min, self._output_max)

        result = self._output_type(integer_value)
        self.logger.debug(f"Rounded {value} to {result}")
        return result

    def truncate(self, value):
        """Truncates.

        Args:
            value (int): Value to round.

        Returns:
            int: Scaled and rounded value.
        """
        rounded = int(value)
        if value != 0:
            # Truncation must use floored int division to ensure truncated
            # towards -infinity, as per truncate of 2's compliment in RCC/HDL
            rounded = rounded // int(2**self._scale_factor)
            if self.saturate_output:
                rounded = numpy.clip(
                    rounded, self._output_min, self._output_max)
        result = self._output_type(rounded)
        self.logger.debug(f"Rounded {value} to {result}")
        return result
