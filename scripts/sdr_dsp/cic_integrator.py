#!/usr/bin/env python3

# Python implementation of CIC filter, with decimator.
#
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
# more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

"""CIC Filter Integrator Stage."""


class Integrator:
    """CIC Integrator class."""

    def __init__(self):
        """Integrator class.

        An Integrator performs a delayed summation, this relates to a discrete time
        integration.

        Returns:
            Instance of the Integrator Class.
        """
        self.reset()

    def reset(self):
        """Clears the internal buffer."""
        self._previous_value = 0

    def call(self, value):
        """Steps the integrator on by one sample.

        Args:
            value (integer): the value to next insert into the integrator.

        Returns:
            The next output integer from the integrator.
        """
        self._previous_value = value + self._previous_value
        return self._previous_value
