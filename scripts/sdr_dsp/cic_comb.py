#!/usr/bin/env python3

# Python implementation of CIC comb for use of decimator and interpolator.
#
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
# more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

"""CIC comb filter stage."""


class Comb:
    """CIC comb filter."""

    def __init__(self, delay):
        """Comb filter class.

        Comb filters work by adding a delayed version of the input signal back into
        the signal, therefore creating a constructive / destructive interference
        pattern within the output samples.
        When used with sinusoidal signals, this pattern looks like a comb of the
        signal being present, then absent.

        Args:
            delay (integer): length of the comb filter.

        Returns:
            Instance of the Comb Class.
        """
        self._delay = delay
        self.reset()

    def reset(self):
        """Clears the internal delay buffer."""
        self._delay_buffer = [0] * (self._delay)

    def call(self, value):
        """Steps the filter on by one sample.

        Args:
            value (integer): the value to next insert into the filter.

        Returns:
            The next output integer from the filter.
        """
        self._delay_buffer.append(value)
        return value - self._delay_buffer.pop(0)
