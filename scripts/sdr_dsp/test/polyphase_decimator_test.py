#!/usr/bin/env python3

# Unit tests of the python implementation of Polyphase Decimator
#
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
# more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

"""Unit tests of the python implementation of Polyphase Decimator core."""

import decimal
import pytest
import random
import numpy as np
import scipy.signal as sig
import matplotlib.pyplot as plt

from sdr_dsp.polyphase_decimator import PolyphaseDecimator, PolyphaseDecimator_Int, PolyphaseDecimator_ComplexInt

show_plots = False

decimal.getcontext().prec = 64


@pytest.fixture(params=[PolyphaseDecimator, PolyphaseDecimator_Int, PolyphaseDecimator_ComplexInt])
def pd_fixture(request):
    return request.param


@pytest.fixture(params=[2, 10])
def decimation_factor(request):
    return request.param


@pytest.fixture(params=[0, 1, 2, 3])
def tap_offset(request):
    return request.param


@pytest.fixture(params=[9, 30])
def taps(request, decimation_factor, tap_offset):
    num_taps = request.param * decimation_factor
    t = sig.firwin(num_taps+tap_offset, 1.0/decimation_factor)
    return t


@pytest.fixture()
def input_signal():
    # Create some samples to interpolate then decimate in the tests.
    sample_length = 1024
    vals = [0, 1, 0, 0, 0,
            complex(4, 1), 0, 0, 0, 0,
            complex(0, -1), 0, 0, complex(0, 2), 0]
    signal = np.fft.fft(vals, sample_length, axis=0)
    return signal / max(abs(signal))


def check_samples(tap_length, decimation_factor, allowed_error, taps,
                  input_signal, interpolated_data, decimated_data,
                  decimated_data_compare=[]):
    # tap_offset of 0 or 1, to correct for taps not being a
    # multiple of decimation rate.
    tap_offset = 1
    if (len(taps) % decimation_factor) != 0:
        tap_offset = 0

    if show_plots:
        plt.figure("1: {}, {}".format(tap_length, decimation_factor))
        plt.plot((tap_length) + np.arange(0, len(input_signal), 1),
                 np.real(input_signal), "b--")
        plt.plot((tap_length/2) + np.arange(0, len(interpolated_data))
                 / (decimation_factor), np.real(interpolated_data), "g-x")
        if len(decimated_data_compare) > 0:
            plt.plot(np.arange(0, len(decimated_data_compare)),
                     np.real(decimated_data_compare), "m-o")
        plt.plot(tap_offset + np.arange(0, len(decimated_data)),
                 np.real(decimated_data), "r-x")

    print("Input Samples: {}\nSample Interp: {}\nSample Decim:  {}"
          .format(input_signal[tap_length:tap_length+10],
                  interpolated_data[
              tap_length * decimation_factor:(tap_length
                                              * decimation_factor) + 10],
                  decimated_data[(2*tap_length)-1:2*tap_length+10]))

    # Skip over any startup filter transient (as we are comparing the original
    # signal to the interpolated -> decimated), there is will some start up,
    # and some filter delay present.
    # (offset tap_length, for transients,
    #  offset by another tap_length-tap_offset for filter delay).
    offset_dec_data = decimated_data[(
        2*tap_length)-tap_offset:len(input_signal)]
    error = [np.abs(a-b) for a, b in zip(
             offset_dec_data, input_signal[tap_length:len(input_signal)])]

    if show_plots:
        plt.figure("2: {}, {}".format(tap_length, decimation_factor))
        plt.plot(error)
        plt.show()

    for n, e in enumerate(error):
        if e >= allowed_error:
            print("error at index {}".format(n))
        assert(e <= allowed_error)


def test_pd_init(pd_fixture):
    """ Test that the fixtures accept the base parameters """
    pd = pd_fixture(decimation_factor=2, taps=[0, 1, 2, 3])
    assert(pd._decimation_factor == 2)


def test_pd_decimation(input_signal, decimation_factor, taps):
    random.seed(22)
    allowed_error = 70

    tap_length = int(len(taps)/decimation_factor)

    pd = PolyphaseDecimator(
        decimation_factor=decimation_factor,
        taps=taps)

    signal_int2 = sig.upfirdn([decimation_factor*t for t in taps], input_signal,
                              up=decimation_factor, down=1)

    signal_dec = pd.decimate_samples(signal_int2)
    # The builtin version
    signal_dec2 = sig.upfirdn(taps, signal_int2, up=1, down=decimation_factor)

    check_samples(tap_length, decimation_factor, allowed_error, taps,
                  input_signal, interpolated_data=signal_int2,
                  decimated_data=signal_dec,
                  decimated_data_compare=signal_dec2)


def test_pd_decimation_int(input_signal, decimation_factor, taps):
    random.seed(22)
    allowed_error = 70

    tap_length = int(len(taps)/decimation_factor)
    taps = [int(t*2**15) for t in taps]

    pd = PolyphaseDecimator_Int(
        decimation_factor=decimation_factor,
        taps=taps)

    input_signal = [int(s.real*2**15) for s in input_signal]

    signal_int2 = [int(s/2**15) for s in
                   sig.upfirdn([decimation_factor*t for t in taps], input_signal,
                               up=decimation_factor, down=1)]

    signal_dec = pd.decimate_samples(signal_int2)
    # The builtin version
    signal_dec2 = [int(s/2**15) for s in
                   sig.upfirdn(taps, signal_int2, up=1, down=decimation_factor)]

    check_samples(tap_length, decimation_factor, allowed_error, taps,
                  input_signal, interpolated_data=signal_int2,
                  decimated_data=signal_dec,
                  decimated_data_compare=signal_dec2)


def test_pd_decimation_complex_int(input_signal, decimation_factor, taps):
    random.seed(22)
    allowed_error = 70

    tap_length = int(len(taps)/decimation_factor)
    taps = [int(t*2**15) for t in taps]

    pd = PolyphaseDecimator_ComplexInt(
        decimation_factor=decimation_factor,
        taps=taps)

    input_signal = [complex(int(s.real*2**15), int(s.imag*2**15))
                    for s in input_signal]

    signal_int2 = [complex(int(s.real/2**15), int(s.imag/2**15)) for s in
                   sig.upfirdn([decimation_factor*t for t in taps], input_signal,
                               up=decimation_factor, down=1)]

    signal_dec = pd.decimate_samples(signal_int2)

    # The builtin version
    signal_dec2 = [complex(int(s.real/2**15), int(s.imag/2**15)) for s in
                   sig.upfirdn(taps, signal_int2, up=1, down=decimation_factor)]

    check_samples(tap_length, decimation_factor, allowed_error, taps,
                  input_signal, interpolated_data=signal_int2,
                  decimated_data=signal_dec,
                  decimated_data_compare=signal_dec2)
