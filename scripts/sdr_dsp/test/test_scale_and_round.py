#!/usr/bin/env python3

# Unit tests of the python scale_and_round implementation
#
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
# more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

from sdr_dsp.scale_and_round import ScaleAndRound

import numpy as np
import pytest
import decimal
import math

decimal.getcontext().prec = 64


@pytest.fixture(params=[8, 16, 32, 64])
def output_bit_size(request):
    """Fixture to supply range of bit sizes for output values."""
    return request.param


UINT8_MAX_VAL = +(2**8)-1
INT8_MAX_VAL = +(2**7)-1
INT8_MIN_VAL = -(2**7)

UINT16_MAX_VAL = +(2**16)-1
INT16_MAX_VAL = +(2**15)-1
INT16_MIN_VAL = -(2**15)

UINT32_MAX_VAL = +(2**32)-1
INT32_MAX_VAL = +(2**31)-1
INT32_MIN_VAL = -(2**31)

UINT64_MAX_VAL = +(2**64)-1
INT64_MAX_VAL = +(2**63)-1
INT64_MIN_VAL = -(2**63)

input_values = [
    1,
    7,
    8,
    9,
    21,
    22,
    2453,
    -1,
    -2,
    -7,
    -8,
    -9,
    -21,
    -69,
    -2453,
    0,
    0x55,
    0x5555,
    0x55555555,
    0x5555555555555555,
    0x77,
    0x7777,
    0x77777777,
    0x7777777777777777,
    0xBBBB,
    0xBBBBBBBB,
    0xbbbbBBBBbbbbBBBB,
    0xF777,
    0xF7777777,
    0xF777777777777777,

    (INT8_MAX_VAL / 2) - 1,     (INT8_MAX_VAL / 2),     (INT8_MAX_VAL / 2) + 1,
    (INT8_MIN_VAL / 2) - 1,     (INT8_MIN_VAL / 2),     (INT8_MIN_VAL / 2) + 1,

    (INT8_MAX_VAL - 2),         (INT8_MAX_VAL - 1),     (INT8_MAX_VAL),
    (INT8_MIN_VAL),             (INT8_MIN_VAL + 1),     (INT8_MIN_VAL + 2),

    (INT16_MAX_VAL / 2) - 1,    (INT16_MAX_VAL / 2),    (INT16_MAX_VAL / 2) + 1,
    (INT16_MIN_VAL / 2) - 1,    (INT16_MIN_VAL / 2),    (INT16_MIN_VAL / 2) + 1,

    (INT16_MAX_VAL - 2),        (INT16_MAX_VAL - 1),    (INT16_MAX_VAL),
    (INT16_MIN_VAL),            (INT16_MIN_VAL + 1),    (INT16_MIN_VAL + 2),

    (INT32_MAX_VAL / 2) - 1,    (INT32_MAX_VAL / 2),    (INT32_MAX_VAL / 2) + 1,
    (INT32_MIN_VAL / 2) - 1,    (INT32_MIN_VAL / 2),    (INT32_MIN_VAL / 2) + 1,

    (INT32_MAX_VAL - 2),        (INT32_MAX_VAL - 1),    (INT32_MAX_VAL),
    (INT32_MIN_VAL),            (INT32_MIN_VAL + 1),    (INT32_MIN_VAL + 2),

    (INT64_MAX_VAL / 2) - 1,    (INT64_MAX_VAL / 2),    (INT64_MAX_VAL / 2) + 1,
    (INT64_MIN_VAL / 2) - 1,    (INT64_MIN_VAL / 2),    (INT64_MIN_VAL / 2) + 1,

    (INT64_MAX_VAL - 2),        (INT64_MAX_VAL - 1),    (INT64_MAX_VAL),
    (INT64_MIN_VAL),            (INT64_MIN_VAL + 1),    (INT64_MIN_VAL + 2),

    (UINT8_MAX_VAL),            (UINT8_MAX_VAL - 1),    (UINT8_MAX_VAL - 2),
    (UINT16_MAX_VAL),           (UINT16_MAX_VAL - 1),   (UINT16_MAX_VAL - 2),
    (UINT32_MAX_VAL),           (UINT32_MAX_VAL - 1),   (UINT32_MAX_VAL - 2),
    (UINT64_MAX_VAL),           (UINT64_MAX_VAL - 1),   (UINT64_MAX_VAL - 2),
]

overflow_type = ["wrap", "saturate"]


@pytest.fixture(params=input_values)
def value_fixture(request):
    """Fixture to supply range of input values."""
    return request.param


@pytest.fixture()
def shift_range(value_fixture):
    # How many bits in value
    if value_fixture == 0:
        bits_in_val = 0
    else:
        bits_in_val = math.ceil(math.log2(abs(value_fixture)))
    return [2, bits_in_val+3]


@pytest.fixture(params=overflow_type)
def overflow_fixture(request):
    """Fixture to supply overflow types."""
    return request.param


def test_half_up(value_fixture, shift_range, output_bit_size, overflow_fixture):

    value = value_fixture
    overflow_type = overflow_fixture
    for scale_factor in shift_range:

        expected_decimal = decimal.Decimal(value) / (2**scale_factor)
        expected = int(math.floor(expected_decimal + decimal.Decimal(0.5)))
        if overflow_type is "saturate":
            expected = np.clip(
                expected, -(2**(output_bit_size-1)), +(2**(output_bit_size-1))-1)

        if output_bit_size == 8:
            expected = np.int8(expected)
        elif output_bit_size == 16:
            expected = np.int16(expected)
        elif output_bit_size == 32:
            expected = np.int32(expected)
        elif output_bit_size == 64:
            expected = np.int64(expected)
        else:
            raise ValueError(f"Unsupported output_bit_size: {output_bit_size}")

        print(
            f"value={value}, scale_factor={scale_factor}, output_bit_size={output_bit_size}, overflow_type={overflow_type}")
        print(f"expected_decimal={expected_decimal}")
        print(f"expected={expected}")

        scaler = ScaleAndRound("half_up", scale_factor,
                               output_bit_size, overflow_type)
        actual = scaler.scale_and_round(value)

        print(f"actual={actual}")
        assert(actual == expected)


def test_half_even(value_fixture, shift_range, output_bit_size, overflow_fixture):

    value = value_fixture
    overflow_type = overflow_fixture
    for scale_factor in shift_range:

        expected_decimal = decimal.Decimal(value) / (2**scale_factor)
        expected = expected_decimal.quantize(
            decimal.Decimal("1."), rounding=decimal.ROUND_HALF_EVEN)
        if overflow_type is "saturate":
            expected = np.clip(
                expected, -(2**(output_bit_size-1)), +(2**(output_bit_size-1))-1)

        if output_bit_size == 8:
            expected = np.int8(expected)
        elif output_bit_size == 16:
            expected = np.int16(expected)
        elif output_bit_size == 32:
            expected = np.int32(expected)
        elif output_bit_size == 64:
            expected = np.int64(expected)
        else:
            raise ValueError(f"Unsupported output_bit_size: {output_bit_size}")

        print(
            f"value={value}, scale_factor={scale_factor}, output_bit_size={output_bit_size}, overflow_type={overflow_type}")
        print(f"expected_decimal={expected_decimal}")
        print(f"expected={expected}")

        scaler = ScaleAndRound("half_even", scale_factor,
                               output_bit_size, overflow_type)
        actual = scaler.scale_and_round(value)

        print(f"actual={actual}")
        assert(actual == expected)


def test_truncate(value_fixture, shift_range, output_bit_size, overflow_fixture):

    value = value_fixture
    overflow_type = overflow_fixture
    for scale_factor in shift_range:

        expected_decimal = decimal.Decimal(value) / (2**scale_factor)
        expected = expected_decimal.quantize(
            decimal.Decimal("1."), rounding=decimal.ROUND_FLOOR)
        if overflow_type is "saturate":
            expected = np.clip(
                expected, -(2**(output_bit_size-1)), +(2**(output_bit_size-1))-1)

        if output_bit_size == 8:
            expected = np.int8(expected)
        elif output_bit_size == 16:
            expected = np.int16(expected)
        elif output_bit_size == 32:
            expected = np.int32(expected)
        elif output_bit_size == 64:
            expected = np.int64(expected)
        else:
            raise ValueError(f"Unsupported output_bit_size: {output_bit_size}")

        print(
            f"value={value}, scale_factor={scale_factor}, output_bit_size={output_bit_size}, overflow_type={overflow_type}")
        print(f"expected_decimal={expected_decimal}")
        print(f"expected={expected}")

        scaler = ScaleAndRound("truncate", scale_factor,
                               output_bit_size, overflow_type)
        actual = scaler.scale_and_round(value)

        print(f"actual={actual}")
        assert(actual == expected)
